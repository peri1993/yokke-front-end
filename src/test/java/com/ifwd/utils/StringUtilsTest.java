package com.yokke.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.yokke.utils.StringUtils;

public class StringUtilsTest {

	@Test
	public void shouldBreakAddressIntoMultiFields() throws Exception{
		String[] address = StringUtils.breakString("9/F, FWD Financial Building, Sheung Wan, Hong Kong", new int[]{20,20,20});
		assertEquals(3, address.length);
		assertEquals("9/F, FWD Financial ", address[0]);
		assertEquals("Building, Sheung ", address[1]);
		assertEquals("Wan, Hong Kong", address[2]);
	}
	
}
