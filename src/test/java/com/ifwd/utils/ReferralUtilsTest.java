package com.yokke.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.yokke.utils.ReferralUtils;

public class ReferralUtilsTest {

	@Test
	public void canTellInvalidRerralCode(){
		// eg. last char not numeric
		assertFalse(ReferralUtils.verifyReferralCode("00x"));
	}
	
	@Test
	public void canVerifyGeneratedReferralCode(){
		final String code = ReferralUtils.generateReferralCode();
		assertTrue(code+" cannot be verified", ReferralUtils.verifyReferralCode(code));
	}
	
}
