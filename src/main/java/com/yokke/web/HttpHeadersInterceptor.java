package com.yokke.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class HttpHeadersInterceptor implements HandlerInterceptor, AwareRequestAttributes {

	private boolean allowWebSearch;
	
	public void setAllowWebSearch(boolean allowWebSearch) {
		this.allowWebSearch = allowWebSearch;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object arg2, Exception arg3)
			throws Exception {

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView)
			throws Exception {

		/*response.addHeader("X-Content-Type-Options", "nosniff");*/
		// can be relaxed to SAMEORIGIN if necessary
		/*response.addHeader("X-Frame-Options", "DENY");*/
//		response.addHeader("X-XSS-Protection", "");
		/*response.addHeader("cache-control", "no-cache, no-store");
		response.addHeader("pragma", "no-cache");*/

		if(!allowWebSearch){
			// according to Rex, supported by Google/Yahoo/Bing
			response.addHeader("X-Robots-Tag", "noindex");			
		}
	}

}