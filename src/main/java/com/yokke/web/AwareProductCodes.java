package com.yokke.web;

public interface AwareProductCodes {

	String EC1 = "EC1";
	String PA2 = "PA2";
	String PA2_STD = "bebas-aksi"; 
	String PA3_1M = "Flash_1M";
	String PA3_3M = "Flash_3M";
	String PA3_7D = "Flash_7D";
	String PA3D = "PA3D";
	String PA3M = "PA3M";				  			  
	String RPP = "RPP";
	String FW4 = "FW4";
	String[] ALL_PRODUCTS = new String[] { EC1, PA2, PA3_1M, PA3_3M, PA3_7D, RPP, FW4 };
	
}
