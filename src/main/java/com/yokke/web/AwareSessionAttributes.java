package com.yokke.web;

public interface AwareSessionAttributes {

	String OWNER = "OWNER";
	String MEMBER = "MEMBER";
	// these are stored in session per product
	String PROPOSAL = "PROPOSAL";
	String QUOTE = "RP";
	// max member
	String MEMBER_MAX = "MEMBER_MAX";
	
}
