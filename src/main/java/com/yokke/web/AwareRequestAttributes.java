package com.yokke.web;

public interface AwareRequestAttributes {

	String ALLOW_WEB_SEARCH = "ALLOW_WEB_SEARCH";
	String DATE_COVERAGE_PARAM	= "riskDate";
	String CRS_PARAM	= "isCrs";
	String BO_PARAM	= "isBo";
	String BANK_NAME_PARAM	= "bankName";
	String BANK_COUNTRY_PARAM	= "bankCountry";
	
}
