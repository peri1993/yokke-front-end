package com.yokke.web;

public interface AwareOwnerParams {
	
	String GENDER = "gender";
	String FIRSTNAME = "firstName";
	String LASTNAME = "lastName";
	String EMAIL = "email";
	String MOBILE = "mobile";
	String DOB = "dateOfBirth";
	String POB = "placeOfBirth";
	String ID_TYPE = "idType";
	String ID_NUMBER = "idNumber";
	String PRIMARY_ADDR = "primaryAddress";
	String CORRESPONDENCY_ADDR = "correspondencyAddress";
	String START_DATE_COVER = "riskDate";
	
}
