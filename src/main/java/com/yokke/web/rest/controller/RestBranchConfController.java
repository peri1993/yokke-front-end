package com.yokke.web.rest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yokke.connector.BranchWsConnector;
import com.yokke.connector.request.BranchIdRequest;
import com.yokke.connector.request.BranchRequest;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.BranchConfResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.web.controller.AuthController;
import com.yokke.web.controller.BranchConfController;

@RestController
public class RestBranchConfController extends AuthController{

	@Override
	protected String viewName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Autowired
	private BranchWsConnector connector;
	
	
	@RequestMapping(value = BranchConfController.LIST_URL, method = RequestMethod.GET)
	public ListResponse<BranchConfResponse> list(HttpServletRequest req) {
		String authority = authority(req);
		ListResponse<BranchConfResponse> response = null;
		String paramKey 	= req.getParameter("paramKey");
		String paramValue 	= req.getParameter("paramValue");
		String page			= getPage(req);
		try {
			response = connector.listBranch(page, authority, getUsernameSession(req), paramKey, paramValue, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	@RequestMapping(value = BranchConfController.UPDATE_URL, method = RequestMethod.GET)
	public BaseResponse update(HttpServletRequest req) {
		String authority = authority(req);
		BaseResponse response = null;
		List<BranchRequest> listModel = setModel(req);
		try {
			response = connector.update(authority, listModel, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List<BranchRequest> setModel(HttpServletRequest req){
		List<BranchConfResponse> listModel = new ArrayList<BranchConfResponse>();
		List<BranchRequest> listResponse = new ArrayList<BranchRequest>();
		String json = req.getParameter("listModel");
		
		try {
			listModel = parseJsonList(json, BranchConfResponse.class);
			for (Iterator iterator = listModel.iterator(); iterator.hasNext();) {
				BranchIdRequest id = new BranchIdRequest();
				BranchRequest model = new BranchRequest();
				LinkedHashMap<String, Object> map =  (LinkedHashMap<String, Object>) iterator.next();
				model.setAddress(String.valueOf(map.get("address")));
				id.setEmpArea(String.valueOf(map.get("empArea")));
				id.setEmpSubArea(String.valueOf(map.get("empSubArea")));
				id.setLandscape(String.valueOf(map.get("landscape")));
				
				model.setId(id);
				model.setEmpSubAreaDesc(String.valueOf(map.get("empSubAreaDescription")));
				model.setLatitudeCode(String.valueOf(map.get("latitudeCode")));
				model.setLongitudeCode(String.valueOf(map.get("longitudeCode")));
				model.setRadius(String.valueOf(map.get("radius")));
				model.setNpwp(String.valueOf(map.get("npwp")));
				listResponse.add(model);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listResponse;
	}

}
