package com.yokke.web.rest.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yokke.connector.AttendanceWSConnector;
import com.yokke.connector.identity.ScheduleAttendanceLogsIdRequest;
import com.yokke.connector.request.AttendanceRequest;
import com.yokke.connector.request.AttendanceRequestLogsId;
import com.yokke.connector.request.BranchIdRequest;
import com.yokke.connector.request.BranchRequest;
import com.yokke.connector.request.PreviewFileRequest;
import com.yokke.connector.request.ScheduleAttendanceLogsRequest;
import com.yokke.connector.request.UploadAttendanceRequest;
import com.yokke.connector.response.AttendanceListUnderResponse;
import com.yokke.connector.response.AttendanceLogsResponse;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.FlagScheduleResponse;
import com.yokke.connector.response.HistoryAttendanceResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.ObjectResponse;
import com.yokke.connector.response.PreviewFileResponse;
import com.yokke.connector.response.ScheduleResponse;
import com.yokke.web.controller.AttendanceHistoryController;
import com.yokke.web.controller.AuthController;
import com.yokke.web.controller.HistoryAttendanceController;
import com.yokke.web.controller.ListAttendanceController;
import com.yokke.web.controller.QrAttendancePostController;

@RestController
public class RestAttendanceController extends AuthController {
	
	@Autowired
	private AttendanceWSConnector connector;

	@Override
	protected String viewName() {
		return null;
	}

	@RequestMapping(value = ListAttendanceController.LIST_SCHEDULER_URL, method = RequestMethod.GET)
	public ListResponse<ScheduleResponse> listSchedule(HttpServletRequest req) {
		String authority = authority(req);
		ListResponse<ScheduleResponse> response = null;
		String username = req.getParameter("username");
		try {
			response = connector.listSchedule(authority, username, INDONESIAN);
			return response;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = ListAttendanceController.LIST_URL, method = RequestMethod.GET)
	public ListResponse<AttendanceListUnderResponse> findListUnder(HttpServletRequest req) {
		String authority = authority(req);
		ListResponse<AttendanceListUnderResponse> response = null;
		String paramKey = req.getParameter("paramKey");
		String paramValue = req.getParameter("paramValue");
		try {
			response = connector.findListUnder(authority, getUsernameSession(req), paramKey, paramValue, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = HistoryAttendanceController.LIST_URL, method = RequestMethod.GET)
	public ListResponse<HistoryAttendanceResponse> listHistoryAttendance(HttpServletRequest request) {
		String authority = authority(request);
		ListResponse<HistoryAttendanceResponse> response = null;
		String username = request.getParameter("username");
		String startDt = null;
		String endDt = null;
		if (null != request.getParameter("date")) {
			String jsonDt = request.getParameter("date");
			startDt = concatStartDate(jsonDt);
			endDt = concatEndDate(jsonDt);
		}else {
			try {
				Date date = new Date();
				String _date = format_date.format(date);
				startDt = concatStartDate(String.valueOf(_date));
				endDt = concatEndDate(String.valueOf(_date));
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		String page = getPage(request);
		try {
			//response = connector.listHistoryAttendance(page, authority, username, INDONESIAN);
			response = connector.listHistoryAttendanceForEmployee(page, startDt, endDt, authority, username, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = AttendanceHistoryController.LIST_URL, method = RequestMethod.GET)
	public ListResponse<HistoryAttendanceResponse> listHistoryEmployeeAttendance(HttpServletRequest request) {
		String authority = authority(request);
		ListResponse<HistoryAttendanceResponse> response = null;
		String startDt = null;
		String endDt = null;
		if (null != request.getParameter("date")) {
			String jsonDt = request.getParameter("date");
			startDt = concatStartDate(jsonDt);
			endDt = concatEndDate(jsonDt);
		}else {
			try {
				Date date = new Date();
				String _date = format_date.format(date);
				startDt = concatStartDate(String.valueOf(_date));
				endDt = concatEndDate(String.valueOf(_date));
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		String page = getPage(request);
		try {
			response = connector.listHistoryAttendanceForEmployee(page, startDt, endDt, authority, getUsernameSession(request), INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = ListAttendanceController.CREATE_URL, method = RequestMethod.GET)
	public BaseResponse createSchedule(HttpServletRequest request) {
		String authority = authority(request);
		List<ScheduleAttendanceLogsRequest> body = new ArrayList<>();
		body.add(setScheduleRequest(request));
		BaseResponse response = null;

		try {
			response = connector.createScheduleAttendance(authority, body, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = ListAttendanceController.DELETE_URL, method = RequestMethod.GET)
	public BaseResponse deleteSchedule(HttpServletRequest request) {
		String authority = authority(request);
		ScheduleAttendanceLogsRequest body = setScheduleList(request);
		BaseResponse response = null;
		try {
			response = connector.deleteScheduleAttendance(authority, body, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = HistoryAttendanceController.PREVIEW_FILE_URL, method = RequestMethod.GET)
	public PreviewFileResponse previewFile(HttpServletRequest req) {

		String authority = authority(req);
		PreviewFileResponse response = null;
		PreviewFileRequest model = setModel(req);
		try {
			response = connector.previewFile(authority, model, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setError(e.getMessage());
			return response;
		}
	}

	@RequestMapping(value = QrAttendancePostController.CREATE_URL, method = RequestMethod.POST)
	public BaseResponse createAttendance(HttpServletRequest request, @RequestBody Map<String, Object> body) {
		String authority = authority(request);
		AttendanceRequest attendanceRequest = setAttendanceRequest(body, request);
		BaseResponse response = null;

		try {
			response = connector.createAttendance(authority, attendanceRequest, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = QrAttendancePostController.LIST_URL, method = RequestMethod.GET)
	public AttendanceLogsResponse listDataAbsence(HttpServletRequest request) {
		String authority = authority(request);
		AttendanceLogsResponse response = null;
		try {
			response = connector.listDataAbsence(authority, getUsernameSession(request), INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = QrAttendancePostController.LOCATION_URL, method = RequestMethod.GET)
	public ObjectResponse<BranchRequest> listLocation(HttpServletRequest request) {
		String authority = authority(request);
		BranchIdRequest body = setIdBranch(request);
		ObjectResponse<BranchRequest> response = null;

		try {
			response = connector.listLocation(authority, body, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = QrAttendancePostController.FLAG_SCHEDULE_URL, method = RequestMethod.GET)
	public FlagScheduleResponse flagSchedule(HttpServletRequest request) {
		String authority = authority(request);
		FlagScheduleResponse response = null;
		try {
			response = connector.flagSchedule(authority, getUsernameSession(request), INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	private PreviewFileRequest setModel(HttpServletRequest req) {
		PreviewFileRequest model = new PreviewFileRequest();
		model.setDirectory(req.getParameter("directory"));
		model.setCreatedBy(req.getParameter("createdBy"));
		model.setFileName(req.getParameter("filename"));
		return model;
	}

	public BranchIdRequest setIdBranch(HttpServletRequest request) {
		BranchIdRequest body = new BranchIdRequest();

		body.setEmpArea(request.getParameter("empArea"));
		body.setEmpSubArea(request.getParameter("empSubArea"));
		body.setLandscape("100");
		return body;
	}

	@SuppressWarnings("unused")
	public AttendanceRequest setAttendanceRequest(Map<String, Object> body, HttpServletRequest request) {
		AttendanceRequest model = new AttendanceRequest();
		AttendanceRequestLogsId modelId = new AttendanceRequestLogsId();
		UploadAttendanceRequest modelUp = new UploadAttendanceRequest();
		String currentDate = getDate();
		String pathImages = null;
		
		String empArea = null;
		String empSubArea = null;
		String landscape = null;
		String longitude = null;
		String latitude = null;
		String startDt = null;
		String endDt = null;
		String barcodeTime = null;
		
		String json = String.valueOf(body.get("content"));
		String jsonContent = json.replace("|", ",");
		String content[] = jsonContent.split(",");
		for (int i = 0; i < content.length; i++) {
			if (i == 0) {
				empArea = content[i];
			}else if (i == 1) {
				empSubArea = content[i];
			}else if (i == 2) {
				landscape = content[i];
			}else if (i == 3) {
				longitude = content[i];
			}else if (i == 4) {
				latitude = content[i];
			}else {
				startDt = content[i].substring(6, 8);	
				endDt = content[i].substring(6, 8);	
				barcodeTime = content[i];
			}
		}
		if (body.get("pathImages") != null) {
			pathImages = body.get("pathImages").toString();
		}
		try {
			if (pathImages == null) {
				modelId.setEmpId(getUsernameSession(request));
				modelId.setLandscape(landscape);
				modelId.setStartDate(startDt);
				modelId.setEndDate(endDt);

				model.setId(modelId);
				model.setEmpArea(empArea);
				model.setEmpSubArea(empSubArea);
				model.setEmpSubAreaDescription(body.get("empSubAreaDesc").toString());
				model.setBarcodeTime(barcodeTime);
				model.setLatitudeCode(String.valueOf(body.get("latitudeCode")));
				model.setLongitudeCode(String.valueOf(body.get("longitudeCode")));
				model.setRadius(body.get("radius").toString());
			} else {
				modelId.setEmpId(getUsernameSession(request));
				modelId.setLandscape("100");
				modelId.setStartDate(currentDate);
				modelId.setEndDate(currentDate);

				modelUp.setContent(body.get("pathImages").toString());
				modelUp.setFileType(body.get("filetype").toString());
				modelUp.setCreatedBy(getUsernameSession(request));

				model.setId(modelId);
				model.setPathImages(modelUp);
				model.setLatitudeCode(String.valueOf(body.get("latitudeCode")));
				model.setLongitudeCode(String.valueOf(body.get("longitudeCode")));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}

	public ScheduleAttendanceLogsRequest setScheduleRequest(HttpServletRequest request) {
		ScheduleAttendanceLogsRequest model = new ScheduleAttendanceLogsRequest();
		ScheduleAttendanceLogsIdRequest schId = new ScheduleAttendanceLogsIdRequest();

		try {
			schId.setEmpId(request.getParameter("empId"));
			schId.setStartDate(request.getParameter("startDate"));
			schId.setEndDate(request.getParameter("endDate"));

			model.setId(schId);
			model.setAddress(request.getParameter("address"));
			model.setTimeStart(request.getParameter("timeStart"));
			model.setTimeEnd(request.getParameter("timeEnd"));
		} catch (Exception e) {
			e.getSuppressed();
		}
		return model;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ScheduleAttendanceLogsRequest setScheduleList(HttpServletRequest request) {
		ScheduleAttendanceLogsRequest listModel = new ScheduleAttendanceLogsRequest();
		List<ScheduleResponse> listResponse = new ArrayList<ScheduleResponse>();
		try {
			String json = request.getParameter("listModel");
			listResponse = parseJsonList(json, ScheduleResponse.class);
			for (Iterator iterator = listResponse.iterator(); iterator.hasNext();) {
				LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) iterator.next();
				ScheduleAttendanceLogsIdRequest schId = new ScheduleAttendanceLogsIdRequest();
				ScheduleAttendanceLogsRequest schedule = new ScheduleAttendanceLogsRequest();

				schId.setEmpId(String.valueOf(map.get("empId")));
				schId.setStartDate(parseDate(String.valueOf(map.get("startDate")), "dd-MM-yyyy", "yyyyMMdd"));
				schId.setEndDate(parseDate(String.valueOf(map.get("endDate")), "dd-MM-yyyy", "yyyyMMdd"));
				schedule.setAddress(String.valueOf("address"));
				schedule.setId(schId);
				schedule.setTimeStart(String.valueOf("timeStart"));
				schedule.setTimeEnd(String.valueOf("timeEnd"));
				listModel = schedule;
			}
			return listModel;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listModel;
	}

}