package com.yokke.web.rest.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yokke.connector.InformationWsConnector;
import com.yokke.connector.request.ChangeWorkHoursRequest;
import com.yokke.connector.response.ChangeWorkHoursDetailResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.ObjectResponse;
import com.yokke.connector.response.OfficeHoursResponse;
import com.yokke.connector.response.TaskResponse;
import com.yokke.connector.response.UserOfficeHoursResponse;
import com.yokke.web.controller.AuthController;
import com.yokke.web.controller.InformationWorkHoursController;

@RestController
public class RestInformationWorkHoursController extends AuthController {

	@Autowired
	private InformationWsConnector informationWsConnector;

	@Override
	protected String viewName() {
		// TODO Auto-generated method stub
		return null;
	}

	@RequestMapping(value = InformationWorkHoursController.DETAIL_HOURS_WORK, method = RequestMethod.GET)
	public ObjectResponse<UserOfficeHoursResponse> detailWorkHours(HttpServletRequest req) {
		String authority = authority(req);
		ObjectResponse<UserOfficeHoursResponse> response = null;
		try {
			response = informationWsConnector.detailWorkHours(authority, getUsernameSession(req), INDONESIAN);
			return response;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return response;
		}
	}
	
	@RequestMapping(value = InformationWorkHoursController.TASK_DETAIL_HOURS_WORK, method = RequestMethod.GET)
	public ObjectResponse<ChangeWorkHoursDetailResponse> taskDetailWorkHours(HttpServletRequest req) {
		String authority = authority(req);
		String instancesId = req.getParameter("instancesId");
		ObjectResponse<ChangeWorkHoursDetailResponse> response = null;
		
		try {
			response = informationWsConnector.taskDetail(authority, instancesId, getUsernameSession(req), INDONESIAN);
			return response;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return response;
		}
	}
	
	@RequestMapping(value = InformationWorkHoursController.LIST_HOURS_WORK, method = RequestMethod.GET)
	public ListResponse<OfficeHoursResponse> listHours(HttpServletRequest req) {
		String authority = authority(req);
		ListResponse<OfficeHoursResponse> response = null;
		try {
			response = informationWsConnector.listHours(authority, INDONESIAN);
			return response;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return response;
		}
	}
	
	@RequestMapping(value = InformationWorkHoursController.REQUEST_CHANGE_WORK_HOURS, method = RequestMethod.GET)
	public TaskResponse requestChangeWorkHours(HttpServletRequest req) {
		String authority = authority(req);
		TaskResponse response = null;
		ChangeWorkHoursRequest request = new ChangeWorkHoursRequest();
		try {
			request = setModel(req);
			response = informationWsConnector.requestChangeWorkHours(authority, request, INDONESIAN);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return response;
	}
	
	private ChangeWorkHoursRequest setModel(HttpServletRequest request) {
		ChangeWorkHoursRequest model = new ChangeWorkHoursRequest();
		try {
			model.setEmployee_id(request.getParameter("employeeId"));
			model.setEmployee_name(request.getParameter("employeeName"));
			model.setStart_time(request.getParameter("startTime"));
			model.setEnd_time(request.getParameter("endTime"));
			model.setReason(request.getParameter("reason"));
			model.setApproval_ds_1(request.getParameter("approval1"));
			model.setApproval_ds_1_name(request.getParameter("approval1Name"));
			model.setApproval_ds_2(request.getParameter("approval2"));
			model.setApproval_ds_2_name(request.getParameter("approval2Name"));
			model.setHours_id(request.getParameter("hoursId"));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return model;
	}
}
