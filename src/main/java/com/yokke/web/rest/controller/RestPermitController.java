package com.yokke.web.rest.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yokke.connector.PermitWSConnector;
import com.yokke.connector.request.ListUploadRequest;
import com.yokke.connector.request.PermitRequest;
import com.yokke.connector.request.PreviewFileRequest;
import com.yokke.connector.request.SortDynamicRequest;
import com.yokke.connector.request.UploadFileRequest;
import com.yokke.connector.response.AbsenceTypeResponse;
import com.yokke.connector.response.ApprovalResponse;
import com.yokke.connector.response.CurrentYearDetail;
import com.yokke.connector.response.HistoryTaskResponse;
import com.yokke.connector.response.HolidayInCurrentYear;
import com.yokke.connector.response.LeaveResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.PreviewFileResponse;
import com.yokke.connector.response.QuotaLeaveResponse;
import com.yokke.connector.response.RemainingQuotaResponse;
import com.yokke.connector.response.UploadResponse;
import com.yokke.web.controller.AuthController;
import com.yokke.web.controller.PermitHistoryController;
import com.yokke.web.controller.PermitRequestController;

@RestController
public class RestPermitController extends AuthController {

	@Autowired
	private PermitWSConnector connector;
	
	@Override
	protected String viewName() {
		return null;
	}
	
	@RequestMapping(value = PermitRequestController.FIND_APPROVAL_URL, method = RequestMethod.GET)
	public ApprovalResponse findApproval(HttpServletRequest req) {

		String authority = authority(req);
		ApprovalResponse response = null;
		try {
			response = connector.findAllApproval(authority, getUsernameSession(req), INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setError(e.getMessage());
			return response;
		}
	}

	@RequestMapping(value = PermitRequestController.GET_QUOTA_TAKEN, method = RequestMethod.GET)
	public String takenQuota(HttpServletRequest req) {
		String authority = authority(req);
		String startDate = req.getParameter("startDate");
		String endDate 	 = req.getParameter("endDate");
		int total = 0;
		QuotaLeaveResponse response = null;
		try {
			if(!"".equals(startDate) && !"".equals(endDate)) {
				response = connector.countLeaveLeft(parseDate(startDate, "dd/MM/yyyy", "yyyyMMdd"),
						parseDate(endDate, "dd/MM/yyyy", "yyyyMMdd"), INDONESIAN, authority);
				if(String.valueOf(response.getTotal()) != null) {
					return String.valueOf(response.getTotal());
				}
			}else {
				return String.valueOf(0);
			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return Integer.toString(total);
	}

	@RequestMapping(value = PermitRequestController.HOLIDAY_CURRENT_YEAR, method = RequestMethod.GET)
	public List<String> findAllHolidayInCurrentYear(HttpServletRequest req) {
		String authority = authority(req);
		HolidayInCurrentYear holidayInCurrentYear = new HolidayInCurrentYear();
		List<String> response = null;
		try {
			response = new ArrayList<>();
			holidayInCurrentYear = connector.findAllHoliday(authority, INDONESIAN);
			for (CurrentYearDetail cyd : holidayInCurrentYear.getData()) {
				response.add(parseDate(cyd.getDate(), "yyyyMMdd", "d/M/yyyy"));
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = PermitRequestController.FIND_ABSENCE_TYPE_URL, method = RequestMethod.GET)
	public List<AbsenceTypeResponse> findAbsenceType(HttpServletRequest req) {
		String authority = authority(req);
		List<AbsenceTypeResponse> response = null;
		try {
			response = connector.findAllAbsenceType(authority, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = PermitRequestController.FIND_REMAINING_QUOTA_URL, method = RequestMethod.GET)
	public ListResponse<RemainingQuotaResponse> findRemainingQuota(HttpServletRequest req) {

		String authority = authority(req);
		ListResponse<RemainingQuotaResponse> response = null;
		try {
			response = connector.findRemainingQuota(authority, getUsernameSession(req), INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = PermitRequestController.CREATE_PERMIT_URL, method = RequestMethod.GET)
	public LeaveResponse createPermit(HttpServletRequest req) {

		String authority = authority(req);
		LeaveResponse response = null;
		PermitRequest leaveRequest = setLeaveRequest(req);

		try {
			leaveRequest.setLeave_start_date(parseDate(leaveRequest.getLeave_start_date(), "dd/MM/yyyy", "yyyyMMdd"));
			leaveRequest.setLeave_end_date(parseDate(leaveRequest.getLeave_end_date(), "dd/MM/yyyy", "yyyyMMdd"));
			response = connector.createLeave(authority, leaveRequest, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	private PermitRequest setLeaveRequest(HttpServletRequest request) {
		PermitRequest model = new PermitRequest();
		
		List<ListUploadRequest> listUpload = new ArrayList<ListUploadRequest>();	
		String uploadJson = request.getParameter("listUpload");
		try {
			listUpload = parseJsonList(uploadJson, ListUploadRequest.class);
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			model.setAbsences_type(request.getParameter("absenceType"));
			model.setAbsenceDescription(request.getParameter("absenceDescription"));
			model.setEmployee_id(request.getParameter("employeeId"));
			model.setEmployee_name(request.getParameter("employeeName"));
			model.setDs1_id(request.getParameter("approvalName1"));
			model.setApproval_ds_1(request.getParameter("approvalId1"));
			model.setDs2_id(request.getParameter("approvalName2"));
			model.setApproval_ds_2(request.getParameter("approvalId2"));
			model.setLeave_status("new");
			model.setSubject(request.getParameter("subject"));
			model.setLeave_reason(request.getParameter("leaveReason"));
			model.setComments(request.getParameter("comments"));
			model.setLeave_start_date(request.getParameter("startdate"));
			model.setLeave_end_date(request.getParameter("enddate"));
			model.setDescription(request.getParameter("description"));
			model.setQuota_taken(request.getParameter("quotaTaken"));
			model.setRemaining_quota(request.getParameter("remainingQuota"));
			model.setTotal_taken_quota(request.getParameter("totalTakenQuota"));
			model.setCarryOverFlag("N");
			model.setlistUpload(listUpload);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.getSuppressed();
		}

		return model;
	}

	@RequestMapping(value = PermitHistoryController.LIST_URL, method = RequestMethod.GET) 
	public ListResponse<HistoryTaskResponse> listPermitHistory(HttpServletRequest req){
	  
		String authority = authority(req);
		ListResponse<HistoryTaskResponse> response = null;
		String paramKey = req.getParameter("paramKey");
		String paramValue = req.getParameter("paramValue");
		String paramValue2 = req.getParameter("paramValue2");
		String orderBy	= req.getParameter("orderBy");
		String column	= req.getParameter("column");
		String page = getPage(req);
		SortDynamicRequest sortRequest = setModelSortDynamic(orderBy, column);
	  
		try { 
		  	  response = connector.historyPermit(page, authority, getUsernameSession(req), paramKey, paramValue, paramValue2, sortRequest, INDONESIAN);
		  	  return response;
		  } catch (Exception e) { 
			  e.printStackTrace();
			  return response;
		  } 
	  }
	
	@SuppressWarnings("unused")
	@RequestMapping(value = PermitRequestController.UPLOAD_FILE_URL, method = RequestMethod.POST)
	public UploadResponse uploadFile(HttpServletRequest request){
		UploadResponse response = new UploadResponse();

		String filename = request.getParameter("filename");
		String fileSize = request.getParameter("fileSize");
		String fileType = request.getParameter("fileType");
		String fileDesc = request.getParameter("fileDesc");
		
		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				Calendar calendar = new GregorianCalendar();
				TimeZone timeZone = calendar.getTimeZone();
				calendar.setTimeZone(timeZone);
				int years = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH)+1;
				String months = null;
				if(month < 10) {
					months = "0".concat(String.valueOf(month));
				}else {
					months = String.valueOf(month);
				}
				String separator = String.valueOf(years).concat("/").concat(months).concat("/");
				
				File folderDin = new File(path_document + separator);
				Path pathDin = Paths.get(String.valueOf(folderDin));
				
				if (!Files.exists(pathDin)) {
					Files.createDirectories(pathDin);
				}
				
				List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				String name = null;
				for (FileItem item : multiparts) {
					if (!item.isFormField()) {
						name = getUsernameSession(request) +"-"+ filename; 
						item.write(new File(pathDin + File.separator + name));
					}
				}
				response.setFilename(name);
				response.setStatus(200);
				response.setMessageupload("Success Upload file");
				return response;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		} else {
			request.setAttribute("message_upload", "Failed upload");
			response.setStatus(400);
			response.setMessageupload("Failed Upload file");
			return response;
		}
		return null;
	}
	
	@RequestMapping(value = PermitRequestController.UPLOAD_FILE_URL2, method = RequestMethod.POST)
	public UploadResponse uploadFile2(@RequestBody String fileByte, HttpServletRequest request){
		UploadResponse response = new UploadResponse();
		try {
			String[] parts = fileByte.split(",");
			String images = parts[1];
			/*
			 * byte[] file_byte = DatatypeConverter.parseBase64Binary(images); BufferedImage
			 * file_image = ImageIO.read(new ByteArrayInputStream(file_byte));
			 * 
			 * UUID fileName = UUID.randomUUID(); Path path =
			 * Paths.get(path_document+File.pathSeparator + fileName); if
			 * (!Files.exists(path)) { Files.createDirectories(path); File file = new
			 * File(path_document + File.separator + fileName); ImageIO.write(file_image,
			 * "PNG", file);
			 * 
			 * Files.deleteIfExists(path); }
			 * 
			 * response.setFilename(String.valueOf(fileName)); response.setFileSize("");
			 * response.setFileType(""); response.setMessageupload("Success Upload");
			 */
			UploadFileRequest req = new UploadFileRequest();
			req.setContent(images);
			String authority = authority(request);
			response = connector.uploadFiles(authority, req, INDONESIAN);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			response.setMessageupload(e.getMessage());
			response.setStatus(400);
		}
		return response;
	}
	
	@RequestMapping(value = PermitRequestController.PREVIEW_FILE_URL, method = RequestMethod.GET)
	public PreviewFileResponse previewFile(HttpServletRequest req) {

		String authority = authority(req);
		PreviewFileResponse response = null;
		PreviewFileRequest model = setModel(req);
		try {
			response = connector.previewFile(authority, model, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setError(e.getMessage());
			return response;
		}
	}
	
	private PreviewFileRequest setModel(HttpServletRequest req) {
		PreviewFileRequest model = new PreviewFileRequest();
		model.setDirectory(req.getParameter("directory"));
		model.setCreatedBy(req.getParameter("createdBy"));
		model.setFileName(req.getParameter("filename"));
		return model;
	}

	 
}
