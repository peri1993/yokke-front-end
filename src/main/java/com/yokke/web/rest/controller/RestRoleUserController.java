package com.yokke.web.rest.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yokke.connector.RoleUserWSConnector;
import com.yokke.connector.request.RoleRequest;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.RoleResponse;
import com.yokke.connector.response.UserRoleResponse;
import com.yokke.web.controller.AuthController;
import com.yokke.web.controller.RoleManagementController;
import com.yokke.web.controller.UserManagementController;

@RestController
public class RestRoleUserController extends AuthController {

	@Override
	protected String viewName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Autowired
	private RoleUserWSConnector connector;

	@RequestMapping(value = RoleManagementController.LIST_ROLE, method = RequestMethod.GET)
	public ListResponse<RoleResponse> listRole(HttpServletRequest req) {
		String authority = authority(req);
		ListResponse<RoleResponse> response = null;
		RoleRequest request = null;
		try {
			response = connector.getAllRole(request, INDONESIAN, authority);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = UserManagementController.LIST_USER, method = RequestMethod.GET)
	public ListResponse<UserRoleResponse> listUser(HttpServletRequest req) {
		String authority = authority(req);
		ListResponse<UserRoleResponse> response = null;
		String paramKey = req.getParameter("paramKey");
		String paramValue = req.getParameter("paramValue");
		String paramValue2 = req.getParameter("paramValue2");
		String page = getPage(req);
		try {
			response = connector.getAllUser(page, paramKey, paramValue, paramValue2, INDONESIAN, authority);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = RoleManagementController.DELETE_ROLE, method = RequestMethod.GET)
	public BaseResponse deleteRole(HttpServletRequest req) {
		String authority = authority(req);
		BaseResponse response = null;
		String roleId = req.getParameter("roleId");
		try {
			response = connector.deleteRole(roleId, INDONESIAN, authority);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = UserManagementController.DELETE_USER, method = RequestMethod.GET)
	public BaseResponse deleteUser(HttpServletRequest req) {
		String authority = authority(req);
		BaseResponse response = null;
		String userId = req.getParameter("userId");
		try {
			response = connector.deleteUser(userId, INDONESIAN, authority);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}
	
}
