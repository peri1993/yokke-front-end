package com.yokke.web.rest.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yokke.connector.NotificationWSConnector;
import com.yokke.connector.response.CheckAssignerTaskResponse;
import com.yokke.connector.response.ListNotificationsDetailResponse;
import com.yokke.connector.response.ListNotificationsResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.web.controller.AuthController;
import com.yokke.web.controller.NotificationsController;

@RestController
public class RestNotificationController extends AuthController {

	@Override
	protected String viewName() {
		return null;
	}

	@Autowired
	private NotificationWSConnector connector;

	@RequestMapping(value = NotificationsController.LIST_USER_URL, method = RequestMethod.GET)
	public ListResponse<ListNotificationsResponse> listNotif(HttpServletRequest request) {
		String authority = authority(request);
		ListResponse<ListNotificationsResponse> response = null;
		try {
			response = connector.listNotifTask(authority, getUsernameSession(request), INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}

	}

	@RequestMapping(value = NotificationsController.LIST_URL, method = RequestMethod.GET)
	public ListResponse<ListNotificationsDetailResponse> listDetailNotif(HttpServletRequest req) {
		String authority = authority(req);
		ListResponse<ListNotificationsDetailResponse> response = null;
		String paramKey 	= req.getParameter("paramKey");
		String paramValue 	= req.getParameter("paramValue");
		String paramValue2 	= req.getParameter("paramValue2");
		String page			= getPage(req);
		try {
			response = connector.listDetailNotifTask(page, authority, getUsernameSession(req), paramKey, paramValue, paramValue2, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	public CheckAssignerTaskResponse checkAssigner(String instancesId, String username, String authority) {
		CheckAssignerTaskResponse response = null;
		try {
			response = connector.checkAssignerTask(instancesId, username, authority, INDONESIAN);
			return response;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return response;
		}
	}
	
}
