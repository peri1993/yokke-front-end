package com.yokke.web.rest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yokke.connector.ReAssignTaskWSConnector;
import com.yokke.connector.request.ApprovalChangeRequest;
import com.yokke.connector.request.SortDynamicRequest;
import com.yokke.connector.response.AssignedTaskResponse;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.ObjectResponse;
import com.yokke.connector.response.ReAssignTaskResponse;
import com.yokke.connector.response.TaskLogsResponse;
import com.yokke.connector.response.UsernameResponse;
import com.yokke.web.controller.AssignedTaskController;
import com.yokke.web.controller.AssignerHistoryAllController;
import com.yokke.web.controller.AssignerHistoryController;
import com.yokke.web.controller.AuthController;

@RestController
public class RestReAssignTaskController extends AuthController {

	@Override
	protected String viewName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Autowired
	private ReAssignTaskWSConnector connector;
	
	@RequestMapping(value = AssignedTaskController.LIST_URL, method = RequestMethod.GET)
	public ListResponse<AssignedTaskResponse> listAssign(HttpServletRequest req) {
		String authority = authority(req);
		ListResponse<AssignedTaskResponse> response = null;
		String paramKey = req.getParameter("paramKey");
		String paramValue = req.getParameter("paramValue");
		String orderBy	= req.getParameter("orderBy");
		String column = req.getParameter("column");
		SortDynamicRequest sortRequest = setModelSortDynamic(orderBy, column);
		
		String page = getPage(req);
		try {
			response = connector.listAssigner(authority, page, paramKey, paramValue, sortRequest, INDONESIAN);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}
	
	@RequestMapping(value = AssignedTaskController.DETAIL_ASSIGN_URL, method = RequestMethod.GET)
	public ObjectResponse<AssignedTaskResponse> detail(HttpServletRequest req) {
		String authority = authority(req);
		ObjectResponse<AssignedTaskResponse> response = null;
		String instancesId = req.getParameter("instancesId");
		try {
			response = connector.detail(instancesId, authority, INDONESIAN);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;

	}
	
	@RequestMapping(value = AssignedTaskController.SEARCH_URL, method = RequestMethod.GET)
	public UsernameResponse search(HttpServletRequest req) {
		String authority = authority(req);
		UsernameResponse response = null;
		String username = req.getParameter("username");
		try {
			response = connector.findUsername(username, INDONESIAN, authority);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}
	
	@RequestMapping(value = AssignedTaskController.UPDATE_URL, method = RequestMethod.GET)
	public BaseResponse update(HttpServletRequest req) {
		String authority = authority(req);
		ReAssignTaskResponse response = null;
		ApprovalChangeRequest request = setModel(req);
		try {
			response = connector.update(request, authority, INDONESIAN);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}
	
	@RequestMapping(value = AssignedTaskController.UPDATE_LIST_URL, method = RequestMethod.GET)
	public BaseResponse updateList(HttpServletRequest req) {
		String authority = authority(req);
		ReAssignTaskResponse response = null;
		String username = req.getParameter("username");
		List<ApprovalChangeRequest> request = setModelList(req, username);
		try {
			response = connector.updateList(request, authority, INDONESIAN);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}
	
	@RequestMapping(value = AssignerHistoryController.LIST_URL, method = RequestMethod.GET)
	public ListResponse<TaskLogsResponse> findHistoryAssigner(HttpServletRequest request){
		String authority 	= authority(request);
		String paramKey 	= request.getParameter("paramKey");
		String paramValue	= request.getParameter("paramValue");
		String paramValue2 	= request.getParameter("paramValue2");
		ListResponse<TaskLogsResponse> response = null;
		String page = getPage(request);
		try {
			response = connector.findHistoryAssigner(page, authority, paramKey, paramValue, paramValue2, getUsernameSession(request), INDONESIAN);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = AssignerHistoryAllController.LIST_URL, method = RequestMethod.GET)
	public ListResponse<TaskLogsResponse> findAllHistoryAssigner(HttpServletRequest request){
		String authority = authority(request);
		String paramKey	 = request.getParameter("paramKey");
		String paramValue= request.getParameter("paramValue");
		String paramValue2 = request.getParameter("paramValue2");
		ListResponse<TaskLogsResponse> response = null;
		String page = getPage(request);
		try {
			response = connector.findAllHistoryAssigner(page, paramKey, paramValue, paramValue2, authority, INDONESIAN);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<ApprovalChangeRequest> setModelList(HttpServletRequest req, String username) {
		List<ApprovalChangeRequest> response = new ArrayList<ApprovalChangeRequest>();
		if(req.getParameter("listModel") != null) {
			List<AssignedTaskResponse> listAssigned = new ArrayList<AssignedTaskResponse>();
			String json = req.getParameter("listModel");
			try {
				// mapping json to list model
				listAssigned = parseJsonList(json, AssignedTaskResponse.class);
				for (Iterator iterator = listAssigned.iterator(); iterator.hasNext();) {
					ApprovalChangeRequest approval = new ApprovalChangeRequest();
					LinkedHashMap<String, Object> maps =  (LinkedHashMap<String, Object>) iterator.next();
					approval.setAssigner(String.valueOf(maps.get("assigner")));
					approval.setUsername(getUsernameSession(req));
					approval.setInstancesId(String.valueOf(maps.get("instancesId")));
					approval.setDs1_id(String.valueOf(maps.get("approvalDs1")));
					approval.setDs2_id(String.valueOf(maps.get("approvalDs2")));
					response.add(approval);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// validation approval change 
			for (int i = 0; i < response.size(); i++) {
				ApprovalChangeRequest approval = response.get(i);
				if(approval.getAssigner().contains(approval.getDs1_id()) && approval.getAssigner().contains(approval.getDs2_id())) {
					approval.setDs1_id(username);
					approval.setDs2_id(username);
				}else if(approval.getAssigner().contains(approval.getDs1_id()) && !approval.getAssigner().contains(approval.getDs2_id())) {
					approval.setDs1_id(username);
				}else {
					approval.setDs2_id(username);
				}
			}
			
		}
		return response;
	}
	
	private ApprovalChangeRequest setModel(HttpServletRequest req) {
		ApprovalChangeRequest model = new ApprovalChangeRequest();
		model.setDs1_id(req.getParameter("ds1_id"));
		model.setDs2_id(req.getParameter("ds2_id"));
		model.setInstancesId(req.getParameter("instancesId"));
		model.setAssigner(req.getParameter("assigner"));
		model.setUsername(getUsernameSession(req));
		return model;
	}
}
