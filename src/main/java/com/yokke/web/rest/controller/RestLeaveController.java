package com.yokke.web.rest.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yokke.connector.LeaveWSConnector;
import com.yokke.connector.request.LeaveRequest;
import com.yokke.connector.request.SortDynamicRequest;
import com.yokke.connector.request.TaskApprovalRequest;
import com.yokke.connector.response.AbsenceTypeResponse;
import com.yokke.connector.response.ApprovalResponse;
import com.yokke.connector.response.ApproveRejectResponse;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.BlockLeaveResponse;
import com.yokke.connector.response.CurrentYearDetail;
import com.yokke.connector.response.DataDateResponse;
import com.yokke.connector.response.HistoryTaskResponse;
import com.yokke.connector.response.HolidayInCurrentYear;
import com.yokke.connector.response.LastRemainingResponse;
import com.yokke.connector.response.LeaveResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.QuotaLeaveResponse;
import com.yokke.connector.response.RemainingQuotaResponse;
import com.yokke.connector.response.TaskDetailResponse;
import com.yokke.connector.response.TaskLogsResponse;
import com.yokke.connector.response.TaskResponse;
import com.yokke.web.controller.AuthController;
import com.yokke.web.controller.InboxController;
import com.yokke.web.controller.LeaveController;
import com.yokke.web.controller.LeaveHistoryController;

@RestController
public class RestLeaveController extends AuthController {

	@Autowired
	private LeaveWSConnector connector;

	@Override
	protected String viewName() {
		return null;
	}

	@RequestMapping(value = LeaveController.FIND_APPROVAL_URL, method = RequestMethod.GET)
	public ApprovalResponse findApproval(HttpServletRequest req) {

		String authority = authority(req);
		ApprovalResponse response = null;
		try {
			response = connector.findAllApproval(authority, getUsernameSession(req), INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setError(e.getMessage());
			return response;
		}
	}

	@RequestMapping(value = LeaveController.GET_QUOTA_TAKEN, method = RequestMethod.GET)
	public String takenQuota(HttpServletRequest req) {
		String authority = authority(req);
		String startDate = req.getParameter("startDate");
		String endDate	 = req.getParameter("endDate");
		int total = 0;
		QuotaLeaveResponse response = null;
		try {
			if(!"".equals(startDate) && !"".equals(endDate)) {
				response = connector.countLeaveLeft(parseDate(startDate, "dd/MM/yyyy", "yyyyMMdd"),
						parseDate(endDate, "dd/MM/yyyy", "yyyyMMdd"), INDONESIAN, authority);
				if(String.valueOf(response.getTotal()) != null) {
					return String.valueOf(response.getTotal());
				}
			}else {
				return String.valueOf(0);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return Integer.toString(total);
	}

	@RequestMapping(value = LeaveController.HOLIDAY_CURRENT_YEAR, method = RequestMethod.GET)
	public List<String> findAllHolidayInCurrentYear(HttpServletRequest req) {
		String authority = authority(req);
		HolidayInCurrentYear holidayInCurrentYear = new HolidayInCurrentYear();
		List<String> response = null;
		try {
			response = new ArrayList<>();
			holidayInCurrentYear = connector.findAllHoliday(authority, INDONESIAN);
			for (CurrentYearDetail cyd : holidayInCurrentYear.getData()) {
				response.add(parseDate(cyd.getDate(), "yyyyMMdd", "dd/MM/yyyy"));
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = LeaveController.FIND_ABSENCE_TYPE_URL, method = RequestMethod.GET)
	public List<AbsenceTypeResponse> findAbsenceType(HttpServletRequest req) {
		String authority = authority(req);
		List<AbsenceTypeResponse> response = null;
		try {
			response = connector.findAllAbsenceType(authority, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	@RequestMapping(value = LeaveController.FIND_ABSENCE_TYPE_ALL_URL, method = RequestMethod.GET)
	public List<AbsenceTypeResponse> findAllAbsencesType(HttpServletRequest req) {
		String authority = authority(req);
		List<AbsenceTypeResponse> response = null;
		try {
			response = connector.findAllAbsence(authority, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = LeaveController.FIND_REMAINING_QUOTA_URL_DTL + "/{instanceId}", method = RequestMethod.GET)
	public ListResponse<RemainingQuotaResponse> findRemainingQuotaDtl(@PathVariable("instanceId") String instanceId,
			HttpServletRequest req) {

		String authority = authority(req);
		ListResponse<RemainingQuotaResponse> response = null;
		try {
			response = connector.findRemainingQuota(authority, instanceId, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = LeaveController.FIND_REMAINING_QUOTA_URL, method = RequestMethod.GET)
	public ListResponse<RemainingQuotaResponse> findRemainingQuota(HttpServletRequest req) {

		String authority = authority(req);
		ListResponse<RemainingQuotaResponse> response = null;
		try {
			response = connector.findRemainingQuota(authority, getUsernameSession(req), INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	@RequestMapping(value = LeaveController.FIND_REMAINING_QUOTA_BY_REQUESTER_URL, method = RequestMethod.GET)
	public ListResponse<RemainingQuotaResponse> findRemainingQuotaByRequester(HttpServletRequest req) {

		String authority = authority(req);
		ListResponse<RemainingQuotaResponse> response = null;
		String requester = req.getParameter("requester");
		try {
			response = connector.findRemainingQuota(authority, requester, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	@RequestMapping(value = LeaveController.FIND_LAST_REMAINING, method = RequestMethod.GET)
	public LastRemainingResponse findLastRemainingQuota(HttpServletRequest req) {
		LastRemainingResponse response = null;
		try {
			
			response = setTotalPendingQuota(req);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private LastRemainingResponse setTotalPendingQuota(HttpServletRequest request) {
		String authority = authority(request);
		List<DataDateResponse> listData = new ArrayList<DataDateResponse>();
		LastRemainingResponse response = connector.findLastRemaining(authority, getUsernameSession(request), INDONESIAN);
		try {
			ListResponse<DataDateResponse> listDateResponse = connector.listDataDate(authority, getUsernameSession(request), INDONESIAN);
			if (listDateResponse != null) {
				if (!listDateResponse.getListResponse().isEmpty()) {
					for (Iterator iterator = listDateResponse.getListResponse().iterator(); iterator.hasNext();) {
						DataDateResponse data = new DataDateResponse();
						LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) iterator.next();
						data.setStartDate(String.valueOf(map.get("startDate")));
						data.setEndDate(String.valueOf(map.get("endDate")));
						listData.add(data);
					}
				}
			}
			
			if (!listData.isEmpty()) {
				response.setRemainingPending(setDataPending(request, listData));
			}else {
				response.setRemainingPending("0");
			}
			
			response.setTotalRequest(setTotal(response));
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}
	
	private String setTotal(LastRemainingResponse data) {
		Integer response = 0;
		if (data.getRemaining() != null && data.getRemainingPending() !=null) {
			response = Integer.valueOf(data.getRemaining())-Integer.valueOf(data.getRemainingPending());
		}
		return Integer.toString(response);
		
	}
	
	private String setDataPending(HttpServletRequest request, List<DataDateResponse> listData) {
		Integer count = 0;
		for (int i = 0; i < listData.size(); i++) {
			String response = getDays(request, listData.get(i).getStartDate(), listData.get(i).getEndDate());
			count = count + Integer.valueOf(response);
		}
		return Integer.toString(count);
	}
	
	private String getDays(HttpServletRequest request, String startDate, String endDate) {
		String authority = authority(request);
		int total = 0;
		QuotaLeaveResponse response = null;
		if(!"".equals(startDate) && !"".equals(endDate)) {
			response = connector.countLeaveLeft(startDate,endDate, INDONESIAN, authority);
			if(String.valueOf(response.getTotal()) != null) {
				return String.valueOf(response.getTotal());
			}
		}else {
			return String.valueOf(0);
		}
		return Integer.toString(total);
	}
	
	
	@RequestMapping(value = LeaveController.FIND_LAST_REMAINING_CARRY, method = RequestMethod.GET)
	public LastRemainingResponse findLastRemainingCarry(HttpServletRequest req) {
		String authority = authority(req);
		LastRemainingResponse response = null;
		try {
			response = connector.findLastRemainingCarry(authority, getUsernameSession(req), INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = LeaveController.CREATE_LEAVE_URL, method = RequestMethod.GET)
	public LeaveResponse createLeave(HttpServletRequest req) {

		String authority = authority(req);
		LeaveResponse response = null;
		LeaveRequest leaveRequest = setLeaveRequest(req);

		try {
			leaveRequest.setLeave_start_date(parseDate(leaveRequest.getLeave_start_date(), "dd/MM/yyyy", "yyyyMMdd"));
			leaveRequest.setLeave_end_date(parseDate(leaveRequest.getLeave_end_date(), "dd/MM/yyyy", "yyyyMMdd"));
			response = connector.createLeave(authority, leaveRequest, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	private LeaveRequest setLeaveRequest(HttpServletRequest request) {
		LeaveRequest model = new LeaveRequest();
		try {
			model.setAbsences_type(request.getParameter("absenceType"));
			model.setAbsenceDescription(request.getParameter("absenceDescription"));
			model.setEmployee_id(request.getParameter("employeeId"));
			model.setEmployee_name(request.getParameter("employeeName"));
			model.setDs1_id(request.getParameter("approvalName1"));
			model.setApproval_ds_1(request.getParameter("approvalId1"));
			model.setDs2_id(request.getParameter("approvalName2"));
			model.setApproval_ds_2(request.getParameter("approvalId2"));
			model.setLeave_status("new");
			model.setSubject(request.getParameter("subject"));
			model.setLeave_reason(request.getParameter("leaveReason"));
			model.setComments(request.getParameter("comments"));
			model.setLeave_start_date(request.getParameter("startdate"));
			model.setLeave_end_date(request.getParameter("enddate"));
			model.setDescription(request.getParameter("description"));
			model.setQuota_taken(request.getParameter("quotaTaken"));
			model.setRemaining_quota(request.getParameter("remainingQuota"));
			model.setTotal_taken_quota(request.getParameter("totalTakenQuota"));
			model.setCarryOverFlag(request.getParameter("carryOverFlag"));
			if(null != request.getParameter("lastRemaining")) {
				model.setLastRemaining(request.getParameter("lastRemaining"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.getSuppressed();
		}

		return model;
	}

	@RequestMapping(value = InboxController.LIST_URL, method = RequestMethod.GET)
	public ListResponse<TaskResponse> listInbox(HttpServletRequest req) {
		String authority = authority(req);
		ListResponse<TaskResponse> response = null;
		String paramKey 	= req.getParameter("paramKey");
		String paramValue 	= req.getParameter("paramValue");
		String paramValue2 	= req.getParameter("paramValue2");
		String column		= req.getParameter("column");
		String orderBy		= req.getParameter("orderBy");
		String page			= getPage(req);
		SortDynamicRequest sortRequest = setModelSortDynamic(orderBy, column);
		try {
			response = connector.findInboxTask(page, authority, getUsernameSession(req), paramKey, paramValue, paramValue2, sortRequest, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	@RequestMapping(value = InboxController.HISTORY_TASK_LOGS, method = RequestMethod.GET)
	public ListResponse<TaskLogsResponse> historyTaskLogs(HttpServletRequest req) {
		String authority = authority(req);
		String instancesId = req.getParameter("instancesId");
				
		ListResponse<TaskLogsResponse> response = null;
		try {
			response = connector.historyTaskLogs(authority, instancesId, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = InboxController.DETAIL_TASK_URL, method = RequestMethod.GET)
	public TaskDetailResponse detailTask(HttpServletRequest req) {

		String authority = authority(req);
		String instanceId = req.getParameter("instanceId");
		TaskDetailResponse response = null;
		try {
			response = connector.detailTask(authority, instanceId, getUsernameSession(req) , INDONESIAN);
			if (response != null) {
				response.setLeave_start_date(parseDate(response.getLeave_start_date(), "yyyyMMdd", "dd/MM/yyyy"));
				response.setLeave_end_date(parseDate(response.getLeave_end_date(), "yyyyMMdd", "dd/MM/yyyy"));
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	@RequestMapping(value = InboxController.DETAIL_TASK_WORK_HOURS_URL, method = RequestMethod.GET)
	public TaskDetailResponse detailTaskWorkHours(HttpServletRequest req) {

		String authority = authority(req);
		String instanceId = req.getParameter("instanceId");
		TaskDetailResponse response = null;
		try {
			response = connector.detailTask(authority, instanceId, getUsernameSession(req) , INDONESIAN);
			response.setLeave_start_date(parseDate(response.getLeave_start_date(), "yyyyMMdd", "dd/MM/yyyy"));
			response.setLeave_end_date(parseDate(response.getLeave_end_date(), "yyyyMMdd", "dd/MM/yyyy"));
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = InboxController.APPROVE_URL, method = RequestMethod.GET)
	public BaseResponse Approve(HttpServletRequest req) {

		String authority = authority(req);
		TaskApprovalRequest body = new TaskApprovalRequest();
		body.setTaskId(req.getParameter("taskId"));
		body.setComments(req.getParameter("comments"));
		body.setInstancesId(req.getParameter("instancesId"));
		body.setUsername(getUsernameSession(req));
		BaseResponse response = null;
		try {
			response = connector.approve(authority, body, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = InboxController.REJECT_URL, method = RequestMethod.GET)
	public BaseResponse Reject(HttpServletRequest req) {

		String authority = authority(req);
		TaskApprovalRequest body = new TaskApprovalRequest();
		body.setTaskId(req.getParameter("taskId"));
		body.setComments(req.getParameter("comments"));
		body.setInstancesId(req.getParameter("instancesId"));
		body.setUsername(getUsernameSession(req));
		BaseResponse response = null;
		try {
			response = connector.reject(authority, body, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	@RequestMapping(value = InboxController.APPROVE_LIST_URL, method = RequestMethod.GET)
	public BaseResponse ApprovedList(HttpServletRequest req) {
		String authority = authority(req);
		List<TaskApprovalRequest> listModel = setModelListApprove(req);
		BaseResponse response = new BaseResponse();
		/*for (ApproveRejectResponse approveRejectResponse : approveRejectResponses) {
			TaskApprovalRequest body = new TaskApprovalRequest();
			String instancesId = approveRejectResponse.getTaskId();
			body.setTaskId(instancesId);
			body.setComments("APP");
			body.setLeave_status("new");
			body.setInstancesId(approveRejectResponse.getInstancesId());
			body.setUsername(getUsernameSession(req));
			list.add(body);
		}*/
		try {
			response = connector.approveList(authority, listModel, INDONESIAN);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<TaskApprovalRequest> setModelListApprove(HttpServletRequest req){
		List<TaskApprovalRequest> listModel = new ArrayList<TaskApprovalRequest>();
		List<ApproveRejectResponse> listRequest = new ArrayList<ApproveRejectResponse>();
		try {
			String json = req.getParameter("listModel");
			listRequest = parseJsonList(json, ApproveRejectResponse.class);
			for (Iterator iterator = listRequest.iterator(); iterator.hasNext();) {
				LinkedHashMap<String, Object> map =  (LinkedHashMap<String, Object>) iterator.next();
				TaskApprovalRequest request = new TaskApprovalRequest();
				request.setTaskId(String.valueOf(map.get("taskId")));
				request.setComments(String.valueOf("APP"));
				request.setLeave_status(String.valueOf("new"));
				request.setInstancesId(String.valueOf(map.get("instancesId")));
				request.setUsername(getUsernameSession(req));
				listModel.add(request);
				
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return listModel;
	}

	@RequestMapping(value = InboxController.REJECT_LIST_URL, method = RequestMethod.GET)
	public BaseResponse RejectList(HttpServletRequest req) {
		String authority = authority(req);
		List<TaskApprovalRequest> listModel = setModelListReject(req);
		BaseResponse response = new BaseResponse();
		/*
		 * for (ApproveRejectResponse approveRejectResponse : approveRejectResponses) {
		 * TaskApprovalRequest body = new TaskApprovalRequest();
		 * body.setTaskId(approveRejectResponse.getTaskId());
		 * body.setInstancesId(approveRejectResponse.getInstancesId());
		 * body.setComments("Revise"); body.setUsername(getUsernameSession(req));
		 * list.add(body);
		 */
			try {
				response = connector.rejectList(authority, listModel, INDONESIAN);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return response;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List<TaskApprovalRequest> setModelListReject(HttpServletRequest req){
		List<TaskApprovalRequest> listModel = new ArrayList<TaskApprovalRequest>();
		List<ApproveRejectResponse> listRequest = new ArrayList<ApproveRejectResponse>();
		try {
			String json = req.getParameter("listModel");
			listRequest = parseJsonList(json, ApproveRejectResponse.class);
			for (Iterator iterator = listRequest.iterator(); iterator.hasNext();) {
				LinkedHashMap<String, Object> map =  (LinkedHashMap<String, Object>) iterator.next();
				TaskApprovalRequest request = new TaskApprovalRequest();
				request.setTaskId(String.valueOf(map.get("taskId")));
				request.setInstancesId(String.valueOf(map.get("instancesId")));
				request.setComments(String.valueOf("Reject"));
				request.setUsername(getUsernameSession(req));
				listModel.add(request);
				
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return listModel;
	}

	@RequestMapping(value = InboxController.CEK_ACTIVE_DATA_URL, method = RequestMethod.GET)
	public BaseResponse cekActiveData(HttpServletRequest req) {

		String authority = authority(req);
		String instancesId = req.getParameter("instanceId");
		BaseResponse response = null;
		try {
			response = connector.cekDataActive(authority, instancesId, INDONESIAN);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return response;
		}
	}

	/*
	 * @RequestMapping(value = InboxController.UPDATE_DATA_URL, method =
	 * RequestMethod.GET) public LeaveResponse updateData(HttpServletRequest req) {
	 * 
	 * String authority = authority(req); LeaveRequest leaveRequest =
	 * setLeaveRequest(req); LeaveResponse response = null; try {
	 * leaveRequest.setLeave_start_date(parseDate(leaveRequest.getLeave_start_date()
	 * , "dd/MM/yyyy", "yyyyMMdd"));
	 * leaveRequest.setLeave_end_date(parseDate(leaveRequest.getLeave_end_date(),
	 * "dd/MM/yyyy", "yyyyMMdd")); response = connector.updateDataMinova(authority,
	 * leaveRequest, INDONESIAN); return response; } catch (Exception e) {
	 * e.printStackTrace(); return response; } }
	 */
	
	
	
	@RequestMapping(value = LeaveHistoryController.LIST_URL, method = RequestMethod.GET) 
	public ListResponse<HistoryTaskResponse> listLeaveHistory(HttpServletRequest req){
	  
		String authority = authority(req);
		ListResponse<HistoryTaskResponse> response = null;
		String paramKey 	= req.getParameter("paramKey");
		String paramValue 	= req.getParameter("paramValue");
		String paramValue2 	= req.getParameter("paramValue2");
		String column 	= req.getParameter("column");
		String orderBy		= req.getParameter("orderBy");
		String page			= getPage(req);
		SortDynamicRequest sortRequest = setModelSortDynamic(orderBy, column);
	  
		try { 
		  	  response = connector.historyLeave(page, authority, getUsernameSession(req), paramKey, paramValue, paramValue2, sortRequest,  INDONESIAN);
		  	  return response;
		  } catch (Exception e) { 
			  e.printStackTrace();
			  return response;
		  } 
	  }
	
	 @RequestMapping(value = LeaveController.CHECK_BLOCK_LEAVE_URL, method = RequestMethod.GET)
	 public boolean checkLeave(HttpServletRequest request) {
		 String authority = authority(request);
		 boolean response = false;
		 try {
			 BlockLeaveResponse model = connector.checkBlockLeave(authority, getUsernameSession(request), INDONESIAN);
			 response = model.isFlag();
			return response;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return response;
		}
	 }
}
