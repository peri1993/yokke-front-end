package com.yokke.web;

import javax.servlet.http.HttpServletRequest;

import com.yokke.utils.CookieUtiles;

// after first register, there can be multi entry points, eg. homepage / highlights page
public interface AwareCookieBasedCampaign {

	String COOKIE_PROMO_CODE = "promoCode";
	
	String COOKIE_CASHTREE_CODE = "cashTreeClickId";

	default String getPromoCodeFromCookie(HttpServletRequest request) {
		return CookieUtiles.getCookieInfo(request, COOKIE_PROMO_CODE);
	}
	
	default String getCashTreeClickIdFromCookie(HttpServletRequest request) {
		return CookieUtiles.getCookieInfo(request, COOKIE_CASHTREE_CODE);
	}
	
}
