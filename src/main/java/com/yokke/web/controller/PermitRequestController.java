package com.yokke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.web.UrlsRegistry;

@Controller
public class PermitRequestController extends AuthController {

	private static final String PERMISSION_MODULE = "RequestPermit";
	public static final String MAIN_URL = Routes.PERMIT_REQUEST;
	public static final String LIST_URL = MAIN_URL + "/list";
	public static final String FIND_APPROVAL_URL = MAIN_URL + "/findApproval";
	public static final String FIND_ABSENCE_TYPE_URL = MAIN_URL + "/findAbsenceType";
	public static final String FIND_ABSENCE_TYPE_ID_URL = MAIN_URL + "/findAbsenceTypeId";
	public static final String FIND_REMAINING_QUOTA_URL = MAIN_URL + "/findRemainingQuota";
	public static final String FIND_REMAINING_QUOTA_URL_DTL = MAIN_URL + "/findRemainingQuotaDtl";
	public static final String CREATE_PERMIT_URL = MAIN_URL + "/createPermit";
	public static final String HOLIDAY_CURRENT_YEAR = MAIN_URL + "/getHolidayCurrentYear";
	public static final String GET_QUOTA_TAKEN = MAIN_URL + "/takenQuota";
	public static final String FIND_EMPLOYEE_DATA_URL = MAIN_URL + "/findEmployeeData";
	public static final String UPLOAD_FILE_URL = MAIN_URL + "/upload-file";
	public static final String UPLOAD_FILE_URL2 = MAIN_URL + "/upload-file2";
	public static final String PREVIEW_FILE_URL = MAIN_URL + "/preview-file";
	
	@Override
	protected String viewName() {
		return "cms/permit/request/permit-request-index";
	}

	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}

	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}

	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}

	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}

	public String getFIND_APPROVAL_URL() {
		return MAIN_URL + FIND_APPROVAL_URL;
	}

	public String getFIND_ABSENCE_TYPE_URL() {
		return MAIN_URL + FIND_ABSENCE_TYPE_URL;
	}

	public String getFIND_REMAINING_QUOTA_URL() {
		return MAIN_URL + FIND_REMAINING_QUOTA_URL;
	}

	public String getCREATE_LEAVE_URL() {
		return MAIN_URL + CREATE_PERMIT_URL;
	}

	public String getEMPLOYEE_DATA_URL() {
		return MAIN_URL + FIND_EMPLOYEE_DATA_URL;
	}

	public String getUPLOAD_FILE_URL() {
		return MAIN_URL + UPLOAD_FILE_URL;
	}
	
	public String getUPLOAD_FILE_URL2() {
		return MAIN_URL + UPLOAD_FILE_URL2;
	}
	
	public String getPREVIEW_FILE_URL() {
		return MAIN_URL + PREVIEW_FILE_URL;
	}
}
