package com.yokke.web.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.connector.AttendanceWSConnector;
import com.yokke.connector.response.FileExtentionResponse;
import com.yokke.web.UrlsRegistry;

@Controller
public class AttendanceHistoryController extends AuthController {
	
	private static final String PERMISSION_MODULE		= "AttendanceHistory";
	public static final String MAIN_URL 				= Routes.HISTORY_EMPLOYEE;
	public static final String LIST_URL					= MAIN_URL + "/list";
	public static final String DOWNLOAD_FILE_URL	= "/download-file";
	
	@Autowired
	private AttendanceWSConnector wsconnector;
	
	@Override
	protected String viewName() {
		return "cms/attendance/listattendance/detail-employee-history";
	}
	
	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}
	
	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + DOWNLOAD_FILE_URL, method = RequestMethod.GET)
	public ResponseEntity<byte[]> download(Model model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		String filename = null;
		String name = request.getParameter("filename");
		String directory = request.getParameter("directory");
		String status = request.getParameter("status");
		filename = name.concat("-").concat(status);
		try {
			
			Path pat = Paths.get(String.valueOf(directory + File.separator + filename));
			
			byte[] contents = Files.readAllBytes(pat);
			String[] extention = filename.split(".");
			
			FileExtentionResponse fileType = wsconnector.findExtention(authority(request), name, status, INDONESIAN);
					
			if ("png".equals(fileType.getFileType())) {
				if (extention.length >= 1) {
					filename = extention[0].concat(".png");
				}else {
					filename = filename.concat(".png");
				}
			} else {
				if (extention.length >= 1) {
					filename = extention[0].concat(".jpg");
				}else {
					filename = filename.concat(".jpg");
				}
			}
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.setContentType(MediaType.APPLICATION_JSON);
			responseHeaders.set("Content-Disposition", "attachment");
			responseHeaders.setContentDispositionFormData(filename, filename);
			responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			rsp = new ResponseEntity<byte[]>(contents, responseHeaders, HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rsp;
	}
	
	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}
	
	public String getDOWNLOAD_FILE_URL() {
		return MAIN_URL + DOWNLOAD_FILE_URL;
	}
}
