package com.yokke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.connector.NotificationWSConnector;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.CheckAssignerTaskResponse;
import com.yokke.web.UrlsRegistry;
import com.yokke.web.rest.controller.RestNotificationController;

@Controller
public class NotificationsController extends AuthController {

	private static final String PERMISSION_MODULE	= "Notification";
	public static final String MAIN_URL				= Routes.NOTIFICATIONS;
	public static final String LIST_USER_URL		= MAIN_URL + "/list-user";
	public static final String LIST_URL				= MAIN_URL + "/list";
	public static final String DETAIL_URL 			= "/detail";
	public static final String UPDATE_URL 			= MAIN_URL + "/update";
	
	@Override
	protected String viewName() {
		// TODO Auto-generated method stub
		return "cms/notifications/notifications-index";
	}
	
	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}
	
	@Autowired
	private RestNotificationController controller;
	
	@Autowired
	private NotificationWSConnector connector;
	
	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + DETAIL_URL, method = RequestMethod.GET)
	public String detail(Model model, HttpServletRequest request, HttpServletResponse response) {
		String instancesId = request.getParameter("instancesId");
		String process = request.getParameter("process");
		String status = request.getParameter("status");
		String id = request.getParameter("id");
		
		BaseResponse rsp = connector.updateNotification(id, getUsernameSession(request), instancesId, authority(request), INDONESIAN);
		
		if ("WAITING FOR APPROVAL".equals(status)) {
			if (null != instancesId) {
				request.getSession().setAttribute(INSTANCES_ID, instancesId);
				request.getSession().setAttribute("notif", true);
				if ("Perubahan Jam Masuk Kerja".equals(process)) {
					return dispatchResult("cms/inbox/task-detail-work-hours", permission(), request, INDONESIAN, model);
				}else {
					CheckAssignerTaskResponse res = controller.checkAssigner(instancesId, getUsernameSession(request),
							authority(request));

					if (res.isCheckAssignerFlag()) {
						return dispatchResult("cms/inbox/task-detail", permission(), request, INDONESIAN, model);
					}else {
						return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
					}
				}
			}
		}else {
			if (null != instancesId) {
				request.getSession().setAttribute(INSTANCES_ID, instancesId);
				request.getSession().setAttribute("notif", true);
				if ("Permit Bank Mega".equals(process)) {
					request.getSession().setAttribute("fileUploadFlag", true);
					request.getSession().setAttribute("module", Routes.PERMIT_HISTORY);
					return dispatchResult("cms/history/history-task-detail", permission(), request, INDONESIAN, model);
				}else if("Perubahan Jam Masuk Kerja".equals(process)) {
					request.getSession().setAttribute("fileUploadFlag", false);
					request.getSession().setAttribute("module", Routes.INFOR_WORK_HOURS);
					return dispatchResult("cms/inbox/task-detail-work-hours-inbox", permission(), request, INDONESIAN, model);
				}else {
					request.getSession().setAttribute("fileUploadFlag", false);
					request.getSession().setAttribute("module", Routes.HISTORY);
					return dispatchResult("cms/history/history-task-detail", permission(), request, INDONESIAN, model);
				}
			}
		}
		return null;
	}
	
	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}
	
	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}
	
	public String getLIST_USER_URL() {
		return MAIN_URL + LIST_USER_URL;
	}
	
	public String getDETAIL_URL() {
		return MAIN_URL + DETAIL_URL;
	}
	
	public String getUPDATE_URL() {
		return MAIN_URL + UPDATE_URL;
	}
	
}
