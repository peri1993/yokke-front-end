package com.yokke.web.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.connector.RoleUserWSConnector;
import com.yokke.connector.request.RoleRequest;
import com.yokke.connector.request.UserRoleRequest;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.MenuResponse;
import com.yokke.connector.response.ObjectResponse;
import com.yokke.connector.response.RoleResponse;
import com.yokke.connector.response.UserRoleResponse;
import com.yokke.connector.response.UsernameResponse;
import com.yokke.web.UrlsRegistry;

@Controller
public class UserManagementController extends AuthController {

	private static final String PERMISSION_MODULE = "UserManagement";
	public static final String MAIN_URL = Routes.USER_MANAGEMENT;
	private static final String ADD_LDAP_URL = MAIN_URL + "/add-ldap";
	private static final String ADD_USER_URL = MAIN_URL + "/add-user";
	public static final String LIST_USER = MAIN_URL + "/list-user";
	public static final String CREATED_USER = MAIN_URL + "/create-user";
	public static final String DELETE_USER = MAIN_URL + "/delete-user";
	public static final String SAVE_URL = "/save";
	public static final String UPDATE_URL = "/update";

	@Override
	protected String viewName() {
		return "cms/admin/usermanagement/user-management-index";
	}

	private String viewLdap() {
		return "cms/admin/usermanagement/user-management-add-ldap";
	}

	private String viewAdd() {
		return "cms/admin/usermanagement/user-management-add";
	}
	
	@Autowired
	private RoleUserWSConnector connector;

	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}

	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}

	@RequestMapping(value = MAIN_URL + ADD_LDAP_URL, method = RequestMethod.GET)
	public String addLdap(Model model, HttpServletRequest request, HttpServletResponse response) {
		String username = request.getParameter("username");
		request.setAttribute("data", null);
		try {
			if(username != null) {
				UsernameResponse resp = connector.findUsername(username, INDONESIAN, authority(request));
				if (resp.getUsername() != null) {
					request.setAttribute("data", resp);
					request.setAttribute("msg", null);
				} else {
					request.setAttribute("data", null);
					request.setAttribute("msg", resp.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dispatchResult(viewLdap(), permission(), request, INDONESIAN, model);
	}
	
	
	@RequestMapping(value = MAIN_URL + SAVE_URL, method = RequestMethod.POST)
	public String save(Model model, HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		String username = request.getParameter("username");
		String fullname = request.getParameter("fullname");
		String roleId = request.getParameter("roleId");
		try {
			BaseResponse resp = new BaseResponse();
			UserRoleRequest user = new UserRoleRequest();
			if(id != null) {
				user.setId(id);
				user.setUpdatedBy(getUsernameSession(request));
				user.setRoleId(roleId);
				resp = connector.updateUser(user, INDONESIAN, authority(request));
			}else {
				user.setUsername(username);
				user.setCreatedBy(getUsernameSession(request));
				user.setFullname(fullname);
				user.setRoleId(roleId);
				resp = connector.createUser(user, INDONESIAN, authority(request));
			}
			
			if(resp.getError() != null) {
				request.setAttribute("error", resp.getError());
				request.setAttribute("userId", id);
				if(id != null) {
					ObjectResponse<UserRoleResponse> userRole = connector.detailUser(id, INDONESIAN, authority(request));
					if (userRole.getObjectResponse() != null) {
						UserRoleResponse detail = setDetail(userRole);
						request.setAttribute("data", detail);
						listRole(request);
						return dispatchResult(viewAdd(), permission(), request, INDONESIAN, model);
					}	
				}
				this.viewAdd(model, request, response);
				return dispatchResult(viewAdd(), permission(), request, INDONESIAN, model);
			}else {
				this.view(model, request, response);
				return dispatchResult(request, INDONESIAN, model);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	@RequestMapping(value = MAIN_URL + ADD_USER_URL, method = RequestMethod.GET)
	public String viewAdd(Model model, HttpServletRequest request, HttpServletResponse response) {
		String userId = request.getParameter("userId");
		try {
			
			if(userId != null) {
				ObjectResponse<UserRoleResponse> user = connector.detailUser(userId, INDONESIAN, authority(request));
				if (user.getObjectResponse() != null) {
					UserRoleResponse detail = setDetail(user);
					request.setAttribute("data", detail);
				}		
				listRole(request);
				
			}else {
				String username = request.getParameter("username");
				String fullname = request.getParameter("fullname");
				UserRoleResponse user = new UserRoleResponse();
				user.setUsername(username);
				user.setFullname(fullname);
				request.setAttribute("data", user);
				listRole(request);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return dispatchResult(viewAdd(), permission(), request, INDONESIAN, model);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RoleResponse> listRole(HttpServletRequest request){
		ListResponse<RoleResponse> listRole = new ListResponse<RoleResponse>();
		List<RoleResponse> listRoleResponse = new ArrayList<RoleResponse>();
		
		RoleRequest roleRequest = new RoleRequest();
		listRole = connector.getAllRole(roleRequest, INDONESIAN, authority(request));
		
		try {
			String json = new ObjectMapper().writeValueAsString(listRole.getListResponse());
			List<RoleResponse> response = parseJsonList(json, RoleResponse.class);
			
			for (Iterator iterator = response.iterator(); iterator.hasNext();) {
				LinkedHashMap<String, String> roleMap =  (LinkedHashMap<String, String>) iterator.next();
				RoleResponse role = new RoleResponse();
				String roleId = String.valueOf(roleMap.get("role_id"));
				String listMenu = new ObjectMapper().writeValueAsString(roleMap.get("listMenu"));
				role.setRole_id(new Long(roleId));
				role.setRoleName(roleMap.get("roleName"));
				role.setListMenu(listMenu(listMenu));
				listRoleResponse.add(role);
				request.setAttribute("listRole", listRoleResponse);
			}
			
			return response;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public List<MenuResponse> listMenu(String listMenu){
		List<MenuResponse> menuResponse = new ArrayList<MenuResponse>();
		
		try {
			menuResponse = parseJsonList(listMenu, MenuResponse.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return menuResponse;
	}
	
	
	private UserRoleResponse setDetail(ObjectResponse<UserRoleResponse> object) {
		UserRoleResponse user = new UserRoleResponse();
		try {
			String json = new ObjectMapper().writeValueAsString(object.getObjectResponse());
			user = parseJson(json, UserRoleResponse.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return user;
	}

	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getADD_LDAP_URL() {
		return MAIN_URL + ADD_LDAP_URL;
	}

	public String getLIST_USER() {
		return MAIN_URL + LIST_USER;
	}

	public String getCREATED_USER() {
		return MAIN_URL + CREATED_USER;
	}

	public String getDELETE_USER() {
		return MAIN_URL + DELETE_USER;
	}

	public String getUPDATE_URL() {
		return MAIN_URL + UPDATE_URL;
	}

	public String getSAVE_URL() {
		return MAIN_URL + SAVE_URL;
	}
	
	public String getADD_USER_URL() {
		return MAIN_URL + ADD_USER_URL;
	}
}
