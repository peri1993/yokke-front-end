package com.yokke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.web.UrlsRegistry;

@Controller
public class HistoryAttendanceController extends AuthController{
	
	private static final String PERMISSION_MODULE			= "HistoryAttendance";
	public static final String MAIN_URL						= Routes.HISTORY_ATTENDANCE;
	public static final String LIST_URL						= MAIN_URL + "/list";
	public static final String PREVIEW_FILE_URL 			= MAIN_URL + "/preview-file";
	
	@Override
	protected String viewName() {
		return "cms/attendance/listattendance/detail-history-attendance";
	}
	
	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}
	
	private String permission() {
		return PERMISSION_MODULE;
	}
	
	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}
	
	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}
	
	public static String getPREVIEW_FILE_URL() {
		return MAIN_URL + PREVIEW_FILE_URL;
	}
}
