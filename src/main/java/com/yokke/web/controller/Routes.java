package com.yokke.web.controller;

import org.springframework.stereotype.Controller;

@Controller
public class Routes {
	public static final String LOGIN = "/login";
	public static final String DASHBOARD = "/dashboard";
	public static final String ERROR = "/invalid-token";
	
	public static final String PERMISSION = "/permission";
	public static final String ROLES = "/roles";

	public static final String INQUIRY = "/inquiryGCN/inquiry";
	//public static final String REJECTED = "/report/rejected";
	
	public static final String INBOX = "/inbox";
	public static final String TASK_DETAIL = "/task-detail";
	public static final String ASSIGNER_HISTORY = "/history-assigner";
	public static final String ALL_ASSIGNER_HISTORY = "/history-all";
	
	public static final String HISTORY = "/history";
	public static final String DETAIL_HISTORY = "/detail-history";
	
	public static final String LEAVE = "/leave";
	
	// added for menu permit
	public static final String PERMIT_REQUEST 		 = "/permit-request";
	public static final String PERMIT_INBOX 		 = "/permit-inbox";
	public static final String PERMIT_TASK 			 = "/permit-task";
	public static final String PERMIT_HISTORY 		 = "/permit-history";
	
	// added for notifications
	public static final String NOTIFICATIONS 		 = "/notifications";
	public static final String NOTIFICATIONS_DETAIL  = "/notifications";
	
	// added for Admin
	public static final String ROLE_MANAGEMENT 		 = "/role-management";
	public static final String USER_MANAGEMENT 		 = "/user-management";
	public static final String ASSIGNED_TASK   		 = "/assigned-task";
	public static final String BRANCH_CONF			 = "/branch-conf";
	
	// added for Attendance
	public static final String INPUT_ATTENDANCE	 	 = "/input-attendance";
	public static final String LIST_ATTENDANCE		 = "/list-attendance";
	public static final String QR_ATTENDANCE		 = "/qr-attendance";
	public static final String HISTORY_ATTENDANCE	 = "/history-attendance";
	public static final String HISTORY_EMPLOYEE		 = "/history-employee-attendance";
	public static final String INFOR_WORK_HOURS 	 = "/information-work-hours";
	
	public String getHISTORY_EMPLOYEE() {
		return HISTORY_EMPLOYEE;
	}

	public String getHISTORY_ATTENDANCE() {
		return HISTORY_ATTENDANCE;
	}

	public String getQR_ATTENDANCE() {
		return QR_ATTENDANCE;
	}

	public String getINPUT_ATTENDANCE() {
		return INPUT_ATTENDANCE;
	}
	
	public String getLIST_ATTENDANCE() {
		return LIST_ATTENDANCE;
	}
	
	public static String getLOGIN() {
		return LOGIN;
	}

	public String getDASHBOARD() {
		return DASHBOARD;
	}
	
	public String getPERMISSION() {
		return PERMISSION;
	}

	public String getROLES() {
		return ROLES;
	}

	public String getINQUIRY() {
		return INQUIRY;
	}
	
	public String getTASK_DETAIL() {
		return TASK_DETAIL;
	}
	
	public String getINBOX() {
		return INBOX;
	}
	
	public String getLEAVE() {
		return LEAVE;
	}
	
	public String getHISTORY() {
		return HISTORY;
	}
	
	public String getDETAIL_HISTORY() {
		return DETAIL_HISTORY;
	}

	public String getPERMIT_REQUEST() {
		return PERMIT_REQUEST;
	}
	
	public String getPERMIT_INBOX() {
		return PERMIT_INBOX;
	}
	
	public String getPERMIT_TASK() {
		return PERMIT_TASK;
	}
	
	public String getPERMIT_HISTORY() {
		return PERMIT_HISTORY;
	}
	
	public String getNOTIFICATIONS() {
		return NOTIFICATIONS;
	}
	
	public String getNOTIFICATIONS_DETAIL() {
		return NOTIFICATIONS_DETAIL;
	}
	
	public String getROLE_MANAGEMENT() {
		return ROLE_MANAGEMENT;
	}
	
	public String getUSER_MANAGEMENT() {
		return USER_MANAGEMENT;
	}

	public String getASSIGNED_TASK() {
		return ASSIGNED_TASK;
	}
	
	public String getBRANCH_CONF() {
		return BRANCH_CONF;
	}

	public String getASSIGNER_HISTORY() {
		return ASSIGNER_HISTORY;
	}
	
	public String getALL_ASSIGNER_HISTORY() {
		return ALL_ASSIGNER_HISTORY;
	}
	
	public String getINFOR_WORK_HOURS() {
		return INFOR_WORK_HOURS;
	}
	
}
