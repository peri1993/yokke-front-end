package com.yokke.web.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.web.UrlsRegistry;

@Controller
public class AssignerHistoryAllController extends AuthController{
	
	private static final String PERMISSION_MODULE 	= "AllAssignerHistory";
	public static final String MAIN_URL				= Routes.ALL_ASSIGNER_HISTORY;
	public static final String LIST_URL				= MAIN_URL + "/list";
	public static final String DETAIL_URL			= "/detail";
	public static final String DOWNLOAD_URL 		= "/download-file";
	
	@Override
	protected String viewName() {
		return "cms/admin/assignertask/assigner-history-alldata-index";
	}
	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}
	
	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + DETAIL_URL, method = RequestMethod.GET)
	public String detail(Model model, HttpServletRequest request, HttpServletResponse response) {
		String instancesId = request.getParameter("instanceId");
		String bussinessProcess = request.getParameter("businessProcess");
		
		if (null != instancesId) {
			request.getSession().setAttribute(INSTANCES_ID, instancesId);
		}
		if(bussinessProcess != null) {
			if("Izin".equals(bussinessProcess)) {
				request.getSession().setAttribute("bussinessProcess", "permit");
				return dispatchResult("cms/admin/assignertask/assigner-detail-allhistory", permission(), request, INDONESIAN, model);
			}else if("Cuti".equals(bussinessProcess)||"Cuti - Carry Over".equals(bussinessProcess)) {
				request.getSession().setAttribute("bussinessProcess", "leave");
				return dispatchResult("cms/admin/assignertask/assigner-detail-allhistory", permission(), request, INDONESIAN, model);
			}else {
				return dispatchResult("cms/inbox/task-detail-work-hours-inbox", permission(), request, INDONESIAN, model);
			}
		}
		return null;
	}
	
	@RequestMapping(value = MAIN_URL + DOWNLOAD_URL, method = RequestMethod.GET)
	public ResponseEntity<byte[]> download(Model model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		String filename = null;
		String name = request.getParameter("filename");
		String directory = request.getParameter("directory");
		String createdBy = request.getParameter("createdBy");
		filename = createdBy.concat("-").concat(name);
		try {
			// download file in temp folder
			Path pat = Paths.get(String.valueOf(directory + File.separator + filename));
			byte[] contents = Files.readAllBytes(pat);

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.setContentType(MediaType.APPLICATION_JSON);
			responseHeaders.set("Content-Disposition", "attachment");
			responseHeaders.setContentDispositionFormData(filename, filename);
			responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			rsp = new ResponseEntity<byte[]>(contents, responseHeaders, HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rsp;
	}
	
	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}
	
	public String getDETAIL_URL() {
		return MAIN_URL + DETAIL_URL;
	}
	
	public String getDOWNLOAD_URL() {
		return MAIN_URL + DOWNLOAD_URL;
	}
}
