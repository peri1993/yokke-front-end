package com.yokke.web.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.connector.RoleUserWSConnector;
import com.yokke.connector.request.MenuRequest;
import com.yokke.connector.request.RoleMenuRequest;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.MenuResponse;
import com.yokke.connector.response.ObjectResponse;
import com.yokke.connector.response.RoleResponse;
import com.yokke.constant.ConstantProperties;
import com.yokke.web.UrlsRegistry;

@Controller
public class RoleManagementController extends AuthController {

	private static final String PERMISSION_MODULE = "RoleManagement";
	public static final String MAIN_URL = Routes.ROLE_MANAGEMENT;
	public static final String ADD_GROUP = MAIN_URL + "/add-group";
	public static final String LIST_ROLE = MAIN_URL + "/list-role";
	public static final String CREATED_ROLE = MAIN_URL + "/create-role";
	public static final String DELETE_ROLE	= MAIN_URL + "/delete-role";
	public static final String SAVE_URL = "/save";
	public static final String UPDATE_URL = "/update";

	@Override
	protected String viewName() {
		return "cms/admin/rolemanagement/role-management-index";
	}

	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}
	
	@Autowired
	private RoleUserWSConnector connector;

	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}

	@RequestMapping(value = MAIN_URL + ADD_GROUP, method = RequestMethod.GET)
	public String add(Model model, HttpServletRequest request, HttpServletResponse response) {
		String roleId = request.getParameter("roleId");
		if(roleId != null) {
			String authority = authority(request);
			ObjectResponse<RoleResponse> objectResponse = connector.detailRole(roleId, INDONESIAN, authority);
			RoleResponse role = setDetailRole(objectResponse);
			request.setAttribute("nameRole", role.getRoleName());
			request.setAttribute("roleId", role.getRole_id());
			if(role.getListMenu()!= null) {
				for (int i = 0; i < role.getListMenu().size(); i++) {
					MenuResponse menu = role.getListMenu().get(i);
					request.setAttribute(ConstantProperties.setFlagMenu(menu.getName()), true);
				}				
			}
		}
		
		return dispatchResult("cms/admin/rolemanagement/role-management-add", permission(), request, INDONESIAN, model);
	}
	
	private RoleResponse setDetailRole(ObjectResponse<RoleResponse> request) {
		RoleResponse response = new RoleResponse();
		if(request.getObjectResponse() != null) {
			try {
				String json = new ObjectMapper().writeValueAsString(request.getObjectResponse());
				response = parseJson(json, RoleResponse.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return response;
	}
	
	@RequestMapping(value = MAIN_URL + SAVE_URL, method = RequestMethod.POST)
	public String save(Model model, HttpServletRequest request, HttpServletResponse response) {
		BaseResponse resp = null;
		try {
			String authority = authority(request);
			RoleMenuRequest role = setBody(request);
			resp = connector.createRole(role, INDONESIAN, authority);
			if(resp.getError() != null) {
				request.setAttribute("error", resp.getError());
				request.setAttribute("nameRole", role.getRoleName());
				if(role.getListMenu() != null) {
					for (int i = 0; i < role.getListMenu().size(); i++) {
						MenuRequest menu = role.getListMenu().get(i);
						request.setAttribute(ConstantProperties.setFlagMenu(menu.getName()), true);
					}
				}
				return dispatchResult("cms/admin/rolemanagement/role-management-add", permission(), request, INDONESIAN, model);
			}else {
				this.view(model, request, response);
				return dispatchResult(request, INDONESIAN, model);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = MAIN_URL + UPDATE_URL, method = RequestMethod.POST)
	public String update(Model model, HttpServletRequest request, HttpServletResponse response) {
		BaseResponse resp = null;
		try {
			String authority = authority(request);
			RoleMenuRequest role = setBodyUpdate(request);
			resp = connector.updateRole(role, INDONESIAN, authority);
			if(resp.getError() != null) {
				request.setAttribute("error", resp.getError());
				request.setAttribute("nameRole", role.getRoleName());
				if(role.getListMenu() != null) {
					for (int i = 0; i < role.getListMenu().size(); i++) {
						MenuRequest menu = role.getListMenu().get(i);
						request.setAttribute(ConstantProperties.setFlagMenu(menu.getName()), true);
					}
				}
				return dispatchResult("cms/admin/rolemanagement/role-management-add", permission(), request, INDONESIAN, model);
			
			}else {
				request.setAttribute("message", resp.getMessage());
				this.view(model, request, response);
				return dispatchResult(request, INDONESIAN, model);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	private RoleMenuRequest setBody(HttpServletRequest request) {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		RoleMenuRequest role = new RoleMenuRequest();
		List<MenuRequest> listMenu = new ArrayList<MenuRequest>();
		
		try {
			for (String name: request.getParameterMap().keySet()) {
				for (String value : request.getParameterValues(name)) {
					if("menu[]".equals(name)) {
						MenuRequest menu = new MenuRequest();
						menu.setName(ConstantProperties.getModuleCategoryNameByName(value));
						listMenu.add(menu);
					}
				}
			}
			role.setCreatedDt(now);
			role.setCreatedBy(getUsernameSession(request));
			role.setRoleName(request.getParameter("nameRole"));
			role.setListMenu(listMenu);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return role;
	}
	
	private RoleMenuRequest setBodyUpdate(HttpServletRequest request) {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		RoleMenuRequest role = new RoleMenuRequest();
		List<MenuRequest> listMenu = new ArrayList<MenuRequest>();
		
		try {
			for (String name: request.getParameterMap().keySet()) {
				for (String value : request.getParameterValues(name)) {
					if("menu[]".equals(name)) {
						MenuRequest menu = new MenuRequest();
						menu.setName(ConstantProperties.getModuleCategoryNameByName(value));
						listMenu.add(menu);
					}
				}
			}
			role.setRole_id(new Long(request.getParameter("roleId")));
			role.setUpdatedDt(now);
			role.setUpdatedBy(getUsernameSession(request));
			role.setRoleName(request.getParameter("nameRole"));
			role.setListMenu(listMenu);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return role;
	}
	
	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getADD_GROUP() {
		return MAIN_URL + ADD_GROUP;
	}

	public String getLIST_ROLE() {
		return MAIN_URL + LIST_ROLE;
	}

	public String getCREATED_ROLE() {
		return MAIN_URL + CREATED_ROLE;
	}

	public String getSAVE_URL() {
		return MAIN_URL + SAVE_URL;
	}

	public String getUPDATE_URL() {
		return MAIN_URL + UPDATE_URL;
	}

	public String getDELETE_ROLE() {
		return MAIN_URL + DELETE_ROLE;
	}

}
