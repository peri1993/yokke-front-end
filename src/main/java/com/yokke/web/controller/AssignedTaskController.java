package com.yokke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.web.UrlsRegistry;

@Controller
public class AssignedTaskController extends AuthController {

	private static final String PERMISSION_MODULE = "AssignedTask";
	public static final String MAIN_URL = Routes.ASSIGNED_TASK;
	public static final String LIST_URL = MAIN_URL + "/list-assign";
	public static final String DETAIL_ASSIGN_URL = MAIN_URL + "/detail";
	public static final String SEARCH_URL = MAIN_URL + "/search";
	public static final String UPDATE_URL = MAIN_URL + "/update";
	public static final String UPDATE_LIST_URL = MAIN_URL + "/update-list";
	public static final String DETAIL_URL = "/detail-assign";
	
	@Override
	protected String viewName() {
		return "cms/admin/assignertask/assigner-task-index";
	}
	
	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}
	
	private String permission() {
		return PERMISSION_MODULE;
	}
	
	private String viewDetail() {
		return "cms/admin/assignertask/detail-assigner";
	}
	
	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL+DETAIL_URL, method = RequestMethod.GET)
	public String detail(Model model, HttpServletRequest request, HttpServletResponse response) {
		String instancesId = request.getParameter("instanceId");
		if(instancesId != null) {
			request.setAttribute(INSTANCES_ID, instancesId);
		}
		return dispatchResult(viewDetail(), permission(), request, INDONESIAN, model);
	}
	
	public String getMAIN_URL() {
		return MAIN_URL;
	}
	
	public String getLIST_URL() {
		return LIST_URL;
	}
	
	public String getDETAIL_URL() {
		return MAIN_URL + DETAIL_URL;
	}
	
	public String getDETAIL_ASSIGN_URL() {
		return MAIN_URL + DETAIL_ASSIGN_URL;
	}
	
	public String getSEARCH_URL() {
		return MAIN_URL + SEARCH_URL;
	}
	
	public String getUPDATE_URL() {
		return MAIN_URL + UPDATE_URL;
	}
	
	public String getUPDATE_LIST_URL() {
		return MAIN_URL + UPDATE_LIST_URL;
	}

}
