package com.yokke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BranchConfController extends AuthController {

	public static final String MAIN_URL = Routes.BRANCH_CONF;
	private static final String PERMISSION_MODULE = "BranchConfig";
	public static final String UPDATE_URL = MAIN_URL + "/update";
	public static final String LIST_URL = MAIN_URL + "/list";

	@Override
	protected String viewName() {
		return "cms/admin/branchconf/branch-conf-index";
	}

	private String permission() {
		return PERMISSION_MODULE;
	}

	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}

	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}

	public String getUPDATE_URL() {
		return MAIN_URL + UPDATE_URL;
	}
}
