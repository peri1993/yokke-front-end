package com.yokke.web.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.LoginResponse;
import com.yokke.connector.response.OAuthResponse;
import com.yokke.connector.response.ObjectResponse;

@Controller
public abstract class AuthController extends BaseController {

	public static final String INSTANCES_ID = "instanceId";
	public static final SimpleDateFormat format_date = new SimpleDateFormat("yyyyMMdd");

	protected <T> T parseJson(String response, Class<T> clazz)
			throws IOException, JsonParseException, JsonMappingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(response, clazz);
	}

	protected <T> List<T> parseJsonList(String response, Class<T> clazz)
			throws IOException, JsonParseException, JsonMappingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(response, new TypeReference<List<T>>() {
		});
	}

	public boolean isLogin(HttpServletRequest request) {
		if (request.getSession().getAttribute(SESSION_USER) != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	protected String dispatchResult(String viewName, String module, HttpServletRequest request, Locale locale,
			Model model) {

		if (!"TaskDetail".equals(module)) {
			if (!checkPermission(request, module)) {
				return super.dispatchResult("error-not-authorize", module, request, locale, model);
			}
		}

		if (isLogin(request)) {
			return super.dispatchResult(viewName, module, request, locale, model);
		} else {
			request.getSession().setAttribute(TARGET_URL, getServletPath(request));
			return "redirect:" + LoginController.MAIN_URL;
		}
	}

	protected String invalidToken(String viewName, String module, HttpServletRequest request, Locale locale,
			Model model) {
		return super.dispatchResult("error-not-authorize", module, request, locale, model);
	}

	public static String getFullURL(HttpServletRequest request) {
		StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
		String queryString = request.getQueryString();
		if (queryString == null) {
			return requestURL.toString();
		} else {
			return requestURL.append('?').append(queryString).toString();
		}
	}

	public static String getServletPath(HttpServletRequest request) {
		StringBuilder pathinfo = new StringBuilder(request.getServletPath());
		String queryString = request.getQueryString();
		if (queryString == null) {
			return pathinfo.toString();
		} else {
			return pathinfo.append('?').append(queryString).toString();
		}
	}

	@Override
	protected String dispatchResult(HttpServletRequest request, Locale locale, Model model) {
		if (isLogin(request)) {
			return super.dispatchResult(request, locale, model);
		} else {
			return "redirect:" + LoginController.MAIN_URL;
		}
	}

	public LoginResponse getUserSession(HttpServletRequest request) {
		return (LoginResponse) request.getSession().getAttribute(SESSION_USER);
	}

	public String getUsernameSession(HttpServletRequest request) {
		String username = null;
		try {
			OAuthResponse response = (OAuthResponse) request.getSession().getAttribute(SESSION_USER);
			username = response.getUsername();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return username;
	}

	@SuppressWarnings({ "unchecked" })
	private Boolean checkPermission(HttpServletRequest request, String module) {
		Map<String, Boolean> mapPageModules = (Map<String, Boolean>) request.getSession()
				.getAttribute("mapsPageModules");
		if (mapPageModules == null) {
			return false;
		}
		Boolean isAllowAccess = mapPageModules.get(module);
		if (isAllowAccess == null) {
			return false;
		}
		return isAllowAccess;
	}

	@SuppressWarnings("rawtypes")
	public void checkResponseObject(ObjectResponse object, HttpServletRequest request) {
		if (object != null) {
			if (object.getError() != null || object.getErrorKey() != null) {
				request.setAttribute(ERROR_STATUS, object.getStatus());
				request.setAttribute(ERROR_MESSAGE, object.getMessage());
				request.getSession().setAttribute(TARGET_URL, null);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public void checkResponseList(ListResponse list, HttpServletRequest request) {
		if (list != null) {
			if (list.getError() != null || list.getErrorKey() != null) {
				request.setAttribute(ERROR_STATUS, list.getStatus());
				request.setAttribute(ERROR_MESSAGE, list.getMessage());
				request.getSession().setAttribute(TARGET_URL, null);
			}
		}
	}

	public List<NameValuePair> listParamInquiry(String paramCif, String paramGcn, String paramName,
			String parambirtDate, String paramMother, String paramktp, String paramNpwp, String paramNoHp,
			Long secPage) {
		List<NameValuePair> dataList = new ArrayList<>();

		if (paramCif != null && paramCif != STRING_EMPTY) {
			dataList.add(new BasicNameValuePair("idnParty", paramCif));
		}
		if (paramGcn != null && paramGcn != STRING_EMPTY) {
			dataList.add(new BasicNameValuePair("idnSvp", paramGcn));
		}
		if (paramName != null && paramName != STRING_EMPTY) {
			dataList.add(new BasicNameValuePair("customerName", paramName));
		}
		if (parambirtDate != null && parambirtDate != STRING_EMPTY) {
			dataList.add(new BasicNameValuePair("dteBirth", parambirtDate));
		}
		if (paramMother != null && paramMother != STRING_EMPTY) {
			dataList.add(new BasicNameValuePair("nmeMother", paramMother));
		}
		if (paramktp != null && paramktp != STRING_EMPTY) {
			dataList.add(new BasicNameValuePair("idnId", paramktp));
		}
		if (paramNpwp != null && paramNpwp != STRING_EMPTY) {
			dataList.add(new BasicNameValuePair("idnTax", paramNpwp));
		}

		if (paramNoHp != null && paramNoHp != STRING_EMPTY) {
			dataList.add(new BasicNameValuePair("phoneNumber", paramNoHp));
		}
		if (secPage != null && secPage != 0) {
			dataList.add(new BasicNameValuePair("page", String.valueOf(secPage)));
			dataList.add(new BasicNameValuePair("size", String.valueOf(PAGE_SIZE)));
		} else {
			dataList.add(new BasicNameValuePair("page", String.valueOf(0)));
			dataList.add(new BasicNameValuePair("size", String.valueOf(PAGE_SIZE)));
		}
		return dataList;
	}

	public String authority(HttpServletRequest req) {
		try {
			OAuthResponse authRespones = (OAuthResponse) req.getSession().getAttribute(SESSION_USER);
			return authRespones.getAccess_token();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void setDefaultSessionParameter(HttpServletRequest request) {
		request.getSession().setAttribute("todayParam", false);
		request.getSession().setAttribute("noPage", String.valueOf(0));
		request.getSession().setAttribute("param1", String.valueOf(0));
		request.getSession().setAttribute("param2", String.valueOf(0));
	}

	public void setDefaultSessionParameterToday(HttpServletRequest request) {
		request.getSession().setAttribute("todayParam", true);
		request.getSession().setAttribute("noPage", String.valueOf(0));
		request.getSession().setAttribute("param1", String.valueOf(0));
		request.getSession().setAttribute("param2", String.valueOf(0));
	}
	
	public String getPage(HttpServletRequest request) {
		String page = request.getParameter("secPage");
		if(page == null) {
			return "0";
		}else {
			return page;
		}
		
	}
}
