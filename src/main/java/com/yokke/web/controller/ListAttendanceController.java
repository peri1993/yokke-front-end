package com.yokke.web.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.connector.AttendanceWSConnector;
import com.yokke.connector.response.FileExtentionResponse;
import com.yokke.web.UrlsRegistry;

@Controller
public class ListAttendanceController extends AuthController{
	
	private static final String PERMISSION_MODULE 	= "ListAttendance";
	public static final String MAIN_URL 			= Routes.LIST_ATTENDANCE;
	public static final String LIST_URL				= MAIN_URL + "/list";
	public static final String LIST_SCHEDULER_URL 	= MAIN_URL + "/list-schedule";
	public static final String DETAIL_URL			= "/detail-attendance";
	public static final String CREATE_PAGE_URL		= "/create-attendance";
	public static final String UPDATE_URL			= "/update-attendance";
	public static final String CREATE_URL			= MAIN_URL + "/create";
	public static final String DELETE_URL			= MAIN_URL + "/delete";
	public static final String DOWNLOAD_FILE_URL	= "/download-file";
	
	@Autowired
	private AttendanceWSConnector wsconnector;
	
	@Override
	protected String viewName() {
		return "cms/attendance/listattendance/index-listdata-employee";
	}

	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}
	
	protected String viewDetail() {
		return "cms/attendance/listattendance/detail-history-attendance";
	}
	
	protected String viewCreate() {
		return "cms/attendance/listattendance/create-schedule-attendance";
	}
	
	protected String viewEdit() {
		return "cms/attendance/listattendance/edit-schedule-attendance";
	}
	
	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + DETAIL_URL, method = RequestMethod.GET)
	public String detail(Model model, HttpServletRequest request, HttpServletResponse response) {
		String username = request.getParameter("username");
		String fullname = request.getParameter("fullname");
		request.getSession().setAttribute("username", username);
		request.getSession().setAttribute("fullname", fullname);
		return dispatchResult(viewDetail(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + CREATE_PAGE_URL, method = RequestMethod.GET)
	public String create(Model model, HttpServletRequest request, HttpServletResponse response) {
		String username = request.getParameter("username");
		String fullname = request.getParameter("fullname");
		request.getSession().setAttribute("username", username);
		request.getSession().setAttribute("fullname", fullname);
		return dispatchResult(viewCreate(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + UPDATE_URL, method = RequestMethod.GET)
	public String update(Model model, HttpServletRequest request, HttpServletResponse response) {
		String username = request.getParameter("username");
		String fullname = request.getParameter("fullname");
		request.getSession().setAttribute("username", username);
		request.getSession().setAttribute("fullname", fullname);
		return dispatchResult(viewEdit(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + DOWNLOAD_FILE_URL, method = RequestMethod.GET)
	public ResponseEntity<byte[]> download(Model model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		String filename = null;
		String name = request.getParameter("filename");
		String directory = request.getParameter("directory");
		String status = request.getParameter("status");
		filename = name.concat("-").concat(status);
		try {
		
			Path pat = Paths.get(String.valueOf(directory + File.separator + filename));;
			
			byte[] contents = Files.readAllBytes(pat);
			String[] extention = filename.split(".");
			
			FileExtentionResponse fileType = wsconnector.findExtention(authority(request), name, status, INDONESIAN);
					
			if ("png".equals(fileType.getFileType())) {
				if (extention.length >= 1) {
					filename = extention[0].concat(".png");
				}else {
					filename = filename.concat(".png");
				}
			} else {
				if (extention.length >= 1) {
					filename = extention[0].concat(".jpg");
				}else {
					filename = filename.concat(".jpg");
				}
			}
			
			File fileRemove = new File(String.valueOf(pat));
			if (fileRemove.exists()) {
				fileRemove.delete();
			}
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.setContentType(MediaType.APPLICATION_JSON);
			responseHeaders.set("Content-Disposition", "attachment");
			responseHeaders.setContentDispositionFormData(filename, filename);
			responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			rsp = new ResponseEntity<byte[]>(contents, responseHeaders, HttpStatus.OK);


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rsp;
	}
	
	public String getDELETE_URL() {
		return MAIN_URL + DELETE_URL;
	}
	
	public String getUPDATE_URL() {
		return MAIN_URL + UPDATE_URL;
	}

	public String getCREATE_PAGE_URL() {
		return MAIN_URL + CREATE_PAGE_URL;
	}
	
	public String getCREATE_URL() {
		return MAIN_URL + CREATE_URL;
	}

	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}

	public String getDETAIL_URL() {
		return MAIN_URL + DETAIL_URL;
	}
	
	public String getLIST_SCHEDULER_URL() {
		return MAIN_URL + LIST_SCHEDULER_URL;
	}
	
	public String getDOWNLOAD_FILE_URL() {
		return MAIN_URL + DOWNLOAD_FILE_URL;
	}

}
