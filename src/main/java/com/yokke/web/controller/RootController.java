package com.yokke.web.controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.yokke.web.UrlsRegistry;

@Controller
public class RootController extends BaseController {
	
	@SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(RootController.class);

	static final String EN_URL = "/en";
	static final String IN_URL = "/";
	
	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), EN_URL, IN_URL);
	}

	@Override
	protected String viewName() {
		return "root";
	}
	
	@RequestMapping(value = { EN_URL, IN_URL }, method = RequestMethod.GET)
	public String homepage(Model model, HttpServletRequest request, HttpServletResponse response) 
									throws UnsupportedEncodingException, NoSuchAlgorithmException {
		
		return "redirect:" + DashboardController.MAIN_URL;
	}

}
