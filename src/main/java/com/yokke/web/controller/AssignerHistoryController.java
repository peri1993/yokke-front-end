package com.yokke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.web.UrlsRegistry;

@Controller
public class AssignerHistoryController extends AuthController {
	
	private static final String PERMISSION_MODULE 	= "AssignerHistory";
	public static final String MAIN_URL				= Routes.ASSIGNER_HISTORY;
	public static final String LIST_URL				= MAIN_URL + "/list";
	public static final String DETAIL_URL			= "/detail";
	public static final String DOWNLOAD_URL 		= "/download-file";
	
	@Override
	protected String viewName() {
		return "cms/admin/assignertask/assigner-history-index";
	}
	
	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}
	
	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + DETAIL_URL, method = RequestMethod.GET)
	public String detail(Model model, HttpServletRequest request, HttpServletResponse response) {
		String instanceId = request.getParameter("instanceId");
		String businessProcess = request.getParameter("businessProcess");
		
		if (null != instanceId) {
			request.getSession().setAttribute(INSTANCES_ID, instanceId);
		}
		if(businessProcess != null) {
			if("Izin".equals(businessProcess)) {
				request.getSession().setAttribute("bussinessProcess", "permit");
				return dispatchResult("cms/admin/assignertask/assigner-detail-allhistory", permission(), request, INDONESIAN, model);
			}else if("Cuti".equals(businessProcess)||"Cuti - Carry Over".equals(businessProcess)) {
				request.getSession().setAttribute("bussinessProcess", "leave");
				return dispatchResult("cms/admin/assignertask/assigner-detail-allhistory", permission(), request, INDONESIAN, model);
			}else {
				return dispatchResult("cms/inbox/task-detail-work-hours-inbox", permission(), request, INDONESIAN, model);
			}
		}
		return null;
	}

	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}

	public String getDETAIL_URL() {
		return MAIN_URL + DETAIL_URL;
	}
	
	public String getDOWNLOAD_URL() {
		return MAIN_URL + DOWNLOAD_URL;
	}
	
}
