package com.yokke.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.connector.LdapWsConnector;
import com.yokke.connector.LoginWSConnector;
import com.yokke.connector.request.LoginRequest;
import com.yokke.connector.response.LdapResponse;
import com.yokke.connector.response.MenuResponse;
import com.yokke.connector.response.OAuthResponse;
import com.yokke.web.UrlsRegistry;

@Controller
public class LoginController extends BaseController {

	public static final String MAIN_URL = Routes.LOGIN;
	private static final String SUBMIT_URL = "/submit";
	private static final String LOGOUT_URL = "/logout";
	private static final String LOGIN_PORTAL_URL = "/login-portal";

	@Autowired
	protected LoginWSConnector connector;

	@Autowired
	protected LdapWsConnector ldapConnector;

	private Map<String, Boolean> mapPageModules;

	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	@Override
	protected String viewName() {
		return "cms/login";
	}

	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String show(Model model, HttpServletRequest request, HttpServletResponse response) {
		if (request.getSession().getAttribute(LoginController.SESSION_USER) != null) {
			return "redirect:" + DashboardController.MAIN_URL;
		} else {
			return dispatchResult(request, INDONESIAN, model);
		}
	}

	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String showError(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult("error-not-authorize", "", request, INDONESIAN, model);
	}

	@RequestMapping(value = MAIN_URL + LOGIN_PORTAL_URL, method = RequestMethod.GET)
	public String loginPortal(Model model, HttpServletRequest request, HttpServletResponse response) {
		LoginRequest param = new LoginRequest();
		LoginController loginMenu = new LoginController();

		param.setUsername(request.getParameter("username2").toString());
		param.setUsername(String.valueOf(request.getParameter("userName1")));

		try {

			OAuthResponse resp = connector.login();
			if (resp == null) {
				model.addAttribute("errMsg", "Connection error");
				return dispatchResult(request, INDONESIAN, model);
			}
			if (resp.hasError()) {
				model.addAttribute("errMsg", resp.getError());
				request.getSession().setAttribute("message", resp.getError());
				request.getSession().setAttribute("description", resp.getError_description());
				return dispatchResult(request, INDONESIAN, model);
			}
			if (resp != null) {
				if (0 == resp.getStatus()) {

					LdapResponse ldapResponse = new LdapResponse();
					ldapResponse = connector.login_ldap_by_pass(param, INDONESIAN, resp.getAccess_token());
					if ("SUCCESS".equals(ldapResponse.getMessage()) && 0 == ldapResponse.getStatus()) {
						resp.setUsername(ldapResponse.getUsername());
						resp.setDisplayName(ldapResponse.getUsername());
						resp.setFullname(ldapResponse.getFullname());
						resp.setJobDesc(ldapResponse.getJobDesc());
						if (ldapResponse.getListMenuAccess() != null) {
							loginMenu.getMenuUserLogin(ldapResponse.getListMenuAccess(), request, response);
						}

						request.getSession().setAttribute(SESSION_USER, resp);
						request.getSession().setAttribute("message", null);
					} else {
						request.getSession().setAttribute("message", ldapResponse.getStatus());
						request.getSession().setAttribute("description", ldapResponse.getMessage());
						return "redirect:" + MAIN_URL;
					}

				}
			} else {
				request.getSession().setAttribute("message", true);
				request.getSession().setAttribute("description", "Connection refused to LDAP");
				return "redirect:" + MAIN_URL;
			}

		} catch (Exception e) {
			request.getSession().setAttribute("message", "Connection error !!!");
			model.addAttribute("errMsg", "Connection error");
			e.printStackTrace();
			return "redirect:" + MAIN_URL;
		}

		String targetUrl = (String) request.getSession().getAttribute(TARGET_URL);
		if (targetUrl != null) {
			request.getSession().setAttribute(TARGET_URL, null);
			return "redirect:" + targetUrl;
		} else {
			return "redirect:" + DashboardController.MAIN_URL;
		}

	}

	@SuppressWarnings("unused")
	@RequestMapping(value = MAIN_URL + SUBMIT_URL, method = RequestMethod.POST)
	public String login(Model model, HttpServletRequest request, HttpServletResponse response) {
		LoginRequest param = new LoginRequest();
		LoginController loginMenu = new LoginController();
		param.setUsername(request.getParameter("userName").toString());
		param.setPassword(request.getParameter("password").toString());
		String username = request.getParameter("userName");
		String password = request.getParameter("password");
		loginMenu.removeSessionMenu(request, response);
		try {

			/* LOGIN LDAP */

			/*
			 * OAuthResponse resp = connector.login(); if (resp == null) {
			 * model.addAttribute("errMsg", "Connection error"); return
			 * dispatchResult(request, INDONESIAN, model); } if (resp.hasError()) {
			 * model.addAttribute("errMsg", resp.getError());
			 * request.getSession().setAttribute("message", resp.getError());
			 * request.getSession().setAttribute("description",
			 * resp.getError_description()); return dispatchResult(request, INDONESIAN,
			 * model); } if (resp != null) { if (0 == resp.getStatus()) {
			 * 
			 * LdapResponse ldapResponse = new LdapResponse(); ldapResponse =
			 * connector.login_ldap(param, INDONESIAN, resp.getAccess_token()); if
			 * ("SUCCESS".equals(ldapResponse.getMessage()) && 0 ==
			 * ldapResponse.getStatus()) { resp.setUsername(ldapResponse.getUsername());
			 * resp.setDisplayName(ldapResponse.getUsername());
			 * resp.setFullname(ldapResponse.getFullname());
			 * resp.setJobDesc(ldapResponse.getJobDesc());
			 * 
			 * if (ldapResponse.getListMenuAccess() != null) {
			 * loginMenu.getMenuUserLogin(ldapResponse.getListMenuAccess(), request,
			 * response); }
			 * 
			 * request.getSession().setAttribute(SESSION_USER, resp);
			 * request.getSession().setAttribute("message", null); } else {
			 * request.getSession().setAttribute("message", ldapResponse.getStatus());
			 * request.getSession().setAttribute("description", ldapResponse.getMessage());
			 * return "redirect:" + MAIN_URL; }
			 * 
			 * } } else { request.getSession().setAttribute("message", true);
			 * request.getSession().setAttribute("description",
			 * "Connection refused to LDAP"); return "redirect:" + MAIN_URL; }
			 */
			/* LOGIN BY PASS LDAP */

			OAuthResponse resp = connector.login();
			if (resp == null) {
				model.addAttribute("errMsg", "Connection error");
				return dispatchResult(request, INDONESIAN, model);
			}
			if (resp.hasError()) {
				model.addAttribute("errMsg", resp.getError());
				request.getSession().setAttribute("message", resp.getError());
				request.getSession().setAttribute("description", resp.getError_description());
				return dispatchResult(request, INDONESIAN, model);
			}
			if (resp != null) {
				if (0 == resp.getStatus()) {

					LdapResponse ldapResponse = new LdapResponse();
					ldapResponse = connector.login_ldap_by_pass(param, INDONESIAN, resp.getAccess_token());
					if ("SUCCESS".equals(ldapResponse.getMessage()) && 0 == ldapResponse.getStatus()) {
						resp.setUsername(ldapResponse.getUsername());
						resp.setDisplayName(ldapResponse.getUsername());
						resp.setFullname(ldapResponse.getFullname());
						resp.setJobDesc(ldapResponse.getJobDesc());
						if (ldapResponse.getListMenuAccess() != null) {
							loginMenu.getMenuUserLogin(ldapResponse.getListMenuAccess(), request, response);
						}

						request.getSession().setAttribute(SESSION_USER, resp);
						request.getSession().setAttribute("message", null);
					} else {
						request.getSession().setAttribute("message", ldapResponse.getStatus());
						request.getSession().setAttribute("description", ldapResponse.getMessage());
						return "redirect:" + MAIN_URL;
					}
				}
			} else {
				request.getSession().setAttribute("message", true);
				request.getSession().setAttribute("description", "Connection refused to LDAP");
				return "redirect:" + MAIN_URL;
			}

		} catch (Exception e) {
			request.getSession().setAttribute("message", "Connection error !!!");
			model.addAttribute("errMsg", "Connection error");
			e.printStackTrace();
			return "redirect:" + MAIN_URL;
		}

		String targetUrl = (String) request.getSession().getAttribute(TARGET_URL);
		if (targetUrl != null) {
			request.getSession().setAttribute(TARGET_URL, null);
			return "redirect:" + targetUrl;
		} else {
			return "redirect:" + DashboardController.MAIN_URL;
		}
	}

	@RequestMapping(value = MAIN_URL + LOGOUT_URL, method = RequestMethod.GET)
	public String logout(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return "redirect:" + MAIN_URL;
	}

	public void getMenuUserLogin(List<MenuResponse> modules, HttpServletRequest request, HttpServletResponse response) {
		mapPageModules = new HashMap<>();

		for (int j = 0; j < modules.size(); j++) {
			MenuResponse menu = modules.get(j);
			String menuSession = menu.getName().replaceAll("[^A-Za-z0-9]", "");
			request.getSession().setAttribute(menuSession, String.valueOf(true));
			mapPageModules.put(menuSession, true);
			request.getSession().setAttribute("mapsPageModules", mapPageModules);
		}
	}

	public void removeSessionMenu(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute("Inbox");
		request.getSession().removeAttribute("RequestLeave");
		request.getSession().removeAttribute("HistoryLeave");
		request.getSession().removeAttribute("RequestPermit");
		request.getSession().removeAttribute("HistoryPermit");
		request.getSession().removeAttribute("AssignedTask");
		request.getSession().removeAttribute("Notification");
		request.getSession().removeAttribute("RoleManagement");
		request.getSession().removeAttribute("UserManagement");
		request.getSession().removeAttribute("AssignerHistory");
		request.getSession().removeAttribute("BranchConfig");
		request.getSession().removeAttribute("AllAssignerHistory");
		request.getSession().removeAttribute("InformationWorkHours");
	}

	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getSUBMIT_URL() {
		return MAIN_URL + SUBMIT_URL;
	}

	public String getLOGOUT_URL() {
		return MAIN_URL + LOGOUT_URL;
	}

	public String getLOGIN_PORTAL_URL() {
		return MAIN_URL + LOGIN_PORTAL_URL;
	}
}
