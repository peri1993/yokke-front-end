package com.yokke.web.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.web.UrlsRegistry;

@Controller
public class DashboardController extends AuthController {
	
	@SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(DashboardController.class);
	
	public static final String MAIN_URL = Routes.DASHBOARD;
	public static final String DOWNLOAD_URL = "/download-apk";
	
	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}
	
	@Override
	protected String viewName() {
		return "cms/dashboard/dash-index";
	}
	
	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String show(Model model, HttpServletRequest request) {
		return dispatchResult(request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + DOWNLOAD_URL, method = RequestMethod.GET)
	public ResponseEntity<byte[]> download(Model model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		try {
			// download file in temp folder
			Path pat = Paths.get(String.valueOf(path_apk + File.separator + apk_name));
			byte[] contents = Files.readAllBytes(pat);

			HttpHeaders responseHeaders = new HttpHeaders();
			//responseHeaders.setContentType(MediaType.parseMediaType(type));
			responseHeaders.set("Content-Disposition", "attachment");
			responseHeaders.setContentDispositionFormData(apk_name, apk_name);
			responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			rsp = new ResponseEntity<byte[]>(contents, responseHeaders, HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsp;
	}
	
	public String getDOWNLOAD_URL() {
        return MAIN_URL+DOWNLOAD_URL;
    }

}
