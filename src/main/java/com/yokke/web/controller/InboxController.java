package com.yokke.web.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.connector.response.CheckAssignerTaskResponse;
import com.yokke.web.UrlsRegistry;
import com.yokke.web.rest.controller.RestNotificationController;

@Controller
public class InboxController extends AuthController {
	private static final String PERMISSION_MODULE = "Inbox";
	public static final String MAIN_URL = Routes.INBOX;
	public static final String DETAIL_URL = "/detail";
	public static final String DETAIL_TASK_URL = MAIN_URL + "/task-detail";
	public static final String DETAIL_TASK_WORK_HOURS_URL = MAIN_URL + "/task-detail-work-hours";
	public static final String LIST_URL = MAIN_URL + "/list";
	public static final String APPROVE_URL = MAIN_URL + "/approve";
	public static final String REJECT_URL = MAIN_URL + "/reject";
	public static final String APPROVE_LIST_URL = MAIN_URL + "/approve-task/list";
	public static final String REJECT_LIST_URL = MAIN_URL + "/reject-task/list";
	public static final String CEK_ACTIVE_DATA_URL = MAIN_URL + "/cek-active-data";
	public static final String UPDATE_DATA_URL = MAIN_URL + "/update-data";
	public static final String DOWNLOAD_URL = "/download-file";
	public static final String HISTORY_TASK_LOGS = MAIN_URL + "/history-task-logs";

	@Override
	protected String viewName() {
		return "cms/inbox/inbox-index";
	}

	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}

	private String detailChangeWorkHours() {
		return "cms/inbox/task-detail-work-hours";
	}

	private String permissionChangeWorkHours() {
		return "InformationWorkHours";
	}

	@Autowired
	private RestNotificationController controller;

	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(INSTANCES_ID);
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}

	@RequestMapping(value = MAIN_URL + DETAIL_URL, method = RequestMethod.GET)
	public String detail(Model model, HttpServletRequest request, HttpServletResponse response) {
		String instanceId = request.getParameter("instanceId");
		String businessProcess = request.getParameter("businessProcess");

		if (null != instanceId) {
			request.getSession().setAttribute(INSTANCES_ID, instanceId);
		}
		if (businessProcess != null) {
			if ("Izin".equals(businessProcess)) {
				request.getSession().setAttribute("businessProcess", "permit");
			} else if ("Perubahan Jam Masuk Kerja".equals(businessProcess)) {
				request.getSession().setAttribute("businessProcess", "changeWorkHours");
				return dispatchResult(detailChangeWorkHours(), permissionChangeWorkHours(), request, INDONESIAN, model);
			} else {
				request.getSession().setAttribute("businessProcess", "leave");
			}
		}

		CheckAssignerTaskResponse res = controller.checkAssigner(instanceId, getUsernameSession(request),
				authority(request));

		if (res.isCheckAssignerFlag()) {
			return dispatchResult("cms/inbox/task-detail", permission(), request, INDONESIAN, model);
		}else {
			return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
		}
		
	}

	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}

	@RequestMapping(value = MAIN_URL + DOWNLOAD_URL, method = RequestMethod.GET)
	public ResponseEntity<byte[]> download(Model model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		String filename = null;
		String name = request.getParameter("filename");
		String directory = request.getParameter("directory");
		String createdBy = request.getParameter("createdBy");
		filename = createdBy.concat("-").concat(name);
		try {
			// download file in temp folder
			Path pat = Paths.get(String.valueOf(directory + File.separator + filename));
			byte[] contents = Files.readAllBytes(pat);

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.setContentType(MediaType.APPLICATION_JSON);
			responseHeaders.set("Content-Disposition", "attachment");
			responseHeaders.setContentDispositionFormData(filename, filename);
			responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			rsp = new ResponseEntity<byte[]>(contents, responseHeaders, HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rsp;
	}

	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}

	public String getDETAIL_URL() {
		return MAIN_URL + DETAIL_URL;
	}

	public String getDETAIL_TASK_URL() {
		return MAIN_URL + DETAIL_TASK_URL;
	}

	public String getAPPROVE_LIST_URL() {
		return APPROVE_LIST_URL;
	}

	public String getAPPROVE_URL() {
		return MAIN_URL + APPROVE_URL;
	}

	public String getREJECT_URL() {
		return MAIN_URL + REJECT_URL;
	}

	public String getCEK_ACTIVE_DATA_URL() {
		return MAIN_URL + CEK_ACTIVE_DATA_URL;
	}

	public String getUPDATE_DATA_URL() {
		return MAIN_URL + UPDATE_DATA_URL;
	}

	public String getDOWNLOAD_URL() {
		return MAIN_URL + DOWNLOAD_URL;
	}

	public String getHISTORY_TASK_LOGS() {
		return MAIN_URL + HISTORY_TASK_LOGS;
	}

	public String getDETAIL_TASK_WORK_HOURS_URL() {
		return MAIN_URL + DETAIL_TASK_WORK_HOURS_URL;
	}
}
