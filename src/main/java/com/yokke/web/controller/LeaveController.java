package com.yokke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.web.UrlsRegistry;

@Controller
public class LeaveController extends AuthController {
	private static final String PERMISSION_MODULE 		= "RequestLeave";
	public static final String MAIN_URL 				= Routes.LEAVE;
	public static final String LIST_URL 				= MAIN_URL + "/list";
	public static final String FIND_APPROVAL_URL		= MAIN_URL + "/findApproval";
	public static final String FIND_ABSENCE_TYPE_URL	= MAIN_URL + "/findAbsenceType";
	public static final String FIND_ABSENCE_TYPE_ALL_URL= MAIN_URL + "/findAbsenceType-all";
	public static final String FIND_ABSENCE_TYPE_ID_URL	= MAIN_URL + "/findAbsenceTypeId";
	public static final String FIND_REMAINING_QUOTA_URL	= MAIN_URL + "/findRemainingQuota";
	public static final String FIND_REMAINING_QUOTA_BY_REQUESTER_URL= MAIN_URL + "/findRemainingQuotaByRequester";
	public static final String FIND_REMAINING_QUOTA_URL_DTL	= MAIN_URL + "/findRemainingQuotaDtl";
	public static final String FIND_LAST_REMAINING		= MAIN_URL + "/findLastRemaining";
	public static final String FIND_LAST_REMAINING_CARRY = MAIN_URL + "/findLastRemainingCarryOver"; 
	public static final String CREATE_LEAVE_URL			= MAIN_URL + "/createLeave";
	public static final String HOLIDAY_CURRENT_YEAR		= MAIN_URL + "/getHolidayCurrentYear";
    public static final String GET_QUOTA_TAKEN          = MAIN_URL + "/takenQuota";
    public static final String FIND_EMPLOYEE_DATA_URL 	= MAIN_URL +"/findEmployeeData";
    public static final String CHECK_BLOCK_LEAVE_URL 	= MAIN_URL + "/checkBlockLeave";
    
	@Override
	protected String viewName() {
		return "cms/leave/leave-index";
	}

	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}

	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}

	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}

	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}
	
	public String getFIND_APPROVAL_URL() {
		return MAIN_URL + FIND_APPROVAL_URL;
	}
	
	public String getFIND_ABSENCE_TYPE_URL() {
		return MAIN_URL + FIND_ABSENCE_TYPE_URL;
	}
	
	public String getFIND_REMAINING_QUOTA_URL() {
		return MAIN_URL + FIND_REMAINING_QUOTA_URL;
	}
	
	public String getCREATE_LEAVE_URL() {
		return MAIN_URL + CREATE_LEAVE_URL;
	}
	
	public String getEMPLOYEE_DATA_URL() {
		return MAIN_URL + FIND_EMPLOYEE_DATA_URL;
	}
	
	public String getCHECK_BLOCK_LEAVE_URL() {
		return MAIN_URL + CHECK_BLOCK_LEAVE_URL;
	}
	
	public String getFIND_ABSENCE_TYPE_ALL_URL() {
		return MAIN_URL + FIND_ABSENCE_TYPE_ALL_URL;
	}
	
	public String getFIND_REMAINING_QUOTA_BY_REQUESTER_URL() {
		return MAIN_URL + FIND_REMAINING_QUOTA_BY_REQUESTER_URL;
	}

	public static String getFIND_LAST_REMAINING() {
		return FIND_LAST_REMAINING;
	}

	public static String getFIND_LAST_REMAINING_CARRY() {
		return FIND_LAST_REMAINING_CARRY;
	}
	
	
}
