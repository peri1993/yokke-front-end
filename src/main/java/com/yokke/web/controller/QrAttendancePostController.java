package com.yokke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.web.UrlsRegistry;

@Controller
public class QrAttendancePostController extends AuthController {

	private static final String PERMISSION_MODULE = "QrAttendance";
	public static final String MAIN_URL = Routes.QR_ATTENDANCE;
	public static final String LIST_URL = MAIN_URL + "/list";
	public static final String LOCATION_URL = MAIN_URL + "/location";
	public static final String CREATE_URL = MAIN_URL + "/create-attendance";
	public static final String FLAG_SCHEDULE_URL = MAIN_URL + "/flag-schedule";

	@Override
	protected String viewName() {
		return "cms/attendance/scannerQr/index-scanner-Qr";
	}

	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}

	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}

	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}

	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getCREATE_URL() {
		return MAIN_URL + CREATE_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}

	public String getLOCATION_URL() {
		return MAIN_URL + LOCATION_URL;
	}
	
	public String getFLAG_SCHEDULE_URL() {
		return MAIN_URL + FLAG_SCHEDULE_URL;
	}
}
