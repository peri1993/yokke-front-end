package com.yokke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.web.UrlsRegistry;

@Controller
public class PermitHistoryController extends AuthController{

	private static final String PERMISSION_MODULE 		= "HistoryPermit";
	public static final String MAIN_URL 				= Routes.PERMIT_HISTORY;
	public static final String LIST_URL 				= MAIN_URL + "/list";
	public static final String UPLOAD_FILE 				= MAIN_URL + "/upload";
	public static final String DETAIL_HISTORY_TASK_URL 	= "/detail";
	
	@Override
	protected String viewName() {
		return "cms/permit/history/permit-history-index";
	}
	
	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}
	
	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}
	
	@RequestMapping(value = MAIN_URL + "/invalid-token", method = RequestMethod.GET)
	public String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute(SESSION_USER);
		return invalidToken("error-not-authorize", null, request, INDONESIAN, model);
	}

	@RequestMapping(value = MAIN_URL + DETAIL_HISTORY_TASK_URL, method = RequestMethod.GET)
	public String detail(Model model, HttpServletRequest request, HttpServletResponse response) {
		String instanceId = request.getParameter("instanceId");
		if (null != instanceId) {
			request.getSession().setAttribute(INSTANCES_ID, instanceId);
			request.getSession().setAttribute("fileUploadFlag", true);
			request.getSession().setAttribute("module", MAIN_URL);
		}
		return dispatchResult("cms/history/history-task-detail", permission(), request, INDONESIAN, model);
	}
	
	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getLIST_URL() {
		return MAIN_URL + LIST_URL;
	}
	
	public String getUPLOAD_FILE() {
		return MAIN_URL + UPLOAD_FILE;
	}

	public String getDETAIL_HISTORY_TASK_URL() {
		return MAIN_URL + DETAIL_HISTORY_TASK_URL;
	}
	

}
