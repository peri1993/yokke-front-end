package com.yokke.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yokke.web.UrlsRegistry;

@Controller
public class InformationWorkHoursController extends AuthController {

	private static final String PERMISSION_MODULE = "InformationWorkHours";
	public static final String MAIN_URL = Routes.INFOR_WORK_HOURS;
	public static final String DETAIL_HOURS_WORK = MAIN_URL + "/detail";
	public static final String TASK_DETAIL_HOURS_WORK = MAIN_URL + "/task-detail";
	public static final String LIST_HOURS_WORK = MAIN_URL + "/list";
	public static final String REQUEST_CHANGE_WORK_HOURS = MAIN_URL + "/request-work-hours";

	@Override
	protected String viewName() {
		return "cms/attendance/informworkhours/information-work-hours-index";
	}

	@Override
	protected void registerEntryUrls(UrlsRegistry registry) {
		registry.put(viewName(), MAIN_URL, MAIN_URL);
	}

	private String permission() {
		return PERMISSION_MODULE;
	}

	@RequestMapping(value = MAIN_URL, method = RequestMethod.GET)
	public String view(Model model, HttpServletRequest request, HttpServletResponse response) {
		return dispatchResult(viewName(), permission(), request, INDONESIAN, model);
	}

	public String getMAIN_URL() {
		return MAIN_URL;
	}

	public String getDETAIL_HOURS_WORK() {
		return MAIN_URL + DETAIL_HOURS_WORK;
	}

	public String getLIST_HOURS_WORK() {
		return MAIN_URL + LIST_HOURS_WORK;
	}

	public String getTASK_DETAIL_HOURS_WORK() {
		return MAIN_URL + TASK_DETAIL_HOURS_WORK;
	}
	
	public String getREQUEST_CHANGE_WORK_HOURS() {
		return MAIN_URL + REQUEST_CHANGE_WORK_HOURS;
	}

}
