package com.yokke.web.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.yokke.connector.BaseWsConnector;
import com.yokke.connector.request.FilterlistRequest;
import com.yokke.connector.request.SortDynamicRequest;
import com.yokke.web.AwareCookieBasedCampaign;
import com.yokke.web.AwareProductCodes;
import com.yokke.web.AwareRequestAttributes;
import com.yokke.web.AwareSessionAttributes;
import com.yokke.web.AwareSupportedLocales;
import com.yokke.web.RequestLocaleResolver;
import com.yokke.web.UrlsRegistry;

@Controller
public abstract class BaseController implements MessageSourceAware, AwareRequestAttributes, AwareSessionAttributes,
		AwareSupportedLocales, AwareProductCodes, AwareCookieBasedCampaign, InitializingBean {

	public static final String SESSION_USER = "_user";
	public static final String PERMISSION_USER = "_permission";
	public static final String TARGET_URL = "_target_url";
	
	public static final String ERROR_STATUS = "errorStatus";
	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String STRING_EMPTY = "";
	public static final String ZERO_STRING = "0";
	public static final String PAGE_SIZE = "10";
	public static final String PAGE_SIZE_2 = "20";
	public static final String patternDate = "yyyy-MM-dd";
	public static final String INVALID_TOKEN = "invalid_token";
	public static final DateFormat simpleDateFormat = new SimpleDateFormat(patternDate);
	public static final SimpleDateFormat format2 = new SimpleDateFormat("yyyyMMdd");
	public static final SimpleDateFormat format_date = new SimpleDateFormat("dd/MM/yyyy");
	
	protected static final String LANG_SWITCH = "langSwitch";
	protected static final String ERROR_DEAULT = "Some fields are mandatory";
	private static ZipOutputStream zout;
	
	private static final Logger logger = LoggerFactory.getLogger(BaseWsConnector.class);

	@Autowired
	protected MessageSource messageSource;
	
	@Autowired
	protected UrlsRegistry urlsRegistry;

	@Value("#{prop['allow.web.search']}")
	protected boolean allowWebSearch;

	@Value("#{prop['cookie.time']}")
	protected int cookieTime;
	
	@Value("#{prop['path.dockument']}")
	public String path_document;

	@Value("#{prop['path.apk']}")
	public String path_apk;
	
	@Value("#{prop['apk.name']}")
	public String apk_name;
	
	@Value("#{prop['path.temp']}")
	public String temp;
	
	@ModelAttribute("allowWebSearch")
	public boolean allowWebSearch() {
		return this.allowWebSearch;
	}

	//added new by bima, properties : for processing format date
	protected String parseDate(String oldDate, String oldFormat, String newFormat) throws ParseException {
		String newDate = null;
		if(oldFormat != null && newFormat != null && oldDate !=null) {
			SimpleDateFormat sdf = new SimpleDateFormat(oldFormat);
			Date date = sdf.parse(oldDate);
			sdf.applyPattern(newFormat);
			newDate = sdf.format(date);
		}
		return newDate;
	}
	
	protected abstract String viewName();
	//protected abstract String invalidToken(Model model, HttpServletRequest request, HttpServletResponse response);

	@Override
	// maybe autowire is enough but MessageSourceAware requires this
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	protected void ajaxReturn(HttpServletResponse response, Object object) {
		response.setContentType("text/json;charset=utf-8");
		// return data
		ObjectMapper mapper = new ObjectMapper();
		try {
			final String objStr = mapper.writeValueAsString(object);
			response.getWriter().print(objStr);
		} catch (Exception e) {
			logger.error(ExceptionUtils.getStackTrace(e));
		}
	}

	protected static Locale getLocaleFromPath(String enPath, HttpServletRequest request) {
		return request.getRequestURI().toLowerCase().contains(enPath) ? ENGLISH : INDONESIAN;
	}

	protected static Locale getLocaleFromParam(HttpServletRequest request) {
		final String lang = request.getParameter("lang");
		return ENGLISH.getLanguage().equalsIgnoreCase(lang) ? ENGLISH : INDONESIAN;
	}

	protected static String formatDate(String date) {
		if (!StringUtils.isBlank(date)) {
			// incoming request has format dd/mm/yyyy
			String[] arr = date.split("/");
			if (arr.length == 3) {
				// format to yyyy-mm-dd
				return verifyDateRules(date, arr);
			} else {
				return null;
			}
		}
		return null;
	}

	protected static Date stringToDate(String strDate) {
		try {
			if (!StringUtils.isBlank(strDate)) {
				Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(strDate);
				return date1;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// add by jason
	protected static String verifyDateRules(String date, String[] arr) {
		String dateStr = arr[2] + "-" + arr[1] + "-" + arr[0];
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			dateFormat.setLenient(false);
			dateFormat.parse(dateStr);
		} catch (Exception e) {
			return null;
		}
		return dateStr;

	}

	protected static String proposalKey(String product) {
		return PROPOSAL + "_" + product;
	}

	protected static String quoteKey(String product) {
		return QUOTE + "_" + product;
	}

	protected void registerEntryUrls(UrlsRegistry registry) {
		// for controllers to register urls, making it easy to get urls from jsp
	}

	protected String dispatchResult(String viewName,String auth, HttpServletRequest request, Locale locale, Model model) {
		model.addAttribute("urlsRegistry", urlsRegistry);
		if (!model.containsAttribute(LANG_SWITCH)) {
			setupLangSwitch(model, viewName(), locale);
		}
		RequestLocaleResolver.setRequestLocale(request, locale);
		return viewName;
	}
	
	protected String dispatchResult(HttpServletRequest request, Locale locale, Model model) {
		model.addAttribute("urlsRegistry", urlsRegistry);
		if (!model.containsAttribute(LANG_SWITCH)) {
			setupLangSwitch(model, viewName(), locale);
		}
		RequestLocaleResolver.setRequestLocale(request, locale);
		return viewName();
	}

	protected void setupLangSwitch(Model model, String pageId, Locale locale) {
		Locale langSwitchLocale = (locale == ENGLISH) ? INDONESIAN : ENGLISH;
		model.addAttribute(LANG_SWITCH, urlsRegistry.get(pageId, langSwitchLocale));
	}

	protected boolean isWithinDuration(String duration) {
		if (!StringUtils.isBlank(duration)) {
			final String[] arr = duration.split("-");
			final int from = Integer.parseInt(arr[0]);
			final int to = Integer.parseInt(arr[1]);
			final DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			final int now = Integer.parseInt(dateFormat.format(new Date()));
			return (from <= now && now <= to);
		}
		return false;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		registerEntryUrls(urlsRegistry);
	}

	public FilterlistRequest setFilter(HttpServletRequest req) {
		FilterlistRequest filter = new FilterlistRequest();
		if (req.getParameter("sortType") != null) {
			filter.setSortType(req.getParameter("sortType").toString());
		}
		if (req.getParameter("sortBy") != null) {
			filter.setSortBy(req.getParameter("sortBy").toString());
		}
		if (req.getParameter("page") != null) {
			filter.setPage(req.getParameter("page").toString());
		}
		if (req.getParameter("pageSize") != null) {
			filter.setPageSize(req.getParameter("pageSize").toString());
		}
		if (req.getParameter("qlue") != null) {
			filter.setQlue(req.getParameter("qlue").toString());
		}
		if (req.getParameter("column") != null) {
			filter.setColumn(req.getParameter("column").toString());
		}

		return filter;
	}
	
	public FilterlistRequest setDefaultFilter(String keyColumn) {
		FilterlistRequest filter = new FilterlistRequest();
		filter.setSortType("ASC");
		filter.setSortBy(keyColumn);
		filter.setPage("1");
		filter.setPageSize("100");
		filter.setQlue("");
		filter.setColumn("");
		return filter;
	}
	
	/*
	 * public String parseStringToDateFormat(String date) { Date dt = null; try { dt
	 * = format_date.parse(date); } catch (ParseException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } return
	 * format_date.format(dt); }
	 */
	
	public String getDate() {
		String formatDt = null;
		try {
			Date date = Calendar.getInstance().getTime();
			formatDt = format2.format(date);
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		return formatDt;
	}
	
	public String parseDate(String strDate) {
		String formateDateStr = STRING_EMPTY;
		try {
			if (strDate != null && strDate != STRING_EMPTY) {
				String formateYear = String.valueOf(strDate.subSequence(0, 4));
				String formateMonth = String.valueOf(strDate.subSequence(4, 6));
				String formateDate = String.valueOf(strDate.subSequence(6, 8));
				formateDateStr = formateYear.concat("-").concat(formateMonth).concat("-").concat(formateDate);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return formateDateStr;
	}

	public String parseDate2(String strDate) {
		String formateDateStr = STRING_EMPTY;
		try {
			String formateYear = String.valueOf(strDate.subSequence(4, 8));
			String formateMonth = String.valueOf(strDate.subSequence(2, 4));
			String formateDate = String.valueOf(strDate.subSequence(0, 2));

			if (strDate != null && strDate != STRING_EMPTY) {
				formateDateStr = formateYear.concat("-").concat(formateMonth).concat("-").concat(formateDate);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return formateDateStr;
	}
	
	public SortDynamicRequest setModelSortDynamic(String flagSort, String sortColumn) {
		SortDynamicRequest resp = new SortDynamicRequest();
		resp.setFlagSort(Boolean.valueOf(flagSort));
		resp.setColumn(sortColumn);
		return resp;
	}
	
	public String concatStartDate(String strDate) {
		String formateDateStr = STRING_EMPTY;
		try {
			if (strDate != null && strDate != STRING_EMPTY) {
				String formateYear = String.valueOf(strDate.subSequence(0, 4));
				String formateMonth = String.valueOf(strDate.subSequence(4, 6));
				String formateDate = String.valueOf("01");
				formateDateStr = formateYear.concat(formateMonth).concat(formateDate);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return formateDateStr;
	}

	public String concatEndDate(String strDate) {
		String formateDateStr = STRING_EMPTY;
		try {
			if (strDate != null && strDate != STRING_EMPTY) {
				String formateYear = String.valueOf(strDate.subSequence(0, 4));
				String formateMonth = String.valueOf(strDate.subSequence(4, 6));
				String formateDate = String.valueOf("31");
				formateDateStr = formateYear.concat(formateMonth).concat(formateDate);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return formateDateStr;
	}
	
	public static void unzip(String zipFile, String unziploc) {
		try (ZipInputStream zin = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)))) {

			ZipEntry ze = null;
			while ((ze = zin.getNextEntry()) != null) {

				File file = new File(ze.getName());
				File root = file.getParentFile();
				if (!root.exists())
					root.mkdirs();
				file.createNewFile();
				BufferedOutputStream buffout = new BufferedOutputStream(
						new FileOutputStream(ze.getName()));
				byte[] buffer = new byte[1024];
				int count = -1;
				while ((count = zin.read(buffer)) != -1) {
					buffout.write(buffer, 0, count);
				}
				buffout.close();
			}

			System.out.println("Contents extracted to " + (new File(unziploc)).getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	public void zip(String path, File files, String filename) throws IOException, FileNotFoundException {

		zout = new ZipOutputStream(
				new BufferedOutputStream(new FileOutputStream(path + files.separator + filename.concat(".zip"))));
		zout.setLevel(Deflater.BEST_COMPRESSION);

		File file = new File(path + files.separator + filename);
		if (!file.exists()) {
			System.out.println("File " + file.getAbsolutePath() + " not found ");
			System.out.println("Aborted.");
			return;
		}
		ZipEntry ze = new ZipEntry(path + files.separator + filename);
		zout.putNextEntry(ze);

		BufferedInputStream buffin = new BufferedInputStream(new FileInputStream(file));

		byte[] buffer = new byte[1024];
		int count = -1;
		while ((count = buffin.read(buffer)) != -1) {
			zout.write(buffer, 0, count);
		}
		buffin.close();

		zout.closeEntry();
		zout.close();
	}
	
}
