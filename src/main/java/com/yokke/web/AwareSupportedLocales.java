package com.yokke.web;

import java.util.Locale;

public interface AwareSupportedLocales {

	static final Locale ENGLISH = Locale.ENGLISH;
	static final Locale INDONESIAN = new Locale("in", "ID");
	
}
