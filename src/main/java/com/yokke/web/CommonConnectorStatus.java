package com.yokke.web;

public interface CommonConnectorStatus {
	String HTTP_SUCCESS = "000";
	String HTTP_INTERNAL_SERVER_ERROR	= "001";
	String HTTP_INCOMPLETE_PARAM	= "002";
}
