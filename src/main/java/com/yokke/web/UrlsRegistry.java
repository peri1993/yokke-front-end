package com.yokke.web;

import java.util.Locale;

import org.springframework.stereotype.Component;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

@Component
public class UrlsRegistry implements AwareSupportedLocales{

	private Table<String,String,String> table = HashBasedTable.create();
	
	public void put(String pageId, String enUrl, String inUrl){
		table.put(pageId, ENGLISH.getLanguage(), enUrl);
		table.put(pageId, INDONESIAN.getLanguage(), inUrl);
	}
	
	public String get(String pageId, Locale locale){
		return get(pageId, locale.getLanguage());
	}
	
	public String get(String pageId, String lang){
		return table.get(pageId, lang);
	}
}
