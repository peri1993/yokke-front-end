package com.yokke.connector;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.yokke.connector.request.RoleMenuRequest;
import com.yokke.connector.request.RoleRequest;
import com.yokke.connector.request.UserRoleRequest;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.ObjectResponse;
import com.yokke.connector.response.RoleResponse;
import com.yokke.connector.response.UserRoleResponse;
import com.yokke.connector.response.UsernameResponse;

@Component
public class RoleUserWSConnector extends BaseWsConnector {

	@Value("#{prop['minova.url']}")
	private String ws_minova;

	@Value("#{prop['ws.url']}")
	private String ws_ess;

	private static final String FIND_ALL_ROLE = "/all-role";
	private static final String CREATE_ROLE = "/create-role";
	private static final String DELETE_ROLE = "/delete-role";
	private static final String DETAIL_ROLE = "/detail-role";
	private static final String UPDATE_ROLE = "/update-role";
	private static final String LIST_USER_URL = "/list-user";
	private static final String DELETE_USER_URL = "/delete-user/";
	private static final String DETAIL_USER = "/detail-user/";
	private static final String FIND_USERNAME = "/find-username";
	private static final String CREATE_USER_URL = "/create-user";
	private static final String UPDATE_USER_URL = "/update-user";

	@SuppressWarnings("unchecked")
	public ListResponse<RoleResponse> getAllRole(RoleRequest request, Locale locale, String authority) {
		String url = ws_ess + FIND_ALL_ROLE;
		return consumeWs(url, HttpMethod.GET, null, null, ListResponse.class, locale, authority);
	}

	public BaseResponse createRole(RoleMenuRequest request, Locale locale, String authority) {
		String url = ws_ess + CREATE_ROLE;
		return consumeWs(url, HttpMethod.POST, request, null, BaseResponse.class, locale, authority);
	}

	public BaseResponse updateRole(RoleMenuRequest request, Locale locale, String authority) {
		String url = ws_ess + UPDATE_ROLE;
		return consumeWs(url, HttpMethod.POST, request, null, BaseResponse.class, locale, authority);
	}

	public BaseResponse deleteRole(String roleId, Locale locale, String authority) {
		String url = ws_ess + DELETE_ROLE;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("roleId", roleId));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, BaseResponse.class, locale, authority);
	}

	public BaseResponse deleteUser(String userId, Locale locale, String authority) {
		String url = ws_ess + DELETE_USER_URL + userId;
		return consumeWs(url, HttpMethod.GET, null, null, BaseResponse.class, locale, authority);
	}

	@SuppressWarnings("unchecked")
	public ObjectResponse<RoleResponse> detailRole(String roleId, Locale locale, String authority) {
		String url = ws_ess + DETAIL_ROLE;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("roleId", roleId));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ObjectResponse.class, locale, authority);
	}

	@SuppressWarnings("unchecked")
	public ObjectResponse<UserRoleResponse> detailUser(String id, Locale locale, String authority) {
		String url = ws_ess + DETAIL_USER + id;
		return consumeWs(url, HttpMethod.GET, null, null, ObjectResponse.class, locale, authority);
	}

	@SuppressWarnings("unchecked")
	public ListResponse<UserRoleResponse> getAllUser(String page, String paramKey, String paramValue,
			String paramValue2, Locale locale, String authority) {
		String url = ws_ess + LIST_USER_URL;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam = pagging(page);
		if (!"".equals(paramKey)) {
			if (!"createdDate".equals(paramKey) && !"updatedDate".equals(paramKey)) {
				formDataParam.add(new BasicNameValuePair(paramKey, paramValue));
			} else {
				formDataParam = setFormDataDate(paramKey, paramValue, paramValue2);
			}
		}
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}

	private List<NameValuePair> setFormDataDate(String key, String val, String val2) {
		List<NameValuePair> formDate = new ArrayList<>();

		if ("createdDate".equals(key)) {
			formDate.add(new BasicNameValuePair("createdStartDate", val));
			formDate.add(new BasicNameValuePair("createdEndDate", val2));
		} else {
			formDate.add(new BasicNameValuePair("updatedStartDate", val));
			formDate.add(new BasicNameValuePair("updatedEndDate", val2));
		}
		return formDate;
	}

	public UsernameResponse findUsername(String username, Locale locale, String authority) {
		String url = ws_minova + FIND_USERNAME;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("username", username));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, UsernameResponse.class, locale, authority);
	}

	public BaseResponse createUser(UserRoleRequest request, Locale locale, String authority) {
		String url = ws_ess + CREATE_USER_URL;
		return consumeWs(url, HttpMethod.POST, request, null, BaseResponse.class, locale, authority);
	}

	public BaseResponse updateUser(UserRoleRequest request, Locale locale, String authority) {
		String url = ws_ess + UPDATE_USER_URL;
		return consumeWs(url, HttpMethod.POST, request, null, BaseResponse.class, locale, authority);
	}

}
