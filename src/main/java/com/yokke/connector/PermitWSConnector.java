package com.yokke.connector;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.yokke.connector.request.PermitRequest;
import com.yokke.connector.request.PreviewFileRequest;
import com.yokke.connector.request.SortDynamicRequest;
import com.yokke.connector.request.TaskApprovalRequest;
import com.yokke.connector.request.UploadFileRequest;
import com.yokke.connector.response.AbsenceTypeResponse;
import com.yokke.connector.response.ApprovalResponse;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.HistoryTaskResponse;
import com.yokke.connector.response.HolidayInCurrentYear;
import com.yokke.connector.response.LeaveResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.PreviewFileResponse;
import com.yokke.connector.response.QuotaLeaveResponse;
import com.yokke.connector.response.RemainingQuotaResponse;
import com.yokke.connector.response.TaskDetailResponse;
import com.yokke.connector.response.TaskLogsResponse;
import com.yokke.connector.response.TaskResponse;
import com.yokke.connector.response.UploadResponse;

@Component
public class PermitWSConnector extends BaseWsConnector{

	@Value("#{prop['minova.url']}")
	private String ws_minova;

	@Value("#{prop['ws.url']}")
	private String ws_ess;

	private static final String FIND_APPROVAL_NAME 		= "/request/approval/"; 
	private static final String FIND_ABSENCE_TYPE 		= "/leaveType-permit";
	private static final String HOLIDAY_WS       		= "/getHolidayCurrentYear";
	private static final String FIND_REMAINING_QUOTA 	= "/remainingQuota/";
	private static final String CREATE_PERMIT 			= "/create-permit";
	private static final String TASK_OWNER				= "/task-owner";
	private static final String DETAIL_TASK 			= "/task-detail";
	private static final String APPROVE_TASK 			= "/approve-task";
	private static final String REJECT_TASK 			= "/reject-task";
	private static final String HOLIDAY_BETWEEN 		= "/getHolidayBetween";
	private static final String CEK_TASK_ACTIVE			= "/cek-data-active";
	private static final String UPLOAD_FILE_2 			= "/upload-m-file2";
	private static final String LEAVE_HISTORY_TASK 		= "/history-task/permit";
	private static final String HISTORY_TASK_LOGS 		= "/detail-history-task/";
	private static final String PREVIEW_FILE			= "/download-files";
	
	public QuotaLeaveResponse countLeaveLeft(String startDate, String endDate, Locale locale, String authority) {
		String url = ws_minova + HOLIDAY_BETWEEN + "/"+startDate+"/"+endDate;
		return consumeWs(url, HttpMethod.GET, null, null, QuotaLeaveResponse.class, locale, authority);
	}
	
	public ApprovalResponse findAllApproval(String authority, String username, Locale locale){
		String url = ws_minova+FIND_APPROVAL_NAME+username;
		return consumeWs(url, HttpMethod.GET, null, null, ApprovalResponse.class, locale, authority);
	}
	
	public List<AbsenceTypeResponse> findAllAbsenceType(String authority, Locale locale){
		String url = ws_minova+FIND_ABSENCE_TYPE;
		return consumeWsList(url, HttpMethod.GET, null, null, AbsenceTypeResponse.class, locale, authority);
	}
	
	public HolidayInCurrentYear findAllHoliday(String authority, Locale locale){
		String url = ws_minova+HOLIDAY_WS;
		return consumeWs(url, HttpMethod.GET, null, null, HolidayInCurrentYear.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<RemainingQuotaResponse> findRemainingQuota(String authority, String username, Locale locale) {
		String url = ws_minova+FIND_REMAINING_QUOTA+username;
		return consumeWs(url, HttpMethod.GET, null, null, ListResponse.class, locale, authority);
	}
	
	public LeaveResponse createLeave(String authority, PermitRequest permitRequest, Locale locale) {
		String url = ws_ess+CREATE_PERMIT;
		return consumeWs(url, HttpMethod.POST, permitRequest, null, LeaveResponse.class, locale, authority);
	}
	
	public List<TaskResponse> findTaskOwner(String authority, String username, Locale locale){
		String url = ws_ess+TASK_OWNER;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("username", username));
		return consumeWsList(url, HttpMethod.GET, null, formDataParam, TaskResponse.class, locale, authority);
	}
	
	public TaskDetailResponse detailTask(String authority, String instanceId, Locale locale) {
		String url = ws_ess+DETAIL_TASK;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("instancesId", instanceId));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, TaskDetailResponse.class, locale, authority);
	}
	
	public BaseResponse approve(String authority, TaskApprovalRequest body, Locale locale) {
		String url = ws_ess + APPROVE_TASK;
		return consumeWs(url, HttpMethod.PUT, body, null, BaseResponse.class, locale, authority);
	}
	
	public BaseResponse reject(String authority, TaskApprovalRequest body, Locale locale) {
		String url = ws_ess + REJECT_TASK;
		return consumeWs(url, HttpMethod.PUT, body, null, BaseResponse.class, locale, authority);
	}
	
	public BaseResponse cekDataActive(String authority, String instanceId, Locale locale) {
		String url = ws_ess + CEK_TASK_ACTIVE;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("instancesId", instanceId));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, BaseResponse.class, locale, authority);
	}
	
	/*
	 * public LeaveResponse updateDataMinova(String authority, LeaveRequest request,
	 * Locale locale) { String url = ws_minova + UPDATE_LEAVE; return consumeWs(url,
	 * HttpMethod.POST, request, null, LeaveResponse.class, locale, authority); }
	 */
	
	public UploadResponse uploadFiles(String authority, UploadFileRequest body, Locale locale) {
		String url = ws_ess + UPLOAD_FILE_2;
		return consumeWs(url, HttpMethod.POST, body, null, UploadResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<HistoryTaskResponse> historyPermit(String page, String authority, String username, String paramKey, String paramValue, String paramValue2, SortDynamicRequest sortRequest, Locale locale ) {
		String url = ws_ess + LEAVE_HISTORY_TASK;
		List<NameValuePair> formDataParam = new ArrayList<>();
		if (paramKey != null && !"".equals(paramKey)) {
			if("startDt".equals(paramKey) || "endDt".equals(paramKey)) {
				formDataParam = paggingWithFilterByDate(page, paramKey, paramValue, paramValue2);
			}else {
				formDataParam.add(new BasicNameValuePair(paramKey, paramValue));
			}
			formDataParam = paggingWithFilter(page, formDataParam);
		}else {
			formDataParam = pagging(page);
		}
		
		if (null != sortRequest) {
			if (null != sortRequest.getColumn() && !"".equals(sortRequest.getColumn())) {
				formDataParam = sortDynamic(sortRequest, formDataParam);
			}
		}
		
		formDataParam.add(new BasicNameValuePair("username", username));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<TaskLogsResponse> historyTaskLogs(String authority, String instancesId, Locale locale ) {
		String url = ws_ess + HISTORY_TASK_LOGS + instancesId;
		return consumeWs(url, HttpMethod.GET, null, null, ListResponse.class, locale, authority);
	}
	
	public PreviewFileResponse previewFile(String authority, PreviewFileRequest request, Locale locale){
		String url = ws_ess+PREVIEW_FILE;
		return consumeWs(url, HttpMethod.POST, request, null, PreviewFileResponse.class, locale, authority);
	}
}
