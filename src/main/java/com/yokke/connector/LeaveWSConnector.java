
package com.yokke.connector;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Component;

import com.yokke.connector.request.LeaveRequest;
import com.yokke.connector.request.SortDynamicRequest;
import com.yokke.connector.request.TaskApprovalRequest;
import com.yokke.connector.response.AbsenceTypeResponse;
import com.yokke.connector.response.ApprovalResponse;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.BlockLeaveResponse;
import com.yokke.connector.response.DataDateResponse;
import com.yokke.connector.response.HistoryTaskResponse;
import com.yokke.connector.response.HolidayInCurrentYear;
import com.yokke.connector.response.LastRemainingResponse;
import com.yokke.connector.response.LeaveResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.QuotaLeaveResponse;
import com.yokke.connector.response.RemainingQuotaResponse;
import com.yokke.connector.response.TaskDetailResponse;
import com.yokke.connector.response.TaskLogsResponse;
import com.yokke.connector.response.TaskResponse;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpMethod;

@Component
public class LeaveWSConnector extends BaseWsConnector{

	private static final String FIND_APPROVAL_NAME 		= "/request/approval/"; 
	private static final String FIND_ABSENCE_TYPE 		= "/leaveType-leave";
	private static final String FIND_ABSENCE_TYPE_ALL 	= "/leaveType-all";
	private static final String HOLIDAY_WS       		= "/getHolidayCurrentYear";
	private static final String FIND_REMAINING_QUOTA 	= "/remainingQuota/";
	private static final String CREATE_LEAVE 			= "/create-leave";
	private static final String LIST_INBOX				= "/list-inbox";
	private static final String DETAIL_TASK 			= "/task-detail";
	private static final String APPROVE_TASK 			= "/approve-task";
	private static final String APPROVE_TASK_LIST		= "/approve-task-list";
	private static final String REJECT_TASK 			= "/reject-task";
	private static final String REJECT_TASK_LIST		= "/reject-task-list";
	private static final String HOLIDAY_BETWEEN 		= "/getHolidayBetween";
	private static final String CEK_TASK_ACTIVE			= "/cek-data-active";
	private static final String CHECK_BLOCK_LEAVE		= "/history/check-block-leave/";
	private static final String LEAVE_HISTORY_TASK 		= "/history-task/leave";
	private static final String HISTORY_TASK_LOGS 		= "/detail-history-task/";
	private static final String FIND_LAST_REMAINING		= "/find-last-remaining/";
	private static final String FIND_LAST_REMAINING_CARRY = "/find-last-remaining-carry-over/";
	private static final String FIND_DATA_DATE_LIST 	= "/find-list-date/";
	
	public QuotaLeaveResponse countLeaveLeft(String startDate, String endDate, Locale locale, String authority) {
		String url = ws_minova + HOLIDAY_BETWEEN + "/"+startDate+"/"+endDate;
		return consumeWs(url, HttpMethod.GET, null, null, QuotaLeaveResponse.class, locale, authority);
	}
	
	public ApprovalResponse findAllApproval(String authority, String username, Locale locale){
		String url = ws_minova+FIND_APPROVAL_NAME+username;
		return consumeWs(url, HttpMethod.GET, null, null, ApprovalResponse.class, locale, authority);
	}
	
	public List<AbsenceTypeResponse> findAllAbsenceType(String authority, Locale locale){
		String url = ws_minova+FIND_ABSENCE_TYPE;
		return consumeWsList(url, HttpMethod.GET, null, null, AbsenceTypeResponse.class, locale, authority);
	}
	
	public List<AbsenceTypeResponse> findAllAbsence(String authority, Locale locale){
		String url = ws_minova+FIND_ABSENCE_TYPE_ALL;
		return consumeWsList(url, HttpMethod.GET, null, null, AbsenceTypeResponse.class, locale, authority);
	}
	
	
	public HolidayInCurrentYear findAllHoliday(String authority, Locale locale){
		String url = ws_minova+HOLIDAY_WS;
		return consumeWs(url, HttpMethod.GET, null, null, HolidayInCurrentYear.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<RemainingQuotaResponse> findRemainingQuota(String authority, String username, Locale locale) {
		String url = ws_minova+FIND_REMAINING_QUOTA+username;
		return consumeWs(url, HttpMethod.GET, null, null, ListResponse.class, locale, authority);
	}
	
	public LeaveResponse createLeave(String authority, LeaveRequest leaveRequest, Locale locale) {
		String url = ws_ess+CREATE_LEAVE;
		return consumeWs(url, HttpMethod.POST, leaveRequest, null, LeaveResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<TaskResponse> findInboxTask(String page, String authority, String username,String paramKey, String paramValue, String paramValue2, SortDynamicRequest sortRequest, Locale locale){
		String url = ws_ess+LIST_INBOX;
		List<NameValuePair> formDataParam = new ArrayList<>();
		if (paramKey != null && !"".equals(paramKey)) {
			if("startDt".equals(paramKey) || "endDt".equals(paramKey)) {
				formDataParam = paggingWithFilterByDate(page, paramKey, paramValue, paramValue2);
			}else {
				formDataParam.add(new BasicNameValuePair(paramKey, paramValue));
			}
			formDataParam = paggingWithFilter(page, formDataParam);
		}else {
			formDataParam = pagging(page);
		}
		
		if (null != sortRequest) {
			if (null != sortRequest.getColumn() && !"".equals(sortRequest.getColumn())) {
				formDataParam = sortDynamic(sortRequest, formDataParam);
			}
		}

		formDataParam.add(new BasicNameValuePair("username", username));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}
	
	public TaskDetailResponse detailTask(String authority, String instanceId, String username, Locale locale) {
		String url = ws_ess+DETAIL_TASK;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("instancesId", instanceId));
		formDataParam.add(new BasicNameValuePair("username", username));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, TaskDetailResponse.class, locale, authority);
	}
	
	public BaseResponse approve(String authority, TaskApprovalRequest body, Locale locale) {
		String url = ws_ess + APPROVE_TASK;
		return consumeWs(url, HttpMethod.PUT, body, null, BaseResponse.class, locale, authority);
	}
	
	public BaseResponse approveList(String authority, List<TaskApprovalRequest> body, Locale locale) {
		String url = ws_ess + APPROVE_TASK_LIST;
		return consumeWs(url, HttpMethod.PUT, body, null, BaseResponse.class, locale, authority);
	}
	
	public BaseResponse reject(String authority, TaskApprovalRequest body, Locale locale) {
		String url = ws_ess + REJECT_TASK;
		return consumeWs(url, HttpMethod.PUT, body, null, BaseResponse.class, locale, authority);
	}
	
	public BaseResponse rejectList(String authority, List<TaskApprovalRequest> body, Locale locale) {
		String url = ws_ess + REJECT_TASK_LIST;
		return consumeWs(url, HttpMethod.PUT, body, null, BaseResponse.class, locale, authority);
	}
	
	public BaseResponse cekDataActive(String authority, String instanceId, Locale locale) {
		String url = ws_ess + CEK_TASK_ACTIVE;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("instancesId", instanceId));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, BaseResponse.class, locale, authority);
	}
	
	/*
	 * public LeaveResponse updateDataMinova(String authority, LeaveRequest request,
	 * Locale locale) { String url = ws_minova + UPDATE_LEAVE; return consumeWs(url,
	 * HttpMethod.POST, request, null, LeaveResponse.class, locale, authority); }
	 */
	
	@SuppressWarnings("unchecked")
	public ListResponse<HistoryTaskResponse> historyLeave(String page, String authority, String username,String paramKey, String paramValue, String paramValue2, SortDynamicRequest sortRequest, Locale locale) {

		String url = ws_ess + LEAVE_HISTORY_TASK;
		List<NameValuePair> formDataParam = new ArrayList<>();
		if (paramKey != null && !"".equals(paramKey)) {
			if("startDt".equals(paramKey) || "endDt".equals(paramKey)) {
				formDataParam = paggingWithFilterByDate(page, paramKey, paramValue, paramValue2);
			}else {
				formDataParam.add(new BasicNameValuePair(paramKey, paramValue));
			}
			formDataParam = paggingWithFilter(page, formDataParam);
		}else {
			formDataParam = pagging(page);
		}
		
		if (null != sortRequest) {
			if (null != sortRequest.getColumn() && !"".equals(sortRequest.getColumn())) {
				formDataParam = sortDynamic(sortRequest, formDataParam);
			}
		}
		
		formDataParam.add(new BasicNameValuePair("username", username));
		
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<TaskLogsResponse> historyTaskLogs(String authority, String instancesId, Locale locale ) {
		String url = ws_ess + HISTORY_TASK_LOGS + instancesId;
		return consumeWs(url, HttpMethod.GET, null, null, ListResponse.class, locale, authority);
	}
	
	public BlockLeaveResponse checkBlockLeave(String authority, String username, Locale locale) {
		String url = ws_minova + CHECK_BLOCK_LEAVE + username;
		return consumeWs(url, HttpMethod.GET, null, null, BlockLeaveResponse.class, locale, authority);
	}
	
	public LastRemainingResponse findLastRemaining(String authority, String username, Locale locale) {
		String url = ws_minova + FIND_LAST_REMAINING + username;
		return consumeWs(url, HttpMethod.GET, null, null, LastRemainingResponse.class, locale, authority);
	}
	
	public LastRemainingResponse findLastRemainingCarry(String authority, String username, Locale locale) {
		String url = ws_minova + FIND_LAST_REMAINING_CARRY + username;
		return consumeWs(url, HttpMethod.GET, null, null, LastRemainingResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<DataDateResponse> listDataDate(String authority, String requesterId, Locale locale){
		String url = ws_ess + FIND_DATA_DATE_LIST + requesterId;
		return consumeWs(url, HttpMethod.GET, null, null, ListResponse.class, locale, authority);
	}
	
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	private ContentInvestigasiResponse setBody(String datasJson, String reason) throws JsonParseException, JsonProcessingException, IOException {
//		ContentInvestigasiResponse content = new ContentInvestigasiResponse();
	//	List<CustomerRequest> listReq = new ArrayList<>();
	//	List<CustomerResponse> listData = parseJsonList(datasJson, CustomerResponse.class);
	//	for (Iterator iterator = listData.iterator(); iterator.hasNext();) {
	//		CustomerRequest model = new CustomerRequest();
	//		LinkedHashMap<String, String> customerResponse =  (LinkedHashMap<String, String>) iterator.next();
	//		model.setCustomer(setDetailConsumer(customerResponse));
	//		model.setExistingGcn(customerResponse.get("idnSvp"));
	//		model.setNewGcn(customerResponse.get("newIdnSvp"));
	//		if ("true".equals(String.valueOf(customerResponse.get("goldenRecord")))) {
	//			model.setGoldenRecord(true);
	//		}else {
	//			model.setGoldenRecord(false);
	//		}
	//		model.setGroup(Long.valueOf(String.valueOf(customerResponse.get("group"))));
	//		listReq.add(model);
	//		
	//	}
	//	content.setContent(listReq);
	//	content.setRejectReason(reason);
	//	return content;
	//}
	
	//private CustomerDetailRequest setDetailConsumer(LinkedHashMap<String, String> customerResponse) {
	//	CustomerDetailRequest detail = new CustomerDetailRequest();
	//	Branch branch = new Branch();
	//	detail.setIdnParty(customerResponse.get("idnParty"));
	//	detail.setIdnSvp(customerResponse.get("idnSvp"));
	//	detail.setNmeFull(customerResponse.get("nmeFull"));
	//	
	//	try {
	//		String json = new ObjectMapper().writeValueAsString(customerResponse.get("branch"));
	//		branch = parseJson(json, Branch.class);
	//		detail.setBranch(branch);
	//	} catch (JsonGenerationException e) {
	//		e.printStackTrace();
	//	} catch (JsonMappingException e) {
	//		e.printStackTrace();
	//	} catch (IOException e) {
	//		e.printStackTrace();
	//	}
	//	
	//	return detail;
	//}
	
}
