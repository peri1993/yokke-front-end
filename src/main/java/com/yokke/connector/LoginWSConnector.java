package com.yokke.connector;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.yokke.connector.request.LoginRequest;
import com.yokke.connector.response.LdapResponse;
import com.yokke.connector.response.OAuthResponse;

@Component
public class LoginWSConnector extends BaseWsConnector {

	@Value("#{prop['login.ws.url']}")
	private String wsUrl;
	
	@Value("#{prop['oauth.salt']}")
	private String wsKey;

	@Value("#{prop['oauth.grantType']}")
	private String grantType;

	@Value("#{prop['oauth.scopes']}")
	private String scopes;

	@Value("#{prop['oauth.authUsername']}")
	private String authUsername;

	@Value("#{prop['oauth.authPassword']}")
	private String authPassword;
	
	@Value("#{prop['oauth.username']}")
	private String oauth_username;
	
	@Value("#{prop['oauth.password']}")
	private String oauth_password;

	/*
	 * ldap config
	 */

	@Value("#{prop['initial.context.factory']}")
	private String ldapFactory;

	@Value("#{prop['provide.url']}")
	private String ldapUrl;

	@Value("#{prop['security.authentication']}")
	private String ldapAuth;

	@Value("#{prop['security.principal']}")
	private String ldapPrincipal;

	@Value("#{prop['security.credential']}")
	private String ldapCredential;
	
	@Value("#{prop['gateway.ws.url']}")
	private String loginGateway;

	@Value("#{prop['ess.ws.login']}")
	private String ws_login;
	
	@Value("#{prop['ess.ws.login.by.pass']}")
	private String ws_login_by_pass;
	
	public OAuthResponse auth(String userName) {
		OAuthResponse authResponse = new OAuthResponse();
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

			String encoding = authUsername + ":" + authPassword;
			byte[] encodedAuth = Base64.encodeBase64(encoding.getBytes(Charset.forName("US-ASCII")));
			String authHeader = new String(encodedAuth);

			List<NameValuePair> form = new ArrayList<>();
			form.add(new BasicNameValuePair("grant_type", grantType));
			form.add(new BasicNameValuePair("scopes", scopes));
			form.add(new BasicNameValuePair("username", userName));
			form.add(new BasicNameValuePair("password", wsKey));
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, Consts.UTF_8);

			HttpPost httpPost = new HttpPost(wsUrl + "/oauth/token");

			httpPost.addHeader("Authorization", "Basic " + authHeader);
			httpPost.setEntity(entity);

			// Create a custom response handler
			ResponseHandler<String> responseHandler = response -> {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity responseEntity = response.getEntity();
					return responseEntity != null ? EntityUtils.toString(responseEntity) : null;
				} else {
					throw new ClientProtocolException("Unexpected response status: " + status);
				}
			};
			String responseBody = httpclient.execute(httpPost, responseHandler);
			authResponse = parseJson(responseBody, OAuthResponse.class);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return authResponse;

	}
	
	public OAuthResponse login() {
		List<NameValuePair> form = new ArrayList<>();
		form.add(new BasicNameValuePair("grant_type", grantType));
		form.add(new BasicNameValuePair("username", oauth_username));
		form.add(new BasicNameValuePair("password", oauth_password));
		
		return consumeLogin(loginGateway, HttpMethod.POST, null, form, OAuthResponse.class, null);
	}

	public OAuthResponse login(String username, String password) {
		List<NameValuePair> form = new ArrayList<>();
		form.add(new BasicNameValuePair("grant_type", grantType));
		form.add(new BasicNameValuePair("username", username));
		form.add(new BasicNameValuePair("password", password));
		
		return consumeLogin(loginGateway, HttpMethod.POST, null, form, OAuthResponse.class, null);
	}
	public LdapResponse login_ldap(LoginRequest request, Locale locale, String authority) {
		String url = ws_login;
		return consumeWs(url, HttpMethod.POST, request, null, LdapResponse.class, locale, authority);
	}
	
	public LdapResponse login_ldap_by_pass(LoginRequest request, Locale locale, String authority) {
		String url = ws_login_by_pass;
		return consumeWs(url, HttpMethod.POST, request, null, LdapResponse.class, locale, authority);
	}

}
