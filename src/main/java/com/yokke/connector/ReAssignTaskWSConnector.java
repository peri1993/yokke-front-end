package com.yokke.connector;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.yokke.connector.request.ApprovalChangeRequest;
import com.yokke.connector.request.SortDynamicRequest;
import com.yokke.connector.response.AssignedTaskResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.ObjectResponse;
import com.yokke.connector.response.ReAssignTaskResponse;
import com.yokke.connector.response.TaskLogsResponse;
import com.yokke.connector.response.UsernameResponse;

@Component
public class ReAssignTaskWSConnector extends BaseWsConnector{

	private static final String LIST_ASSIGN_TASK			= "/list-assign";
	private static final String DETAIL_ASSIGN_TASK 			= "/detail-approval/";
	private static final String FIND_USERNAME 				= "/find-username";
	private static final String UPDATE_APPROVAL 			= "/update-approval";
	private static final String UPDATE_APPROVAL_LIST		= "/update-approval-list";
	private static final String LIST_HISTORY_ASSIGNER		= "/find-history-assigner/";
	private static final String LIST_ALL_HISTORY_ASSIGNER	= "/find-history-assigner";
	
	@SuppressWarnings("unchecked")
	public ListResponse<AssignedTaskResponse> listAssigner(String authority, String page, String paramKey, String paramValue, SortDynamicRequest sortRequest, Locale locale) {
		String url = ws_ess + LIST_ASSIGN_TASK;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam = pagging(page);
		if (paramKey != null && !"".equals(paramKey)) {
			formDataParam.add(new BasicNameValuePair(paramKey, paramValue));
		}
		
		if (null != sortRequest) {
			if (null != sortRequest.getColumn() && !"".equals(sortRequest.getColumn())) {
				formDataParam = sortDynamic(sortRequest, formDataParam);
			}
		}
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ObjectResponse<AssignedTaskResponse> detail(String instancesId, String authority, Locale locale) {
		String url = ws_ess + DETAIL_ASSIGN_TASK + instancesId;
		return consumeWs(url, HttpMethod.GET, null, null, ObjectResponse.class, locale, authority);
	}
	
	public UsernameResponse findUsername(String username, Locale locale, String authority) {
		String url = ws_minova + FIND_USERNAME;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("username", username));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, UsernameResponse.class, locale, authority);
	}
	
	public ReAssignTaskResponse update(ApprovalChangeRequest request, String authority, Locale locale) {
		String url = ws_ess + UPDATE_APPROVAL;
		return consumeWs(url, HttpMethod.POST, request, null, ReAssignTaskResponse.class, locale, authority);
	}
	
	public ReAssignTaskResponse updateList(List<ApprovalChangeRequest> request, String authority, Locale locale) {
		String url = ws_ess + UPDATE_APPROVAL_LIST;
		return consumeWs(url, HttpMethod.POST, request, null, ReAssignTaskResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<TaskLogsResponse> findHistoryAssigner(String page, String authory, String paramKey, String paramValue, String paramValue2, String username, Locale locale){
		String url = ws_ess + LIST_HISTORY_ASSIGNER + username;
		List<NameValuePair> formDataParam = new ArrayList<>();
		if (paramKey != null && !"".equals(paramKey)) {
			if("createdDt".equals(paramKey)) {
				formDataParam = paggingWithFilterByDate(page, paramKey, paramValue, paramValue2);
			}else {
				formDataParam.add(new BasicNameValuePair(paramKey, paramValue));
			}
			formDataParam = paggingWithFilter(page, formDataParam);
		}else {
			formDataParam = pagging(page);
		}
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authory);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<TaskLogsResponse> findAllHistoryAssigner(String page, String paramKey, String paramValue, String paramValue2, String authority, Locale locale){
		String url = ws_ess + LIST_ALL_HISTORY_ASSIGNER;
		List<NameValuePair> formDataParam = new ArrayList<>();
		if (paramKey != null && !"".equals(paramKey)) {
			if("createdDt".equals(paramKey)) {
				formDataParam = paggingWithFilterByDate(page, paramKey, paramValue, paramValue2);
			}else {
				formDataParam.add(new BasicNameValuePair(paramKey, paramValue));
			}
			formDataParam = paggingWithFilter(page, formDataParam);
		}else {
			formDataParam = pagging(page);
		}
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}
}
