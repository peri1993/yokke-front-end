package com.yokke.connector;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.yokke.connector.request.BranchRequest;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.BranchConfResponse;
import com.yokke.connector.response.ListResponse;

@Component
public class BranchWsConnector extends BaseWsConnector{
	
	
	private static final String LIST_BRANCH 	= "/list-branch";
	private static final String UPDATE_URL		= "/update-branch-maps";

	@SuppressWarnings("unchecked")
	public ListResponse<BranchConfResponse> listBranch(String page, String authority, String username,String paramKey, String paramValue, Locale locale) {
		String url = ws_ess + LIST_BRANCH;
		List<NameValuePair> formDataParam = new ArrayList<>();
		if (paramKey != null && !"".equals(paramKey)) {
			formDataParam.add(new BasicNameValuePair(paramKey, paramValue));
			formDataParam = paggingWithFilter(page, formDataParam);
		}else {
			formDataParam = pagging(page);
		}

		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}
	
	public BaseResponse update(String authority, List<BranchRequest> listModel, Locale locale) {
		String url = ws_ess + UPDATE_URL;
		return consumeWs(url, HttpMethod.POST, listModel, null, BaseResponse.class, locale, authority);
	}
	
}
