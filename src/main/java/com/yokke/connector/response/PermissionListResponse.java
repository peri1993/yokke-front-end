package com.yokke.connector.response;

import java.util.List;

import com.yokke.model.Permission;

public class PermissionListResponse extends BaselistResponse {

	private static final long serialVersionUID = 1L;
	private List<Permission> permissions;
	public List<Permission> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
}
