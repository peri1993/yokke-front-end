package com.yokke.connector.response;

public class LoginResponse extends UserProfileResponse {

	private static final long serialVersionUID = 1L;

	private String sessionToken;
	
	public String getSessionToken() {
		return sessionToken;
	}
	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}
	
}
