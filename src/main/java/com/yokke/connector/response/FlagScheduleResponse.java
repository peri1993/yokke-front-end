package com.yokke.connector.response;

@SuppressWarnings("serial")
public class FlagScheduleResponse extends BaseResponse {

	private String flagAttendance;

	public String getFlagAttendance() {
		return flagAttendance;
	}

	public void setFlagAttendance(String flagAttendance) {
		this.flagAttendance = flagAttendance;
	}

}
