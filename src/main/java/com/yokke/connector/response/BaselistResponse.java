package com.yokke.connector.response;

public class BaselistResponse extends BaseResponse {

	private static final long serialVersionUID = 1L;
	
	protected Integer total;
	protected Integer from;
	protected Integer to;
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getFrom() {
		return from;
	}
	public void setFrom(Integer from) {
		this.from = from;
	}
	public Integer getTo() {
		return to;
	}
	public void setTo(Integer to) {
		this.to = to;
	}
}
