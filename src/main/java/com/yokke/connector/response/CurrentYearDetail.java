package com.yokke.connector.response;

@SuppressWarnings("serial")
public class CurrentYearDetail extends BaseResponse{
    private CurrentIdDeepDetail id;
    private String date;
    private String dayType;
    private String description;
	public CurrentIdDeepDetail getId() {
		return id;
	}
	public void setId(CurrentIdDeepDetail id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDayType() {
		return dayType;
	}
	public void setDayType(String dayType) {
		this.dayType = dayType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
    
    
}
