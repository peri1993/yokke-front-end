package com.yokke.connector.response;

@SuppressWarnings("serial")
public class ListNotificationsDetailResponse extends BaseResponse {

	private String instancesId;
	private String businessProcess;
	private String subject;
	private String stages;
	private String requester;
	private String createdDt;
	private String description;

	public String getInstancesId() {
		return instancesId;
	}

	public void setInstancesId(String instancesId) {
		this.instancesId = instancesId;
	}

	public String getBusinessProcess() {
		return businessProcess;
	}

	public void setBusinessProcess(String businessProcess) {
		this.businessProcess = businessProcess;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getStages() {
		return stages;
	}

	public void setStages(String stages) {
		this.stages = stages;
	}

	public String getRequester() {
		return requester;
	}

	public void setRequester(String requester) {
		this.requester = requester;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
