package com.yokke.connector.response;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

@SuppressWarnings("serial")
public class LdapResponse extends BaseResponse {

	private String uid;
	private String loginDisabled;
	private String sn;
	private String passwordAllowChange;
	private String loginTime;
	private String passwordMinimumLength;
	private String passwordRequired;
	private String passwordUniqueRequired;
	private String o;
	private String ObjectClass;
	private String passwordExpirationInterval;
	private String givenName;
	private String co;
	private String cn;
	private String departmentNumber;
	@JsonProperty("ACL")
	private String ACL;
	private String displayName;
	private String passwordExpirationTime;
	private String username;

	@JsonProperty("ResponseCode")
	private String ResponseCode;

	@JsonProperty("ResponseDescription")
	private String ResponseDescription;

	@JsonProperty("fullname")
	private String fullname;
	private String externalId;


	@JsonProperty("jobDesc")
	private String jobDesc;

	private String id;
	private String operation;
	private List<MenuResponse> listMenuAccess;
	
	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getLoginDisabled() {
		return loginDisabled;
	}

	public void setLoginDisabled(String loginDisabled) {
		this.loginDisabled = loginDisabled;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getPasswordAllowChange() {
		return passwordAllowChange;
	}

	public void setPasswordAllowChange(String passwordAllowChange) {
		this.passwordAllowChange = passwordAllowChange;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public String getPasswordMinimumLength() {
		return passwordMinimumLength;
	}

	public void setPasswordMinimumLength(String passwordMinimumLength) {
		this.passwordMinimumLength = passwordMinimumLength;
	}

	public String getPasswordRequired() {
		return passwordRequired;
	}

	public void setPasswordRequired(String passwordRequired) {
		this.passwordRequired = passwordRequired;
	}

	public String getPasswordUniqueRequired() {
		return passwordUniqueRequired;
	}

	public void setPasswordUniqueRequired(String passwordUniqueRequired) {
		this.passwordUniqueRequired = passwordUniqueRequired;
	}

	public String getO() {
		return o;
	}

	public void setO(String o) {
		this.o = o;
	}

	public String getObjectClass() {
		return ObjectClass;
	}

	public void setObjectClass(String objectClass) {
		ObjectClass = objectClass;
	}

	public String getPasswordExpirationInterval() {
		return passwordExpirationInterval;
	}

	public void setPasswordExpirationInterval(String passwordExpirationInterval) {
		this.passwordExpirationInterval = passwordExpirationInterval;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getCo() {
		return co;
	}

	public void setCo(String co) {
		this.co = co;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getDepartmentNumber() {
		return departmentNumber;
	}

	public void setDepartmentNumber(String departmentNumber) {
		this.departmentNumber = departmentNumber;
	}

	public String getACL() {
		return ACL;
	}

	public void setACL(String aCL) {
		ACL = aCL;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getPasswordExpirationTime() {
		return passwordExpirationTime;
	}

	public void setPasswordExpirationTime(String passwordExpirationTime) {
		this.passwordExpirationTime = passwordExpirationTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getResponseCode() {
		return ResponseCode;
	}

	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}

	public String getResponseDescription() {
		return ResponseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		ResponseDescription = responseDescription;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getJobDesc() {
		return jobDesc;
	}

	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	public List<MenuResponse> getListMenuAccess() {
		return listMenuAccess;
	}

	public void setListMenuAccess(List<MenuResponse> listMenuAccess) {
		this.listMenuAccess = listMenuAccess;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
