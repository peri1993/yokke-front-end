package com.yokke.connector.response;

@SuppressWarnings("serial")
public class AbsenceTypeResponse extends BaseResponse {

	private BaseCustRefAbsencetype_Id id;
	private String leaveQuota;
	private String description;
	private String quotadeduction;
	private String paidtype;
	private String remarks;

	public BaseCustRefAbsencetype_Id getId() {
		return id;
	}

	public void setId(BaseCustRefAbsencetype_Id id) {
		this.id = id;
	}

	public String getLeaveQuota() {
		return leaveQuota;
	}

	public void setLeaveQuota(String leaveQuota) {
		this.leaveQuota = leaveQuota;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getQuotadeduction() {
		return quotadeduction;
	}

	public void setQuotadeduction(String quotadeduction) {
		this.quotadeduction = quotadeduction;
	}

	public String getPaidtype() {
		return paidtype;
	}

	public void setPaidtype(String paidtype) {
		this.paidtype = paidtype;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
