package com.yokke.connector.response;

import java.sql.Timestamp;

public class InstancesLogsResponse {

	private Long instancesId;
	private String assigner;
	private String requesterId;
	private String requesterName;
	private String businessProcess;
	private String subject;
	private Timestamp createdDt;

	public Long getInstancesId() {
		return instancesId;
	}

	public void setInstancesId(Long instancesId) {
		this.instancesId = instancesId;
	}

	public String getAssigner() {
		return assigner;
	}

	public void setAssigner(String assigner) {
		this.assigner = assigner;
	}

	public String getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(String requesterId) {
		this.requesterId = requesterId;
	}

	public String getRequesterName() {
		return requesterName;
	}

	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}

	public String getBusinessProcess() {
		return businessProcess;
	}

	public void setBusinessProcess(String businessProcess) {
		this.businessProcess = businessProcess;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Timestamp getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Timestamp createdDt) {
		this.createdDt = createdDt;
	}

}
