package com.yokke.connector.response;

import java.util.List;

@SuppressWarnings("serial")
public class ListResponse<T> extends BaseResponse {

	private List<T> listResponse;
	private Long total;
	private Integer from;
	private Long totalRows;

	public List<T> getListResponse() {
		return listResponse;
	}

	public void setListResponse(List<T> listResponse) {
		this.listResponse = listResponse;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Integer getFrom() {
		return from;
	}

	public void setFrom(Integer from) {
		this.from = from;
	}

	public Long getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(Long totalRows) {
		this.totalRows = totalRows;
	}

	/*
	 * public List<T> getObjectResponse() { return objectResponse; }
	 * 
	 * public void setObjectResponse(List<T> objectResponse) { this.objectResponse =
	 * objectResponse; }
	 */
}
