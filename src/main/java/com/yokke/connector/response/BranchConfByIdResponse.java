package com.yokke.connector.response;

public class BranchConfByIdResponse {
	
	private String empArea;
	private String empSubArea;
	private String landscape;
	private String empSubAreaDescription;
	private String address;
	private String npwp;
	private String latitudeCode;
	private String longitudeCode;
	private String username;
	
	public String getEmpArea() {
		return empArea;
	}
	public void setEmpArea(String empArea) {
		this.empArea = empArea;
	}
	public String getEmpSubArea() {
		return empSubArea;
	}
	public void setEmpSubArea(String empSubArea) {
		this.empSubArea = empSubArea;
	}
	public String getLandscape() {
		return landscape;
	}
	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}
	public String getEmpSubAreaDescription() {
		return empSubAreaDescription;
	}
	public void setEmpSubAreaDescription(String empSubAreaDescription) {
		this.empSubAreaDescription = empSubAreaDescription;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public String getLatitudeCode() {
		return latitudeCode;
	}
	public void setLatitudeCode(String latitudeCode) {
		this.latitudeCode = latitudeCode;
	}
	public String getLongitudeCode() {
		return longitudeCode;
	}
	public void setLongitudeCode(String longitudeCode) {
		this.longitudeCode = longitudeCode;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
}
