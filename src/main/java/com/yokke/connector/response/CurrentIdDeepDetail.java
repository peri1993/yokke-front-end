package com.yokke.connector.response;

@SuppressWarnings("serial")
public class CurrentIdDeepDetail extends BaseResponse{

	private String landscape;
	private String phId;
	private String countryId;
	private String year;
	public String getLandscape() {
		return landscape;
	}
	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}
	public String getPhId() {
		return phId;
	}
	public void setPhId(String phId) {
		this.phId = phId;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	
}
