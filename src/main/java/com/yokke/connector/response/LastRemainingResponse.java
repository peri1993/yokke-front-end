package com.yokke.connector.response;

@SuppressWarnings("serial")
public class LastRemainingResponse extends BaseResponse {
	private String remaining;
	private String remainingPending;
	private String totalRequest;

	public String getRemaining() {
		return remaining;
	}

	public void setRemaining(String remaining) {
		this.remaining = remaining;
	}

	public String getRemainingPending() {
		return remainingPending;
	}

	public void setRemainingPending(String remainingPending) {
		this.remainingPending = remainingPending;
	}

	public String getTotalRequest() {
		return totalRequest;
	}

	public void setTotalRequest(String totalRequest) {
		this.totalRequest = totalRequest;
	}

}
