package com.yokke.connector.response;

@SuppressWarnings("serial")
public class PreviewFileResponse extends BaseResponse {

	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	
}
