package com.yokke.connector.response;

@SuppressWarnings("serial")
public class RemainingQuotaResponse extends BaseResponse {

	private RemainingIDQuotaResponse id;
	private String startDeduction;
	private String endDeduction;
	private String originalQuota;
	private String userChange;
	private String lastChange;
	private String createdBy;
	private String createdDate;
	private String remainingQuota;

	public RemainingIDQuotaResponse getId() {
		return id;
	}

	public void setId(RemainingIDQuotaResponse id) {
		this.id = id;
	}

	public String getStartDeduction() {
		return startDeduction;
	}

	public void setStartDeduction(String startDeduction) {
		this.startDeduction = startDeduction;
	}

	public String getEndDeduction() {
		return endDeduction;
	}

	public void setEndDeduction(String endDeduction) {
		this.endDeduction = endDeduction;
	}

	public String getOriginalQuota() {
		return originalQuota;
	}

	public void setOriginalQuota(String originalQuota) {
		this.originalQuota = originalQuota;
	}

	public String getUserChange() {
		return userChange;
	}

	public void setUserChange(String userChange) {
		this.userChange = userChange;
	}

	public String getLastChange() {
		return lastChange;
	}

	public void setLastChange(String lastChange) {
		this.lastChange = lastChange;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getRemainingQuota() {
		return remainingQuota;
	}

	public void setRemainingQuota(String remainingQuota) {
		this.remainingQuota = remainingQuota;
	}

}
