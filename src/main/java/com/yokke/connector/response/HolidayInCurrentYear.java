package com.yokke.connector.response;

import java.util.List;

@SuppressWarnings("serial")
public class HolidayInCurrentYear extends BaseResponse{
    private List<CurrentYearDetail> data;

	public List<CurrentYearDetail> getData() {
		return data;
	}

	public void setData(List<CurrentYearDetail> data) {
		this.data = data;
	}
    
    
}
