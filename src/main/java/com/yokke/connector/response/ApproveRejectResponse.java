package com.yokke.connector.response;

public class ApproveRejectResponse {

	private String absencesDesc;
	private String absencesType;
	private String businessProcess;
	private String approvalStagesName;
	private String createdDt;
	private String endDate;
	private String flagTaskComplete;
	private String instancesId;
	private String requesterId;
	private String requesterName;
	private Boolean selected;
	private String startDate;
	private String taskId;

	public String getApprovalStagesName() {
		return approvalStagesName;
	}

	public void setApprovalStagesName(String approvalStagesName) {
		this.approvalStagesName = approvalStagesName;
	}

	public String getFlagTaskComplete() {
		return flagTaskComplete;
	}

	public void setFlagTaskComplete(String flagTaskComplete) {
		this.flagTaskComplete = flagTaskComplete;
	}

	public String getAbsencesDesc() {
		return absencesDesc;
	}

	public void setAbsencesDesc(String absencesDesc) {
		this.absencesDesc = absencesDesc;
	}

	public String getAbsencesType() {
		return absencesType;
	}

	public void setAbsencesType(String absencesType) {
		this.absencesType = absencesType;
	}

	public String getBusinessProcess() {
		return businessProcess;
	}

	public void setBusinessProcess(String businessProcess) {
		this.businessProcess = businessProcess;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getInstancesId() {
		return instancesId;
	}

	public void setInstancesId(String instancesId) {
		this.instancesId = instancesId;
	}

	public String getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(String requesterId) {
		this.requesterId = requesterId;
	}

	public String getRequesterName() {
		return requesterName;
	}

	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

}
