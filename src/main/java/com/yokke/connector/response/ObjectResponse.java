package com.yokke.connector.response;

import org.codehaus.jackson.annotate.JsonProperty;

@SuppressWarnings("serial")
public class ObjectResponse<T> extends BaseResponse {
	@JsonProperty("objectResponse")
	private T objectResponse;

	public T getObjectResponse() {
		return objectResponse;
	}

	public void setObjectResponse(T objectResponse) {
		this.objectResponse = objectResponse;
	}

}
