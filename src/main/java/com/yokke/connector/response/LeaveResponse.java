package com.yokke.connector.response;

@SuppressWarnings("serial")
public class LeaveResponse extends BaseResponse {

	private Long processId;
	private Long taskId;

	public Long getProcessId() {
		return processId;
	}

	public void setProcessId(Long processId) {
		this.processId = processId;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

}
