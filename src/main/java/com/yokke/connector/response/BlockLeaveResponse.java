package com.yokke.connector.response;

@SuppressWarnings("serial")
public class BlockLeaveResponse extends BaseResponse {
	private boolean flag;

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

}
