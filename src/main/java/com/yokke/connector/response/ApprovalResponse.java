package com.yokke.connector.response;

@SuppressWarnings("serial")
public class ApprovalResponse extends BaseResponse{

	private String username;
	private String fullname;
	private String joinDate;
	private String approval1;
	private String nameApproval1;
	private String approval2;
	private String nameApproval2;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}

	public String getApproval1() {
		return approval1;
	}

	public void setApproval1(String approval1) {
		this.approval1 = approval1;
	}

	public String getNameApproval1() {
		return nameApproval1;
	}

	public void setNameApproval1(String nameApproval1) {
		this.nameApproval1 = nameApproval1;
	}

	public String getApproval2() {
		return approval2;
	}

	public void setApproval2(String approval2) {
		this.approval2 = approval2;
	}

	public String getNameApproval2() {
		return nameApproval2;
	}

	public void setNameApproval2(String nameApproval2) {
		this.nameApproval2 = nameApproval2;
	}

}
