package com.yokke.connector.response;

@SuppressWarnings("serial")
public class TaskResponse extends BaseResponse{

	private String instancesId;
	private String taskId;
	private String assigner;
	private String requesterId;
	private String requesterName;
	private String businessProcess;
	private String subject;
	private String createdDt;
	
	public String getInstancesId() {
		return instancesId;
	}
	public void setInstancesId(String instancesId) {
		this.instancesId = instancesId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getAssigner() {
		return assigner;
	}
	public void setAssigner(String assigner) {
		this.assigner = assigner;
	}
	public String getRequesterId() {
		return requesterId;
	}
	public void setRequesterId(String requesterId) {
		this.requesterId = requesterId;
	}
	public String getRequesterName() {
		return requesterName;
	}
	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}
	public String getBusinessProcess() {
		return businessProcess;
	}
	public void setBusinessProcess(String businessProcess) {
		this.businessProcess = businessProcess;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}
	
	

}
