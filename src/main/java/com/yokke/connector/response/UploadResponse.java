package com.yokke.connector.response;

@SuppressWarnings("serial")
public class UploadResponse extends BaseResponse {
	private String filename;
	private String messageupload;
	private String fileSize;
	private String fileType;

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getMessageupload() {
		return messageupload;
	}

	public void setMessageupload(String messageupload) {
		this.messageupload = messageupload;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

}
