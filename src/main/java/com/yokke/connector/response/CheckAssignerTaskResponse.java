package com.yokke.connector.response;

@SuppressWarnings("serial")
public class CheckAssignerTaskResponse extends BaseResponse{
	
	private boolean checkAssignerFlag;

	public boolean isCheckAssignerFlag() {
		return checkAssignerFlag;
	}

	public void setCheckAssignerFlag(boolean checkAssignerFlag) {
		this.checkAssignerFlag = checkAssignerFlag;
	}

	
}
