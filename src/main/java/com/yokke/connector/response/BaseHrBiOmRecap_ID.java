package com.yokke.connector.response;

@SuppressWarnings("serial")
public class BaseHrBiOmRecap_ID extends BaseResponse{
	
	private String landscape;
	private String emp_id;
	
	public String getLandscape() {
		return landscape;
	}
	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}
	public String getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}
	
	
}
