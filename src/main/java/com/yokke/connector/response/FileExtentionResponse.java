package com.yokke.connector.response;

@SuppressWarnings("serial")
public class FileExtentionResponse extends BaseResponse {

	private String fileType;

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

}
