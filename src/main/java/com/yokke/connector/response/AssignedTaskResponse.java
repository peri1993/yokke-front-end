package com.yokke.connector.response;

@SuppressWarnings("serial")
public class AssignedTaskResponse extends BaseResponse {

	private String absencesDesc;
	private String absencestype;
	private String approvalDs1;
	private String approvalDs2;
	private String assigner;
	private String assignerName;
	private String bussinesProcess;
	private String createdDt;
	private String endDate;
	private String flagTaskComplete;
	private Long instancesId;
	private String nameApprovalDs1;
	private String nameApprovalDs2;
	private String requesterId;
	private String requesterName;
	private String startDate;
	private String stages;
	private String subject;
	private Long taskId;
	private String updatedDt;

	public String getAssignerName() {
		return assignerName;
	}

	public void setAssignerName(String assignerName) {
		this.assignerName = assignerName;
	}

	public String getAbsencesDesc() {
		return absencesDesc;
	}

	public void setAbsencesDesc(String absencesDesc) {
		this.absencesDesc = absencesDesc;
	}

	public String getAbsencestype() {
		return absencestype;
	}

	public void setAbsencestype(String absencestype) {
		this.absencestype = absencestype;
	}

	public String getApprovalDs1() {
		return approvalDs1;
	}

	public void setApprovalDs1(String approvalDs1) {
		this.approvalDs1 = approvalDs1;
	}

	public String getApprovalDs2() {
		return approvalDs2;
	}

	public void setApprovalDs2(String approvalDs2) {
		this.approvalDs2 = approvalDs2;
	}

	public String getAssigner() {
		return assigner;
	}

	public void setAssigner(String assigner) {
		this.assigner = assigner;
	}

	public String getBussinesProcess() {
		return bussinesProcess;
	}

	public void setBussinesProcess(String bussinesProcess) {
		this.bussinesProcess = bussinesProcess;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getFlagTaskComplete() {
		return flagTaskComplete;
	}

	public void setFlagTaskComplete(String flagTaskComplete) {
		this.flagTaskComplete = flagTaskComplete;
	}

	public Long getInstancesId() {
		return instancesId;
	}

	public void setInstancesId(Long instancesId) {
		this.instancesId = instancesId;
	}

	public String getNameApprovalDs1() {
		return nameApprovalDs1;
	}

	public void setNameApprovalDs1(String nameApprovalDs1) {
		this.nameApprovalDs1 = nameApprovalDs1;
	}

	public String getNameApprovalDs2() {
		return nameApprovalDs2;
	}

	public void setNameApprovalDs2(String nameApprovalDs2) {
		this.nameApprovalDs2 = nameApprovalDs2;
	}

	public String getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(String requesterId) {
		this.requesterId = requesterId;
	}

	public String getRequesterName() {
		return requesterName;
	}

	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(String updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getStages() {
		return stages;
	}

	public void setStages(String stages) {
		this.stages = stages;
	}

}
