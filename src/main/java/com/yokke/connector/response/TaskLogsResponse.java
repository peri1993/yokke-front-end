package com.yokke.connector.response;

import java.sql.Timestamp;

public class TaskLogsResponse {

	private Long id;
	private Long taskId;
	private InstancesLogsResponse instanceId;
	private String stages;
	private String createdBy;
	private Timestamp createdDt;
	private String quotaTaken;
	private String flagTaskComplete;

	public String getQuotaTaken() {
		return quotaTaken;
	}

	public void setQuotaTaken(String quotaTaken) {
		this.quotaTaken = quotaTaken;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public InstancesLogsResponse getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(InstancesLogsResponse instanceId) {
		this.instanceId = instanceId;
	}

	public String getStages() {
		return stages;
	}

	public void setStages(String stages) {
		this.stages = stages;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Timestamp createdDt) {
		this.createdDt = createdDt;
	}

	public String getFlagTaskComplete() {
		return flagTaskComplete;
	}

	public void setFlagTaskComplete(String flagTaskComplete) {
		this.flagTaskComplete = flagTaskComplete;
	}

}
