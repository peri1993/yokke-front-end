package com.yokke.connector.response;

@SuppressWarnings("serial")
public class BaseCustRefAbsencetype_Id extends BaseResponse {

	private String landscape;

	private String absencetype;

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getAbsencetype() {
		return absencetype;
	}

	public void setAbsencetype(String absencetype) {
		this.absencetype = absencetype;
	}

}
