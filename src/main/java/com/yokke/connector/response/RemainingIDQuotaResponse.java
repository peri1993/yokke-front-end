package com.yokke.connector.response;

public class RemainingIDQuotaResponse {
	private String landscape;
	private String empId;
	private String startDate;
	private String endDate;
	private String seq;
	private String absencetype;

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getAbsencetype() {
		return absencetype;
	}

	public void setAbsencetype(String absencetype) {
		this.absencetype = absencetype;
	}

}
