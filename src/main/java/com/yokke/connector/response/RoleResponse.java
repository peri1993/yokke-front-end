package com.yokke.connector.response;

import java.util.List;

@SuppressWarnings("serial")
public class RoleResponse extends BaseResponse {

	private Long role_id;
	private String roleName;
	private String createdBy;
	private String createdDt;
	private String updatedBy;
	private String updatedDt;
	private List<MenuResponse> listMenu;
	private String menuAccess;

	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public List<MenuResponse> getListMenu() {
		return listMenu;
	}

	public void setListMenu(List<MenuResponse> listMenu) {
		this.listMenu = listMenu;
	}

	public String getMenuAccess() {
		return menuAccess;
	}

	public void setMenuAccess(String menuAccess) {
		this.menuAccess = menuAccess;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(String updatedDt) {
		this.updatedDt = updatedDt;
	}

}
