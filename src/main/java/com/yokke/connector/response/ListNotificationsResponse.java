package com.yokke.connector.response;

@SuppressWarnings("serial")
public class ListNotificationsResponse extends BaseResponse {

	private String instancesId;
	private String businessProcess;
	private String description;
	private String stages;
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInstancesId() {
		return instancesId;
	}

	public void setInstancesId(String instancesId) {
		this.instancesId = instancesId;
	}

	public String getBusinessProcess() {
		return businessProcess;
	}

	public void setBusinessProcess(String businessProcess) {
		this.businessProcess = businessProcess;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStages() {
		return stages;
	}

	public void setStages(String stages) {
		this.stages = stages;
	}

}
