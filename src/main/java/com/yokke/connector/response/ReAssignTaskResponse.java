package com.yokke.connector.response;

@SuppressWarnings("serial")
public class ReAssignTaskResponse extends BaseResponse {

	private String assigner;

	public String getAssigner() {
		return assigner;
	}

	public void setAssigner(String assigner) {
		this.assigner = assigner;
	}

}
