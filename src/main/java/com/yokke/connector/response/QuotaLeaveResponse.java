package com.yokke.connector.response;

@SuppressWarnings("serial")
public class QuotaLeaveResponse extends BaseResponse {
	private int total;
	private String errorCode;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
