package com.yokke.connector;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.yokke.connector.response.LdapResponse;
import com.yokke.utils.LdapEncryptPassword;

@Component
public class LdapWsConnector extends BaseWsConnector {

	@Value("#{prop['initial.context.factory']}")
	private String ldapFactory;

	@Value("#{prop['provide.url']}")
	private String ldapUrl;

	@Value("#{prop['security.authentication']}")
	private String ldapAuth;

	@Value("#{prop['security.principal']}")
	private String ldapPrincipal;

	@Value("#{prop['security.credential']}")
	private String ldapCredential;

	@Value("#{prop['security.salt']}")
	private String ldapSalt;

	@Value("#{prop['security.secret']}")
	private String ldapSecret;

	@Value("#{prop['operation.ldap.search']}")
	private String operationSearch;

	@Value("#{prop['operation.ldap.verifyPassword']}")
	private String operationVerifyPassword;

	@Value("#{prop['operation.ldap.changePassword']}")
	private String operationChangePassword;

	@Value("#{prop['operation.ldap.resetPassword']}")
	private String operationResetPassword;

	@Value("#{prop['operation.ldap.deleteUser']}")
	private String operationDeleteUser;

	@Value("#{prop['operation.ldap.addUser']}")
	private String operationAddUser;

	@Value("#{prop['operation.ldap.resetPasswordMail']}")
	private String operationResetPasswordMail;
	
	public LdapResponse search(String username) {
		
		String url = ldapUrl;
		List<NameValuePair> form = new ArrayList<>();
		form.add(new BasicNameValuePair("operation", operationSearch));
		form.add(new BasicNameValuePair("id", username));
		
		URI uri = null;
		try {

			uri = new URIBuilder(url).addParameters(form).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return consumeWs(String.valueOf(uri), HttpMethod.GET, null, null, LdapResponse.class, null, null);
	}

	
	
	public LdapResponse verifyPassword(String username, String password, Locale locale) throws Exception{
		
		String url = ldapUrl;
		String encPassword = "";
		List<NameValuePair> form = new ArrayList<>();
		form.add(new BasicNameValuePair("operation", operationVerifyPassword));
		form.add(new BasicNameValuePair("id", username));

		try {
			encPassword = LdapEncryptPassword.encrypt(ldapSalt, password, ldapSecret);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		form.add(new BasicNameValuePair("password", encPassword));
		
		URI uri = null;
		try {

			uri = new URIBuilder(url).addParameters(form).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return consumeWs(String.valueOf(uri), HttpMethod.GET, null, null, LdapResponse.class, null, null);
	}
	
}
