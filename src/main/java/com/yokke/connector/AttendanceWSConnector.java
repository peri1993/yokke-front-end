package com.yokke.connector;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.yokke.connector.request.AttendanceRequest;
import com.yokke.connector.request.BranchIdRequest;
import com.yokke.connector.request.BranchRequest;
import com.yokke.connector.request.PreviewFileRequest;
import com.yokke.connector.request.ScheduleAttendanceLogsRequest;
import com.yokke.connector.response.AttendanceListUnderResponse;
import com.yokke.connector.response.AttendanceLogsResponse;
import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.BranchConfResponse;
import com.yokke.connector.response.FileExtentionResponse;
import com.yokke.connector.response.FlagScheduleResponse;
import com.yokke.connector.response.HistoryAttendanceResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.ObjectResponse;
import com.yokke.connector.response.PreviewFileResponse;
import com.yokke.connector.response.ScheduleResponse;

@Component
public class AttendanceWSConnector extends BaseWsConnector{
	
	private static final String FIND_EMP_UNDER		= "/find-list-under/";
	private static final String LIST_BRANCH			= "/list-branch";
	private static final String LIST_QR				= "/QR-Code/";
	private static final String LIST_SCHEDULE		= "/list-schedule-attendance/";
	private static final String LIST_HISTORY_EMP	= "/list-history-attendance/";
	private static final String CREATE_SCHEDULE		= "/create-schedule-attendance";
	private static final String DELETE_SCHEDULE		= "/delete-schedule-attendance";
	private static final String PREVIEW_FILE		= "/download-files-attendance";
	private static final String CREATE_ATTENDANCE	= "/create-attendance";
	private static final String LIST_DATA_ABSENCE	= "/list-data-absence/";
	private static final String LIST_LOCATION		= "/find-branch";
	private static final String FLAG_SCHEDULE_USER	= "/flag-attendance/";
	private static final String LIST_HISTORY_ABSENSE_EMPLOYEE = "/list-history-attendance-employee/";
	private static final String FIND_EXTENTION_FILE = "/find-extention-file";
	
	@SuppressWarnings("unchecked")
	public ListResponse<ScheduleResponse> listSchedule(String authority, String username, Locale locale){
		String url = ws_ess+LIST_SCHEDULE+username;
		return consumeWs(url, HttpMethod.GET, null, null, ListResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<AttendanceListUnderResponse> findListUnder(String authority, String username, String key, String value, Locale locale){
		String url = ws_ess+FIND_EMP_UNDER+username;
		List<NameValuePair> formDataParam = new ArrayList<>();
		if (key != null && !"".equals(value)) {
			formDataParam.add(new BasicNameValuePair(key, value));
		}	
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<HistoryAttendanceResponse> listHistoryAttendanceForEmployee(String page, String startDt, String endDt, String authority, String username, Locale locale){
		String url = ws_ess + LIST_HISTORY_ABSENSE_EMPLOYEE + username + "/start-date/" + startDt +"/end-date/" + endDt;
		
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam = pagging(page);
		
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<BranchConfResponse> listBranch(String page, String authority, String username,String paramKey, String paramValue, Locale locale) {
		String url = ws_ess + LIST_BRANCH;
		List<NameValuePair> formDataParam = new ArrayList<>();
		if (paramKey != null && !"".equals(paramKey)) {
			formDataParam.add(new BasicNameValuePair(paramKey, paramValue));
			formDataParam = paggingWithFilter(page, formDataParam);
		}else {
			formDataParam = pagging(page);
		}

		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}
	
	public BaseResponse listQrCode(String authority, BranchConfResponse body, Locale locale){
		String url = ws_ess + LIST_QR + body.getEmpArea()+"/"+body.getEmpSubArea()+"/"+body.getLandscape() +"/"+body.getLongitudeCode()+"/"+body.getLatitudeCode() + "/v1";
		return consumeWs(url, HttpMethod.GET, null, null, BaseResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<BranchConfResponse> listBranchById(String page, String authority, Locale locale){
		String url = ws_ess + LIST_BRANCH;
		return consumeWs(url, HttpMethod.GET, null, null, ListResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<HistoryAttendanceResponse> listHistoryAttendance(String page, String authority, String username, Locale locale){
		String url = ws_ess + LIST_HISTORY_EMP + username;
		return consumeWs(url, HttpMethod.GET, null, null, ListResponse.class, locale, authority);
	}
	
	public BaseResponse createScheduleAttendance(String authority, List<ScheduleAttendanceLogsRequest> body, Locale locale) {
		String url = ws_ess + CREATE_SCHEDULE;
		return consumeWs(url, HttpMethod.POST, body, null, BaseResponse.class, locale, authority);
	}
	
	public BaseResponse deleteScheduleAttendance(String authority, ScheduleAttendanceLogsRequest body, Locale locale) {
		String url = ws_ess + DELETE_SCHEDULE;
		return consumeWs(url, HttpMethod.POST, body, null, BaseResponse.class, locale, authority);
	}
	
	public PreviewFileResponse previewFile(String authority, PreviewFileRequest request, Locale locale){
		String url = ws_ess+PREVIEW_FILE;
		return consumeWs(url, HttpMethod.POST, request, null, PreviewFileResponse.class, locale, authority);
	}
	
	public BaseResponse createAttendance(String authority, AttendanceRequest body, Locale locale) {
		String url = ws_ess + CREATE_ATTENDANCE;
		return consumeWs(url, HttpMethod.POST, body, null, BaseResponse.class, locale, authority);
	}
	
	public AttendanceLogsResponse listDataAbsence(String authority, String username, Locale locale){
		String url = ws_ess + LIST_DATA_ABSENCE + username;
		return consumeWs(url, HttpMethod.GET, null, null, AttendanceLogsResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ObjectResponse<BranchRequest> listLocation(String authority, BranchIdRequest body, Locale locale){
		String url = ws_ess + LIST_LOCATION;
		return consumeWs(url, HttpMethod.POST, body, null, ObjectResponse.class, locale, authority);
	}
	
	public FlagScheduleResponse flagSchedule(String authority, String username, Locale locale) {
		String url = ws_ess + FLAG_SCHEDULE_USER + username;
		return consumeWs(url, HttpMethod.GET, null, null, FlagScheduleResponse.class, locale, authority);
	}
	
	public FileExtentionResponse findExtention(String authority, String filename, String status, Locale locale){
		String url = ws_ess+FIND_EXTENTION_FILE;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("filename", filename));
		formDataParam.add(new BasicNameValuePair("status", status));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, FileExtentionResponse.class, locale, authority);
	}
}
