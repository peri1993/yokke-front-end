package com.yokke.connector;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.yokke.connector.response.BaseResponse;
import com.yokke.connector.response.CheckAssignerTaskResponse;
import com.yokke.connector.response.ListNotificationsDetailResponse;
import com.yokke.connector.response.ListNotificationsResponse;
import com.yokke.connector.response.ListResponse;

@Component
public class NotificationWSConnector extends BaseWsConnector {

	private static final String LIST_NOTIFICATIONS_USER 	= "/list-notif-user/";
	private static final String LIST_NOTIFICATIONS     		= "/list-notif/";
	private static final String CHECK_ASSIGNER_TASK_OWNER 	= "/check-assigner";
	private static final String UPDATE_NOTIF				= "/update-notif";

	@SuppressWarnings("unchecked")
	public ListResponse<ListNotificationsResponse> listNotifTask(String authority, String username, Locale locale) {
		String url = ws_ess + LIST_NOTIFICATIONS_USER + username;
		List<NameValuePair> formDataParam = new ArrayList<>();
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}

	@SuppressWarnings("unchecked")
	public ListResponse<ListNotificationsDetailResponse> listDetailNotifTask(String page, String authority, String username,String paramKey, String paramValue, String paramValue2, Locale locale) {
		String url = ws_ess + LIST_NOTIFICATIONS + username;
		List<NameValuePair> formDataParam = new ArrayList<>();
		if (paramKey != null && !"".equals(paramKey)) {
			if("createdDt".equals(paramKey)) {
				formDataParam = paggingWithFilterByDate(page, paramKey, paramValue, paramValue2);
			}else {
				formDataParam.add(new BasicNameValuePair(paramKey, paramValue));
			}
			formDataParam = paggingWithFilter(page, formDataParam);
		}else {
			formDataParam = pagging(page);
		}

		return consumeWs(url, HttpMethod.GET, null, formDataParam, ListResponse.class, locale, authority);
	}
	
	public CheckAssignerTaskResponse checkAssignerTask(String instancesId, String username, String authority, Locale locale) {
		String url = ws_ess + CHECK_ASSIGNER_TASK_OWNER;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("instancesId", instancesId));
		formDataParam.add(new BasicNameValuePair("username", username));
		
		return consumeWs(url, HttpMethod.GET, null, formDataParam, CheckAssignerTaskResponse.class, locale, authority);
	}
	
	public BaseResponse updateNotification(String id, String username, String instancesId, String authority, Locale locale) {
		String url = ws_ess + UPDATE_NOTIF;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("id", id));
		formDataParam.add(new BasicNameValuePair("username", username));
		formDataParam.add(new BasicNameValuePair("instancesId", instancesId));
		
		return consumeWs(url, HttpMethod.GET, null, formDataParam, BaseResponse.class, locale, authority);
	}
}
