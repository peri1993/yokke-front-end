package com.yokke.connector.request;

public class FilterlistRequest {
	private String sortType;
	private String sortBy;
	private String page;
	private String pageSize;
	private String qlue;
	private String column;
	
	public String buildQuery(){
		String params = "";
		if(this.getPage() != null){
			params += "page=" + this.getPage();
		}
		if(this.getPageSize() != null){
			params += "&pageSize=" + this.getPageSize();
		}
		if(this.getSortBy() != null){
			params += "&sortBy=" + this.getSortBy();
		}
		if(this.getSortType() != null){
			params += "&sortType=" + this.getSortType();
		}
		if(this.getQlue() != null){
			params += "&qlue=" + this.getQlue();
		}
		if(this.getColumn() != null){
			params += "&column=" + this.getColumn();
		}
		return params;
	}
	
	public String getSortType() {
		return sortType;
	}
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getQlue() {
		return qlue;
	}
	public void setQlue(String qlue) {
		this.qlue = qlue;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	
}
