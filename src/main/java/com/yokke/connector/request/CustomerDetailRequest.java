package com.yokke.connector.request;

import com.yokke.model.Branch;

public class CustomerDetailRequest {

	private String idnParty;
	private String idnSvp;
	private String nmeFull;
	private Branch branch;

	public String getIdnParty() {
		return idnParty;
	}

	public void setIdnParty(String idnParty) {
		this.idnParty = idnParty;
	}

	public String getIdnSvp() {
		return idnSvp;
	}

	public void setIdnSvp(String idnSvp) {
		this.idnSvp = idnSvp;
	}

	public String getNmeFull() {
		return nmeFull;
	}

	public void setNmeFull(String nmeFull) {
		this.nmeFull = nmeFull;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

}
