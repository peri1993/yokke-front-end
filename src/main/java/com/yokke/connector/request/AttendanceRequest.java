package com.yokke.connector.request;

public class AttendanceRequest {
	
	private AttendanceRequestLogsId id;
	private String empSubAreaDescription;
	private String longitudeCode;
	private String latitudeCode;
	private String radius;
	private String empArea;
	private String empSubArea;
	private String barcodeTime;
	private UploadAttendanceRequest pathImages;
	
	public UploadAttendanceRequest getPathImages() {
		return pathImages;
	}
	public void setPathImages(UploadAttendanceRequest pathImages) {
		this.pathImages = pathImages;
	}
	public AttendanceRequestLogsId getId() {
		return id;
	}
	public void setId(AttendanceRequestLogsId id) {
		this.id = id;
	}
	public String getEmpSubAreaDescription() {
		return empSubAreaDescription;
	}
	public void setEmpSubAreaDescription(String empSubAreaDescription) {
		this.empSubAreaDescription = empSubAreaDescription;
	}
	public String getLongitudeCode() {
		return longitudeCode;
	}
	public void setLongitudeCode(String longitudeCode) {
		this.longitudeCode = longitudeCode;
	}
	public String getLatitudeCode() {
		return latitudeCode;
	}
	public void setLatitudeCode(String latitudeCode) {
		this.latitudeCode = latitudeCode;
	}
	public String getRadius() {
		return radius;
	}
	public void setRadius(String radius) {
		this.radius = radius;
	}
	public String getEmpArea() {
		return empArea;
	}
	public void setEmpArea(String empArea) {
		this.empArea = empArea;
	}
	public String getEmpSubArea() {
		return empSubArea;
	}
	public void setEmpSubArea(String empSubArea) {
		this.empSubArea = empSubArea;
	}
	public String getBarcodeTime() {
		return barcodeTime;
	}
	public void setBarcodeTime(String barcodeTime) {
		this.barcodeTime = barcodeTime;
	}
	
}
