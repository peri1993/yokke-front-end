package com.yokke.connector.request;

public class CustomerRequest {

	private CustomerDetailRequest customer;
	private String existingGcn;
	private String newGcn;
	private Boolean goldenRecord;
	private Long group;

	public CustomerDetailRequest getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDetailRequest customer) {
		this.customer = customer;
	}

	public String getExistingGcn() {
		return existingGcn;
	}

	public void setExistingGcn(String existingGcn) {
		this.existingGcn = existingGcn;
	}

	public String getNewGcn() {
		return newGcn;
	}

	public void setNewGcn(String newGcn) {
		this.newGcn = newGcn;
	}

	public Boolean getGoldenRecord() {
		return goldenRecord;
	}

	public void setGoldenRecord(Boolean goldenRecord) {
		this.goldenRecord = goldenRecord;
	}

	public Long getGroup() {
		return group;
	}

	public void setGroup(Long group) {
		this.group = group;
	}

}
