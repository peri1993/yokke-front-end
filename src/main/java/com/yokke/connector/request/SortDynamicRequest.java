package com.yokke.connector.request;

public class SortDynamicRequest {

	private boolean flagSort;
	private String column;

	public boolean isFlagSort() {
		return flagSort;
	}

	public void setFlagSort(boolean flagSort) {
		this.flagSort = flagSort;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

}
