package com.yokke.connector;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.yokke.connector.request.ChangeWorkHoursRequest;
import com.yokke.connector.response.ChangeWorkHoursDetailResponse;
import com.yokke.connector.response.ListResponse;
import com.yokke.connector.response.ObjectResponse;
import com.yokke.connector.response.OfficeHoursResponse;
import com.yokke.connector.response.TaskResponse;
import com.yokke.connector.response.UserOfficeHoursResponse;

@Component
public class InformationWsConnector extends BaseWsConnector {

	private static String DETAIL_WORK_HOURS = "/information-work-hours/";
	private static String LIST_WORK_HOURS = "/list-work-hours";
	private static String TASK_DETAIL_WORK_HOURS = "/task-detail-work-hours";
	private static String CHANGE_WORK_HOURS_URL = "/update-work-hours";

	@SuppressWarnings("unchecked")
	public ObjectResponse<UserOfficeHoursResponse> detailWorkHours(String authority, String username, Locale locale) {
		String url = ws_ess + DETAIL_WORK_HOURS + username;
		return consumeWs(url, HttpMethod.GET, null, null, ObjectResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ListResponse<OfficeHoursResponse> listHours(String authority, Locale locale) {
		String url = ws_ess + LIST_WORK_HOURS;
		return consumeWs(url, HttpMethod.GET, null, null, ListResponse.class, locale, authority);
	}
	
	@SuppressWarnings("unchecked")
	public ObjectResponse<ChangeWorkHoursDetailResponse> taskDetail(String authority, String instanceId, String username, Locale locale){
		String url = ws_ess + TASK_DETAIL_WORK_HOURS;
		List<NameValuePair> formDataParam = new ArrayList<>();
		formDataParam.add(new BasicNameValuePair("instancesId", instanceId));
		formDataParam.add(new BasicNameValuePair("username", username));
		return consumeWs(url, HttpMethod.GET, null, formDataParam, ObjectResponse.class, locale, authority);
	}
	
	public TaskResponse requestChangeWorkHours(String authority, ChangeWorkHoursRequest request, Locale locale) {
		String url = ws_ess + CHANGE_WORK_HOURS_URL;
		return consumeWs(url, HttpMethod.POST, request, null, TaskResponse.class, locale, authority);
	}
}
