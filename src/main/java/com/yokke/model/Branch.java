package com.yokke.model;

public class Branch {
	private Long cdeBranch;
	private Long id;
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCdeBranch() {
		return cdeBranch;
	}

	public void setCdeBranch(Long cdeBranch) {
		this.cdeBranch = cdeBranch;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
