package com.yokke.model;

import java.util.Map;

public class PageAuthModules {
	
	private Map<String, Boolean> MapsPageModules;

	public Map<String, Boolean> getMapsPageModules() {
		return MapsPageModules;
	}

	public void setMapsPageModules(Map<String, Boolean> mapsPageModules) {
		MapsPageModules = mapsPageModules;
	}
	
	
}
