package com.yokke.model;

import java.io.Serializable;

import javax.persistence.Embeddable;


@SuppressWarnings("serial")
@Embeddable
public class EmbedHistoryLeave implements Serializable{
	
	private String landscape;
	private String empId;
	private String startDate;
	private String endDate;
	
	public String getLandscape() {
		return landscape;
	}
	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
