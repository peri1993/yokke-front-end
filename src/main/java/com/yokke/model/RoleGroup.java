package com.yokke.model;

import java.util.List;

public class RoleGroup {
	private String name;
	private List<Integer> modules;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getModules() {
		return modules;
	}

	public void setModules(List<Integer> modules) {
		this.modules = modules;
	}

}
