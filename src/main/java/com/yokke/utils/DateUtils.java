package com.yokke.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;

public class DateUtils {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd";

	public static String formatGiDateToEcom(String dateStr) {

		String dateFormatFr = "dd/MM/yyyy";

		if (StringUtils.isBlank(dateStr)) {
			return null;
		}

		return formatDate(dateStr, dateFormatFr, DATE_FORMAT);
	}

	public static String formatEcomDateToGi(String dateStr) {

		String dateFormatTo = "dd/MM/yyyy";

		if (StringUtils.isBlank(dateStr)) {
			return null;
		}

		return formatDate(dateStr, DATE_FORMAT, dateFormatTo);
	}

	public static String formatEcomDateToGiNonNullable(String dateStr) {
		if (StringUtils.isBlank(dateStr)) {
			return "";
		}

		return formatEcomDateToGi(dateStr);
	}

	public static String formatEcomDateToGiNonNullable(Date dat) {
		String dateFormatTo = "dd/MM/yyyy";

		if (dat == null) {
			return "";
		}

		return formatDate(dat, dateFormatTo);
	}

	public static String formatDateToEcom(Date date) {
		if (date == null) {
			return null;
		}

		return formatDate(date, DATE_FORMAT);
	}

	public static Date toDateTime(String datStr) throws ParseException {
		String dateFormat = "yyyy-MM-dd hh:mm:ss";

		if (StringUtils.isBlank(datStr)) {
			return null;
		}

		return toDate(datStr, dateFormat);
	}

	public static Date toDate(String dateStr) throws ParseException {

		if (StringUtils.isBlank(dateStr)) {
			return null;
		}

		return toDate(dateStr, DATE_FORMAT);
	}

	public static Date toDateWithZeroTime(Date dat) throws ParseException {

		if (dat == null) {
			return null;
		}

		return toDate(formatDate(dat, DATE_FORMAT), DATE_FORMAT);
	}

	public static Date toDateFrGi(String datStr) throws ParseException {
		String dateFormat = "dd/MM/yyyy";

		if (StringUtils.isBlank(datStr)) {
			return null;
		}

		return toDate(datStr, dateFormat);
	}

	public static boolean isDate(String datStr) {
		if (StringUtils.isBlank(datStr)) {
			return false;
		}
		try {
			toDate(datStr, DATE_FORMAT);
			return true;
		} catch (ParseException ex) {
			return false;
		}
	}

	public static boolean isPastDate(String datStr) throws ParseException {
		if (!isDate(datStr)) {
			return false;
		} else {
			return isPastDate(toDate(datStr));
		}
	}

	public static boolean isPastDate(Date dat) {
		if (dat == null) {
			return false;
		}

		Calendar now = Calendar.getInstance();
		Calendar dob = Calendar.getInstance();
		dob.setTime(dat);

		if (now.after(dob)) {
			return true;
		} else {
			return false;
		}
	}

	public static Integer getAge(String datStr) throws ParseException {
		if (!isDate(datStr)) {
			return null;
		} else {
			return getAge(toDate(datStr));
		}
	}

	public static Integer getAge(Date dat) {
		if (!isPastDate(dat)) {
			return null;
		} else {
			Calendar now = Calendar.getInstance();
			Calendar dob = Calendar.getInstance();
			dob.setTime(dat);

			int age = now.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
			if ((dob.get(Calendar.MONTH) > now.get(Calendar.MONTH)) || (dob.get(Calendar.MONTH) == now.get(Calendar.MONTH) && dob.get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH))) {
				age--;
			}
			return age;
		}
	}

	public static boolean isAge18OrAbove(String datStr) throws ParseException {
		Integer age = getAge(datStr);
		return isAge18OrAbove(age);
	}

	public static boolean isAge18OrAbove(Date dat) {
		Integer age = getAge(dat);
		return isAge18OrAbove(age);
	}

	private static boolean isAge18OrAbove(Integer age) {
		if (age != null && age >= 18) {
			return true;
		} else {
			return false;
		}
	}

	/* generic functions */
	public static int dayDifference(Date date1, Date date2) {
		long diff = date2.getTime() - date1.getTime();
		return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

	}
	public static int monthDifference(Date dateStart, Date dateEnd) {
		Calendar sDate = Calendar.getInstance();
		Calendar eDate = Calendar.getInstance();
		sDate.setTime(dateStart);
		eDate.setTime(dateEnd);
		
		int yearDiff = yearDifference(dateStart, dateEnd);
		int monthDiff = eDate.get(Calendar.MONTH) - sDate.get(Calendar.MONTH);

		return (yearDiff * (sDate.getMaximum(Calendar.MONTH)+1) ) + monthDiff;
	}
	public static int yearDifference(Date dateStart, Date dateEnd) {
		Calendar sDate = Calendar.getInstance();
		Calendar eDate = Calendar.getInstance();
		sDate.setTime(dateStart);
		eDate.setTime(dateEnd);
		
		int yearDiff = eDate.get(Calendar.YEAR) - sDate.get(Calendar.YEAR);

		return yearDiff;
	}	
	

	public static String formatDate(Date dat, String dateFormat) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		return simpleDateFormat.format(dat);
	}

	public static Date toDate(String datStr, String dateFormat) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		// By default, SimpleDateFormat.setLenient() is set to true, you should
		// always turn it off to make your date validation more strictly.
		formatter.setLenient(false);

		Date date = formatter.parse(datStr);
		return date;
	}

	public static String formatDate(String dateStr, String dateFormatFr, String dateFormatTo) {

		if (StringUtils.isBlank(dateStr)) {
			return "";
		}

		DateFormat fromFormat = new SimpleDateFormat(dateFormatFr);
		fromFormat.setLenient(false);
		DateFormat toFormat = new SimpleDateFormat(dateFormatTo);
		toFormat.setLenient(false);
		Date date = new Date();
		try {
			date = fromFormat.parse(dateStr);
		} catch (ParseException e) {

			e.printStackTrace();
		}

		return toFormat.format(date);
	}

	public static Date addDate(Date date, int days) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, days); // number of days to add
		return c.getTime();

	}
	
	

}
