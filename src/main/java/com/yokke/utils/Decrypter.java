package com.yokke.utils;

import javax.crypto.Cipher;

import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.SecretKeyFactory;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.spec.IvParameterSpec;

public class Decrypter {
    public static int METHOD_ENCRYPT = 1;
    public static int METHOD_DECRYPT = 2;
    public static int METHOD_OTHER = 3;

    private static String salt;
    private static String passPhrase;
    
    private static int iterationCount = 1024;
    private static int keyStrength = 256;
    
    public String getSalt() {
		return salt;
	}

	public void setSalt(String value) {
		salt = value;
	}

	public String getPassPhrase() {
		return passPhrase;
	}

	public void setPassPhrase(String value) {
		passPhrase = value;
	}
    
    private Cipher initCipher(int method) throws Exception {
    	SecretKeyFactory factory = SecretKeyFactory .getInstance("PBKDF2WithHmacSHA1");
		PBEKeySpec pbeKeySpec = new PBEKeySpec(passPhrase.toCharArray(), salt.getBytes(), iterationCount, keyStrength);
		Key secretKey = factory.generateSecret(pbeKeySpec);
		byte[] key = new byte[16];
		byte[] iv = new byte[16];
		System.arraycopy(secretKey.getEncoded(), 0, key, 0, 16);
		System.arraycopy(secretKey.getEncoded(), 16, iv, 0, 16);
		
		SecretKey secret = new SecretKeySpec(key, "AES");
		AlgorithmParameterSpec params = new IvParameterSpec(iv);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
    	cipher.init(method==METHOD_ENCRYPT?Cipher.ENCRYPT_MODE:Cipher.DECRYPT_MODE, secret, params);
    	
    	return cipher;
    }

    public String encrypt(String encstring) throws NoSuchAlgorithmException, NoSuchProviderException, GeneralSecurityException {
		Cipher cipher;
		try {
			cipher = initCipher(METHOD_ENCRYPT);
			byte[] bytesForText = cipher.doFinal(encstring.getBytes());
			String base64EncryptedData = Base64.encodeBase64String(bytesForText);
			
			return new String(base64EncryptedData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
    
    private String decrypt(String encstring) throws NoSuchAlgorithmException, NoSuchProviderException, GeneralSecurityException {
    	Cipher cipher;
		try {
			cipher = initCipher(METHOD_DECRYPT);
			byte[] encryptedString = Base64.decodeBase64(encstring.getBytes());
			byte[] bytesForText = cipher.doFinal(encryptedString);
			
			return new String(bytesForText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

    public static String main(String text, String saltNew, String passNew, int iteration, int keyStr, int method) throws Exception {
    	iterationCount = iteration;
    	keyStrength = keyStr;
    	
		return main(text, saltNew, passNew, method);
	}

    public static String main(String text, int iteration, int keyStr, int method) throws Exception {
    	iterationCount = iteration;
    	keyStrength = keyStr;
    	
		return main(text, method);
	}
    
    public static String main(String text, String saltNew, String passNew, int method) throws Exception {
    	salt = saltNew;
    	passPhrase = passNew;
    	
		return main(text, method);
	}

	public static String main(String text, int method) throws Exception {
		Decrypter decrypter = new Decrypter();
		String resultStr;
		
		if(method == METHOD_DECRYPT) resultStr = decrypter.decrypt(text);
		else resultStr = decrypter.encrypt(text);
	  
		return resultStr;
	}
}