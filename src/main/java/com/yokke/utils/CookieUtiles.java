package com.yokke.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CookieUtiles {
	

	/**
	 * Get the value of cookie
	 * @param request
	 * @param name Key value in cookie
	 * @return value
	 */
	public static String getCookieInfo(HttpServletRequest request,String name){
		Cookie cookies[] = request.getCookies() ;
        if(cookies != null){
            for(int i=0;i<cookies.length;i++){
            	if(cookies[i].getName().equals(name)){  
            		   return cookies[i].getValue(); 
            	}
            }
        }
        return null;
	}
	
	/**
	 * save value in Cookie   
	 * @param name Key value in Cookie
	 * @param value Value value in Cookie
	 * @param time Effective time of Cookie
	 * @param response
	 * @param request
	 */
	public static void setCookieInfo(String name ,String value,int time,HttpServletResponse response){
		Cookie mycookie = new Cookie(name,value); 
	      //Set the effective time in seconds (s) as the unit
		  mycookie.setMaxAge(time*60*60*24); 
	      mycookie.setPath("/") ;
	      // ref: https://www.owasp.org/index.php/HttpOnly
	      mycookie.setHttpOnly(true);
	      response.addCookie(mycookie); 
	}
	
	/**
	 *  Delete Cookie 
	 * @param request
	 * @param response
	 * @param name  Key value in Cookie
	 */
	 
	public static void deleteCookieInfo(HttpServletRequest request,HttpServletResponse response,String name){
		Cookie cookies[] = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie c = cookies[i];
				if (cookies[i].getName().equals(name)) {
					c.setPath("/");
					c.setMaxAge(0);
					response.addCookie(c);
				}
			}
		}
	}
}
