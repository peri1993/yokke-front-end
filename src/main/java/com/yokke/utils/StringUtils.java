package com.yokke.utils;

import java.text.StringCharacterIterator;

public class StringUtils {

	private static final char DEFAULT_TRIM_WHITESPACE = ' ';

	public static String breakString(String input, int[] linesWidth, int intLineNum) throws Exception {

		String[] lines = breakString(input, linesWidth);
		if (lines == null) {
			return null;
		} else {
			if (intLineNum < 0 || intLineNum > (lines.length - 1))
				throw new Exception("Invalid line number.");

			return lines[intLineNum];
		}
	}

	public static String[] breakString(String input, int[] linesWidth) throws Exception {

		// break one field into multi fields.
		String strInputLeft, strTemp;
		String[] lines = new String[linesWidth.length];
		String strSpace = " ";

		if (input == null || input.isEmpty())
			return null;

		strInputLeft = trimRight(input);

		for (int i = 0; i < linesWidth.length; i++) {
			// throw error found if arrLength[i] < 0
			if (linesWidth[i] < 0) {
				throw new Exception("Invalid line width.");
			} else {
				// break for loop
				if (strInputLeft.length() <= linesWidth[i]) {
					// assign to output array with right trim
					lines[i] = strInputLeft;
					strInputLeft = "";
					break;
				} else {

					// get substring into temp string by arrLength[i]
					String strTempStr = strInputLeft.substring(0, linesWidth[i]);
					int lastIndexOfSpace = findLastIndexOf(strTempStr, strSpace);
					// use linesWidth[i] if space not found
					if (lastIndexOfSpace < 0) {
						lastIndexOfSpace = linesWidth[i] - 1;
					}

					// lastIndexOfSpace + 1 : include space
					strTemp = strInputLeft.substring(0, lastIndexOfSpace + 1);

					// assign to output array
					lines[i] = strTemp;

					// reset strInputLeft = start from "lastIndexOfSpace + 1"
					strInputLeft = strInputLeft.substring(lastIndexOfSpace + 1);
				}
			}
		}

		if (!IsNullOrEmpty(strInputLeft))
			throw new Exception("Input string is too long.");

		return lines;
	}

	public static String concatString(String[] inputs) {
		// concat string(s) to one string
		String rtn = "";

		for (String str : inputs) {
			rtn += str;
		}

		return rtn;
	}

	private static int findLastIndexOf(String input, String target) {
		return input.lastIndexOf(target);
	}

	public static boolean IsNullOrEmpty(String input) {
		if (input == null || input.isEmpty())
			return true;
		else
			return false;
	}

	// http://www.java2s.com/Code/Java/Data-Type/Trimstringfromleftorright.htm
	public static String trimLeft(String string) {
		return trimLeft(string, DEFAULT_TRIM_WHITESPACE);
	}

	public static String trimLeft(final String string, final char trimChar) {
		final int stringLength = string.length();
		int i;

		for (i = 0; i < stringLength && string.charAt(i) == trimChar; i++) {
			/*
			 * increment i until it is at the location of the first char that
			 * does not match the trimChar given.
			 */
		}

		if (i == 0) {
			return string;
		} else {
			return string.substring(i);
		}
	}

	public static String trimRight(final String string) {
		return trimRight(string, DEFAULT_TRIM_WHITESPACE);
	}

	public static String trimRight(final String string, final char trimChar) {
		final int lastChar = string.length() - 1;
		int i;

		for (i = lastChar; i >= 0 && string.charAt(i) == trimChar; i--) {
			/*
			 * Decrement i until it is equal to the first char that does not
			 * match the trimChar given.
			 */
		}

		if (i < lastChar) {
			// the +1 is so we include the char at i
			return string.substring(0, i + 1);
		} else {
			return string;
		}
	}

	public static String HTMLEncode(String aTagFragment) {
		final StringBuffer result = new StringBuffer();
		final StringCharacterIterator iterator = new StringCharacterIterator(aTagFragment);
		char character = iterator.current();
		while (character != StringCharacterIterator.DONE) {
			if (character == '<')
				result.append("&lt;");
			else if (character == '>')
				result.append("&gt;");
			else if (character == '\"')
				result.append("&quot;");
			else if (character == '\'')
				result.append("&#039;");
			else if (character == '\\')
				result.append("&#092;");
			else if (character == '&')
				result.append("&amp;");
			else {
				// the char is not a special one
				// add it to the result as is
				result.append(character);
			}
			character = iterator.next();
		}
		return result.toString();
	}
}
