package com.yokke.utils;

import java.time.LocalDate;
import java.time.Period;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberUtils {

	private static float TOLERANCE = 0.0001f;
	public static boolean greaterThanOrEqualsTo(double d1, double d2){
		return (d1 > d2 || equalsTo(d1, d2));
	}

	public static boolean equalsTo(double d1, double d2) {
		return Math.abs(d1-d2) < TOLERANCE;
	}
	
	public static int calculateAge(LocalDate birthDate, LocalDate currentDate) {
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }
	
	public static String addMobileNumberPrefix(String mobileNumber) {
		if (mobileNumber.length()>5) {
			String numberCountryPrefix = mobileNumber.substring(0, 2);
			String numberNullPrefix	   = mobileNumber.substring(0, 1);
			if (!numberCountryPrefix.equals("62")) {
				if (numberNullPrefix.equals("0")) {
					mobileNumber = "62" + mobileNumber.substring(1);
				} else {
					mobileNumber = "62" + mobileNumber;
				}
			}
		}
		return mobileNumber;
	}
	
	public static boolean validateMobileNumber(String phoneNumber) {
		Pattern pattern = Pattern.compile("^628\\d{6,10}");
	    Matcher matcher = pattern.matcher(phoneNumber);
	    
	    if (matcher.matches()) {
	    	return true;
	    } 
	    return false;
	}
	
	public static boolean getStringNumericValidation(String str){
		if(str == null || str.equals("")) return false;
		
		str = str.trim();
		
		char[] chars = str.toCharArray();
		
		for(char c : chars){
			if(!Character.isDigit(c)){
				return false;
			}
		}
		
		
		return true;
	}
	
}
