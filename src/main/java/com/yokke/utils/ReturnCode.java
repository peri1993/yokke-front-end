package com.yokke.utils;

public enum ReturnCode {
	
	LDAP_SUCCESS("0","SUCCESS")
	,LDAP_INVALID_PASSWORD("97","INVALID_PASSWORD")
	,SUCCESS("01","SUCCESS")
	,DATA_NOT_FOUND("02","DATA NOT FOUND")
	,BAD_REQUEST("03","BAD REQUEST")
	,TIME_OUT("04","REQUEST TIME OUT")
	;
	
	private String code;
	private String desc;

	ReturnCode(String code, String desc){
		this.code = code;
		this.desc = desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}
