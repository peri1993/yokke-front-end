package com.yokke.utils;

import java.util.Random;

public class ReferralUtils {

	private static final char[] symbols;

	static {
		StringBuilder tmp = new StringBuilder();
		for (char ch = '0'; ch <= '9'; ++ch) {
			tmp.append(ch);
		}
		for (char ch = 'A'; ch <= 'Z'; ++ch) {
			tmp.append(ch);
		}
		symbols = tmp.toString().toCharArray();
	}

	public static String generateReferralCode() {
		Random random = new Random();
		char[] buf = new char[6];
		int sum = 0;
		for (int idx = 0; idx < buf.length; ++idx) {
			char c = symbols[random.nextInt(symbols.length)];
			buf[idx] = c;
			sum += c;
		}

		String str = new String(buf);
		str = str + (sum % 10);
		return str;
	}

	public static boolean verifyReferralCode(String str){
		if(!org.apache.commons.lang3.StringUtils.isBlank(str)){
			final int checkDigitIdx = str.length()-1;
			final String checkDigit = ""+str.charAt(checkDigitIdx);
			int sum = 0;
			for(int i = 0; i < checkDigitIdx; i++){
				sum += str.charAt(i);
			}
			final boolean isNumeric = org.apache.commons.lang3.StringUtils.isNumeric(checkDigit);
			return (isNumeric && Integer.valueOf(""+checkDigit) == (sum % 10));
		}
		return false;
	}
	
	//tambahan deudi ifc 28 September 2017
	// untuk penambahan refferal code tanpa generate refferal code
	public static boolean verifyRefferalNewCode(String str){
		
		String patternCode = "EC"; 
		if(!org.apache.commons.lang3.StringUtils.isBlank(str)){
			if(str.length() == 6){
				if(str.substring(0,2).equalsIgnoreCase(patternCode)){
					return true;
				}
			}
		}
		return false;
	}

}
