package com.yokke.utils;

import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

public class Decryptor {

	@SuppressWarnings("unused")
	private static final Pattern pattern = Pattern.compile("([^\\s]+)\\s*(.*)");
	
	private String salt;

	private String password;

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@SuppressWarnings("unused")
	private String value(String pair) {
        final String[] keyValue = StringUtils.split(pair, "=");
        return keyValue[1];
    }
	
	@SuppressWarnings("unused")
	private String decryptAsString(String encstring) throws NoSuchAlgorithmException,
			NoSuchProviderException, GeneralSecurityException {

		SecretKeyFactory factory = SecretKeyFactory
				.getInstance("PBKDF2WithHmacSHA1");
		PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray(),
				salt.getBytes(), 1000, 384);
		Key secretKey = factory.generateSecret(pbeKeySpec);
		byte[] key = new byte[16];
		byte[] iv = new byte[16];
		System.arraycopy(secretKey.getEncoded(), 0, key, 0, 16);
		System.arraycopy(secretKey.getEncoded(), 16, iv, 0, 16);

		SecretKey secret = new SecretKeySpec(key, "AES");
		AlgorithmParameterSpec params = new IvParameterSpec(iv);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		cipher.init(Cipher.DECRYPT_MODE, secret, params);
		byte[] encryptedString = Base64.decodeBase64(encstring.getBytes());
		byte[] bytesForText = cipher.doFinal(encryptedString);

		return new String(bytesForText);
	}

}
