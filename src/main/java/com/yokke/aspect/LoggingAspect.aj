package com.yokke.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public aspect LoggingAspect {

	private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Before("(within(com.yokke.web..*) && @annotation(org.springframework.web.bind.annotation.RequestMapping))"
			+ " || (within(com.yokke.ws..*) && @annotation(javax.ws.rs.Path))")
	public void logAllControllerEntryPoints(ProceedingJoinPoint jp) {		
		logger.trace("executing " + jp.getSignature().toShortString());
	}

}
