package com.yokke.constant;

public class ConstantProperties {

	public final static String INBOX_KEY = "inbox";
	public final static String REQUEST_LEAVE_KEY = "requestLeave";
	public final static String HISTORY_LEAVE_KEY = "historyLeave";
	public final static String REQUEST_PERMIT_KEY = "requestPermit";
	public final static String HISTORY_PERMIT_KEY = "historyPermit";
	public final static String ASSIGNER_TASK_KEY = "assignerTask";
	public final static String NOTIFICATION_KEY = "notification";
	public final static String ROLE_MANAGEMENT_KEY = "roleManagement";
	public final static String USER_MANAGEMENT_KEY = "userManagement";
	public final static String HISTORY_ATTENDANCE_KEY = "historyAttendance";
	public final static String LIST_ATTENDANCE_KEY = "listAttendance";
	public final static String QR_ATTENDANCE_KEY = "qrAttendance";
	public final static String BRANCH_CONF_KEY = "branchConfig";
	public final static String ASSIGNER_HISTORY_KEY = "assignerHistory";
	public final static String ALL_HISTORY_ASSIGNER_KEY = "allAssignerHistory";
	public final static String INFORMATION_WORK_HOURS_KEY = "InformationWorkHours";

	public final static String INBOX_VALUE = "Inbox";
	public final static String REQUEST_LEAVE_VALUE = "Request Leave";
	public final static String HISTORY_LEAVE_VALUE = "History Leave";
	public final static String REQUEST_PERMIT_VALUE = "Request Permit";
	public final static String HISTORY_PERMIT_VALUE = "History Permit";
	public final static String ASSIGNER_TASK_VALUE = "Assigned Task";
	public final static String NOTIFICATION_VALUE = "Notification";
	public final static String ROLE_MANAGEMENT_VALUE = "Role Management";
	public final static String USER_MANAGEMENT_VALUE = "User Management";
	public final static String HISTORY_ATTENDANCE_VALUE = "Attendance History";
	public final static String LIST_ATTENDANCE_VALUE = "List Attendance";
	public final static String QR_ATTENDANCE_VALUE = "Qr Attendance";
	public final static String BRANCH_CONF_VALUE = "Branch Config";
	public final static String ASSIGNER_HISTORY_VALUE = "Assigner History";
	public final static String ALL_HISTORY_ASSIGNER_VALUE = "All Assigner History";
	public final static String INFORMATION_WORK_HOURS_VALUE = "Information Work Hours";

	public static String getModuleCategoryNameByName(String key) {
		if (INBOX_KEY.equals(key)) {
			return INBOX_VALUE;
		} else if (REQUEST_LEAVE_KEY.equals(key)) {
			return REQUEST_LEAVE_VALUE;
		} else if (HISTORY_LEAVE_KEY.equals(key)) {
			return HISTORY_LEAVE_VALUE;
		} else if (REQUEST_PERMIT_KEY.equals(key)) {
			return REQUEST_PERMIT_VALUE;
		} else if (HISTORY_PERMIT_KEY.equals(key)) {
			return HISTORY_PERMIT_VALUE;
		} else if (ASSIGNER_TASK_KEY.equals(key)) {
			return ASSIGNER_TASK_VALUE;
		} else if (NOTIFICATION_KEY.equals(key)) {
			return NOTIFICATION_VALUE;
		} else if (ROLE_MANAGEMENT_KEY.equals(key)) {
			return ROLE_MANAGEMENT_VALUE;
		} else if (USER_MANAGEMENT_KEY.equals(key)) {
			return USER_MANAGEMENT_VALUE;
		} else if (HISTORY_ATTENDANCE_KEY.equals(key)) {
			return HISTORY_ATTENDANCE_VALUE;
		} else if (LIST_ATTENDANCE_KEY.equals(key)) {
			return LIST_ATTENDANCE_VALUE;
		} else if (QR_ATTENDANCE_KEY.equals(key)) {
			return QR_ATTENDANCE_VALUE;
		} else if(BRANCH_CONF_KEY.equals(key)) {
			return BRANCH_CONF_VALUE;
		} else if(ASSIGNER_HISTORY_KEY.equals(key)){
			return ASSIGNER_HISTORY_VALUE;
		} else if(ALL_HISTORY_ASSIGNER_KEY.equals(key)) {
			return ALL_HISTORY_ASSIGNER_VALUE;
		} else if(INFORMATION_WORK_HOURS_KEY.equals(key)) {
			return INFORMATION_WORK_HOURS_VALUE;
		}
			
		return null;
	}

	public static String setFlagMenu(String key) {
		if (INBOX_VALUE.equals(key)) {
			return INBOX_KEY;
		} else if (REQUEST_LEAVE_VALUE.equals(key)) {
			return REQUEST_LEAVE_KEY;
		} else if (HISTORY_LEAVE_VALUE.equals(key)) {
			return HISTORY_LEAVE_KEY;
		} else if (REQUEST_PERMIT_VALUE.equals(key)) {
			return REQUEST_PERMIT_KEY;
		} else if (HISTORY_PERMIT_VALUE.equals(key)) {
			return HISTORY_PERMIT_KEY;
		} else if (ASSIGNER_TASK_VALUE.equals(key)) {
			return ASSIGNER_TASK_KEY;
		} else if (NOTIFICATION_VALUE.equals(key)) {
			return NOTIFICATION_KEY;
		} else if (ROLE_MANAGEMENT_VALUE.equals(key)) {
			return ROLE_MANAGEMENT_KEY;
		} else if (USER_MANAGEMENT_VALUE.equals(key)) {
			return USER_MANAGEMENT_KEY;
		} else if (HISTORY_ATTENDANCE_VALUE.equals(key)) {
			return HISTORY_ATTENDANCE_KEY;
		} else if (LIST_ATTENDANCE_VALUE.equals(key)) {
			return LIST_ATTENDANCE_KEY;
		} else if (QR_ATTENDANCE_VALUE.equals(key)) {
			return QR_ATTENDANCE_KEY;
		} else if(BRANCH_CONF_VALUE.equals(key)) {
			return BRANCH_CONF_KEY;
		} else if(ASSIGNER_HISTORY_VALUE.equals(key)) {
			return ASSIGNER_HISTORY_KEY;
		} else if(ALL_HISTORY_ASSIGNER_VALUE.equals(key)) {
			return ALL_HISTORY_ASSIGNER_KEY;
		} else if(INFORMATION_WORK_HOURS_VALUE.equals(key)) {
			return INFORMATION_WORK_HOURS_KEY;
		}

		return null;
	}

}
