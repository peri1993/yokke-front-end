package com.yokke.listener;

import java.util.Enumeration;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yokke.connector.request.OwnerRequest;
import com.yokke.web.AwareSessionAttributes;

public class SessionListener implements HttpSessionListener, AwareSessionAttributes {

	@SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(SessionListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent evt) {
	}

	private static boolean isBlank(String str){
		return StringUtils.isBlank(str);
	}
	
	@SuppressWarnings("unused")
	@Override
	public void sessionDestroyed(HttpSessionEvent evt) {
		final HttpSession session = evt.getSession();
//		logger.debug("session("+session.getId()+") is being destroyed");
		
		final Enumeration<String> attrNames = session.getAttributeNames();
		while(attrNames.hasMoreElements()){
			Object attribute = session.getAttribute(attrNames.nextElement());
		}

	}

	@SuppressWarnings("unused")
	private boolean canContactOwner(OwnerRequest owner) {
		return (owner != null 
				&& (!isBlank(owner.getMobile()) || !isBlank(owner.getEmail())));
	}

}
