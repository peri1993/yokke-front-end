<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.QrAttendancePostController"
	scope="page" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<script src="${context}/assets/resources/qrcodescanner/instascan.min.js"></script>

<div class="content-wrapper" style="background: white"
	ng-controller="CreateAttendanceController">
	<input type="hidden" ng-model="_csrf.token"
		ng-init="_csrf.token = '${_csrf.token}'" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	<section class="content-header">
		<h1>
			<h1>
				<b>Scan Here</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message
						key="label.header.module.dashboard" /></a></li>
			<li><a href="${context}${cons.MAIN_URL}">List Data Employee</a></li>
			<li class="active"><fmt:message key="label.name.list" /></li>
		</ol>
	</section>
	<div ng-if="errorFlag === 'Y'" class="alert alert-danger ng-hide"
		id="add-item-error" ng-show=hideError>
		<center></center>
	</div>
	<section ng-if="flagSchedule == 'false'" class="content"
		style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title"></strong>
			</div>
			<div class="panel-body table-responsive text-nowrap" align="center">
				<div ng-if="hideCamera == false" align="center">
					<b class="card-title">Scan Qr Code</b>
					<p class="card-text">For Easier Scan Qr For Employee Attendance</p>
					<span class="btn button-scan-qr"> <img
						src="${context}/assets/img/barcode.png" alt="barcode-mega"
						ng-click="clickCamera()">
					</span><br>
					<!-- <button type="submit" class="btn btn-primary mb-2" ng-click="input()" data-target="#listEmp" data-toggle="modal">Post</button> -->
				</div>
				<div class="ng-hide" ng-show="hideCamera"><jsp:include
						page="/WEB-INF/views/cms/attendance/scannerQr/camera-qr.jsp"></jsp:include></div>
				<table class="table table-hover">
					<thead>
						<th>NIP</th>
						<th>Nama Karyawan</th>
						<th>Tanggal</th>
						<th>Jam Masuk</th>
						<th>Jam Pulang</th>
						<th>Lokasi</th>
					</thead>
					<tbody>
						<tr ng-show="hideData == true" class="ng-hide">
							<td>{{datasAbsence.empId}}</td>
							<td>{{datasAbsence.fullName}}</td>
							<td>{{datasAbsence.startDate}}</td>
							<td>{{datasAbsence.timeStart}}</td>
							<td>{{datasAbsence.timeEnd}}</td>
							<td>{{datasAbsence.empSubAreaDescription}}</td>
						</tr>
						<tr ng-show="hideData == false" class="ng-hide">
							<td colspan="8"><center>Anda belum melakukan absen hari ini</center></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>

	<section ng-if="flagSchedule == 'true'" class="content"
		style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title"></strong>
			</div>
			<div class="panel-body table-responsive text-nowrap" align="center">
				<div ng-if="hideContent == false" align="center">
					<b class="card-title">Silahkan Mengambil Foto Anda</b>
					<p class="card-text">*ekstensi file berupa (JPEG, JPG, PNG)</p>
					<span class="btn button-scan-qr"> <img
						src="${context}/assets/img/selfie.png" alt="barcode-mega"><input
						type="file" id="file" name="file" accept="image/x-png,image/jpeg"
						capture=environment
						onchange="angular.element(this).scope().getFileDetails(this)">
					</span><br>
					<!-- <button type="submit" class="btn btn-primary mb-2" ng-click="input()" data-target="#listEmp" data-toggle="modal">Post</button> -->
				</div>
				<div class="ng-hide" ng-show="hideContent == true">
					<div id="imgTest"></div>
				</div>
				<br>
				<span class="btn button-scan-qr" class="ng-hide"
					ng-show="hideContent == true"><button class="btn btn-primary mb-2">Re-Take</button><input
						type="file" id="file" name="file" accept="image/x-png,image/jpeg"
						capture=environment
						onchange="angular.element(this).scope().getFileDetails(this)">
					</span>
				<button type="submit" class="btn btn-primary mb-2"
					ng-click="uploadImg()" class="ng-hide"
					ng-show="hideContent == true">Absen</button>
				<table class="table table-hover">
					<thead>
						<th>NIP</th>
						<th>Nama Karyawan</th>
						<th>Tanggal</th>
						<th>Jam Masuk</th>
						<th>Jam Pulang</th>
						<th>Lokasi</th>
					</thead>
					<tbody>
						<tr ng-show="hideData == true" class="ng-hide">
							<td>{{datasAbsence.empId}}</td>
							<td>{{datasAbsence.fullName}}</td>
							<td>{{datasAbsence.startDate}}</td>
							<td>{{datasAbsence.timeStart}}</td>
							<td>{{datasAbsence.timeEnd}}</td>
							<td>{{datasAbsence.empSubAreaDescription}}</td>
						</tr>
						<tr ng-show="hideData == false" class="ng-hide">
							<td colspan="8"><center>List Task Empty</center></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>

	<!-- The modal -->
	<div class="modal fade" id="listEmp" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Konfirmasi</h4>
				</div>
				<div class="modal-body">
					<div ng-if="loadingAnim === false">{{status}}-{{message}}</div>
					<div ng-if="loadingAnim === true" align="center">
						<img style="width: 10%;height: 10%" src="${context}/assets/img/loader.gif"/>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal" ng-click="refresh()">Close</button>
				</div>
			</div>
		</div>
	</div>

</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>
