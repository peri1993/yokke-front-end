<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.ListAttendanceController"
	scope="page" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<div class="content-wrapper" style="background: white"
	ng-controller="CreateScheduleController">
	<input type="hidden" ng-model="_csrf.token"
		ng-init="_csrf.token = '${_csrf.token}'" name="${_csrf.parameterName}"
		value="${_csrf.token}" /> <input type="hidden" name="username"
		id="username" value="${username}" /> <input type="hidden"
		name="fullname" id="fullname" value="${fullname}" />
		
		
	<section class="content-header">
		<h1>
			<h1>
				<b>Input Data Employee</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message
						key="label.header.module.dashboard" /></a></li>
			<li><a href="${context}${cons.MAIN_URL}">List Data Employee</a></li>
			<li class="active"><fmt:message key="label.name.list" /></li>
		</ol>
	</section>
	<div ng-if="errorFlag === 'Y'" class="alert alert-danger ng-hide"
		id="add-item-error" ng-show=hideError>
		<center></center>
	</div>
	<section class="content" style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">Task List</strong>
			</div>
			<div id=inboxp>
				<div class="panel-body table-responsive"
					style="overflow-x: !important;">
					<table class="table table-hover">
						<thead>
							<form class="form-inline">
								<div class="form-group col-sm-2">
									<label for="empId">Employee ID</label> <label for="fullname"
										style="margin-top: 25px">Employee Name</label>
								</div>
								<div class="form-group col-sm-3">
									<input type="text" class="form-control form-control-sm"
										id="empId" disabled="disabled" value="${username}"> <input
										type="text" class="form-control form-control-sm"
										style="margin-top: 15px" id="fullname" disabled="disabled"
										value="${fullname}">
								</div>
							</form>
							<tr>
								<th style="width: 130px;">Start Date</th>
								<th style="width: 130px;">End Date</th>
								<th style="width: 130px;">Time Start</th>
								<th style="width: 130px">Time End</th>
								<th style="width: 130px">Address</th>
								<th style="width: 20px"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input type="date" class="form-control" ng-model="startDate" /></td>
								<td><input type="date" class="form-control" ng-model="endDate" /></td>
								<td><div class="input-group clockpicker">
										<input type="text" class="form-control clockpicker" ng-model="timeStart" id="timeStart">
										<span class="input-group-addon"> <span
											class="glyphicon glyphicon-time"></span>
										</span>
									</div></td>
								<td><div class="input-group clockpicker">
										<input type="text" class="form-control clockpicker" ng-model="timeEnd" id="timeEnd">
										<span class="input-group-addon"> <span
											class="glyphicon glyphicon-time"></span>
										</span>
									</div></td>
								<td><input type="text" class="form-control" ng-model="address" /></td>
								<td align="right"><a href="#" style="margin-top: 5px"
									class="btn bg-orange btn-flat btn-small" ng-click="input()"> <i
										class="fa fa-fw fa-save"></i>
								</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<%-- <div class="footer">
			<jsp:include
				page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
		</div> --%>
	</section>
	<section class="content" style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#listBody">
				<strong class="panel-title">List Jadwal</strong>
			</div>
			<div id=listBody>
				<div class="panel-body table-responsive"
					style="overflow-x: !important;">
					<table class="table table-hover">
						<thead>
							<tr>
								<th style="width: 130px;">Start Date</th>
								<th style="width: 130px;">End Date</th>
								<th style="width: 130px;">Time Start</th>
								<th style="width: 130px">Time End</th>
								<th style="width: 130px">Address</th>
								<th style="width: 20px"></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="res in datas" ng-show="hideData" class="ng-hide">
								<td>{{res.startDate}}</td>
								<td>{{res.endDate}}</td>
								<td>{{res.timeStart}}</td>
								<td>{{res.timeEnd}}</td>
								<td>{{res.address}}</td>
								<td align="right">
									<a href="#" class="btn bg-orange btn-flat btn-small" id="detail" ng-click="deleteData(res)">
										<i class="fa fa-fw fa-trash"></i>
									</a>
								</td>
							</tr>
							<tr ng-show="hideData == false" class="ng-hide">
								<td colspan="8"><center>List Jadwal Empty</center></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<%-- <div class="footer">
			<jsp:include
				page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
		</div> --%>
	</section>

	<!-- The modal -->
	<div class="modal fade" id="listEmp" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Konfirmasi</h4>
				</div>
				<div class="modal-body">{{status}}-{{message}}</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal" ng-click="refresh()">Close</button>
				</div>
			</div>
		</div>
	</div>

</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>

<script type="text/javascript" src="${context}/assets/resources/Clocktimepicker/bootstrap-clockpicker.min.js"></script>
<script type="text/javascript" src="${context}/assets/resources/Clocktimepicker/bootstrap-clockpicker.js"></script>
<link rel="stylesheet" href="${context}/assets/resources/Clocktimepicker/bootstrap-clockpicker.css">
<script>
	$('.clockpicker').clockpicker({
		autoclose:true,
		donetext:'Done'
	});
</script>