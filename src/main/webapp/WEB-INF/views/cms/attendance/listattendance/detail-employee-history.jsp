<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes" 	scope="page" />
<jsp:useBean id="cons" class="com.yokke.web.controller.AttendanceHistoryController" 	scope="page" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<div class="content-wrapper" style="background: white" ng-controller="HistoryEmployeeAttendanceController">
	<input type="hidden" name="username"
		id="username" value="${username}" /> 
	<input type="hidden"
		name="fullname" id="fullname" value="${fullname}" />
	<section class="content-header">
		<h1>History Absensi</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message key="label.header.module.dashboard"/></a></li>
			<li><a href="${context}${cons.MAIN_URL}">Detail Employee Data</a></li>
			<li class="active"><fmt:message key="label.name.list"/></li>
		</ol>
	</section>
	<section class="content" style="background: white">
	<div class="row" ng-if="deleteFlag === 'Y'" >
			<div class="alert alert-danger ng-hide" id="add-item-error" ng-show="hideData">
				<center></center>
			</div>
	</div>
	<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">Task List</strong>
			</div>
	<div id=inboxp>
	<div class="panel-body table-responsive text-nowrap" 
		style="overflow-x:overlay;max-height: 500px">
			<div class="form-group">
							<div id="" class="col-sm-3" style="width: 220px;">
								<input type="month" placeholder="End date" ng-model="date" ng-change="onChangeValue(date)"
												class="form-control form-control-sm" />
							</div>
							<div id="" class="col-sm-3" style="width: 100px;">
								<a type="submit" class="btn btn-primary"
										ng-click="search()"><fmt:message
											key="label.name.submit" /></a>
							</div>
						</div>
			<br/><br/>
			<div class="">
			
						<table class="table table-hover">
							<thead>
								<tr>
									<th>ID Pegawai</th>
									<th>Nama Pegawai</th>
									<th>Tanggal</th>
									<th>Jam Masuk</th>
									<th>Jam Pulang</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
									<tr ng-repeat="res in datas | orderBy:column:reverse">
										<!-- <td>{{res.role_id}}</td> -->
										<td>{{res.empId}}</td>
										<td>{{res.fullName}}</td>
										<td>{{res.startDate}}</td>
										<td>{{res.timeStart}}  
											<a ng-if="res.fileIn != null" href="${context}${cons.DOWNLOAD_FILE_URL}{{linkAction.filename+res.fileIn+linkAction.directory+res.fileDirIn+linkAction.status+'In'}}">
												<i class="fa fa-fw fa-archive"></i>
											</a>
											<a style="color: black;" ng-if="res.fileIn != null" href="#" ng-click="maps(res.latitudeCode, res.longitudeCode)">
												<i class="fa fa-fw fa-map-marker"></i>
											</a>
										</td>
										<td>{{res.timeEnd}}
											<a ng-if="res.fileOut != null" href="${context}${cons.DOWNLOAD_FILE_URL}{{linkAction.filename+res.fileIn+linkAction.directory+res.fileDirOut+linkAction.status+'Out'}}">
												<i class="fa fa-fw fa-archive"></i>
											</a>
											<a style="color: black;" ng-if="res.fileOut != null" href="#" ng-click="maps(res.latitudeCodeOut, res.longitudeCodeOut)">
												<i class="fa fa-fw fa-map-marker"></i>
											</a>
										</td>
									</tr>
									<tr ng-if="emptyData === true"><td colspan="7"><center>History tidak ditemukan !</center></td></tr>
							</tbody>
						</table>
				</div>
			</div>
		</div>
		</div>
	</section>
</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>

									
									
									
									
									