<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.InformationWorkHoursController" scope="page" />
<div class="content-wrapper" style="background: white"	ng-controller="InformationWorkHours">
	<input type="hidden" ng-model="_csrf.token" ng-init="_csrf.token = '${_csrf.token}'" name="${_csrf.parameterName}" value="${_csrf.token}" />
	
	<section class="content-header">
		<h1>
			<h1>
				<b>Informasi Jam Kerja Karyawan</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message key="label.header.module.dashboard"/></a></li>
			<li><a href="${context}${cons.MAIN_URL}">List Assigned Task</a></li>
			<li class="active">Detail</li>
		</ol>
	</section>
	<div ng-if="errorFlag === 'Y'" class="alert alert-danger ng-hide"
		id="add-item-error" ng-show=hideError>
		<center>{{status}} - {{message}}</center>
	</div>
	<section class="content" style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">Jam Masuk Karyawan</strong>
			</div>
			<div id=inboxp>
				<div class="content" style="background-color: white">
					<div class="row">
						<div id="attendance" class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">Data Jam Masuk Karyawan</div>
								<form>
									<div class="form-group row"
										style="margin-left: 0px; margin-top: 10px">
										<label class="col-sm-5 col-form-label-sm" for="lq">Employee ID</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												id="id" ng-model="datas.empId" disabled />
										</div>
									</div>
									<div class="form-group row" style="margin-left: 0px">
										<label class="col-sm-5 col-form-label-sm" for="rq">FullName</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												id="name" ng-model="datas.fullname" disabled />
										</div>
									</div>
									<div class="form-group row" style="margin-left: 0px">
										<label class="col-sm-5 col-form-label-sm" for="qt">Jam Masuk</label>
										<div class="col-sm-3">
											<input type="text" class="form-control form-control-sm"
												id="subject" ng-model="datas.officeHours.startTime" disabled />
										</div>
										<div class="col-sm-3">
											<input type="text" class="form-control form-control-sm"
												id="subject" ng-model="datas.officeHours.locationExtend" disabled />
										</div>
									</div>
									<div class="form-group row" style="margin-left: 0px">
										<label class="col-sm-5 col-form-label-sm" for="qt">Jam Pulang</label>
										<div class="col-sm-3">
											<input type="text" class="form-control form-control-sm"
												id="subject" ng-model="datas.officeHours.endtTime" disabled />
										</div>
										<div class="col-sm-3">
											<input type="text" class="form-control form-control-sm"
												id="subject" ng-model="datas.officeHours.locationExtend" disabled />
										</div>
									</div>
								</form>
							</div>
							<div class="col-sm-4">
								<a ng-click="update()" type="submit" class="btn btn-primary" style="margin-right: 20px">Ganti Jam Masuk Kerja </a> 
							</div>
						</div>
					</div>
				</div>
				<div ng-if="uiDetail" class="content" style="background-color: white">
					<div class="row">
						<div id="attendance" class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">Jadwal Masuk Yang Tersedia</div>
								<form>
									<div class="form-group row"
										style="margin-left: 0px; margin-top: 10px">
										<label class="col-sm-5 col-form-label-sm" for="lq">Approval</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												id="id" ng-model="approval.approval1" disabled />
										</div>
									</div>
									<div class="form-group row"
										style="margin-left: 0px; margin-top: 10px">
										<label class="col-sm-5 col-form-label-sm" for="lq">Nama Approval</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												id="id" ng-model="approval.nameApproval1" disabled />
										</div>
									</div>
									<div class="form-group row"
										style="margin-left: 0px; margin-top: 10px">
										<label class="col-sm-5 col-form-label-sm" for="lq">Approval</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												id="id" ng-model="approval.approval2" disabled />
										</div>
									</div>
									<div class="form-group row"
										style="margin-left: 0px; margin-top: 10px">
										<label class="col-sm-5 col-form-label-sm" for="lq">Nama Approval</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												id="id" ng-model="approval.nameApproval2" disabled />
										</div>
									</div>
									<div class="form-group row"
										style="margin-left: 0px; margin-top: 10px">
										<label class="col-sm-5 col-form-label-sm" for="lq">Pilih Jadwal Masuk</label>
										<div class="col-sm-6">
											<select class="form-control" ng-model="selectedOption" ng-options="item as item.desc for item in list">
												<option value="">--- Pilih Jadwal ---</option>
  											</select>
										</div>
									</div>
									<div class="form-group row"
										style="margin-left: 0px; margin-top: 10px">
										<label class="col-sm-5 col-form-label-sm" for="lq">Alasan</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												id="id" ng-model="datas.officeHours.reason" />
										</div>
									</div>
								</form>
							</div>
							<div class="col-sm-2">
								<a ng-click="submit(selectedOption, datas, approval)" type="submit" class="btn btn-success" style="margin-right: 20px"
								data-toggle="modal" data-target="#infoModal">Ajukan </a> 
							</div>
							<div class="col-sm-2">
								<div class="form-group col-sm-3">
									<a ng-click="cancel()" type="submit" class="btn btn-danger" style="margin-right: 20px">Batal </a>		
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- The modal -->
	<div class="modal fade" id="infoModal" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Informasi</h4>
				</div>
				<div class="modal-body">
				<strong>{{status}}-{{message}} !</strong><br> 
				<strong>No.Pengajuan :{{instanceId}}-{{taskId}}</strong>
				</div>
				<div class="modal-footer">
					<button type="button" ng-click="refresh()" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>