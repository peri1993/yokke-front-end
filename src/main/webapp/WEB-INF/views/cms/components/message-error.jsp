<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes" 	scope="page" />

<%String status = String.valueOf(request.getAttribute("errorStatus")); %>
<%String message = (String) request.getAttribute("errorMessage"); %>
<c:if test="<%=status != null && message != null %>">
	<div class="alert alert-danger display-none" id="add-item-error">
		<center><%=status%> - <%=message%></center>
	</div>
</c:if>
