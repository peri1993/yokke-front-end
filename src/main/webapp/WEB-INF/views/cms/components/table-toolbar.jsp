<div class="row">
	<div class="col-sm-2 col-xs-12 pull-right">
		<a class="btn btn-sm btn-block btn-social btn-dropbox"
			href="${context}{{linkAction.add}}"> <i class="fa fa-plus"></i> Add New
		</a>
	</div>
	
	<div class="col-sm-4 col-xs-12"></div>
	<div class="col-sm-6 col-xs-12">
		<div class=" input-group">
			<div class="input-group-btn">
				<select name="repeatSelect" id="columnSelected"
					class="form-control input-sm" ng-model="filterData.columnSelected"
					style="width: 100%" class="input-white-plus">

					<option ng-repeat="option in filterData.availableColumn"
						value="{{option.id}}">{{option.name}}</option>
				</select>
			</div>
			<input type="text" id="form-field-1" placeholder="Keyword"
				ng-keypress="$event.keyCode === 13 ? loadDataSearch(currentPage,qlue) : true"
				class="form-control input-sm input-white-plus" ng-model="qlue">
			<span class="input-group-btn">
				<button type="button" ng-click="loadDataSearch(currentPage,qlue)"
					class="btn btn-default btn-sm">
					<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
				</button>
			</span>
		</div>
	</div>
</div>