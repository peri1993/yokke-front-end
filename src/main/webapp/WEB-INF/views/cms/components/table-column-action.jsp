<td style="width: 100px;" align="center">
	<a href="{{linkAction.view + res[keyIndex]}}" class="btn bg-blue btn-flat btn-small">
		<i class="fa fa-fw fa-eye"></i>
	</a>
	<a href="{{linkAction.edit + res[keyIndex]}}" class="btn bg-orange btn-flat btn-small">
		<i class="fa fa-fw fa-edit"></i>
	</a>
	<a  href="" ng-click="onDeleteClick(res[keyIndex])" class="btn bg-maroon btn-flat btn-small">
		<i class="fa fa-fw fa-trash"></i>
	</a>
	<!-- 
	<a href="{{linkAction.view + res[keyIndex]}}"><i class="fa fa-fw fa-eye"></i></a>&nbsp;&nbsp;
	<a href="{{linkAction.edit + res[keyIndex]}}"><i class="fa fa-fw fa-edit"></i></a>&nbsp;&nbsp;
	<a href="" ng-click="onDeleteClick(res[keyIndex])"
		style="color:red;"><i class="fa fa-fw fa-trash"></i></a>
	 -->
</td>	