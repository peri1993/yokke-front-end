<div class="row">
	<div class="col-xs-4">
		Showing page {{pageNumber + 1}} 
		of {{totalPage}} pages
	</div>
	<div class="col-xs-12 pull-right" style="text-align: center">
		<!-- <ul uib-pagination total-items="datas.totalPage" ng-model="currentPage"
			class="pagination-sm" items-per-page="datas.pageSize"
			num-pages="datas.pageNumber"></ul> -->
	
		
		<!-- <ul uib-pagination total-items="totalPage" ng-model="secPage" 
			max-size="1" items-per-page="1" class="pagination-sm" 
			boundary-links="true" num-pages="pageNumber"></ul> -->
			
		<nav aria-label="Page navigation example">
		  <ul class="pagination">
		    <li class="page-item">
		      <a class="page-link" href="#" aria-label="Previous" ng-click="pagging(0)">
		        <!-- <span aria-hidden="true">&laquo;</span> -->
		         <span>First</span>
		      </a>
		      <a ng-if="pageNumber > 0" class="page-link" href="#" aria-label="Previous" ng-click="pagging(pageNumber-1)">
			    <span>< Previous </span>
		      </a>
		    </li>
        	<li class="page-item"><a class="page-link" href="#">{{pageNumber+1}}</a></li>
		    	<li class="page-item">
		      		<a ng-if="last != true" class="page-link" href="#" aria-label="Next" ng-model="secPage" ng-click="pagging(pageNumber+1)">
		        		<span>Next ></span>
		      		</a>
		      		<a class="page-link" href="#" aria-label="Next" ng-model="secPage" ng-click="pagging(totalPage-1)">
		        		<span>Last > </span>
		      		</a>
		    	</li>
		</ul>
		</nav>	
	</div>
</div>