<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<c:set var="message" value="${message}" scope="session" />
<c:set var="description" value="${description}" scope="session" />
<jsp:useBean id="cons" class="com.yokke.web.controller.LoginController"
	scope="page" />
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/>
<title>MEGA ESS</title>
<link rel="icon" type="image/gif/png"
	href="${context}/assets/img/logoMega.png">
<link rel="stylesheet"
	href="${context}/assets/resources/AdminLTE2/bootstrap/css/bootstrap.min.css">
<!-- 
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
 -->
<link rel="stylesheet"
	href="${context}/assets/cms/app/css/loader.css">
<link rel="stylesheet"
	href="${context}/assets/resources/AdminLTE2/dist/css/AdminLTE.min.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <%-- <script src="${context}/assets/cms/app/js/loader.js"></script> --%>
  <%-- <script src="${context}/assets/cms/app/js/loader.js"></script> --%>
</head>
<body>
	<div class="loading-gif" id="overlay">
		<img src="${context}/assets/img/loader.gif">	
	</div>
	<div id="message" class="hold-transition login-page" 
		style="background-image: url(${context}/assets/img/background.png); display:none;">
		<br>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-xs-12"
					style="background-image: url(${context}/assets/img/headerBackground.png); margin-top: -20px;">
					</br>
					<div class="col-sm-4 col-md-4 col-xs-12">
						<div class="text-center">
							<img class="img-fluid" src="${context}/assets/img/logoMega.png" />
							<br> <br>
							<%
								Date date = new Date();
								DateFormat df = new SimpleDateFormat("E, dd MMMM yyyy");
							%>
							Welcome,
							<c:out value="<%=df.format(date)%>" />
						</div>
					</div>
					<div class="col-sm-8 col-md-8 col-xs-12">
						<div class="login-box-body"
							style="background: transparent; color: #000000; font-family: Arial; font-weight: bold; float: right; font-size: 40px; margin-top: 30px;">
							ESS </div>
					</div>
	
	
				</div>
			</div>
	
			<div class="row" style="background-color: white;">
				<c:if test="${message != null}">
					<div class="alert alert-danger display-none" id="add-item-error">
						<center><c:out value="${message} -  ${description}"></c:out> </center>
					</div>
				</c:if>
				<div class="col">
					<div class="login-box">
						<div class="login-box-body">
							<!-- <p class="login-box-msg">Sign in to start your session</p> -->
							<form id="loginForm" action="${context}${cons.SUBMIT_URL}"
								method="post">
								
								<div id="usernameCnt" class="form-group has-feedback">
									<input required="required" type="text" id="username"
										name="userName" class="form-control" placeholder="Username">
									<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
								</div>
								<div id="passwordCnt" class="form-group has-feedback">
									<input required="required" type="password" id="password"
										name="password" class="form-control" placeholder="Password">
									<span class="glyphicon glyphicon-lock form-control-feedback"></span>
								</div>
	
								<%-- <input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" /> --%>
								<c:if test="${errMsg != null}">
									<label class="control-label" for="inputError"
										style="color: #dd4b39"><i class="fa fa-times-circle-o"></i>
										${errMsg}</label>
									<br />
								</c:if>
								<div class="row">
									<div class="col-xs-12">
										<button id="btnSubmit" type="submit" style="float: right;"
											class="btn btn-success">Login</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="background-color: #ec6211; height: 40px;" /></div>
	</div>
	
	<script
		src="${context}/assets/resources/AdminLTE2/plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script
		src="${context}/assets/resources/AdminLTE2/bootstrap/js/bootstrap.min.js"></script>
</body>
<script type="text/javascript">
	$(window).load(function(){
		$('#overlay').delay("10000").fadeOut();
		$('#message').delay("10000").fadeIn();	
	});
</script>  
</html>
