<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.PermitHistoryController"
	scope="page" />

<div class="content-wrapper" style="background: white"
	ng-controller="PermitHistoryController">
	<section class="content-header">
		<h1>
			<h1>
				<b>History Izin</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message
						key="label.header.module.dashboard" /></a></li>
			<li><a href="${context}${routes.PERMIT_HISTORY}"><i
					class="fa fa-reply"></i> List Task Permit History</a></li>
			<li class="active"><fmt:message key="label.name.list" /></li>
		</ol>
	</section>
	<div ng-if="errorFlag === 'Y'" class="alert alert-danger ng-hide"
		id="add-item-error" ng-show=hideError>
		<center>{{status}} - {{message}}</center>
	</div>
	<section class="content" style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">List Permit History</strong>
			</div>
			<div id=inboxp>
				<div class="panel-body table-responsive text-nowrap"
					style="overflow-x: overlay;max-height:500px;">
					<table class="table table-hover">
						<thead>
							<form class="form-inline">
								<div class="form-group col-sm-3">
									<select class="form-control" ng-model="paramKey"
										ng-change="inputParam(paramKey)">
										<option value="" disabled selected>--Masukan Pilihan
											Anda--</option>
										<option value="instancesId">No. Pengajuan</option>
										<option value="absencesDesc">Jenis Izin</option>
										<option value="requesterId">ID Pemohon</option>
										<option value="requesterName">Nama Pemohon</option>
										<option value="startDt">Tanggal Ambil</option>
										<option value="endDt">Tanggal Akhir</option>
									</select>
								</div>
								<div class="col-sm-3" ng-if="dateFlag === 'N'">
									<input type="text" class="form-control form-control-sm"
										ng-model="paramVal"
										ng-change="onChangeValue(paramKey, paramVal)"
										onkeypress="return alpha(event)">
								</div>
								<div class="col-sm-3 ng-hide" ng-if="dateFlag === 'Y'"
									ng-show="hideData">
									<div class="form-group">
										<div id="">
											<input type="date" placeholder="Start date"
												ng-model="paramVal"
												ng-change="onChangeValue(paramKey, paramVal)"
												class="form-control form-control-sm" />
										</div>
									</div>
								</div>
								<div class="col-sm-3 ng-hide" ng-if="dateFlag === 'Y'"
									ng-show="hideData">
									<div class="form-group">
										<div id="">
											<input type="date" placeholder="End date"
												ng-model="paramVal2"
												ng-change="onChangeValue2(paramKey, paramVal2)"
												class="form-control form-control-sm" />
										</div>
									</div>
								</div>
								<div class="form-group col-sm-2">
									<button type="submit" class="btn btn-primary mb-2"
										ng-click="search()">Submit</button>
								</div>
							</form>
							<tr>
								<th class="col-sm-0"
									ng-click='sortColumn("instancesId")'
									ng-class='sortClass("instancesId")'>No.Pengajuan</th>
								<th ng-click='sortColumn("absencesDesc")'
									ng-class='sortClass("absencesDesc")'>Jenis Izin</th>
								<th ng-click='sortColumn("absencesDesc")'
									ng-class='sortClass("absencesDesc")'>Jumlah Kuota Ambil</th>	
								<th ng-click='sortColumn("startDate")'
									ng-class='sortClass("startDate")'>Periode Cuti</th>
								<th
									ng-click='sortColumn("approvalStagesName")'
									ng-class='sortClass("endDate")'><center>Status</center></th>
								<th>Detail Task</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="res in datas | orderBy:column:reverse"
								ng-show="hideData" class="ng-hide">
								<td>{{res.instancesId}}-{{res.taskId}}</td>
								<td>{{res.absencesDesc}}</td>
								<td align="center">{{res.quotaTaken}} Hari</td>
								<td>{{res.startDate}} - {{res.endDate}}</td>
								<td align="center">{{res.approvalStagesName}}
									<div ng-if="res.flagTaskComplete == 'N'">
										<i class="" style="font-size: 15px"></i>
									</div>
									<div ng-if="res.flagTaskComplete == 'Y'">
										<i class="glyphicon glyphicon-ok
									"></i>
									</div>
								</td>
								<td align="center"><a
									href='<c:url value="${cons.DETAIL_HISTORY_TASK_URL}?instanceId={{res.instancesId}}"></c:url>'
									class="btn bg-orange btn-flat btn-small" id="detail"> <i
										class="fa fa-fw fa-edit"></i>
								</a></td>
							</tr>
							<tr ng-show="hideData  == false" class="ng-hide">
								<td colspan="7"><center>List Task Empty</center></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="footer">
			<jsp:include
				page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
		</div>
	</section>
</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>
