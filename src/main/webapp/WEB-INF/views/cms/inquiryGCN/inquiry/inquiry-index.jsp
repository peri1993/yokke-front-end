<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons" class="com.yokke.web.controller.LeaveController"
	scope="page" />
<div class="content-wrapper" ng-controller="InquiryController">
	<section class="content-header">
		<h1><fmt:message key="label.header.inquiry"/></h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-dashboard"></i> <fmt:message key="label.header.module.dashboard"/></a></li>
			<li><a href="${context}${routes.INQUIRY}"><fmt:message key="label.header.inquiry"/></a></li>
			<li><a href="#"><fmt:message key="label.name.search"/></a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body">
						<form  method="post">
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
								  <div class="form-group row">
								    	<label  class="col-sm-2 col-form-label"><fmt:message key="label.header.inquiry.cifno"/></label>
								    	<div class="col-sm-5">
								     		 <input type="text" ng-model="paramCif" class="form-control" />
								    	</div>
								  </div>
								  <div class="form-group row">
								    	<label  class="col-sm-2 col-form-label"><fmt:message key="label.header.inquiry.gcnno"/></label>
								    	<div class="col-sm-5">
								     		 <input type="text" ng-model="paramGcn" class="form-control" />
								    	</div>
								  </div>
								  <div class="form-group row">
								    	<label  class="col-sm-2 col-form-label"><fmt:message key="label.header.inquiry.nama"/></label>
								    	<div class="col-sm-5">
								     		 <input type="text" ng-model="paramName" class="form-control" />
								    	</div>
								  </div>
								  <div class="form-group row">
								    	<label  class="col-sm-2 col-form-label"><fmt:message key="label.header.inquiry.birthdate"/></label>
								    	<div class="col-sm-3">
								     		 <input type="date" ng-model="parambirtDate" class="form-control" />
								    	</div>
								  </div>
								  <div class="form-group row">
								    	<label  class="col-sm-2 col-form-label"><fmt:message key="label.header.inquiry.mother"/></label>
								    	<div class="col-sm-5">
								     		 <input type="text" ng-model="paramMother" class="form-control" />
								    	</div>
								  </div>
								  <div class="form-group row">
								    	<label  class="col-sm-2 col-form-label"><fmt:message key="label.header.inquiry.idno"/></label>
								    	<div class="col-sm-3">
								     		 <input type="text" ng-model="paramktp"  onkeypress="return isNumberDot(event)" class="form-control" />
								    	</div>
								  </div>
								  <div class="form-group row">
								    	<label  class="col-sm-2 col-form-label"><fmt:message key="label.header.inquiry.npwp"/></label>
								    	<div class="col-sm-3">
								     		 <input type="text" ng-model="paramNpwp" onkeypress="return isNumberDot(event)" class="form-control" />
								    	</div>
								  </div>
								  <div class="form-group row">
								    	<label  class="col-sm-2 col-form-label"><fmt:message key="label.header.inquiry.hp"/></label>
								    	<div class="col-sm-3">
								     		 <input type="text" ng-model="paramNoHp" onkeypress="return isNumber(event)" class="form-control" />
								    	</div>
								  		<div class="col-sm-2 col-xs-6 pull-right">
								     		  <button type="submit" ng-click="search()" class="btn btn-success"><fmt:message key="label.name.submit"/></button>
								    	</div>
								  </div>

						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-title" style="color: #ffffff; background-color: #f39c12; height: 30px;" ng-if="tableClick">
			<p class="font-weight-bold">
				<b><fmt:message key="label.header.inquiry.result"/></b>
			</p> 
		</div>
		<div class="row" ng-if="tableClick">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body" style="overflow-x:auto!important">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th style="width: 50px;"><fmt:message key="label.header.inquiry.gcnno"/></th>
									<th style="width: 50px;"><fmt:message key="label.header.inquiry.nama"/></th>
									<th style="width: 50px;"><fmt:message key="label.header.inquiry.dob"/></th>
									<th style="width: 50px;"><fmt:message key="label.header.inquiry.hp"/></th>
									<th style="width: 50px;"><fmt:message key="label.header.inquiry.mother"/></th>
									<th style="width: 50px;"><fmt:message key="label.header.inquiry.idno"/></th>
									<th style="width: 50px;"><fmt:message key="label.header.inquiry.npwp"/></th>
									<th style="width: 50px;"><fmt:message key="label.header.inquiry.gender"/></th>
									<%-- <th style="width: 50px;"><fmt:message key="label.header.inquiry.kolektility"/></th> --%>
								</tr>
							</thead>
							<tbody>
								<tr
									ng-repeat="res in datas track by $index| orderBy:sortBy:sortReverse">
									<td><a href="${context}${cons.DETAIL_URL}{{linkAction.paramId}}{{res.idnSvp}}" >{{res.idnSvp}}</a></td>
									<td>{{res.nmeFull}}</td>
									<td>{{res.dteBirth}}</td>
									<td>{{res.phoneNumber}}</td>
									<td>{{res.nmeMother}}</td>
									<td>{{res.idnId}}</td>
									<td>{{res.idnTax}}</td>
									<td>{{res.cdeGender == 'P' ? "Perempuan" : "Laki - laki"}}</td>
									<!-- <td>{{res.kolectib}}</td> -->
								</tr>
								<tr ng-if="emptyData === false"><td colspan="7"><center><fmt:message key="label.name.not.found"/></center></td></tr>
							</tbody>
						</table>
					</div>
					<div class="box-footer clearfix">
						<jsp:include page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>
