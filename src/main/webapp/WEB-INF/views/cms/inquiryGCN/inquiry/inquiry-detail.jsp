<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons" class="com.yokke.web.controller.LeaveController"
	scope="page" />
<div class="content-wrapper" ng-controller="InquiryDetailController">
	<section class="content-header">
		<h1><fmt:message key="label.header.inquiry"/></h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-dashboard"></i> <fmt:message key="label.header.module.dashboard"/></a></li>
			<li><a href="${context}${routes.INQUIRY}"><fmt:message key="label.header.inquiry"/></a></li>
			<li><a href="#"><fmt:message key="label.name.detail"/></a></li>
		</ol>
	</section>
	<section class="content">
		<div class="panel panel-title" style="color: #ffffff; background-color: #f39c12; height: 30px;">
			<p class="font-weight-bold">
				<b><fmt:message key="label.detail.inquiry"/></b>
			</p> 
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body" style="overflow-x:auto!important">
						<div id="flip-tabs">
							<fmt:message key="label.header.inquiry.gcnno"/>			<c:out value="${gcnNo}"/>
						</div>
						<c:if test="${CB}">
							<table class="table table-bordered table-hover" style="width: 1400px;">
								<thead style="background-color: #f39c12;">
									<tr>
										<th style="width: 30px;"><fmt:message key="label.name.investigasi.source"/></th>
										<th style="width: 150px;"><fmt:message key="label.header.inquiry.cifno"/></th>
										<th style="width: 100px;"><fmt:message key="label.name.rekomendasi.branch"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.nama"/></th>
										<th style="width: 50px;"><fmt:message key="label.header.inquiry.dob"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.mother"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.idno"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.npwp"/></th>
										<th style="width: 50px;"><fmt:message key="label.header.inquiry.gender"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${datas}" var="item">
										<c:if test="${item.dataSource == 'CB'}">
											<tr>
												<td><a data-toggle="collapse" data-target="#colCB" href="#" ng-click="search('${item.idnParty}','${item.dataSource}')">${item.dataSource}</a></td>
												<td>${item.idnParty}</td>
												<td>${item.branch.name}</td>
												<td>${item.nmeFull}</td>
												<td>${item.dteBirth}</td>
												<td>${item.nmeMother}</td>
												<td>${item.idnId}</td>
												<td>${item.idnTax}</td>
												<td>${item.cdeGender}</td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
							<div class="collapse" id="colCB">
								<div class="card card-body">
									<table class="table table-bordered table-hover" style="width: 1400px;">
										<thead style="background-color: #c8c8b9;">
											<tr>
												<th style="width: 100px;"><fmt:message key="label.detail.inquiry.account.noaccount"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.branccode"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.jenis"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.status"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.type.card"/></th>
												<th style="width: 120px;"><fmt:message key="label.detail.inquiry.account.opendate"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.currency"/></th>
												<th style="width: 80px;"><fmt:message key="label.header.terlapor.slik"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.collectibilities"/></th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="rescb in dataDetailCB track by $index| orderBy:sortBy:sortReverse">
												<td>{{rescb.accountNumber}}</td>
												<td>{{rescb.branchCode}}</td>
												<td>{{rescb.jenis}}</td>
												<td>{{rescb.active}}</td>
												<td>{{rescb.typeCard}}</td>
												<td>{{rescb.dateOpen | date:"yyyy-MM-dd"}}</td>
												<td>{{rescb.currency}}</td>
												<td>{{rescb.slik}}</td>
												<td>{{rescb.colectibilities}}</td>
											</tr>
											<tr ng-if="dataRowCB === false"><td colspan="9"><center><fmt:message key="label.name.not.found"/></center></td></tr>
										</tbody>
									</table>
								</div>
							</div>
						</c:if>
						<c:if test="${CC}">
							<table class="table table-bordered table-hover" style="width: 1400px;">
								<thead style="background-color: #f39c12;">
									<tr>
										<th style="width: 30px;"><fmt:message key="label.name.investigasi.source"/></th>
										<th style="width: 150px;"><fmt:message key="label.header.inquiry.cifno"/></th>
										<th style="width: 100px;"><fmt:message key="label.name.rekomendasi.branch"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.nama"/></th>
										<th style="width: 50px;"><fmt:message key="label.header.inquiry.dob"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.mother"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.idno"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.npwp"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.hp"/></th>
										<th style="width: 50px;"><fmt:message key="label.header.inquiry.gender"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${datas}" var="item">
										<c:if test="${item.dataSource == 'CC' }">
											<tr>
												<td><a data-toggle="collapse" href="#colCC" data-target="#colCC" ng-click="search('${item.idnParty}','${item.dataSource}')">${item.dataSource}</a></td>
												<td>${item.idnParty}</td>
												<td>${item.branch.name}</td>
												<td>${item.nmeFull}</td>
												<td>${item.dteBirth}</td>
												<td>${item.nmeMother}</td>
												<td>${item.idnId}</td>
												<td>${item.idnTax}</td>
												<td>${item.idnMobilePhone}</td>
												<td>${item.cdeGender}</td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
							<div class="accordion-body collapse" id="colCC">
								<div class="card card-body">
									<table class="table table-bordered table-hover" style="width: 1400px;">
										<thead style="background-color: #c8c8b9;">
											<tr>
												<th style="width: 100px;"><fmt:message key="label.detail.inquiry.account.noaccount"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.cardno"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.jenis"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.status"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.type"/></th>
												<th style="width: 120px;"><fmt:message key="label.detail.inquiry.account.opendate"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.currency"/></th>
												<th style="width: 80px;"><fmt:message key="label.header.terlapor.slik"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.collectibilities"/></th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="rescc in dataDetailCC track by $index| orderBy:sortBy:sortReverse">
												<td>{{rescc.accountNumber}}</td>
												<td>{{rescc.cardNumber}}</td>
												<td>{{rescc.jenis}}</td>
												<td>{{rescc.active}}</td>
												<td>{{rescc.type}}</td>
												<td>{{rescc.dateOpen | date:"yyyy-MM-dd"}}</td>
												<td>{{rescc.currency}}</td>
												<td>{{rescc.slik}}</td>
												<td>{{rescc.colectibilities}}</td>
											</tr>
											<tr ng-if="dataRowCC === false"><td colspan="9"><center><fmt:message key="label.name.not.found"/></center></td></tr>
										</tbody>
									</table>
								</div>
							</div>
						</c:if>
						<c:if test="${JF}">
							<table class="table table-bordered table-hover" style="width: 1400px;">
								<thead style="background-color: #f39c12;">
									<tr>
										<th style="width: 30px;"><fmt:message key="label.name.investigasi.source"/></th>
										<th style="width: 150px;"><fmt:message key="label.header.inquiry.cifno"/></th>
										<th style="width: 100px;"><fmt:message key="label.name.rekomendasi.branch"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.nama"/></th>
										<th style="width: 50px;"><fmt:message key="label.header.inquiry.dob"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.mother"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.idno"/></th>
										<th style="width: 100px;"><fmt:message key="label.header.inquiry.npwp"/></th>
										<th style="width: 50px;"><fmt:message key="label.header.inquiry.gender"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${datas}" var="item">
										<c:if test="${item.dataSource == 'JF' }">
											<tr>
												<td><a  data-toggle="collapse" href="#colJF" data-target="#colJF" ng-click="search('${item.idnParty}','${item.dataSource}')">${item.dataSource}</a></td>
												<td>${item.idnParty}</td>
												<td>${item.branch.name}</td>
												<td>${item.nmeFull}</td>
												<td>${item.dteBirth}</td>
												<td>${item.nmeMother}</td>
												<td>${item.idnId}</td>
												<td>${item.idnTax}</td>
												<td>${item.cdeGender}</td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
							<div class="collapse" id="colJF">
								<div class="card card-body">
									<table class="table table-bordered table-hover" style="width: 1300px;">
										<thead style="background-color: #c8c8b9;">
											<tr>
												<th style="width: 100px;"><fmt:message key="label.detail.inquiry.account.noaccount"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.branccode"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.jenis"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.status"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.type.card"/></th>
												<th style="width: 120px;"><fmt:message key="label.detail.inquiry.account.opendate"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.currency"/></th>
												<th style="width: 80px;"><fmt:message key="label.header.terlapor.slik"/></th>
												<th style="width: 80px;"><fmt:message key="label.detail.inquiry.account.collectibilities"/></th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="resjf in dataDetailJF track by $index| orderBy:sortBy:sortReverse">
												<td>{{resjf.accountNumber}}</td>
												<td>{{resjf.branchCode}}</td>
												<td>{{resjf.jenis}}</td>
												<td>{{resjf.active}}</td>
												<td>{{resjf.typeCard}}</td>
												<td>{{resjf.dateOpen | date:"yyyy-MM-dd"}}</td>
												<td>{{resjf.currency}}</td>
												<td>{{resjf.slik}}</td>
												<td>{{resjf.colectibilities}}</td>
											</tr>
											<tr ng-if="dataRowJF === false"><td colspan="9"><center><fmt:message key="label.name.not.found"/></center></td></tr>
										</tbody>
									</table>
								</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="rows">
			<div class="col-sm-2 col-xs-6 pull-left">
				<a  href="${context}${routes.INQUIRY}" class="btn btn-danger"><fmt:message key="label.name.back"/></a>
			</div>
		</div>
	</section>
</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>
<script>
	$("#colCC").on("click", function() {
	  $(this).closest('.collapse-group').find('.collapse').collapse('hide');
	});

</script>						
						
						
						
						
					
					
					