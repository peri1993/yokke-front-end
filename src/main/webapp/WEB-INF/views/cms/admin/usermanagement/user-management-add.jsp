<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<%-- 
<jsp:useBean id="cons" class="com.yokke.web.controller.UserManagementController"
	scope="page" /> --%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<div class="content-wrapper" style="background: white">
	<section class="content-header">
		<h1>
			<h1>
				<b><fmt:message key="label.header.module.usermanagement" /></b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message
						key="label.header.module.dashboard" /></a></li>
			<li><a href="${context}${routes.USER_MANAGEMENT}">User
					Management List</a></li>
			<li class="active">Update</li>
		</ol>
	</section>
	<section class="content" style="background: white">
	<div class="row" ng-if="deleteFlag === 'Y'" >
			<div class="alert alert-danger ng-hide" id="add-item-error" ng-show="hideData">
				<center>{{status}} - {{error}}</center>
			</div>
	</div>
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">User Management</strong>
			</div>
			<c:if test="${error != null}">
				<div class="alert alert-danger display-none" id="add-item-error">
					<center><c:out value="${error}"></c:out></center>
				</div>
			</c:if>
		<div class="form-group row">
			<table class="table table-hover" id="groupManagement">

				<div class="col-xs-12">
						<form id="groupForm"
							action="${context}${'/user-management/save'}"
							method="post">
							<c:if test="${data.id != null }">
								<input type="hidden" name="id" value="${data.id}" />
							</c:if>
							<div class="box-body">
								<div class="form-group row">
									<label class="col-sm-4 col-form-label"><fmt:message
											key="label.header.user.username" /> </label>
									<div class="col-sm-8">
										<input required="required" type="text" id="username"
											name="username" class="form-control" placeholder="username"
											value="${data.username}" readonly="readonly">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Full name</label>
									<div class="col-sm-8">
										<input required="required" type="text" id="name"
											name="fullname" class="form-control" placeholder="full name"
											value="${data.fullname}" readonly="readonly">
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-4 col-form-label">Role </label>
									<div class="col-sm-8">
										<select class="form-control" id="exampleFormControlSelect2" name="roleId" >
									    	<c:forEach items="${listRole}" var="item">
									    		 <option value="${item.role_id}" <c:if test="${item.role_id eq data.role.role_id}">selected="selected"</c:if>>
									    		 	<c:out value="${item.role_id}"/> - <c:out value="${item.roleName}"/> 
									    		 </option>
									    	</c:forEach>
									    </select>
									</div>
								</div>
								<div class="modal-footer">
									<c:if test="${data.id != null}">
									 	<button id="btnAdd" type="submit" class="btn btn-primary"><fmt:message key="label.name.update"/></button>
									 </c:if>
								     <c:if test="${data.id == null}">
									 	<button id="btnAdd" type="submit" class="btn btn-primary"><fmt:message key="label.name.create"/></button>
									 </c:if>
									<a href="${context}${routes.USER_MANAGEMENT}" type="button"
										class="btn btn-danger" data-dismiss="modal"><fmt:message
											key="label.name.back" /></a>
								</div>
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<c:if test="${errMsg != null}">
									<label class="control-label" for="inputError"
										style="color: #dd4b39"><i class="fa fa-times-circle-o"></i>
										${errMsg}</label>
									<br />
								</c:if>

							</div>
						</form>
				</div>
			</table>
		</div>
		</div>
	</section>
</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>