<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.UserManagementController"
	scope="page" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/cms/components/message-error.jsp"></jsp:include>
<div class="content-wrapper" ng-controller="UserManagementController"
	style="background: white">
	<input type="hidden" name="lookDetailUser" id="lookDetailUser"
		value="${lookDetailUser}"></input> <input type="hidden"
		name="setGroupName" id="setGroupName" value="${setGroupName}"></input>
	<section class="content-header">
		<h1>
			<h1>
				<b><fmt:message key="label.header.module.usermanagement" /></b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message
						key="label.header.module.dashboard" /></a></li>
			<li><a href="${context}${cons.MAIN_URL}"><fmt:message
						key="label.header.module.usermanagement" /></a></li>
			<li class="active"><fmt:message key="label.name.list" /></li>
		</ol>
	</section>
	<div ng-if="deleteFlag === 'Y'" class="alert alert-danger ng-hide"
		id="add-item-error" ng-show=hideData>
		<center>{{status}} - {{error}}</center>
	</div>
	<section class="content" style="background: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title"><fmt:message
						key="label.header.module.users" /></strong>
			</div>
			<div class="row">
				<%
					String msgLdap = (String) request.getAttribute("ldapMsg");
				%>
				<c:if test="<%=msgLdap != null%>">
					<div class="alert alert-danger display-none" id="add-item-error">
						<center><%=msgLdap%></center>
					</div>
				</c:if>
			</div>
			<div>
				<div class="panel-body table-responsive" style="overflow-x:scroll!important;">
					<table class="table table-hover">
						<thead>
							<form class="form-inline" action="${context}${cons.MAIN_URL}" novalidate="novalidate" method="get">
								<div class="form-group col-sm-3">
									<select class="form-control" ng-model="paramKey"
										ng-change="inputParam(paramKey)">
										<option value="" disabled selected>Select your option</option>
										<option value="username"><fmt:message
												key="label.header.user.username" /></option>
										<option value="fullname">Full Name</option>
										<option value="createdBy">Created By</option>
										<option value="updatedBy">Updated By</option>
										<option value="createdDate">Created Date</option>
										<option value="updatedDate">Updated Date</option>
										<option value="roleName">Role Name</option>
									</select>
								</div>
								<div class="col-sm-3" ng-if="dateFlag === 'N'">
									<input type="text" class="form-control" ng-model="paramVal"
										ng-change="onChangeValue(paramKey, paramVal)" placeholder="Find Here" onkeypress="return alpha(event)">
								</div>
								<div class="col-sm-3 ng-hide" ng-if="dateFlag === 'Y'"
									ng-show="hideData">
									<div class="form-group">
										<div id="">
											<input type="date" placeholder="Start date"
												ng-model="paramVal"
												ng-change="onChangeValue(paramKey, paramVal)"
												class="form-control form-control-sm" />
										</div>
									</div>
								</div>
								<div class="col-sm-3 ng-hide" ng-if="dateFlag === 'Y'"
									ng-show="hideData">
									<div class="form-group">
										<div id="">
											<input type="date" placeholder="End date"
												ng-model="paramVal2"
												ng-change="onChangeValue2(paramKey, paramVal2)"
												class="form-control form-control-sm" />
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<a type="submit" class="btn btn-primary"
										ng-click="search(paramKey, paramValue)"><fmt:message
											key="label.name.submit" /></a>
								</div>
							</form>
							<div class="col-sm-2 col-xs-12 pull-right">
								<a href="${context}${cons.ADD_LDAP_URL}"
									class="btn btn-sm btn-block btn-social btn-dropbox"> <i
									class="fa fa-plus"></i> Tambah User Baru
								</a>
							</div>
							<tr>
								<th style="width: 20px;" ng-click='sortColumn("index")' ng-class='sortClass("index")'>No.</th>
								<th style="width: 100px;" ng-click='sortColumn("username")' ng-class='sortClass("username")'>Username</th>
								<th style="width: 100px;" ng-click='sortColumn("fullname")' ng-class='sortClass("fullname")'>Nama Lengkap</th>
								<th style="width: 90px;" ng-click='sortColumn("createdBy")' ng-class='sortClass("createdBy")'>Created By</th>
								<th style="width: 130px;" ng-click='sortColumn("createdDt")' ng-class='sortClass("createdDt")'>Tanggal Pembuatan</th>
								<th style="width: 80px;" ng-click='sortColumn("updateBy")' ng-class='sortClass("updateBy")'>Updated By</th>
								<th style="width: 130px;" ng-click='sortColumn("updateDt")' ng-class='sortClass("updateDt")'>Tanggal Update</th>
								<th style="width: 50px;" ng-click='sortColumn("role.roleName")' ng-class='sortClass("role.roleName")'>Role</th>
								<th style="width: 50px;"></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-if="emptyData === 'false'"
								ng-repeat="res in datas | orderBy:column:reverse"
								ng-show="hideData" class="ng-hide">
								<td>{{$index+1 + from}}</td>
								<td>{{res.username}}</td>
								<td>{{res.fullname}}</td>
								<td>{{res.createdBy}}</td>
								<td>{{res.createdDt}}</td>
								<td>{{res.updatedBy}}</td>
								<td>{{res.updatedDt}}</td>
								<td>{{res.role.roleName}}</td>
								<td align="center"><a
									href='<c:url value="${cons.ADD_USER_URL}{{linkAction.userId+res.id}}">
													</c:url>'
									class="btn bg-orange btn-flat btn-small" id="detailGroup">
										<i class="fa fa-fw fa-edit"></i>
								</a> <a href="#" ng-click="deleteUser(res.id)"
									class="btn bg-maroon btn-flat btn-small" id="idUser"> <i
										class="fa fa-fw fa-trash"></i>
								</a></td>
							</tr>
							<tr ng-if="emptyData === 'true'">
								<td colspan="9"><center>
										<fmt:message key="label.name.not.found" />
									</center></td>
							</tr>
						</tbody>
					</table>
					<div class="box-footer clearfix">
						<jsp:include
							page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>