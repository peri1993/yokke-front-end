<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.UserManagementController"
	scope="page" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>

<div class="content-wrapper" style="background-color: white">
	<section class="content-header">
		<h1>
			<h1>
				<b><fmt:message key="label.header.module.usermanagement" /></b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message
						key="label.header.module.dashboard" /></a></li>
			<li><a href="${context}${routes.USER_MANAGEMENT}">User
					Management List</a></li>
			<li class="active"><fmt:message key="label.name.add" /></li>
		</ol>
	</section>
	<section class="content" style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">Search</strong>
			</div>
		<%
			String msg = (String) request.getAttribute("msg");
		%>
		<c:if test="<%=msg != null%>">
			<div class="alert alert-danger display-none" id="add-item-error">
				<center><%=msg%></center>
			</div>
		</c:if>
		<br>
		<div class="form-group row">
			<div class="col-xs-12 col-md-10">
				<div class="box-header">
					<form id="getUserLdap" action="${context}${cons.ADD_LDAP_URL}"
						method="get">
						<label class="col-sm-2 col-form-label">Username </label>
						<div class="col-sm-6">
							<input required="required" type="text" id="username"
								name="username" class="form-control" placeholder="Username"
								value="${username}">
						</div>
						<div class="col-sm-4">
							<button id="search" type="submit" class="btn btn-primary">
								<fmt:message key="label.name.search" />
							</button>
						</div>
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<c:if test="${errMsg != null}">
							<label class="control-label" for="inputError"
								style="color: #dd4b39"><i class="fa fa-times-circle-o"></i>${errMsg}</label>
							<br />
						</c:if>
					</form>
				</div>
			</div>
		</div>
		<br> <br>
		<c:if test="${data != null}">
			<div class="form-group row">
				<div class="col-xs-12">
						<div class="box-body">
							<table class="table table-hover">
								<thead>
									<tr>
										<th style="width: 50px;"><fmt:message
												key="label.header.user.nip" /></th>
										<th style="width: 50px;"><fmt:message
												key="label.header.user.name" /></th>
										<th style="width: 100px;">Action</th>
									</tr>
								</thead>
								<tbody>
									<!-- <tr
										ng-repeat="res in datas track by res.id| orderBy:sortBy:sortReverse"> -->
									<tr>
										<td>${data.username}</td>
										<td>${data.fullname}</td>
										<td style="width: 100px;" align="center"><a
											href='<c:url value="${cons.ADD_USER_URL}">
																<c:param name="username" value="${data.username}"/>
																<c:param name="fullname" value="${data.fullname}"/>
															</c:url>'
											class="btn bg-blue btn-flat btn-small" id="detailUser">
												<i class="fa fa-fw fa-arrow-right"></i>
										</a></td>
									</tr>

								</tbody>
							</table>
						</div>
				</div>
			</div>
		</c:if>
		</div>
	</section>
</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>