<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.AssignedTaskController" scope="page" />
<div class="content-wrapper" style="background: white"	ng-controller="DetailAssignerController">
	<input type="hidden" name="instanceId" id="instanceId" value="${instanceId}"></input> 
	<input type="hidden" ng-model="_csrf.token" ng-init="_csrf.token = '${_csrf.token}'" name="${_csrf.parameterName}" value="${_csrf.token}" />
	
	<section class="content-header">
		<h1>
			<h1>
				<b>Re-Assigner Task Detail</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message key="label.header.module.dashboard"/></a></li>
			<li><a href="${context}${cons.MAIN_URL}">List Assigned Task</a></li>
			<li class="active">Detail</li>
		</ol>
	</section>
	<div class="panel panel-info">
		<div id="alert" ng-if="flag" ng-show="flag"
			class="alert alert-success alert-dismissable ng-hide" align="center"
			data-dismiss="alert">
			<strong>{{status}} - {{message}} </strong><br> 
		</div>
		<div id="alert" ng-if="flagError" ng-show="flagError"
			class="alert alert-danger alert-dismissable ng-hide" align="center"
			data-dismiss="alert">
			<strong>{{status}} - {{message}} </strong><br> 
		</div>
		<div class="panel-heading">
			<strong>Header</strong>
		</div>
		
		<div class="" id="col-header">
			<div class="content" style="background-color: white">
				<div class="row">
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Employee</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq">Requester
										ID</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="id" ng-model="datas.requesterId" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="rq">Requester
										Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="name" ng-model="datas.requesterName" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Business Process</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="subject" ng-model="datas.businessProcess" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Assigner</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="subject" ng-model="datas.assigner" disabled />
									</div>
								</div>
							</form>
						</div>
					</div>
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Approval</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-4 col-form-label-sm" for="qt">Approval
										1</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval1" ng-model="datas.approvalDs1" disabled />
									</div>
									<div class="col-sm-2">
										<a type="submit" class="btn btn-primary" style="margin-right: 20px"
											data-toggle="modal" data-target="#modalSearch1"><i class="fa fa-fw fa-edit"></i></a>
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-4 col-form-label-sm" for="trq">Approval
										1 Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval1Name" ng-model="datas.nameApprovalDs1" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-4 col-form-label-sm" for="qt">Approval
										2</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval2" ng-model="datas.approvalDs2" disabled />
									</div>
									<div class="col-sm-2">
										<a type="submit" class="btn btn-primary" style="margin-right: 20px"
											data-toggle="modal" data-target="#modalSearch2" ng-click="keyclock('ds2_id')"><i class="fa fa-fw fa-edit"></i></a>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-4 col-form-label-sm" for="trq">Approval
										2 Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval2name" ng-model="datas.nameApprovalDs2" disabled />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- The modal -->
	<div class="modal fade" id="modalSearch1" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm5" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Search Username</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body table-responsive" style="overflow-x:auto!important;">
					<table class="table table-hover">
						<thead>
							<form class="form-inline">
  								<div class="form-group col-sm-4">
  									<label for="sid" class="sr-only" >Search</label>
    								<input type="text" ng-model="username" class="form-control form-control-sm" id="sid" placeholder="Username" >
  								</div>
  								<div class="form-group col-sm-2" >
					      			<button type="submit" class="btn btn-primary mb-2" ng-click="search('ds1_id')">Submit</button>
					    		</div>
							</form>
							<tr>
								<th style="width: 50px;" class="col-sm-0">Username</th>
								<th style="width: 150px;">Fullname</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{detail.username}}</td>
								<td>{{detail.fullname}}</td>
							</tr>
							<tr ng-show="hideData == false" class="ng-hide">
								<td colspan="2"><center>Username Not Found !!!</center></td>
							</tr>
						</tbody>
					</table>
				</div>
				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-secondary"
						ng-click="add()" data-dismiss="modal">Add</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalSearch2" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm5" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Search Username</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body table-responsive" style="overflow-x:auto!important;">
					<table class="table table-hover">
						<thead>
							<form class="form-inline">
  								<div class="form-group col-sm-4">
  									<label for="sid" class="sr-only" >Search</label>
    								<input type="text" ng-model="username" class="form-control form-control-sm" id="sid" placeholder="Username" >
  								</div>
  								<div class="form-group col-sm-2" >
					      			<button type="submit" class="btn btn-primary mb-2" ng-click="search('ds2_id')">Submit</button>
					    		</div>
							</form>
							<tr>
								<th style="width: 50px;" class="col-sm-0">Username</th>
								<th style="width: 150px;">Fullname</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{detail.username}}</td>
								<td>{{detail.fullname}}</td>
							</tr>
							<tr ng-show="hideData == false" class="ng-hide">
								<td colspan="2"><center>Username Not Found !!!</center></td>
							</tr>
						</tbody>
					</table>
				</div>
				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-secondary"
						ng-click="add()" data-dismiss="modal">Add</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="panel-info" style="height: 60px;">
		<div class="col-md-12">
			<div class="form-group row">
				<div class="col-sm-4">
					<a ng-click="update()" type="submit" class="btn btn-primary" style="margin-right: 20px">Update</a> 
					<a href="#" ng-click="reset()" type="submit" class="btn btn-danger" style="margin-right: 20px"> Reset </a>
					<a href="${context}${routes.ASSIGNED_TASK}" type="submit" class="btn btn-success"> Back </a>
				</div>
				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>