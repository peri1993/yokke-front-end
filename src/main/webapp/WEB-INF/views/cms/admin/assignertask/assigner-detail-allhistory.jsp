<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.AssignerHistoryAllController" scope="page" />
<div class="content-wrapper" style="background: white"
	ng-controller="DetailTaskController">
	<input type="hidden" name="idRecom" id="instanceId"
		value="${instanceId}"></input> 
	<input type="hidden" name="businessProcess" id="businessProcess" value="${businessProcess}"></input> 
	<input type="hidden"
		ng-model="_csrf.token" ng-init="_csrf.token = '${_csrf.token}'"
		name="${_csrf.parameterName}" value="${_csrf.token}" />
	<section class="content-header">
		<h1>
			<h1>
				<b>Detail Task</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.ALL_ASSIGNER_HISTORY}"><i
					class="fa fa-reply"></i> List Task</a></li>
			<li><a href="${context}${routes.TASK_DETAIL}">Detail</a></li>
		</ol>
	</section>
	<div class="panel panel-info">
		<div class="panel-heading">
			<strong>Header</strong>
		</div>
		<div class="" id="col-header">
			<div class="content" style="background-color: white">
				<div class="row">
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Employee</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq">Requester
										ID</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="id" ng-model="datas.employee_id" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="rq">Requester
										Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="name" ng-model="datas.employee_name" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Subject</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="subject" ng-model="datas.subject" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Description</label>
									<div class="col-sm-6">
										<textarea rows="6" class="form-control" cols="26" id="desc"
											ng-model="datas.description" disabled></textarea>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Approval</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Approval
										1</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval1" ng-model="datas.approval_ds_1" disabled />
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Approval
										1 Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval1Name" ng-model="datas.ds1_id" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Approval
										2</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval2" ng-model="datas.approval_ds_2" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Approval
										2 Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval2name" ng-model="datas.ds2_id" disabled />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<strong>Main</strong>
		</div>
		<div class="" id="col-body">
			<div class="content" style="">
				<div class="row">
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Absence</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-3 col-form-label-sm" for="att">Absence
										Type</label>
									<div class="col-sm-6">
										<input class="form-control form-control-sm" id="att"
											ng-model="datas.absenceDescription" disabled />
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-3 col-form-label-sm" for="startdate">Start
										Date</label>
									<div class="col-sm-5">
										<div class="form-group">
											<div id="">
												<input type="text" ng-model="datas.leave_start_date"
													class="form-control form-control-sm" datepicker disabled />
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-3 col-form-label-sm" for="enddate">End
										Date</label>
									<div class="col-sm-5">
										<div class="form-group">
											<div id="">
												<input id="enddate" ng-model="datas.leave_end_date"
													class="form-control form-control-sm" disabled />
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-3 col-form-label-sm" for="tqt">Total
										Quota Taken</label>
									<div class="col-sm-4">
										<input type="text" ng-model="quotaTaken"
											class="form-control form-control-sm" id="tqt" disabled>
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-3 col-form-label-sm" for="tqt">Reason
										For Leave</label>
									<div class="col-sm-6">
										<textarea type="text" class="form-control"
											ng-model="datas.leave_reason" cols="40" rows="6"
											style="margin-top: 9px" disabled></textarea>
									</div>
								</div>
							</form>
						</div>

					</div>
					<div class="col-md-1"></div>
					<div id="quota" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Current Quota</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq">Leave
										Quota</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="lq" ng-model="datasRemaining.originalQuota" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="rq">Remain
										Quota</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="rq" ng-model="datasRemaining.remainingQuota" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Quota
										Taken</label>
									<div class="col-sm-6">
										<input ng-model="currentQuotaTaken" type="text"
											class="form-control form-control-sm" id="qt" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Total
										Remaining Quota</label>
									<div class="col-sm-6">
										<input ng-model="currentTotalRemainingToken" type="text"
											class="form-control form-control-sm" id="trq" disabled>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div ng-if="carryFlag" id="quota" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Carry Over</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq">Leave
										Quota</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="lq" ng-model="datasRemaining.originalQuota" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="rq">Remain
										Quota</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="rq" ng-model="nextremainingQuota" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Quota
										Taken</label>
									<div class="col-sm-6">
										<input ng-model="nextQuotaTaken" type="text"
											class="form-control form-control-sm" id="qt" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Total
										Remaining Quota</label>
									<div class="col-sm-6">
										<input ng-model="nextTotalRemainingToken" type="text"
											class="form-control form-control-sm" id="trq" disabled>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<strong>Footer</strong>
		</div>
		<div class="" id="col-footer">
			<div class="content" style="background-color: white">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#comments">Comment</a></li>
					<li ng-if="businessProcess === 'Y'"><a data-toggle="tab" href="#attach">File</a></li>
				</ul>
				<div class="tab-content">
					<div id="comments" class="tab-pane fade in active form-group row"
						style="margin-top: 5px">
						<label class="col-sm-2 col-form-label-sm" for="trq">Comments</label>
						<div class="col-sm-3" style="margin-top: 5px">
							<textarea rows="7" cols="40" id="desc" class="form-control"
								ng-model="datas.comments"></textarea>
						</div>
					</div>
					<div id="attach" class="tab-pane fade form-group-row custom-file">
						<div class="form-group row" ng-if="buttonDownload === true">
							<li ng-repeat="file in listFileUpload track by $index"><label
								class="custom-file-label col-sm-12" for="atc"
								style="margin-top: 10px">File Type : {{file.fileType}}</label> <a
								href='<c:url value="${cons.DOWNLOAD_URL}{{linkAction.filename+file.fileName+linkAction.directory+file.directory+linkAction.createdBy+file.createdBy}}">
												</c:url>'
								class="btn bg-green btn-flat btn-small">Download</a> 
								File name : <a href="#" ng-click="preview(file.directory, file.createdBy, file.fileName)">{{file.fileName}}</a> </li>
						</div>
						<div class="form-group row" ng-if="buttonDownload === false">
							<div class="col-sm-3">
								<strong>No Attachment File</strong>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-info" style="height: 20px;">
		<div class="col-md-12">
			<div class="form-group row">
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>