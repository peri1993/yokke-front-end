<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes" 	scope="page" />
<jsp:useBean id="cons" class="com.yokke.web.controller.AssignedTaskController" 	scope="page" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<div class="content-wrapper" style="background: white" ng-controller="AssignedTaskController">
	<input type="hidden" ng-model="_csrf.token"
		ng-init="_csrf.token = '${_csrf.token}'" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	<section class="content-header">
		<h1>
			<h1>
				<b>Assigner Task</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message key="label.header.module.dashboard"/></a></li>
			<li><a href="${context}${cons.MAIN_URL}">Assigned Task</a></li>
			<li class="active"><fmt:message key="label.name.list"/></li>
		</ol>
	</section>
	<div ng-if="errorFlag === 'Y'" class="alert alert-danger ng-hide"
		id="add-item-error" ng-show=hideError>
		<center>{{status}} - {{message}}</center>
	</div>
	<section class="content" style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">Task List</strong>
			</div>
			<div id=inboxp>
				<div class="panel-body table-responsive text-nowrap" style="overflow-x: overlay;max-height:500px;">
					<table class="table table-hover">
						<thead>
							<form class="form-inline">
  								<div class="form-group col-sm-3">
									<select class="form-control" ng-model="paramKey" ng-change="inputParam(paramKey)">
    									<option value="" disabled selected>Select your option</option>
										<option value="instancesId">No. Pengajuan</option>
										<option value="businessProcess">Tipe Pengajuan</option>
										<option value="absencesDesc">Jenis Pengajuan</option>
										<option value="requesterId">NIP Pemohon</option>
										<option value="requesterName">Nama Pemohon</option>
										<option value="assigner">NIP Assigner</option>
										<option value="assignerName">Nama Assigner</option>
									</select>
								</div>
  								<div class="col-sm-3"">
							      	<input type="text" class="form-control form-control-sm" ng-model="paramVal" ng-change="onChangeValue(paramKey, paramVal)" onkeypress="return alpha(event)">
							    </div>
  								<div class="form-group col-sm-2" >
					      			<button type="submit" class="btn btn-primary mb-2" ng-click="find()">Submit</button>
					    		</div>
							</form>
							<tr>
								<th style="width: 10px"><input type="checkbox" class="radio ng-hide" ng-show="hideData" ng-click="checkAllList()" ng-model="isAllSelected"></th>
								<th style="width: 50px;" class="col-sm-0" ng-click='sortColumn("instancesId")'>No. Pengajuan</th>
								<th style="width: 150px;" ng-click='sortColumn("businessProcess")' >Tipe Pengajuan</th>
								<th style="width: 130px;" ng-click='sortColumn("absencesDesc")' >Jenis Pengajuan</th>
								<th style="width: 130px;" ng-click='sortColumn("requesterName")' >Nama Pemohon</th>
								<th style="width: 160px;" ng-click='sortColumn("assigner")' >NIP & Nama Assigner</th>
								<th style="width: 160px;" ng-click='sortColumn("startDate")' >Periode Cuti</th>
								<th style="width: 160px; text-align: center" ng-click='sortColumn("approvalStagesName")' >Status</th>
								<th style="width: 20px"></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat = "res in datas | orderBy:column:reverse" ng-show="hideData" class="ng-hide">
								<td><input type="checkbox" class="radio" ng-model="res.selected" ng-click="checkList(res)"></td>
								<td>{{res.instancesId}}</td>
								<td>{{res.businessProcess}}</td>
								<td>{{res.absencesType}} - {{res.absencesDesc}}</td>
								<td>{{res.requesterId}}-{{res.requesterName}}</td>
								<td>{{res.assigner}} - {{res.assignerName}}</td>
								<td>{{res.startDate}} - {{res.endDate}}</td>
								<td>{{res.approvalStagesName}}</td>
								<td align="center">
											<a href='<c:url value="${cons.DETAIL_URL}{{linkAction.intancesId+res.instancesId}}">
													</c:url>' class="btn bg-orange btn-flat btn-small" id="detail">
												<i class="fa fa-fw fa-edit"></i>
											</a>
										</td>	
							</tr>
							<tr ng-show="hideData == false" class="ng-hide">
								<td colspan="8"><center>List Task Empty</center></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div ng-if="listModel.length > 0" class="ng-hide" ng-show="hideData">
			<input type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSearch1" value="Re Assign Task ">
		</div>
		<div ng-if="listModel.length == 0" class="ng-hide" ng-show="hideData">
			<input type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSearch1" disabled="disabled" value="Re Assign Task ">
		</div>
		<div class="footer">
			<jsp:include page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
		</div>
	</section>

	<!-- The modal -->
	<div class="modal fade" id="listEmp" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Approval Task</h4>
				</div>
				<div class="modal-body">
					List Approval
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-secondary" ng-click="" data-dismiss="modal">Submit</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- The modal -->
		<div class="modal fade" id="modalSearch1" tabindex="-1" role="dialog"
			aria-labelledby="modalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm5" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="modalLabel">Search Username</h4>
					</div>
					<div class="modal-body">
						<div class="panel-body table-responsive" style="overflow-x:auto!important;">
						<table class="table table-hover">
							<thead>
								<form class="form-inline">
	  								<div class="form-group col-sm-4">
	  									<label for="sid" class="sr-only" >Search</label>
	    								<input type="text" ng-model="username" class="form-control form-control-sm" id="sid" placeholder="Username" >
	  								</div>
	  								<div class="form-group col-sm-2" >
						      			<button type="submit" class="btn btn-primary mb-2" ng-click="search()">Search</button>
						    		</div>
								</form>
								<tr>
									<th style="width: 50px;" class="col-sm-0">Username</th>
									<th style="width: 150px;">Fullname</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-if="dataFlag === false">
									<td>{{detail.username}}</td>
									<td>{{detail.fullname}}</td>
								</tr>
								<tr ng-if="dataFlag" >
									<td colspan="2"><center>Username Not Found !!!</center></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					</div>
					<div ng-if="detail.username == undefined" class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Close</button>
					</div>
					<div ng-if="detail.username != undefined" class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-secondary" ng-click= "submit()"
							data-dismiss="modal">Submit</button>
					</div>
				</div>
			</div>
		</div>
	
</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>
