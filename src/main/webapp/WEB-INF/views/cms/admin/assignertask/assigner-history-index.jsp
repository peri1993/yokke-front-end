<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.AssignerHistoryController" scope="page" />

<div class="content-wrapper" style="background: white"
	ng-controller="AssignerHistoryController">
	<input type="hidden" ng-model="_csrf.token"
		ng-init="_csrf.token = '${_csrf.token}'" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	<section class="content-header">
		<h1>
			<h1>
				<b>History Approve & Reject</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message
						key="label.header.module.dashboard" /></a></li>
			<li><a href="${context}${routes.ASSIGNER_HISTORY}">List Task</a></li>
			<li class="active"><fmt:message key="label.name.list" /></li>
		</ol>
	</section>
	<section class="content" style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">List History</strong>
			</div>
			<div id=inboxp>
				<div class="panel-body table-responsive text-nowrap"
					style="overflow-x: overlay;max-height: 500px">
					<table class="table table-hover">
						<thead>
							<form class="form-inline">
								<div class="form-group col-sm-3">
									<select class="form-control" ng-model="paramKey"
										ng-change="inputParam(paramKey)">
										<option value="" disabled selected>Select your option</option>
										<option value="instanceId">No. Pengajuan</option>
										<option value="createdBy">ID Pemohon</option>
										<option value="createdDt">Tanggal Pengajuan</option>
										<option value="stages">Status</option>
									</select>
								</div>
								<div class="col-sm-3" ng-if="dateFlag === 'N'">
							      	<input type="text" class="form-control form-control-sm" ng-model="paramVal" ng-change="onChangeValue(paramKey, paramVal)">
							    </div>
							    <div class="col-sm-3 ng-hide" ng-if="dateFlag === 'Y'" ng-show = "hideData">
							      	<div class="form-group">
										<div id="">
											<input type="date" placeholder="Start date" ng-model="paramVal" ng-change="onChangeValue(paramKey, paramVal)" class="form-control form-control-sm" />
										</div>
									</div>
							    </div>
							    <div class="col-sm-3 ng-hide" ng-if="dateFlag === 'Y'" ng-show="hideData">
							      	<div class="form-group">
										<div id="">
											<input type="date" placeholder="End date" ng-model="paramVal2" ng-change="onChangeValue2(paramKey, paramVal2)" class="form-control form-control-sm" />
										</div>
									</div>
							    </div>
  								<div class="form-group col-sm-2" >
					      			<button type="submit" class="btn btn-primary mb-2" ng-click="search()">Submit</button>
					    		</div>
							</form>
							<tr>
								<tr>
								<th class="col-sm-0"
									ng-click='sortColumn("instancesId")' ng-class='sortClass("instancesId")'>No. Pengajuan</th>
								<th class="col-sm-0"
									ng-click='sortColumn("bussinessProcess")' ng-class='sortClass("taskId")'>Jenis Pengajuan</th>
								<th ng-click='sortColumn("createdBy")'>Nama Pemohon</th>
								<th ng-click='sortColumn("createdDt")'
									ng-class='sortClass("absencesDesc")'>Tanggal Pengajuan</th>
								<th ng-click='sortColumn("stages")'
									ng-class='sortClass("requesterId")'>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="res in datas | orderBy:column:reverse"
								ng-show="hideData == true" class="ng-hide">
								<td>{{res.instancesId}}</td>
								<td>{{res.bussinessProcess}}</td>
								<td>{{res.createdBy}}</td>
								<td>{{res.createdDt}}</td>
								<td>{{res.stages}}</td>
								<td align="center"><a
									href='<c:url value="${cons.DETAIL_URL}?instanceId={{res.instancesId}}&businessProcess={{res.bussinessProcess}}"></c:url>'
									class="btn bg-orange btn-flat btn-small" id="detail"> <i
										class="fa fa-fw fa-edit"></i>
								</a></td>
							</tr>
							<tr ng-show="hideData == false" class="ng-hide">
								<td colspan="8"><center>List Task Empty</center></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div ng-if="listModel.length > 0" class="ng-hide" ng-show="hideData">
			<input type="button" class="btn btn-primary" data-toggle="modal"
				ng-click="tesModal()" data-target="#approveTask"
				value="Approve Task"> <input type="button"
				class="btn btn-primary" data-toggle="modal" ng-click="tesModal()"
				data-target="#rejectTask" value="Reject Task">
		</div>
		<div class="footer">
			<jsp:include
				page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
		</div>
	</section>

	<!-- The modal -->
	<div class="modal fade" id="approveTask" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Approval Task</h4>
				</div>
				<div class="modal-body">Apakah anda yakin ?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button ng-if="buttonApprove === true" type="button"
						class="btn btn-secondary" ng-click="onSubmitApproved()"
						data-dismiss="modal" data-target="#successTask"
						data-toggle="modal">Submit</button>
				</div>
			</div>
		</div>
	</div>

	<!-- The modal -->
	<div class="modal fade" id="successTask" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Success</h4>
				</div>
				<div class="modal-body">{{messageValidate}}</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<!-- The modal -->
	<div class="modal fade" id="rejectTask" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Approval Task</h4>
				</div>
				<div class="modal-body">Apakah anda yakin untuk me-reject task
					?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button ng-if="buttonApprove === true" type="button"
						class="btn btn-secondary" ng-click="onSubmitRejected()"
						data-dismiss="modal" data-target="#successTask"
						data-toggle="modal">Submit</button>
				</div>
			</div>
		</div>
	</div>

</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>