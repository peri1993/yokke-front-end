<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes" scope="page" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<div class="content-wrapper" style="background: white">
	<section class="content-header" >
		<h1>Role Management</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message key="label.header.module.dashboard"/></a></li>
			<li><a href="${context}${routes.ROLE_MANAGEMENT}">Role Management List</a></li>
			<li class="active"><fmt:message key="label.name.add"/></li>
		</ol>
	</section>
	<section class="content" style="background: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">Role Menu</strong>
			</div>
		<c:if test="${error != null}">
			<div class="row">
				<div class="alert alert-danger display-none" id="add-item-error">
						<center><c:out value="${error}"></c:out></center>
				</div>
			</div>
		</c:if>
		<div class="row">
			<div class="col-xs-12">
			<table class="table table-hover" id="groupManagement">
				<form id="groupForm" action="${context}${roleId != null ? '/role-management/update' : '/role-management/save'}" method="post">
				<div class="box-header">
					
						<c:if test="${roleId != null }">
							<input type="hidden" name="roleId" value="${roleId}" />
						</c:if>
						<div class="form-group row">
							<label for="inputPassword" class="col-sm-4 col-form-label"><fmt:message
									key="label.header.groupmanagement.user.group.name" /> </label>
							<div class="col-sm-8">
								<input required="required" type="text" id="nameRole"
									name="nameRole" class="form-control" placeholder="Role Name"
									value="${nameRole}" onkeypress="return alpha(event)">
							</div>
						</div>
				</div>
						<div class="box-body">
								 <div class="form-group row">
								    <label for="inputPassword" class="col-sm-4 col-form-label">Menu </label>
								    <div class="col-sm-8">
								      <label for="inputPassword" class="col-sm-12 col-form-label">Task </label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								    	<input class="form-check-input" id="inlineCheckbox1" type="checkbox" name="menu[]" <c:if test="${inbox == true}"> checked="checked" </c:if> value="inbox" />
								  		<label class="form-check-label" for="inlineCheckbox1">Inbox</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								    	<input class="form-check-input" id="inlineCheckbox1" type="checkbox" name="menu[]" <c:if test="${assignerHistory == true}"> checked="checked" </c:if> value="assignerHistory" />
								  		<label class="form-check-label" for="inlineCheckbox1">History Approve/Reject</label>
								    </div>
								 </div>
								 <div class="form-group row">
								    <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-4">
								      <label for="inputPassword" class="col-sm-12 col-form-label">Leave</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${requestLeave == true}"> checked="checked" </c:if> value="requestLeave" />
				  						<label class="form-check-label" for="inlineCheckbox1">Request Leave</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${historyLeave == true}"> checked="checked" </c:if> value="historyLeave" />
				  						<label class="form-check-label" for="inlineCheckbox1">History Leave</label>
								    </div>
								 </div>
								 
								 <div class="form-group row">
								    <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-4">
								      <label for="inputPassword" class="col-sm-12 col-form-label">Permit </label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${requestPermit == true}"> checked="checked" </c:if> value="requestPermit" />
				  						<label class="form-check-label" for="inlineCheckbox1">Request Permit</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${historyPermit == true}"> checked="checked" </c:if> value="historyPermit" />
				  						<label class="form-check-label" for="inlineCheckbox1">History Permit</label>
								    </div>
								 </div>
								 <div class="form-group row">
								    <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-4">
								      <label for="inputPassword" class="col-sm-12 col-form-label">Attendance </label>
								    </div>
								 </div>
								  <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${historyAttendance == true}"> checked="checked" </c:if> value="historyAttendance" />
				  						<label class="form-check-label" for="inlineCheckbox1">History Absensi</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${listAttendance == true}"> checked="checked" </c:if> value="listAttendance" />
				  						<label class="form-check-label" for="inlineCheckbox1">List Data Employee</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${qrAttendance == true}"> checked="checked" </c:if> value="qrAttendance" />
				  						<label class="form-check-label" for="inlineCheckbox1">Scan QR</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${InformationWorkHours == true}"> checked="checked" </c:if> value="InformationWorkHours" />
				  						<label class="form-check-label" for="inlineCheckbox1">Information Work Hours</label>
								    </div>
								 </div>
								 <div class="form-group row">
								    <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-4">
								      <label for="inputPassword" class="col-sm-12 col-form-label"><fmt:message key="label.header.module.cat.admin"/> </label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${assignerTask == true}"> checked="checked" </c:if> value="assignerTask" />
				  						<label class="form-check-label" for="inlineCheckbox1">Re-Assigned Task</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${notification == true}"> checked="checked" </c:if> value="notification" />
				  						<label class="form-check-label" for="inlineCheckbox1">Notification</label>
								    </div>
								 </div>
								
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${roleManagement == true}"> checked="checked" </c:if> value="roleManagement" />
				  						<label class="form-check-label" for="inlineCheckbox1">Role Management</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${userManagement == true}"> checked="checked" </c:if> value="userManagement" />
				  						<label class="form-check-label" for="inlineCheckbox1">User Management</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${branchConfig == true}"> checked="checked" </c:if> value="branchConfig" />
				  						<label class="form-check-label" for="inlineCheckbox1">Branch Config</label>
								    </div>
								 </div>
								 <div class="form-group row">
								   <label for="inputPassword" class="col-sm-4 col-form-label"></label>
								    <div class="col-sm-8">
								       	<input class="form-check-input" type="checkbox" name="menu[]" <c:if test="${allAssignerHistory == true}"> checked="checked" </c:if> value="allAssignerHistory" />
				  						<label class="form-check-label" for="inlineCheckbox1">History Task Selesai</label>
								    </div>
								 </div>
								 <div class="modal-footer">
									  <c:if test="${roleId != null}">
									 	<button id="btnAdd" type="submit" class="btn btn-primary"><fmt:message key="label.name.update"/></button>
									 </c:if>
								     <c:if test="${roleId == null}">
									 	<button id="btnAdd" type="submit" class="btn btn-primary"><fmt:message key="label.name.create"/></button>
									 </c:if>
							        <a href="${context}${routes.ROLE_MANAGEMENT}" type="button" class="btn btn-danger" data-dismiss="modal"><fmt:message key="label.name.back"/></a>
							     </div>
							     		<input type="hidden" name="${_csrf.parameterName}"
												value="${_csrf.token}" />
											<c:if test="${errMsg != null}">
												<label class="control-label" for="inputError"
													style="color: #dd4b39"><i class="fa fa-times-circle-o"></i>
													${errMsg}</label> 
												<br />
											</c:if>
											 <input type="hidden" name="selectedVehicles" id="selectedVehicles"/>
					      	</form>
						</table>
					</div>
			</div>
		</div>
	</section>
</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>
