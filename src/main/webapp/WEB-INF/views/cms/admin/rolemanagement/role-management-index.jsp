<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes" 	scope="page" />
<jsp:useBean id="cons" class="com.yokke.web.controller.RoleManagementController" 	scope="page" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<div class="content-wrapper" style="background: white" ng-controller="RoleManagementController">
	<section class="content-header">
		<h1>Role Management</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message key="label.header.module.dashboard"/></a></li>
			<li><a href="${context}${routes.ROLE_MANAGEMENT}">Role Management</a></li>
			<li class="active"><fmt:message key="label.name.list"/></li>
		</ol>
	</section>
	<div class="box-header">
		<div class="col-sm-2 col-xs-12 pull-right">
			<a href="${context}${cons.ADD_GROUP}"
				class="btn btn-sm btn-block btn-social btn-dropbox"> <i
				class="fa fa-plus"></i>Tambah Role Baru
			</a>
		</div>
	</div>
	<section class="content" style="background: white">
	<div class="row" ng-if="deleteFlag === 'Y'" >
			<div class="alert alert-danger ng-hide" id="add-item-error" ng-show="hideData">
				<center>{{status}} - {{message}}</center>
			</div>
	</div>
	<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">Task List</strong>
			</div>
	<div id=inboxp>
	<div class="panel-body table-responsive" style="overflow-x:scroll; width: auto;">
		<c:if test="${isDelete == true}">
		<div class="form-group row">
			<div class="alert alert-danger display-none" id="add-item-error">
					<center><fmt:message key="label.validation.group.management.deleted"/></center>
			</div>
		</div>
		</c:if>
			<div class="">
						<table class="table table-hover" id="groupManagement">
							<thead>
								<tr>
									<!-- <th style="width: 50px;">ID Role</th> -->
									<th style="width: 95px;">Nama Role</th>
									<th style="width: 85px;">Created By</th>
									<th style="width: 130px;">Tanggal Pembuatan</th>
									<th style="width: 90px;">Updated By</th>
									<th style="width: 130px;">Tanggal Update</th>
									<th style="width: 100px;">Akses Menu</th>
									<th style="width: 60px;"></th>
								</tr>
							</thead>
							<tbody>
									<tr ng-if="emptyData === 'false'" ng-repeat="res in datas track by $index | orderBy:taskId:reverse" ng-show="hideData" class="ng-hide">
										<!-- <td>{{res.role_id}}</td> -->
										<td>{{res.roleName}}</td>
										<td>{{res.createdBy}}</td>
										<td>{{res.createdDt}}</td>
										<td>{{res.updatedBy}}</td>
										<td>{{res.updatedDt}}</td>
										<td>{{res.menuAccess}}</td>
										<td align="center">
											<a href='<c:url value="${cons.ADD_GROUP}{{linkAction.roleId+res.role_id}}">
													</c:url>' class="btn bg-orange btn-flat btn-small" id="detailGroup">
												<i class="fa fa-fw fa-edit"></i>
											</a>
											
											<a href="#" ng-click="deleteRole(res.role_id)" class="btn bg-maroon btn-flat btn-small">
												<i class="fa fa-fw fa-trash"></i>
											</a>
										</td>	
									</tr>
									<tr ng-if="emptyData === 'true'"><td colspan="7"><center>Data Is Empty !</center></td></tr>
							</tbody>
						</table>
				</div>
			</div>
		</div>
		</div>
	</section>
</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>

									
									
									
									
									