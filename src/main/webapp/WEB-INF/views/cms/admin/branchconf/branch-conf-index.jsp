<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.BranchConfController"
	scope="page" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/cms/components/message-error.jsp"></jsp:include>
<div class="content-wrapper" ng-controller="BranchConfController"
	style="background: white">
	<section class="content-header">
		<h1>
			<h1>
				<b><fmt:message key="label.header.module.branchconf" /></b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message
						key="label.header.module.dashboard" /></a></li>
			<li><a href="${context}${cons.MAIN_URL}"><fmt:message
						key="label.header.module.branchconf" /></a></li>
			<li class="active"><fmt:message key="label.name.list" /></li>
		</ol>
	</section>
	<section class="content" style="background: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">List Branch </strong>
			</div>
			<div>
				<div class="panel-body table-responsive text-nowrap" style="overflow-x:overlay;max-height: 500px">
					<table class="table table-hover">
						<thead>
							<form class="form-inline" action="${context}${cons.MAIN_URL}" novalidate="novalidate" method="get">
								<div class="form-group col-sm-3">
									<select class="form-control" ng-model="paramKey"
										ng-change="inputParam(paramKey)">
										<option value="" disabled selected>Select your option</option>
										<option value="empArea">Emp Area</option>
										<option value="empSubArea">Emp Sub Area</option>
										<option value="empSubAreaDescription">Emp Sub Area Description</option>
										<option value="address">Address</option>
										<option value="npwp">NPWP</option>
										<option value="latitudeCode">Latitude Code</option>
										<option value="longitudeCode">Longitude Code</option>
									</select>
								</div>
								<div class="col-sm-3">
									<input type="text" class="form-control" ng-model="paramVal"
										ng-change="onChangeValue(paramKey, paramVal)" placeholder="Find Here" onkeypress="return alpha(event)">
								</div>
								<div class="col-sm-2">
									<a type="submit" class="btn btn-primary"
										ng-click="search(paramKey, paramValue)"><fmt:message
											key="label.name.submit" /></a>
								</div>
							</form>
							<tr>
								<th style="width: 20px;">No.</th>
								<th style="width: 80px;">Emp Area</th>
								<th style="width: 150px;">Emp Sub Area</th>
								<th style="width: 30px;">Landscape</th>
								<th style="width: 130px;">Address</th>
								<th style="width: 80px;">NPWP</th>
								<th style="width: 100px;">Code Latitude</th>
								<th style="width: 100px;">Code Longitude</th>
								<th>Radius</th>
								<th style="width: 50px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-if="emptyData === 'false'"
								ng-repeat="res in datas | orderBy:column:reverse"
								ng-show="hideData" class="ng-hide">
								<td>{{$index+1 + from}}</td>
								<td>{{res.empArea}}</td>
								<td>{{res.empSubArea}}-{{res.empSubAreaDescription}}</td>
								<td>{{res.landscape}}</td>
								<td>{{res.address}}</td>
								<td>{{res.npwp}}</td>
								<td><input name="latitudeCode{{index}}" ng-model="res.latitudeCode" ng-disabled="!enabledEdit[{{$index}}]" /></td>
								<td><input name="latitudeCode{{index}}" ng-model="res.longitudeCode" ng-disabled="!enabledEdit[{{$index}}]" /></td>
								<td><input name="latitudeCode{{index}}" ng-model="res.radius" ng-disabled="!enabledEdit[{{$index}}]" placeholder="Meter" /></td>
								<td align="center">
									<a href="#" ng-click="add($index)"
									class="btn bg-orange btn-flat btn-small" id="detailGroup">
										<i class="fa fa-fw fa-edit"></i>
									</a> 
									<a href="#" ng-click="update(res, $index)"
									class="btn bg-green btn-flat btn-small" id="idUser"> <i
										class="fa fa-fw fa-save"></i>
									</a>
								</td>
							</tr>
							<tr ng-if="emptyData === 'true'">
								<td colspan="9"><center>
										<fmt:message key="label.name.not.found" />
									</center></td>
							</tr>
						</tbody>
					</table>
					<div class="box-footer clearfix">
						<jsp:include
							page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>