<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>

<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.LoginController" scope="page" />
<jsp:useBean id="notif"
	class="com.yokke.web.controller.NotificationsController"
	scope="page" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />

<spring:htmlEscape defaultHtmlEscape="true" />
<!DOCTYPE html>
<html ng-app="GCN_Maintenance">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<title>Mega Employee Mobile</title>
<link rel="icon" type="image/gif/png"
	href="${context}/assets/img/logoMega.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="stylesheet"
	href="${context}/assets/resources/AdminLTE2/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${context}/assets/resources/fontawesome/css/all.css">
<link rel="stylesheet"
	href="${context}/assets/resources/AdminLTE2/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="${context}/assets/cms/app/css/ionicons.css">
<link rel="stylesheet"
	href="${context}/assets/resources/fontawesome/css/fontawesome.css">
<link rel="stylesheet"
	href="${context}/assets/resources/AdminLTE2/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet"
	href="${context}/assets/resources/AdminLTE2/plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="${context}/assets/cms/app/css/loader.css">

<link rel="stylesheet" href="${context}/assets/cms/app/css/styles.css">
<link rel="stylesheet"
	href="${context}/assets/resources/Airdatepicker/css/datepicker.min.css">
<link rel="stylesheet"
	href="${context}/assets/resources/Clocktimepicker/bootstrap-clockpicker.css">
<%-- <link rel="stylesheet" href="${context}/assets/resources/datetimepicker/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="${context}/assets/resources/datetimepicker/css/bootstrap-datetimepicker.css">
 --%>

<script src="${context}/assets/cms/app/js/jquery.min.js"></script>
<script src="${context}/assets/resources/angular/js/angular.min.js"></script>
<script
	src="${context}/assets/resources/angular/js/ui-bootstrap-tpls-2.4.0.min.js"></script>
<script src="${context}/assets/cms/app/js/app.js"></script>
<script src="${context}/assets/cms/app/js/controller.js"></script>
<script src="${context}/assets/cms/app/js/controller-admin.js"></script>
<script src="${context}/assets/cms/app/js/controller-task.js"></script>
<script src="${context}/assets/cms/app/js/controller-attendance.js"></script>
<script src="${context}/assets/cms/app/js/controller-inform-work.js"></script>
<script src="${context}/assets/cms/app/js/app-input.js"></script>
<%-- <script src="${context}/assets/cms/app/js/moment.min.js"></script> --%>
<script src="${context}/assets/cms/app/js/ckeditor.js"></script>
<%-- <script src="${context}/assets/cms/app/js/qrcode.min.js"></script>
<script src="${context}/assets/cms/app/js/qrcode.js"></script> --%>
<script
	src="${context}/assets/resources/Clocktimepicker/bootstrap-clockpicker.js"></script>
<script
	src="${context}/assets/resources/Clocktimepicker/bootstrap-clockpicker.min.js"></script>
<script src="${context}/assets/cms/app/js/popper-min.js"></script>
<%-- <script src="${context}/assets/cms/app/js/qr_packed.js"></script> --%>
<script src="${context}/assets/cms/app/js/jquery-3.4.1.slim.min.js"></script>
<script src="${context}/assets/resources/qrcodescanner/instascan.min.js"></script>

</head>
<body>
	<div class="loading-gif" id="overlay">
		<img src="${context}/assets/img/loader3.gif">
	</div>
	<div class="hold-transition skin-yellow sidebar-mini" id="message"
		style="display: none;">
		<div class="wrapper">
			<header class="main-header">
				<a href="#" class="logo"> <span class="logo-mini"><b>ESS</b></span>
					<span class="logo-lg"><b>Absensi & Izin/Cuti</b></span>
				</a>
				<nav class="navbar navbar-static-top" role="navigation">
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
						role="button"> <i class="fa fa-bars"></i>
					</a>

					<div class="navbar-custom-menu" ng-controller="NotifControllerUser">
						<ul class="nav navbar-nav">
							<%-- <li class="treeview">
							<a href="${context}${routes.QR_ATTENDANCE}">
							<i class="fa fa-qrcode"></i>
							</a>
							</li> --%>
							<li class="dropdown notifications-menu"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"> <i
									class="glyphicon glyphicon-bell"></i> <span
									class="label label-danger ng-hide" ng-show="hideData">{{dataSize}}</span>
							</a>
								<ul class="dropdown-menu">
									<li class="header">
									<center>You have {{dataSize}}notifications</center></li>
									<li>
										<!-- inner menu: contains the actual data -->
										<ul class="menu">
											<li style="font-size: 13px;"
												ng-repeat="res in datas track by $index"><a
												href="${context}${notif.DETAIL_URL}?instancesId={{res.instancesId}}&process={{res.businessProcess}}&status={{res.stages}}&id={{res.id}}">
													<i class="fa fa-circle text-red "></i> {{res.description}}
											</a></li>
										</ul>
									</li>
									<li class="footer"><a
										href="${context}${routes.NOTIFICATIONS}">View all</a></li>
								</ul></li>
							<li class="dropdown user user-menu"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"> <img
									src="${context}/assets/resources/AdminLTE2/dist/img/def_img.png"
									class="user-image" alt="User Image"> <span
									class="hidden-xs">${sessionScope._user.displayName}</span>
							</a>
								<ul class="dropdown-menu">
									<li class="user-header" style="height: auto"><img
										src="${context}/assets/resources/AdminLTE2/dist/img/def_img.png"
										class="img-circle" alt="User Image">
										<p>${sessionScope._user.displayName}-
											${sessionScope._user.fullname}</p>
										<p>${sessionScope._user.jobDesc}</p></li>
									<li class="user-footer">
										<div class="pull-right">
											<a href="${context}${cons.LOGOUT_URL}"
												class="btn btn-default">Sign out</a>
										</div>
									</li>
								</ul></li>
						</ul>
					</div>
				</nav>
			</header>
			<jsp:include page="side-bar.jsp" />
<script type="text/javascript">
		// Registering Service Worker
			if ("serviceWorker" in navigator) {
				if (navigator.serviceWorker.controller) {
					console.log("active service worker found, no need to register");
					} else {
						// Register the service worker
					navigator.serviceWorker.register("${context}/sw.js", {
						scope : "/ess/"
					}).then(function(reg) {
						console.log("Service worker has been registered for scope: "+ reg.scope);
					});
				}
			}
</script>