<!-- Small boxes (Stat box) -->
<div class="row">
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3>
					80<sup style="font-size: 20px">%</sup>
				</h3>
				<p>80 Success</p>
				<p>20 Failed</p>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<a class="small-box-footer">Daily</a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
				<h3>
					100<sup style="font-size: 20px">%</sup>
				</h3>
				<p>285 Success</p>
				<p>0 Failed</p>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<a class="small-box-footer">Weekly</a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-yellow">
			<div class="inner">
				<h3>
					65<sup style="font-size: 20px">%</sup>
				</h3>
				<p>135 Success</p>
				<p>13 Failed</p>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<a class="small-box-footer">Monthly</a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
				<h3>
					45<sup style="font-size: 20px">%</sup>
				</h3>
				<p>12.850 Success</p>
				<p>15.300 Failed</p>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<a class="small-box-footer">Yearly</a>
		</div>
	</div>
	<!-- ./col -->
</div>
<!-- /.row -->