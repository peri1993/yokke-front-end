<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons" class="com.yokke.web.controller.DashboardController" scope="page" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>

<div class="content-wrapper"
	style="background-color: white; min-height: 300px">
	<section class="content-header" style="background-color: white;">
		<!-- <span class="font-weight-bold">
			<h1>Selamat Datang di Aplikasi Mega Employee Mobile</h1>
		</span>
		<hr /> -->
	</section>
	
	<section class="content" style="background-color: white;">
		<!-- Widget -->
		<%-- <jsp:include page="/WEB-INF/views/cms/dashboard/dash-widget.jsp"></jsp:include> --%>
		<ul>
			<div>
				<i class="fa fa-user-circle fa-3x"></i>
			<span class="text-primary font-weight-bold" data-target="#usercol" data-toggle="collapse">
				<strong>Informasi Quota User</strong>
				<i class="fa fa-angle-down pull-right"></i>
			</span>
			</div>
			<div class="text-dark" id="usercol" ng-controller="LeaveController">
				<ul>
					<table class="table">
						<tr class="ng-hide" ng-show="hideData">
							<td style="width: 250px">Kuota Tersisa</td>
							<td>{{datasLastRemaining}}</td>
						</tr>
						<tr class="ng-hide" ng-show="hideDataC">
							<td>Carry Over Quota</td>
							<td>{{datasLastRemainCarry}}</td>
						</tr>
						<tr class="ng-hide" ng-show="hideData">
							<td style="width: 250px">Kuota Pending</td>
							<td>{{datasLastRemainingPending}}</td>
						</tr>
						<tr class="ng-hide" ng-show="hideData">
							<td style="width: 250px">Kuota Sisa Untuk Request</td>
							<td>{{totalRequest}}</td>
						</tr>
						<tr class="ng-hide" ng-show="hideData">
							<td>Sudah Mengambil BlockLeave</td>
							<td>{{datasBlockLeave}}</td>
						</tr>
						<tr ng-controller="LeaveHistoryController">
							<td>Jumlah History Cuti</td>
							<td>{{countDatas}}</td>
						</tr>
						<tr ng-controller="PermitHistoryController">
							<td>Jumlah History Izin</td>
							<td>{{countDatas}}</td>
						</tr>
					</table>
				</ul>
			</div>
			<hr/>
		</ul>
		<!-- <ul>
			<i class="fas fa-exclamation-circle fa-3x"></i>
			<span class="text-primary font-weight-bold" data-toggle="collapse" data-target="#collapseExample">
				<strong>Perhatian </strong>
				<i class="fa fa-angle-down pull-right"></i>
			</span>
			<p class="text-dark collapse" id="collapseExample">
				<span class="text-dark"># Agar keamanan aplikasi ini tetap
					terjaga, diharapkan kepada seluruh User sistem ini</span> 
				<span class="text-danger">untuk tidak memberikan usename dan
					password kepada orang lain.
				</span><br><br>
				<span>
					# Sistem ini dilengkapi dengan <span class="text-danger">Log
					System Security System,</span> guna dapat memantau segala aktivitas User
				terhadap sistem ini. <span class="text-danger"> dengan
					memberikan Username dan Password anda Kepada orang lain maka
					bertanggug jawab penuh</span> terhadap data yang anda kelola.
				</span><br><br>
				<span>
					# Setiap menggunakan aplikasi ini diharapkan kepada seluruh user
				untuk selalu melakukan <span class="text-danger">"Logout" [
					menu ini terdapat pada sebelah kanan atas ]</span> guna keamanan sistem
				ini tetap terjaga.
				</span>
			</p>
			<hr/>
		</ul>
		<ul>
			<i class="fas fa-newspaper fa-3x"></i>
			<span class="text-primary font-weight-bold">
				<strong>Berita Terkini</strong>
				<i class="fa fa-angle-down pull-right"></i>
			</span>
			<p># Saat ini tidak berita terbaru.</p>
			<hr />

		</ul>
		<ul>
			<i class="far fa-calendar fa-3x"></i>
			<span class="text-primary font-weight-bold"><strong>
					Agenda Terkini</strong></span>
			<p># Saat ini tidak agenda terbaru.</p>
			<hr />
		</ul> -->
		<%-- <ul>
			<i class="fab fa-google-play fa-3x"></i>
			<span class="text-primary font-weight-bold"><strong>
					<!-- <a href="https://drive.google.com/open?id=18Q6sA2uZgNTgYlSfRB9WNMYk1h9rbELf">Download APK</a></strong></span> -->
				<a href='<c:url value="${cons.DOWNLOAD_URL}">
					</c:url>'>Download APK
				</a>
			<hr />
		</ul> --%>
	</section>
</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>
