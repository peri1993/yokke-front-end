<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<div class="content-wrapper">
	<section class="content-header">
   	<h1>Partner
   	<c:choose>
   		<c:when test="${partner.partnerId != null}">
   			<small>Edit</small></h1>
   		</c:when>
   		<c:otherwise>
   			<small>Add</small></h1>
   		</c:otherwise>
   	</c:choose>
      <ol class="breadcrumb"></ol>
	</section>
   <section class="content">
	   <div class="row">
	   	<div class="col-md-8">
	   		<div class="box box-primary">
	            <form action="${context}${constant.SAVE_URL}" method="post" enctype="multipart/form-data">
	            	<input type="hidden" name="partnerId" value="${partner.partnerId}"/>
	            	<input type="hidden" name="createdDate" value='<fmt:formatDate value="${partner.createdDate}" pattern="yyyy-MM-dd H:m:s"/>'/>
	           		<div class="box-body">
	           			<div class="form-group">
	                  	<label>Status</label>
	                  	<select name="isActive" class="form-control">
	                  		<c:choose>
	                  			<c:when test="${partner.isActive}">
	                  				<option value="false">Inactive</option>
	                  				<option value="true" selected="selected">Active</option>	
	                  			</c:when>
	                  			<c:otherwise>
	                  				<option value="false">Inactive</option>
	                  				<option value="true">Active</option>
	                  			</c:otherwise>
	                  		</c:choose>
	                  	</select>
	                	</div>
	                	<div class="form-group">
	                  	<label for="entryName">Partner Code</label>
	                  	<input type="text" name="partnerCode" class="form-control" value="${partner.partnerCode}" required="required"/>
	                	</div>
	                	<div class="form-group">
	                  	<label for="entryName">Partner Name</label>
	                  	<input type="text" name="partnerName" class="form-control" value="${partner.partnerName}" required="required"/>
	                	</div>
	                	<div class="form-group">
	                  	<label for="entryName">Campaign Code</label>
	                  	<input type="text" name="campaignCode" class="form-control" value="${partner.campaignCode}"/>
	                	</div>
	                	<div class="form-group">
	                  	<label for="entryName">Text 1</label>
	                  	<textarea name="text1" class="form-control" id="text1">${partner.text1}</textarea>
	                	</div>
	               	<div class="form-group">
	                  	<label for="entryImg">Upload Banner</label>
	                  	<input type="file" class="form-control" name="banner"/>
	                  	<c:if test="${imageUrl != null}">
	                  		<img width="160px" src="${imageUrl}" />
	                  		<input type="hidden" value="${partner.bannerUrl}" name="fileName"/>
	                  	</c:if>
	                	</div>
	                	<div class="box-footer">
		                	<button type="submit" class="btn btn-primary">Submit</button>
		              	</div>
	              	</div>
	            </form>
	       	</div>
	      </div>
		</div>
   </section>
</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>
<script>
CKEDITOR.replace('text1', {
	customConfig: '${context}/assets/cms/resources/AdminLTE2/plugins/ckeditor/config.js'
});
</script>