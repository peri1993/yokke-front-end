<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.LeaveController" scope="page" />
<div id="LeaveController" style="background: white"
	class="content-wrapper" ng-controller="LeaveController">
	<div class="panel panel-info">
		<div class="panel-heading">
			<strong>Header</strong>
		</div>
		<div class="" id="col-header">
			<div class="content" style="background-color: white">
				<div class="row">
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Employee</div>
							<form>

								<input type="hidden" ng-model="_csrf.token"
									ng-init="_csrf.token = '${_csrf.token}'"
									name="${_csrf.parameterName}" value="${_csrf.token}" />

								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq"> ID
										Pemohon</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="id" ng-model="datas.username" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="rq">Nama
										Pemohon</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="name" ng-model="datas.fullname" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Subject</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											onkeypress="return alpha(event)" id="subject"
											ng-model="subject" />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Deskripsi
										Pengajuan</label>
									<div class="col-sm-6">
										<textarea class="form-control" rows="7" cols="26" id="desc"
											onkeypress="return alpha(event)" ng-model="description"></textarea>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Assigner</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="qt">NIP Assigner 1</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval1" ng-model="datas.approval1" disabled />
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Nama Assigner</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval1Name" ng-model="datas.nameApproval1" disabled />
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="qt"
										style="margin-top: 10px">NIP Assigner 2</label>
									<div class="col-sm-6" style="margin-top: 10px">
										<input type="text" class="form-control form-control-sm"
											id="approval2" ng-model="datas.approval2" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Nama Assigner</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval2name" ng-model="datas.nameApproval2" disabled />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<strong>Main</strong>
		</div>
		<div class="" id="col-body">
			<div class="content" style="background-color: white">
				<div class="row">
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Detail Cuti</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-3 col-form-label-sm" for="att">Jenis
										Cuti</label>
									<div class="col-sm-6">
										<select class="form-control form-control-sm" id="att"
											ng-options="res.description for res in datasAbsence"
											ng-model="absence" ng-change="showCurrentQuotaLeave(absence)"
											required>
										</select>
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-3 col-form-label-sm" for="startdate">Dari Tanggal</label>
									<div class="col-sm-5">
										<div class="form-group">
											<div id="">
												<input type="date" ng-model="startdate"
													ng-change="quotaTakenFunc(startdate,enddate)"
													class="form-control form-control-sm" />
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-3 col-form-label-sm" for="enddate">Sampai Tanggal</label>
									<div class="col-sm-5">
										<div class="form-group">
											<div id="">
												<input type="date" ng-model="enddate"
													ng-change="quotaTakenFunc(startdate,enddate)"
													class="form-control form-control-sm" />

												<!-- <input type="text" ng-model="enddate"
															ng-change="quotaTakenFunc(startdate,enddate)"
															class="form-control form-control-sm" datepicker /> -->
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-3 col-form-label-sm" for="tqt">Total
										Kuota Ambil</label>
									<div class="col-sm-4">
										<input type="text" ng-model="quotaTaken"
											class="form-control form-control-sm" id="tqt" disabled>
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-3 col-form-label-sm" for="tqt">Alasan
										Cuti</label>
									<div class="col-sm-6">
										<textarea type="text" class="form-control" ng-model="reason"
											onkeypress="return alpha(event)" cols="50" rows="6"
											style="margin-top: 9px"></textarea>
									</div>
								</div>
							</form>
						</div>

					</div>
					<div id="quota" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Informasi Kuota Cuti</div>
							<form>
								<div ng-if="currentQuotaDisplay === false" class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq">Kuota
										Cuti</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm "
											id="lq" value="{{currentQuota.leaveQuota}}" disabled>
									</div>
								</div>
								<div ng-if="currentQuotaDisplay" class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq">Kuota
										Cuti</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="lq" value="12" disabled>
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="rq">Kuota
										Tersisa</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="rq" value="{{currentQuota.remainingQuota}}" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Kuota
										Ambil</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="qt" value="{{currentQuota.currentQuotaTaken}}" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Total
										Sisa Kuota</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											value="{{currentQuota.currentTotalRemainingToken}}" disabled>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div ng-if="carryOverFlag" id="quota" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Kuota Carry Over</div>
							<form>
								<div ng-if="currentQuotaDisplay === false" class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq">Kuota
										Cuti</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm "
											id="lq" value="{{currentQuota.leaveQuota}}" disabled>
									</div>
								</div>
								<div ng-if="currentQuotaDisplay" class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq">Kuota
										Cuti</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="lq" value="12" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="rq">Kuota
										Tersisa</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="rq" value="{{nextQuota.remainingQuota}}" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Kuota
										Ambil</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="qt" value="{{nextQuota.currentQuotaTaken}}" disabled>
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Total
										Sisa Kuota</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											value="{{nextQuota.currentTotalRemainingToken}}" disabled>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<strong>Footer</strong>
		</div>
		<div class="" id="col-footer">
			<div class="content" style="background-color: white">
				<div class="tab-content">
					<div id="comments" class="tab-pane fade in active form-group row"
						style="margin-top: 5px">
						<label class="col-sm-2 col-form-label-sm" for="trq">Komentar</label>
						<div class="col-sm-3" style="margin-top: 5px">
							<textarea class="form-control" rows="6" cols="25" id="desc"
								onkeypress="return alpha(event)" ng-model="comments"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-info" style="height: 60px;">
		<div class="form-group row col-md-8">
			<div class="form-group">
				<div class="col-sm-2">
					<strong>Action :</strong>
				</div>
				<div class="col-sm-5">
					<a type="submit" class="btn btn-primary" style="margin-right: 20px"
						ng-click="submit()" data-target="#popupModal" data-toggle="modal"
						id="setAlert"><fmt:message key="label.name.submit" /></a> <a
						type="submit" class="btn btn-danger" ng-click="reset()">Cancel</a>
				</div>
			</div>
		</div>
	</div>
	<!-- The modal -->
	<div class="modal fade" id="popupModal" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Konfirmasi</h4>
				</div>
				<div class="modal-body">
					<div ng-if="loadingAnim === false" id="alert" ng-show="createFlag"
						class="alert alert-success ng-hide" align="center">
						<strong>{{message}} !</strong><br> 
						<strong>Status 		 : {{status}}</strong><br> 
						<strong>No.Pengajuan :{{instanceId}}-{{taskId}}</strong>
					</div>
					<div id="alert" ng-show="errorFlag"
						class="alert alert-danger ng-hide" align="center"
						data-dismiss="alert">
						<strong>{{status}} - {{message}}</strong><br>
					</div>
					<div id="alert" ng-show="validateMandatory"
						class="alert alert-danger ng-hide" align="center"
						data-dismiss="alert">
						<strong>{{messageValidate}} !</strong>
					</div>
					<div ng-if="loadingAnim === true" align="center">
						<img style="width: 10%;height: 10%" src="${context}/assets/img/loader.gif"/>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<style type="text/css">
.highlight a {
	background-color: #29f274 !important;
	color: #ffffff !important;
}
</style>

	<!-- <script>
		$(document).ready(function() {
			$("#alert").hide();
			$("#setAlert").click(function() {
				$("#alert").addClass("alert").fadeIn();
			});

		});
	</script> -->


</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>
