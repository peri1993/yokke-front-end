<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />

<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<div class="content-wrapper" ng-controller="UserAddController">
	<section class="content-header">
		<h1>
			User
			<c:choose>
				<c:when test="${mUser.adminId != null}">
					<small>Edit s</small>
				</c:when>
				<c:otherwise>
					<small>Add</small>
				</c:otherwise>
			</c:choose>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="${context}${routes.USERS}">User</a></li>
			<li class="active">Add / Edit</li>
		</ol>

	</section>
	<section class="content">
		<div class="row">
			<form
				action=""
				method="post">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<input type="hidden" name="adminId" value="${mUser.adminId}" />
				<div class="col-xs-12 col-md-6">
					<div class="box box-primary">

						<div class="box-header with-border">
							<h3 class="box-title">Profile Info</h3>
							<c:if test="${errMsg != null}">
								<div class="callout callout-danger">
									<h4>Error...!</h4>
									<p>${errMsg}</p>
								</div>
							</c:if>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label for="firstName">First Name</label> <input type="text"
									name="firstName" class="form-control" placeholder="First Name"
									value="${(not empty old.firstName) ? old.firstName : mUser.firstName}"
									required="required" />
							</div>
							<div class="form-group">
								<label for="lastName">Last Name</label> <input type="text"
									name="lastName" placeholder="Last Name" class="form-control"
									value="${(not empty old.lastName) ? old.firstName : mUser.lastName}" />
							</div>
							<div class="form-group">
								<label for="email">Email Address</label> <input
									class="form-control" 
									value="${(not empty old.email) ? old.email : mUser.email}"
									type="text"
									name="email" placeholder="Email Address" />
							</div>
						</div>
						<div class="box-footer"></div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Account Info</h3>
						</div>
						<div class="box-body">
							<div class="form-group ${isFormError == true ? 'has-error' : ''}">
								<label for="userName">User Name</label> <input type="text"
									name="userName" class="form-control" placeholder="User Name"
									value="${(not empty old.userName) ? old.userName : mUser.userName}" />
							</div>
							<div class="form-group">
								<label>User Role</label> <select name="roleId"
									class="form-control">
									<option value="0" ${mUser.roleId == null ? 'selected' : ''}></option>
									<c:forEach items="${mRoles}" var="role">
										<option value="${role.id}"
											${(not empty old.roleId) ? 
											((old.roleId == role.id) ? 'selected' : '') : 
											((mUser.roleId == role.id) ? 'selected' : '')}
											>${role.roleName}</option>
									</c:forEach>
								</select>
							</div>
							<div class="form-group">
								<label for="password">Password</label> <input
									class="form-control" type="password" name="password"
									placeholder="**********" />
							</div>
						</div>
						<div class="box-footer"></div>
					</div>
				</div>
				<div class="col-xs-12 col-md-12">
					<div class="col-sm-10"></div>
					<div class="col-sm-2 pull-right">
						<div class="row">
							<button type="submit"
								class="btn btn-block btn-social btn-dropbox">
								<i class="fa fa-save"></i> Save
							</button>
						</div>

					</div>
				</div>
			</form>
		</div>
</div>
</section>
</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>