<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />

<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<div class="content-wrapper" ng-controller="UserAddController">
	<section class="content-header">
		<h1>
			User <small>Profile</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="${context}${routes.USERS}">User</a></li>
			<li class="active">View</li>
		</ol>

	</section>
	<section class="content">
		<div class="row">
			<div class="col-sm-6">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Profile Info</h3>
					</div>
					<div class="box-body">
						<div class="row form-horizontal">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-sm-4 control-label">First Name</label>
									<div class="col-sm-8">
										<span class="form-control">${mUser.firstName}</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Last Name</label>
									<div class="col-sm-8">
										<span class="form-control">${mUser.lastName}</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Email Address</label>
									<div class="col-sm-8">
										<span class="form-control">${mUser.email}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Account Info</h3>
					</div>
					<div class="box-body">
						<div class="row form-horizontal">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-sm-4 control-label">User Name</label>
									<div class="col-sm-8">
										<span class="form-control">${mUser.userName}</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">User Role</label>
									<div class="col-sm-8">
										<span class="form-control">${mUser.roleId}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</section>
</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>