<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<div class="content-wrapper" ng-controller="UserIndexController">
	<section class="content-header">
		<h1>Users <small>List</small></h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="${context}${routes.USERS}">User</a></li>
			<li class="active">List</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header with-border">
						<jsp:include page="/WEB-INF/views/cms/components/table-toolbar.jsp"></jsp:include>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th style="width: 50px;">No</th>
									<th><a href="#"
										ng-click="sortType = 'b.user_name'; sortReverse = !sortReverse; loadDataSort('b.user_name') ">
											User Name <span
											ng-show="sortType == 'b.user_name' && !sortReverse"
											class="fa fa-caret-down"></span> <span
											ng-show="sortType == 'b.user_name' && sortReverse"
											class="fa fa-caret-up"></span>
									</a></th>
									<th><a href="#"
										ng-click="sortType = 'full_name'; sortReverse = !sortReverse; loadDataSort('full_name') ">
											Full Name <span
											ng-show="sortType == 'full_name' && !sortReverse"
											class="fa fa-caret-down"></span> <span
											ng-show="sortType == 'full_name' && sortReverse"
											class="fa fa-caret-up"></span>
									</a></th>
									<th><a href="#"
										ng-click="sortType = 'role_id'; sortReverse = !sortReverse; loadDataSort('b.role_id') ">
											Role <span ng-show="sortType == 'role_id' && !sortReverse"
											class="fa fa-caret-down"></span> <span
											ng-show="sortType == 'role_id' && sortReverse"
											class="fa fa-caret-up"></span>
									</a></th>
									<th>Status</th>
									<th style="width: 100px;">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr
									ng-repeat="res in datas track by res.id| orderBy:sortBy:sortReverse">
									<td>{{$index + from}}</td>
									<td>{{res.userName}}</td>
									<td>{{res.fullName}}</td>
									<td>{{res.roleName}}</td>
									<td>{{res.active ? 'Active' : 'Inactive'}}</td>
									<jsp:include page="/WEB-INF/views/cms/components/table-column-action.jsp"></jsp:include>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="box-footer clearfix">
						<jsp:include page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>
