<%@page import="java.util.List"%>
<%@page import="com.yokke.model.Permission"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes" scope="page" />
								
<aside class="main-sidebar">
	<section class="sidebar" >
		<ul class="sidebar-menu">
			<li class="treeview">
				<a href="https://memo.yokke.com"><span class="logo-lg font-weight-bold"><b>Home</b>
				</a>
				<!-- <ul class="treeview-menu">
					<li><a href="">Welcome</a></li>
				</ul> -->
			</li>
			<li class="treeview">
				<a href="${context}${routes.DASHBOARD}"><span class="logo-lg font-weight-bold"><b>Dashboard</b>
				</a>
				<!-- <ul class="treeview-menu">
					<li><a href="">Welcome</a></li>
				</ul> -->
			</li>
			<c:if test="${sessionScope.Inbox == 'true'|| sessionScope.Notification == 'true' || sessionScope.AssignerHistory == 'true'}">
				<li class="treeview">
						<a href="#"><span>Approval</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-down pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<c:if test="${sessionScope.Inbox == 'true'}">
								<li ng-controller="InboxController"><a href="${context}${routes.INBOX}">Kotak Masuk <span class="label label-danger ng-hide" ng-show="hideData">{{totalRows}}</span></a></li>
							</c:if>
							<c:if test="${sessionScope.Notification == 'true'}">
								<li ng-controller="NotifControllerUser"><a href="${context}${routes.NOTIFICATIONS}">Notifikasi <span class="label label-danger ng-hide" ng-show="hideData">{{dataSize}}</span></a></li>
							</c:if>
							<c:if test="${sessionScope.AssignerHistory == 'true'}">
								<li ng-controller="AssignerHistoryController"><a href="${context}${routes.ASSIGNER_HISTORY}">History Approve/Reject</a></li>
							</c:if>
						</ul>
				</li>
			</c:if>
			<c:if test="${sessionScope.RequestLeave == 'true' || sessionScope.HistoryLeave == 'true'}">
				<li class="treeview">
					<a href="#"><span>Cuti</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-down pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<c:if test="${sessionScope.RequestLeave == 'true'}">
							<li><a href="${context}${routes.LEAVE}">Pengajuan Cuti</a> </li>
						</c:if>
						<c:if test="${sessionScope.HistoryLeave == 'true'}">
							<li ng-controller="LeaveHistoryController"><a href="${context}${routes.HISTORY}">History Cuti</a></li>						
						</c:if>
					</ul>
				</li>
			</c:if>
			<c:if test="${sessionScope.RequestPermit == 'true' || sessionScope.HistoryPermit == 'true'}">
				<li class="treeview">
					<a href="#"><span>Izin</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-down pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<c:if test="${sessionScope.RequestPermit}">
							<li><a href="${context}${routes.PERMIT_REQUEST}">Pengajuan Izin</a> </li>
						</c:if>
						<c:if test="${sessionScope.HistoryPermit}">
							<li ng-controller="PermitHistoryController"><a href="${context}${routes.PERMIT_HISTORY}">History Izin</a></li>
						</c:if>
					</ul>
				</li>
			</c:if>
			<c:if test="${sessionScope.AttendanceHistory == 'true' || sessionScope.ListAttendance == 'true' || sessionScope.InformationWorkHours == 'true' || sessionScope.QrAttendance == 'true'}">
			<li class="treeview">
					<a href="#"><span>Absensi</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-down pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<c:if test="${sessionScope.AttendanceHistory == 'true'}">
						<li><a href="${context}${routes.HISTORY_EMPLOYEE}">History Absensi</a></li>
						</c:if>
						<c:if test="${sessionScope.ListAttendance == 'true'}">
						<li><a href="${context}${routes.LIST_ATTENDANCE}">Informasi Data Pegawai</a></li>
						</c:if>
						<c:if test="${sessionScope.InformationWorkHours == 'true'}">
						<li><a href="${context}${routes.INFOR_WORK_HOURS}">Informasi Jam Kerja</a></li>
						</c:if>
						<c:if test="${sessionScope.QrAttendance == 'true'}">
						<li><a href="${context}${routes.QR_ATTENDANCE}">Scan QR</a></li>
						</c:if>
					</ul>
			</li>
			</c:if>
			<c:if test="${sessionScope.AssignedTask == 'true' || sessionScope.RoleManagement == 'true' || sessionScope.UserManagement == 'true' || sessionScope.BranchConfig == 'true' || sessionScope.AllAssignerHistory == 'true'}">
			<li class="treeview">
					<a href="#"><span>Admin</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-down pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
					<c:if test="${sessionScope.AssignedTask == 'true'}">
						<li><a href="${context}${routes.ASSIGNED_TASK}">Assigned Task</a></li>
					</c:if>
					<c:if test="${sessionScope.RoleManagement == 'true'}">
						<li><a href="${context}${routes.ROLE_MANAGEMENT}">Role Management</a></li>
					</c:if>
					<c:if test="${sessionScope.UserManagement == 'true'}">
						<li><a href="${context}${routes.USER_MANAGEMENT}">User Management</a></li>
					</c:if>
					<c:if test="${sessionScope.BranchConfig == 'true'}">
						<li><a href="${context}${routes.BRANCH_CONF}">Branch Config</a></li>
					</c:if>
					<c:if test="${sessionScope.AllAssignerHistory == 'true'}">
						<li><a href="${context}${routes.ALL_ASSIGNER_HISTORY}">History Task Selesai</a></li>
					</c:if>
					</ul>
				</li>
			</c:if>
		</ul>
	</section>
</aside>