<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<footer class="main-footer" style="background-color: white;">
	<!-- <strong>Copyright &copy; 2019 <a href="#">PT. Bank Mega Indonesia</a>.
	</strong> All rights reserved. -->
</footer>
<script src="${context}/assets/resources/AdminLTE2/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="${context}/assets/resources/AdminLTE2/bootstrap/js/bootstrap.min.js"></script>
<script src="${context}/assets/resources/AdminLTE2/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="${context}/assets/resources/AdminLTE2/dist/js/app.min.js"></script>
<script src="${context}/assets/resources/AdminLTE2/dist/js/demo.js"></script>

<script src="${context}/assets/resources/AdminLTE2/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="${context}/assets/resources/AdminLTE2/plugins/datatables/dataTables.bootstrap.min.js"></script>


<script src="${context}/assets/resources/AdminLTE2/plugins/daterangepicker/daterangepicker.js"></script>

<script>
	$(function() {
		$('.dataTable').DataTable({
			"paging" : true,
			"lengthChange" : false,
			"searching" : false,
			"ordering" : true,
			"info" : true,
			"autoWidth" : false
		});
	});
</script>

<script type="text/javascript">
	$(window).load(function(){
		$('#overlay').delay("8000").fadeOut();
		$('#message').delay("5000").fadeIn();	
	});
</script>  
</div>
</div>

</body>
</html>