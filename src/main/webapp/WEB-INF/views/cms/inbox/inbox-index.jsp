<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.InboxController" scope="page" />

<div class="content-wrapper" style="background: white"
	ng-controller="InboxController">
	<input type="hidden" ng-model="_csrf.token"
		ng-init="_csrf.token = '${_csrf.token}'" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	<section class="content-header">
		<h1>
			<h1>
				<b>Kotak Masuk</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message
						key="label.header.module.dashboard" /></a></li>
			<li><a href="${context}${routes.INBOX}">List Task</a></li>
			<li class="active"><fmt:message key="label.name.list" /></li>
		</ol>
	</section>
	<section class="content" style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">Kotak Masuk</strong>
			</div>
			<div id=inboxp>
				<div class="panel-body table-responsive text-nowrap"
					style="overflow-x: overlay; max-height: 500px">
					<table class="table table-hover">
						<thead>
							<form class="form-inline">
								<div class="form-group col-sm-3">
									<select class="form-control" ng-model="paramKey"
										ng-change="inputParam(paramKey)">
										<option value="" disabled selected>Select your option</option>
										<option value="taskId">Task ID</option>
										<option value="businessProcess">Business Process</option>
										<option value="absencesDesc">Absences Type</option>
										<option value="requesterName">RequesterName</option>
										<option value="requesterId">Requester ID</option>
										<option value="startDt">Start Leave Date</option>
										<option value="endDt">End Leave Date</option>
									</select>
								</div>
								<div class="col-sm-3" ng-if="dateFlag === 'N'">
									<input type="text" class="form-control form-control-sm"
										ng-model="paramVal"
										ng-change="onChangeValue(paramKey, paramVal)"
										onkeypress="return alpha(event)">
								</div>
								<div class="col-sm-3 ng-hide" ng-if="dateFlag === 'Y'"
									ng-show="hideData">
									<div class="form-group">
										<div id="">
											<input type="date" placeholder="Start date"
												ng-model="paramVal"
												ng-change="onChangeValue(paramKey, paramVal)"
												class="form-control form-control-sm" />
										</div>
									</div>
								</div>
								<div class="col-sm-3 ng-hide" ng-if="dateFlag === 'Y'"
									ng-show="hideData">
									<div class="form-group">
										<div id="">
											<input type="date" placeholder="End date"
												ng-model="paramVal2"
												ng-change="onChangeValue2(paramKey, paramVal2)"
												class="form-control form-control-sm" />
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<button type="submit" class="btn btn-primary mb-2"
										ng-click="search()">Submit</button>
								</div>
							</form>
							<tr>
								<th style="width: 10px;"><input type="checkbox"
									class="radio ng-hide" value="value1" ng-click="checkAllList()"
									ng-model="isAllSelected" name="{{taskId}}" ng-show="hideData"></th>
								<th style="width: 30px;" class="col-sm-0"
									ng-click='sortColumn("taskId")' ng-class='sortClass("taskId")'>No.
									Pengajuan</th>
								<th style="width: 130px;"
									ng-click='sortColumn("businessProcess")'
									ng-class='sortClass("businessProcess")'>Jenis Pengajuan</th>
								<th style="width: 130px;" ng-click='sortColumn("absencesDesc")'
									ng-class='sortClass("absencesDesc")'>Jenis Cuti / Izin</th>
								<th style="width: 130px;" ng-click='sortColumn("requesterId")'
									ng-class='sortClass("requesterId")'>Pemohon</th>
								<th style="width: 100px;" ng-click='sortColumn("startDate")'
									ng-class='sortClass("startDate")'>Periode Cuti / Izin</th>
								<th align="right" style="width: 50px">Detail Task</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="res in datasList | orderBy:column:reverse"
								ng-show="hideData" class="ng-hide">
								<td><input type="checkbox" class="radio" value="value1"
									name="{{res.taskId}}" ng-model="res.selected"
									ng-click="checkList(res)"></td>
								<td>{{res.instancesId}}-{{res.taskId}}</td>
								<td>{{res.businessProcess}}</td>
								<td>{{res.absencesDesc}}</td>
								<td>{{res.requesterId}}-{{res.requesterName}}</td>
								<td>{{res.startDate}} - {{res.endDate}}</td>
								<td align="center"><a
									href='<c:url value="${cons.DETAIL_URL}?instanceId={{res.instancesId}}&businessProcess={{res.businessProcess}}"></c:url>'
									class="btn bg-orange btn-flat btn-small" id="detail"> <i
										class="fa fa-fw fa-edit"></i>
								</a></td>
							</tr>
							<tr ng-show="hideData == false" class="ng-hide">
								<td colspan="8"><center>List Task Empty</center></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div ng-if="listModel.length > 0" class="ng-hide" ng-show="hideData">
			<input type="button" class="btn btn-primary" data-toggle="modal"
				ng-click="tesModal()" data-target="#approveTask"
				value="Approve Task"> <input type="button"
				class="btn btn-primary" data-toggle="modal" ng-click="tesModal()"
				data-target="#rejectTask" value="Reject Task">
		</div>
		<div class="footer">
			<jsp:include
				page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
		</div>
	</section>

	<!-- The modal -->
	<div class="modal fade" id="approveTask" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Approval Task</h4>
				</div>
				<div class="modal-body">Apakah anda yakin ?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button ng-if="buttonApprove === true" type="button"
						class="btn btn-secondary" ng-click="onSubmitApproved()"
						data-dismiss="modal" data-target="#successTask"
						data-toggle="modal">Submit</button>
				</div>
			</div>
		</div>
	</div>

	<!-- The modal -->
	<div class="modal fade" id="successTask" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Success</h4>
				</div>
				<div class="modal-body">
				<div ng-if="loadingAnim === false">
					{{messageValidate}}
				</div>
				<div ng-if="loadingAnim === true" align="center">
						<img style="width: 10%;height: 10%" src="${context}/assets/img/loader.gif"/>
						<%-- <jsp:include page="/WEB-INF/views/cms/loading.jsp"></jsp:include> --%>
					</div>
				</div>
				<div class="modal-footer">
					<button ng-click="refresh()" type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<!-- The modal -->
	<div class="modal fade" id="rejectTask" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Approval Task</h4>
				</div>
				<div class="modal-body">Apakah anda yakin untuk me-reject task
					?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button ng-if="buttonApprove === true" type="button"
						class="btn btn-secondary" ng-click="onSubmitRejected()"
						data-dismiss="modal" data-target="#successTask"
						data-toggle="modal">Submit</button>
				</div>
			</div>
		</div>
	</div>

</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>