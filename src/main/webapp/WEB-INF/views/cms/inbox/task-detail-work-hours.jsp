<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.InboxController" scope="page" />
<div class="content-wrapper" style="background: white"
	ng-controller="InformationWorkHoursDetail">
	<input type="hidden" name="idRecom" id="instanceId"
		value="${instanceId}"></input> <input type="hidden"
		name="businessProcess" id="businessProcess" value="${businessProcess}"></input>
	<input type="hidden" ng-model="_csrf.token"
		ng-init="_csrf.token = '${_csrf.token}'" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	<!-- <div id="alert" ng-show="approveFlag"
		class="alert alert-success alert-dismissable ng-hide" align="center"
		data-dismiss="alert">
		<strong>{{message}} !</strong><br> <strong>Status
			:{{status}}</strong>
	</div>
	<div id="alert" ng-show="validateSubmit"
		class="alert alert-danger alert-dismissable ng-hide" align="center"
		data-dismiss="alert">
		<strong>{{messageValidate}} !</strong>
	</div> -->
	<section class="content-header">
		<h1>
			<h1>
				<b>Inbox Detail</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.INBOX}"><i
					class="fa fa-reply"></i> List Task</a></li>
			<li><a href="${context}${routes.TASK_DETAIL}">Detail</a></li>
		</ol>
	</section>
	<div class="panel panel-info">
		<div class="panel-heading">
			<strong>Header</strong>
		</div>
		<div class="" id="col-header">
			<div class="content" style="background-color: white">
				<div class="row">
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Permintaan Perubahan Jam Kerja
								Karyawan</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq">Requester
										ID</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="id" ng-model="datas.employee_id" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="rq">Requester
										Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="name" ng-model="datas.employee_name" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Jam
										Masuk</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="subject" ng-model="datas.start_time" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Jam
										Keluar</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="subject" ng-model="datas.end_time" disabled />
									</div>
								</div>
							</form>
						</div>
					</div>
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Approval</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Approval
										1</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval1" ng-model="datas.approval_ds_1" disabled />
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Approval
										1 Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval1Name" ng-model="datas.approval_ds_1_name"
											disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Approval
										2</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval2" ng-model="datas.approval_ds_2" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Approval
										2 Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval2name" ng-model="datas.approval_ds_2_name"
											disabled />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-info" style="height: 60px;">
		<div class="col-md-12">
			<div class="form-group row">
				<!-- <div class="col-sm-3">
							<strong>Action :</strong>
						</div>
						<div class="col-sm-4">
							<select class="form-control form-control-sm" id="att"
								ng-model="optionButton" required>
								<option value="approve">Approve</option>
								<option value="reject">Reject</option>
							</select>
						</div> -->
				<div class="col-sm-4">
					<a type="submit" class="btn btn-primary" style="margin-right: 20px"
						data-toggle="modal" data-target="#approveTask">Approve</a> <a
						type="submit" class="btn btn-danger" style="margin-right: 20px"
						data-toggle="modal" data-target="#rejectTask">Reject</a> <a
						href="${context}${routes.INBOX}" type="submit"
						class="btn btn-success"> Back </a>
				</div>
				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
	<!-- The modal -->
	<div class="modal fade" id="approveTask" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Approval Task</h4>
				</div>
				<div class="modal-body">Approve No.Pengajuan
					{{datas.instancesId}}-{{datas.taskId}} ?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-secondary"
						ng-click="approve()" data-toggle="modal" data-dismiss="modal"
						data-target="#infoTask">Submit</button>
				</div>
			</div>
		</div>
	</div>
	<!-- The modal -->
	<div class="modal fade" id="rejectTask" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="modalLabel">Approval Task</h4>
				</div>
				<div class="modal-body">Reject No.Pengajuan
					{{datas.instancesId}}-{{datas.taskId}} ?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-secondary" ng-click="reject()"
						data-toggle="modal" data-dismiss="modal" data-target="#infoTask">Submit</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- The modal -->
<div class="modal fade" id="infoTask" tabindex="-1" role="dialog"
	aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalLabel">Informasi</h4>
			</div>
			<div class="modal-body">
				<div id="alert" ng-show="approveFlag"
					class="alert alert-success alert-dismissable ng-hide"
					align="center" data-dismiss="alert">
					<strong>{{status}}-{{message}}</strong><br> 
					<strong></strong>
				</div>
				<div id="alert" ng-show="validateSubmit"
					class="alert alert-danger alert-dismissable ng-hide" align="center"
					data-dismiss="alert">
					<strong>{{messageValidate}} !</strong>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal"
					ng-click="refresh()">Close</button>
			</div>
		</div>
	</div>
</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>