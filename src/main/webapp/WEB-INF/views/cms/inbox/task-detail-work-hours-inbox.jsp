<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"
	scope="page" />
<jsp:useBean id="cons"
	class="com.yokke.web.controller.InboxController" scope="page" />
<div class="content-wrapper" style="background: white"
	ng-controller="InformationWorkHoursDetail">
	<input type="hidden" name="idRecom" id="instanceId"
		value="${instanceId}"></input> 
	<input type="hidden" name="businessProcess" id="businessProcess" value="${businessProcess}"></input> 
	<input type="hidden"
		ng-model="_csrf.token" ng-init="_csrf.token = '${_csrf.token}'"
		name="${_csrf.parameterName}" value="${_csrf.token}" />
	<div id="alert" ng-show="approveFlag"
		class="alert alert-success alert-dismissable ng-hide" align="center"
		data-dismiss="alert">
		<strong>{{message}} !</strong><br> <strong>Status
			:{{status}}</strong>
	</div>
	<div id="alert" ng-show="validateSubmit"
		class="alert alert-danger alert-dismissable ng-hide" align="center"
		data-dismiss="alert">
		<strong>{{messageValidate}} !</strong>
	</div>
	<section class="content-header">
		<h1>
			<h1>
				<b>Inbox Detail</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
			<li><a href="${context}${routes.INBOX}"><i
					class="fa fa-reply"></i> List Task</a></li>
			<li><a href="${context}${routes.TASK_DETAIL}">Detail</a></li>
		</ol>
	</section>
	<div class="panel panel-info">
		<div class="panel-heading">
			<strong>Header</strong>
		</div>
		<div class="" id="col-header">
			<div class="content" style="background-color: white">
				<div class="row">
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Permintaan Perubahan Jam Kerja Karyawan</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="lq">Requester
										ID</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="id" ng-model="datas.employee_id" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="rq">Requester
										Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="name" ng-model="datas.employee_name" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Jam Masuk</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="subject" ng-model="datas.start_time" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Jam Keluar</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="subject" ng-model="datas.end_time" disabled />
									</div>
								</div>
							</form>
						</div>
					</div>
					<div id="attendance" class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">Approval</div>
							<form>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Approval
										1</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval1" ng-model="datas.approval_ds_1" disabled />
									</div>
								</div>
								<div class="form-group row"
									style="margin-left: 0px; margin-top: 10px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Approval
										1 Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval1Name" ng-model="datas.approval_ds_1_name" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="qt">Approval
										2</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval2" ng-model="datas.approval_ds_2" disabled />
									</div>
								</div>
								<div class="form-group row" style="margin-left: 0px">
									<label class="col-sm-5 col-form-label-sm" for="trq">Approval
										2 Name</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											id="approval2name" ng-model="datas.approval_ds_2_name" disabled />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>