<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<jsp:include page="/WEB-INF/views/cms/base-header.jsp"></jsp:include>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="routes" class="com.yokke.web.controller.Routes"	scope="page" />
<jsp:useBean id="cons"	class="com.yokke.web.controller.NotificationsController" scope="page" />

<div class="content-wrapper" style="background: white" ng-controller="NotifController">
	<input type="hidden" ng-model="_csrf.token"
		ng-init="_csrf.token = '${_csrf.token}'" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	<section class="content-header">
		<h1>
			<h1>
				<b>Notifikasi</b>
			</h1>
		</h1>
		<ol class="breadcrumb">
		<li><a href="${context}${routes.DASHBOARD}"><i
					class="fa fa-reply"></i> <fmt:message key="label.header.module.dashboard"/></a></li>
			<li><a href="${context}${routes.NOTIFICATIONS}">List Task</a></li>
			<li class="active"><fmt:message key="label.name.list"/></li>
		</ol>
	</section>
	<section class="content" style="background-color: white">
		<div class="panel panel-info">
			<div class="panel-heading" data-toggle="collapse"
				data-target="#inboxp">
				<strong class="panel-title">List Notifikasi</strong>
			</div>
			<div id=inboxp>
				<div class="panel-body table-responsive text-nowrap" style="overflow-x: overlay;max-height:500px;">
					<table class="table table-hover">
						<thead>
							<form class="form-inline">
  								<div class="form-group col-sm-3">
									<select class="form-control" ng-model="paramKey" ng-change="inputParam(paramKey)">
    									<option value="" disabled selected>--Masukan Pilihan Anda--</option>
										<option value="instancesId">No. Pengajuan</option>
										<option value="businessProcess">Jenis Pengajuan</option>
										<option value="requesterId">ID Pemohon</option>
										<option value="requesterName">Nama Pemohon</option>
										<option value="subject">Subject</option>
										<option value="status">Status</option>
										<option value="createdDt">Tanggal Pengajuan</option>
									</select>
								</div>
  								<div class="col-sm-3" ng-if="dateFlag === 'N'">
							      	<input type="text" class="form-control form-control-sm" ng-model="paramVal" ng-change="onChangeValue(paramKey, paramVal)">
							    </div>
							    <div class="col-sm-3 ng-hide" ng-if="dateFlag === 'Y'" ng-show = "hideData">
							      	<div class="form-group">
										<div id="">
											<input type="date" placeholder="Start date" ng-model="paramVal" ng-change="onChangeValue(paramKey, paramVal)" class="form-control form-control-sm" />
										</div>
									</div>
							    </div>
							    <div class="col-sm-3 ng-hide" ng-if="dateFlag === 'Y'" ng-show="hideData">
							      	<div class="form-group">
										<div id="">
											<input type="date" placeholder="End date" ng-model="paramVal2" ng-change="onChangeValue2(paramKey, paramVal2)" class="form-control form-control-sm" />
										</div>
									</div>
							    </div>
  								<div class="form-group col-sm-2" >
					      			<button type="submit" class="btn btn-primary mb-2" ng-click="search()">Submit</button>
					    		</div>
							</form>
							<tr>
								<th ng-click='sortColumn("instancesId")' ng-class='sortClass("instancesId")'>No. Pengajuan</th>
								<th ng-click='sortColumn("businessProcess")' ng-class='sortClass("businessProcess")'>Jenis Pengajuan</th>
								<th ng-click='sortColumn("requesterId")' ng-class='sortClass("requesterId")'>Id Pemohon</th>
								<th ng-click='sortColumn("subject")' ng-class='sortClass("subject")'>Subject</th>
								<th ng-click='sortColumn("status")' ng-class='sortClass("status")'>Status</th>
								<th ng-click='sortColumn("createDt")' ng-class='sortClass("createDt")'>Tanggal Pengajuan</th>
								<th ng-click='sortColumn("description")' ng-class='sortClass("description")'>Deskripsi</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="res in datas | orderBy:column:reverse" ng-show="hideData" class="ng-hide">
								<td><center>{{res.instancesId}}</center></td>
								<td>{{res.businessProcess}}</td>
								<td>{{res.requester}}</td>
								<td>{{res.subject}}</td>
								<td>{{res.status}}</td>
								<td>{{res.createdDt}}</td>
								<td>{{res.description}}</td>
								<td>
									<div ng-if="res.instancesId.assigner != null">
										<i class="" style="font-size:15px"></i>
									</div>
									<div ng-if="res.instancesId.assigner == null">
										<i class="glyphicon glyphicon-ok"></i>
									</div>
								</td>
								<td align="center"><a
									href='<c:url value="${cons.DETAIL_URL}?instancesId={{res.instancesId}}&process={{res.businessProcess}}&status={{res.status}}&id={{res.id}}"></c:url>'
									class="btn bg-orange btn-flat btn-small" id="detail"> <i
										class="fa fa-fw fa-edit"></i>
								</a></td>
							</tr>
							<tr ng-show="hideData == false" class="ng-hide">
								<td colspan="8"><center>List Task Empty</center></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="footer">
			<jsp:include page="/WEB-INF/views/cms/components/table-pagination.jsp"></jsp:include>
		</div>
	</section>
</div>

<jsp:include page="/WEB-INF/views/cms/base-footer.jsp"></jsp:include>