<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="cons" class="com.yokke.web.controller.LoginController"
	scope="page" />
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/>
<title>ESS Bank Mega</title>
<link rel="icon" type="image/gif/png" href="${context}/assets/img/logoMega.png">
<link rel="stylesheet"
	href="${context}/assets/resources/AdminLTE2/bootstrap/css/bootstrap.min.css">

<link rel="stylesheet"
	href="${context}/assets/resources/AdminLTE2/dist/css/AdminLTE.min.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page"
	style="background-color: #f6871f;">
	<div class="login-box">
		<div class="login-logo">
			<b>ESS</b>Application
		</div>
		<div class="login-box-body">
			<p class="login-box-msg">Session time out. You need to login again</p>
			<%-- <a href="${context}${routes.LOGIN}">Back</a> --%>
			<a href="https://ess.yokke.com/">Back</a>
		</div>
	</div>
	<script
		src="${context}/assets/resources/AdminLTE2/plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script
		src="${context}/assets/resources/AdminLTE2/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
