APP.controller('InformationWorkHours', function($scope, $http, $rootScope,
		$location, $window) {
	$scope.keyIndex = 'id';
	$scope.linkAction = {};
	$scope.dataSize = '';
	$scope.uiDetail = false;
	$scope.model = {};
	$scope.createFlag = false;
	$scope.errorFlag = false;
	$scope.hideData	= false;
	
	$scope.refresh = function(){
		$scope.loadData($window.location.reload());
	}
	
	$scope.update = function(){
		$scope.listHours();
		$scope.loadDataApproval();
		$scope.uiDetail = true;
	};
	
	$scope.cancel = function(){
		$scope.uiDetail = false;
	};
	
	$scope.submit = function(option, datas, approval){
		
		var employeeId = datas.empId;
		var employeeName = datas.fullname;
		var startTime = datas.officeHours.startTime;
		var endTime = datas.officeHours.endtTime;
		var reason = datas.officeHours.reason;
		var approval1 = approval.approval1;
		var approval1Name = approval.nameApproval1;
		var approval2 = approval.approval2;
		var approval2Name = approval.nameApproval2;
		var hoursId = option.id;
		
		$http.get(endPointApps + 'information-work-hours/request-work-hours', {
			params : {
				'employeeId':employeeId
				,'employeeName':employeeName
				,'startTime':startTime
				,'endTime':endTime
				,'reason':reason
				,'approval1':approval1
				,'approval1Name':approval1Name
				,'approval2':approval2
				,'approval2Name':approval2Name
				,'hoursId':hoursId
												
			}
		}).success(function(response) {
			$scope.status = response.status;
			$scope.message = response.message;
			$scope.taskId = response.taskId;
			$scope.instanceId = response.instancesId;
			$scope.error = response.error;
			
			if (($scope.status == 200 || $scope.status == '200')) {
				$scope.createFlag = true;
				$scope.errorFlag = false;
				//$scope.reset();
			}else{
				$scope.errorFlag = true;
				$scope.createFlag = false;
			}
		});
	}

	$scope.listHours = function(){
		$http.get(endPointApps + 'information-work-hours/list', {
			params : {}
		}).success(function(response) {
			$scope.list = response.listResponse;
		});
	};
	
	$scope.loadDataApproval = function(page, qlue) {

		$http.get(endPointApps + 'leave/findApproval', {
			params : {}
		}).success(function(response) {
			$scope.approval = response;
		});
	};
	
	$scope.loadData = function(page, qlue) {
		$scope.page = '';
		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}

		$http.get(endPointApps + 'information-work-hours/detail', {
			params : {}
		}).success(function(response) {

			$scope.datas = response.objectResponse;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			
			if ($scope.datas != undefined) {
				$scope.dataSize = $scope.datas.length;
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
		});
	};

	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('InformationWorkHoursDetail', function($scope, $http, $rootScope,
		$location, $window) {
	$scope.keyIndex = 'id';
	$scope.linkAction = {};
	$scope.dataSize = '';
	$scope.uiDetail = false;
	
	$scope.instancesId = function(){
		return $("#instanceId").val();
	}
	
	$scope.update = function(){
		$scope.listHours();
		$scope.loadDataApproval();
		$scope.uiDetail = true;
	};
	
	$scope.cancel = function(){
		$scope.uiDetail = false;
	};
	
	$scope.loadData = function(page, qlue) {
		$scope.page = '';
		var instancesId = '';
		
		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		
		if($scope.instancesId() != undefined){
			instancesId = $scope.instancesId();
		}
		$http.get(endPointApps + 'information-work-hours/task-detail', {
			params : {
				'instancesId':instancesId
			}
		}).success(function(response) {
			
			$scope.datas = response.objectResponse;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			
			if ($scope.datas != undefined) {
				$scope.dataSize = $scope.datas.length;
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
		});
	};
	
	$scope.refresh = function(){
		$scope.loadData($window.location.href(endPointApps + "/inbox"));
	}
	
	$scope.approve = function() {
		
		if ($scope.datas == null || $scope.datas == undefined) {
			return null;
		}

		$http.get(endPointApps + 'inbox/approve', {
			params : {
				'taskId' : $scope.datas.taskId,
				'comments' : $scope.datas.comments,
				'instancesId' : $scope.datas.instancesId
			}
		}).success(function(response) {
				$scope.message = response.message;
				$scope.status = response.status;
				$scope.error = response.error;
					
				if ($scope.status == 200 || $scope.status == '200') {
					$scope.approveFlag = true;
					$scope.validateSubmit = false;
					$window.location.href = endPointApps + "/inbox";
				} else {
					$scope.approveFlag = false;
					$scope.validateSubmit = true;
					$scope.messageValidate = $scope.status + ' ' + $scope.error;
				}
		});
	};

	$scope.reject = function() {
		
		if ($scope.datas == null || $scope.datas == undefined) {
			return null;
		}

		$http.get(endPointApps + 'inbox/reject', {
			params : {
				'taskId' : $scope.datas.taskId,
				'comments' : $scope.datas.comments,
				'instancesId' : $scope.datas.instancesId
			}
		}).success(function(response) {
				$scope.message = response.message;
				$scope.status = response.status;
				$scope.error = response.error;
					
				if ($scope.status == 200 || $scope.status == '200') {
					$scope.approveFlag = true;
					$scope.validateSubmit = false;
					$window.location.href = endPointApps + "/inbox";
				} else {
					$scope.approveFlag = false;
					$scope.validateSubmit = true;
					$scope.messageValidate = $scope.status + ' Not Found Task';
				}
		});
	};

	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});