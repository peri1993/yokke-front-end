APP
		.controller(
				'InboxController',
				function($scope, $http, $rootScope, $location, $window) {

					$scope.keyIndex = 'id';
					$scope.linkAction = {};
					$scope.hideData = false;
					$scope.listModel = [];
					$scope.listDataModel = [];
					$scope.isAllSelected = false;
					$scope.dateFlag = 'N';
					$scope.paramKey = '';
					$scope.paramValue = '';
					$scope.paramValue2 = '';
					$scope.messageValidate = '';
					$scope.reverse = false;
					$scope.column = '';
					$scope.messageModal = false;
					$scope.loadingAnim = false;
					
					$scope.inputParam = function(key) {
						if ('startDt' == key || 'endDt' == key) {
							$scope.dateFlag = 'Y';
						} else {
							$scope.dateFlag = 'N';
							$scope.paramValue2 = '';
						}

						$scope.paramKey = key;
					}

					$scope.onChangeValue = function(key, value) {
						if ('startDt' == key || 'endDt' == key) {
							$scope.paramValue = value;
						} else {
							$scope.paramValue = value;
						}
					}

					$scope.onChangeValue2 = function(key, value) {
						if ('startDt' == key || 'endDt' == key) {
							$scope.paramValue2 = value;
						} else {
							$scope.paramValue2 = value;
						}
					}
					
					$scope.refresh = function(){
						$scope.loadData(window.location.reload);
					}
					
					$scope.search = function() {
						$scope.loadData();
					}
					
					$scope.sortColumn = function(col){
						$scope.column = col;
						if($scope.reverse){
							$scope.reverse = false;
						}else{
							$scope.reverse = true;
						}
					};
					
					$scope.sortClass = function(col){
						if($scope.column == col ){
							if($scope.reverse){
								return 'arrow-down'; 
							}else{
							   	return 'arrow-up';
						}
						}else{
							return '';
						}
					}; 

					$scope.loadData = function(page, qlue) {
						$scope.page = '';
						var key = '';
						var value = '';
						var value2 = '';
						var column = '';
						var orderBy = '';

						if (page != undefined) {
							$scope.currentPage = page;
						}
						if (qlue != undefined) {
							$scope.qlue = qlue;
						}

						if ($scope.secPage == undefined) {
							$scope.secPage = '0';
						}

						if ($scope.paramKey != undefined) {
							key = $scope.paramKey;
						}
						if ('startDt' == key || 'endDt' == key) {

							if ($scope.paramValue != undefined
									&& $scope.paramValue != '') {
								var dateString = '';
								var _date = $scope.paramValue;
								dateString = _date.getFullYear()
										+ (_date.getMonth() + 1 < 10 ? '0'
												+ (_date.getMonth() + 1)
												: _date.getMonth() + 1)
										+ (_date.getDate() < 10 ? '0'
												+ (_date.getDate()) : _date
												.getDate());
								value = dateString;
							}
							if ($scope.paramValue2 != undefined
									&& $scope.paramValue != '') {
								var dateString = '';
								var _date = $scope.paramValue2;
								dateString = _date.getFullYear()
										+ (_date.getMonth() + 1 < 10 ? '0'
												+ (_date.getMonth() + 1)
												: _date.getMonth() + 1)
										+ (_date.getDate() < 10 ? '0'
												+ (_date.getDate()) : _date
												.getDate());

								value2 = dateString;
							}
						} else {
							if ($scope.paramValue != undefined) {
								value = $scope.paramValue;
							}
						}
						
						/*if($scope.reverse != undefined){
							orderBy = $scope.reverse;
						}
						
						if($scope.column != undefined){
							column = $scope.column;
						}
*/
						$http.get(endPointApps + 'inbox/list', {
							params : {
								'paramKey' 		: key,
								'paramValue' 	: value,
								'paramValue2' 	: value2,
								'secPage' 		: $scope.secPage
							}
						}).success(function(response) {
							$scope.datasList = response.listResponse;
							$scope.message = response.message;
							$scope.status = response.status;
							$scope.error = response.error;
							$scope.datas = response;
							$scope.pageNumber = response.pageNumber;
							$scope.pageSize = response.pageSize;
							$scope.totalPage = response.totalPage;
							$scope.totalRows = response.totalRows;
							$scope.last = response.last;

							if ($scope.datasList != undefined) {
								if ($scope.datasList.length > 0) {
									$scope.hideData = true;
								} else {
									$scope.hideData = false;
								}
							} else {
								$scope.hideData = false;
							}
							
							if($scope.totalPage < 1){
								$scope.totalPage = 1;
							}
						});
					};

					$scope.checkAllList = function() {
						var toggle = $scope.isAllSelected;
						if (toggle == true) {
							angular.forEach($scope.listModel, function(item) {
								item.selected = false;
							});
							$scope.listModel = [];
							angular.forEach($scope.datasList, function(item) {
								item.selected = toggle;
								if (item.selected == true) {
									$scope.listModel.push(item);
								} else {
									var index = $scope.listModel.indexOf(item);
									$scope.listModel.splice(index, 1);
								}
							});
						} else {
							angular.forEach($scope.listModel, function(item) {
								item.selected = false;
							});
							$scope.listModel = [];
						}
					}

					$scope.checkList = function(item) {
						$scope.isAllSelected = $scope.datasList
								.every(function() {
									if (item.selected == true) {
										$scope.listModel.push(item);
									} else {
										var index = $scope.listModel
												.indexOf(item);
										$scope.listModel.splice(index, 1);
									}
								})
					}
					
					$scope.tesModal = function() {
						if ($scope.listModel.length > 0) {
							$scope.buttonApprove = true;
							$scope.messageModal = false;
						} else {
							$scope.buttonApprove = true;
							$scope.messageValidate = 'Silahkan pilih task untuk di approve';
						}
					};
					
					$scope.onSubmitApproved = function() {
						if ($scope.listModel.length > 0) {
							$scope.approvedList($scope.listModel);
							$scope.listModel = [];
						}
					};

					$scope.onSubmitRejected = function() {
						
						if ($scope.listModel.length > 0) {
							$scope.rejectList($scope.listModel);
							$scope.listModel = [];
						} else {
							$scope.messageValidate = 'Silahkan pilih task untuk di reject';
						}
					};

					$scope.rejectList = function(approveRejectResponses) {
						$scope.loadingAnim = true;
						$http.get(endPointApps + 'inbox/reject-task/list', {
							params : {
								'listModel' : JSON.stringify(approveRejectResponses)
							}
						}).success(function(response) {
							var message = response.message;
							var status = response.status;
							
							if(response != null || response != undefined){
								$scope.loadingAnim = false;
							}
							
							if (message != undefined
									&& status == 200) {
								$scope.messageValidate = status + ' - '
										+ message;
								$scope.loadData();
							} else {
								$scope.messageValidate = status + ' - '
								+ message;
								$scope.loadData();
							}
				});
					};
					
					

					$scope.approvedList = function(approveRejectResponses) {
						$scope.loadingAnim = true;
						$http.get(endPointApps + 'inbox/approve-task/list', {
									params : {
										'listModel' : JSON.stringify(approveRejectResponses)
									}
								}).success(function(response) {
									
									var message = response.message;
									var status = response.status;
									
									if(response != null || response != undefined){
										$scope.loadingAnim = false;
									}
									
									if (message != undefined
											&& status == 200) {
										$scope.messageValidate = status + ' - '
												+ message;
										$scope.loadData();
									} else {
										$scope.messageValidate = status + ' - '
										+ message;
										$scope.loadData();
									}
						});
					};

					$scope.pagging = function(secPage) {
						$scope.secPage = secPage;
						$scope.loadData();
					}

					$scope.$watch("currentPage", function() {
						$scope.loadData($scope.currentPage);
					});

				});

APP
		.controller(
				'DetailTaskController',
				function($scope, $http, $rootScope, $location, $window) {
					$scope.keyIndex = 'id';
					$scope.linkAction = {
						filename : "?filename=",
						directory : "&directory=",
						createdBy : "&createdBy="
					};
					
					$scope.loadingAnim = false;
					$scope.buttonDownload = false;
					$scope.businessProcess = 'N';

					$scope.instancesGetValue = function() {
						return $("#instanceId").val();
					}

					$scope.businessProcessGetValue = function() {
						return $("#businessProcess").val();
					}

					$scope.nextQuota = {
						"leaveQuota" : '',
						"remainQuota" : '',
						"quotaTaken" : '',
						"totalRemainingQuota" : ''
					}

					$scope.datasRemaining = {

					}

					$scope.approveFlag = false;
					$scope.validateSubmit = false;

					$scope.loadDataAbsenceTypeById = function(id) {
						$scope.page = '';

						$http
								.get(
										endPointApps
												+ 'leave/findAbsenceType-all/',
										{
											params : {}
										})
								.success(
										function(response) {

											$scope.datasAbsence = response;
											angular
													.forEach(
															response,
															function(value, key) {
																if (value.id.absencetype == id) {
																	if ('' == value.leaveQuota) {
																		$scope.datasRemaining.originalQuota = '0';
																	} else {
																		$scope.datasRemaining.originalQuota = value.leaveQuota;
																	}
																	return;
																}
															});
										});
					};

					$scope.loadDataRemainingQuota = function(startdate, enddate) {

						$http
								.get(
										endPointApps
												+ 'leave/findRemainingQuotaDtl/'
												+ $scope.datas.employee_id, {
											params : {}
										})
								.success(
										function(response) {

											$scope.currentQuotaTaken = parseInt(response.originalQuota)
													- parseInt(response.remainingQuota);
											if (isNaN($scope.currentQuotaTaken))
												$scope.currentQuotaTaken = '';
											$scope.currentTotalRemainingToken = response.originalQuota;
											$scope.datasRemaining = response;
											$scope.message = response.message;
											$scope.status = response.status;
											$scope.error = response.error;
											$scope.quotaTakenFunc(startdate,
													enddate);
										});
					};

					$scope.quotaTakenFunc = function(startdate, enddate) {
						if (startdate != undefined && enddate != undefined) {

							var csrf = $scope._csrf.token;
							var body = {
								startDate : startdate,
								endDate : enddate
							}
							var config = {
								headers : {
									'Accept' : 'application/json, */*',
									'x-csrf-token' : csrf
								}
							};
							$http
									.post(endPointApps + 'leave/takenQuota',
											body, config)
									.then(
											function(response) {
												$scope.quotaTaken = response.data;
												$scope.nextQuota.leaveQuota = $scope.datasRemaining.originalQuota;
												$scope.nextQuota.quotaTaken = response.data;
												$scope.nextQuota.remainQuota = parseInt($scope.nextQuota.leaveQuota)
														- response.data;
												$scope.nextQuota.totalRemainingQuota = $scope.nextQuota.remainQuota;

											},
											function error(response) {
												$scope.postResultMessage = "Error with status: "
														+ response.statusText;
											});
						}
					}

					$scope.loadData = function(page, qlue) {
						$scope.instancesId = '';
						$scope.page = '';

						if ($scope.instancesGetValue() != undefined) {
							var instancesId = $scope.instancesGetValue();
						}

						$http
								.get(endPointApps + 'inbox/task-detail', {
									params : {
										'instanceId' : instancesId
									}
								})
								.success(
										function(response) {

											$scope.datas = response;
											$scope.listFileUpload = response.listFileUpload;
											$scope.message = response.message;
											$scope.status = response.status;
											$scope.error = response.error;
											$scope
													.loadDataAbsenceTypeById(response.absences_type);
											if (response.carryOverFlag != undefined
													&& response.carryOverFlag != 'N') {
												$scope.nextremainingQuota = response.remaining_quota;
												$scope.currentQuotaTaken = response.quota_taken;
												$scope.nextQuotaTaken = response.quota_taken;
												if (parseInt(response.quota_taken) > parseInt(response.remaining_quota)) {
													$scope.nextTotalRemainingToken = '0';
													$scope.quotaTaken = response.total_taken_quota;
													var carryOver = parseInt(response.quota_taken)
															- parseInt(response.remaining_quota);
													$scope.currentQuotaTaken = carryOver;
													$scope
															.loadDataRemainingQuotaUser(
																	$scope.datas.employee_id,
																	carryOver);
												} else {
													$scope.nextTotalRemainingToken = parseInt(response.remaining_quota)
															- parseInt(response.quota_taken);
													$scope.quotaTaken = response.total_taken_quota;
													var carryOver = '0';
													$scope.currentQuotaTaken = carryOver;
													$scope
															.loadDataRemainingQuotaUser(
																	$scope.datas.employee_id,
																	carryOver);
												}

											} else {

												$scope.datasRemaining.remainingQuota = response.remaining_quota;
												$scope.currentQuotaTaken = response.quota_taken;
												if ('1000' == response.absences_type || '3100' == response.absences_type) {
													$scope.currentTotalRemainingToken = parseInt(response.remaining_quota)
													- parseInt(response.quota_taken);
												} else {
													$scope.currentTotalRemainingToken = response.remaining_quota;
												}
												$scope.quotaTaken = response.total_taken_quota;
											}

											if ($scope
													.businessProcessGetValue() != undefined) {
												var flag = $scope
														.businessProcessGetValue();
												if ('leave' == flag) {
													$scope.businessProcess = 'N';
												} else if ('permit' == flag) {
													$scope.businessProcess = 'Y';
												}
											}
											if ($scope.listFileUpload != undefined) {
												if ($scope.listFileUpload.length > 0) {
													$scope.buttonDownload = true;
												}
											}

											if (response.carryOverFlag != undefined
													&& response.carryOverFlag != 'N') {
												$scope.carryFlag = true;
											} else {
												$scope.carryFlag = false;
											}
										});
					};

					$scope.loadDataRemainingQuotaUser = function(requester,
							carryOver) {
						$scope.remaining = '0';
						$http
								.get(
										endPointApps
												+ 'leave/findRemainingQuotaByRequester',
										{
											params : {
												'requester' : requester
											}
										})
								.success(
										function(response) {
											$scope.listRemaining = response.listResponse;
											if ($scope.listRemaining != undefined) {
												if ($scope.listRemaining.length > 1) {
													$scope.datasRemaining.remainingQuota = $scope.listRemaining[0].remainingQuota;
													$scope.currentTotalRemainingToken = parseInt($scope.datasRemaining.remainingQuota)
															- carryOver;

												}
											}
										});
					};
					
					$scope.redirectInbox = function(){
						$window.location.href = endPointApps
						+ "inbox";
					}
					
					$scope.submit = function(option) {
						if (option == '' || option == null
								|| option == undefined) {
							$scope.validateSubmit = true;
							$scope.messageValidate = 'Please Select Action';
						} else {
							$scope.validateSubmit = false;
							if (option == 'approve') {
								this.approve();
							} else if (option == 'reject') {
								this.reject();
							}
						}
					};

					$scope.approve = function() {
						$scope.loadingAnim = true;
						if ($scope.datas == null || $scope.datas == undefined) {
							return null;
						}

						$http.get(endPointApps + 'inbox/approve', {
							params : {
								'taskId' : $scope.datas.taskId,
								'comments' : $scope.datas.comments,
								'instancesId' : $scope.datas.instancesId
							}
						}).success(
								function(response) {
									$scope.message = response.message;
									$scope.status = response.status;
									$scope.error = response.error;
									
									if(response != undefined || response != null){
										$scope.loadingAnim = false;
									}
									
									if ($scope.status == 200
											|| $scope.status == '200') {
										$scope.approveFlag = true;
										$scope.validateSubmit = false;
									} else {
										$scope.approveFlag = false;
										$scope.validateSubmit = true;
										$scope.messageValidate = $scope.status
												+ ' ' + $scope.error;
									}
								});
					};

					$scope.reject = function() {
						$scope.loadingAnim = true;
						if ($scope.datas == null || $scope.datas == undefined) {
							return null;
						}

						$http.get(endPointApps + 'inbox/reject', {
							params : {
								'taskId' : $scope.datas.taskId,
								'comments' : $scope.datas.comments,
								'instancesId' : $scope.datas.instancesId
							}
						}).success(
								function(response) {
									$scope.message = response.message;
									$scope.status = response.status;
									$scope.error = response.error;
									
									if(response != undefined || response != null){
										$scope.loadingAnim = false;
									}
									
									if ($scope.status == 200
											|| $scope.status == '200') {
										$scope.approveFlag = true;
										$scope.validateSubmit = false;
									} else {
										$scope.approveFlag = false;
										$scope.validateSubmit = true;
										$scope.messageValidate = $scope.status
												+ ' Not Found Task';
									}
								});
					};

					$scope.preview = function(directory, createdBy, filename) {

						$http.get(endPointApps + 'permit-request/preview-file',
								{
									params : {
										'directory' : directory,
										'createdBy' : createdBy,
										'filename' : filename
									}
								}).success(function(response) {
							$scope.path = response.path;
							$scope.previewStatus = response.status;
							if ($scope.previewStatus == '200') {
								$window.open($scope.path, '_blank');
							}
						});
					};

					$scope.$watch("currentPage", function() {
						$scope.loadData($scope.currentPage);
					});
				});

APP.controller('NotifController',
		function($scope, $http, $rootScope, $location, $window) {
			$scope.keyIndex = 'id';
			$scope.linkAction = {};
			$scope.hideData = true;
			$scope.dateFlag = 'N';
			$scope.paramKey = '';
			$scope.paramValue = '';
			$scope.paramValue2 = '';
			$scope.reverse = false;
			$scope.column = '';

			$scope.inputParam = function(key) {
				if ('createdDt' == key) {
					$scope.dateFlag = 'Y';
				} else {
					$scope.dateFlag = 'N';
					$scope.paramValue2 = '';
				}

				$scope.paramKey = key;
			}

			$scope.onChangeValue = function(key, value) {
				if ('createdDt' == key) {
					$scope.paramValue = value;
				} else {
					$scope.paramValue = value;
				}
			}

			$scope.onChangeValue2 = function(key, value) {
				if ('createdDt' == key) {
					$scope.paramValue2 = value;
				} else {
					$scope.paramValue2 = value;
				}
			}

			$scope.search = function() {
				$scope.loadData();
			}
			
			$scope.sortColumn = function(col){
				
				$scope.column = col;
				if($scope.reverse){
					$scope.reverse = false;
					$scope.reverseclass = 'arrow-up';
				}else{
					$scope.reverse = true;
					$scope.reverseclass = 'arrow-down';
				}
			};
			
			$scope.sortClass = function(col){
				
				if($scope.column == col ){
					if($scope.reverse){
						return 'arrow-down'; 
					}else{
					   	return 'arrow-up';
				}
				}else{
					return '';
				}
			};

			$scope.loadData = function(page, qlue) {

				$scope.page = '';
				var key = '';
				var value = '';
				var value2 = '';

				if (page != undefined) {
					$scope.currentPage = page;
				}
				if (qlue != undefined) {
					$scope.qlue = qlue;
				}

				if ($scope.secPage == undefined) {
					$scope.secPage = '0';
				}

				if ($scope.paramKey != undefined) {
					key = $scope.paramKey;
				}
				if ('createdDt' == key) {
					if ($scope.paramValue != undefined
							&& $scope.paramValue != '') {
						var dateString = '';
						var _date = $scope.paramValue;
						dateString = _date.getFullYear()
								+ '-'
								+ (_date.getMonth() + 1 < 10 ? '0'
										+ (_date.getMonth() + 1) : _date
										.getMonth() + 1)
								+ '-'
								+ (_date.getDate() < 10 ? '0'
										+ (_date.getDate()) : _date.getDate());
						value = dateString;
					}
					if ($scope.paramValue2 != undefined
							&& $scope.paramValue != '') {
						var dateString = '';
						var _date = $scope.paramValue2;
						dateString = _date.getFullYear()
								+ '-'
								+ (_date.getMonth() + 1 < 10 ? '0'
										+ (_date.getMonth() + 1) : _date
										.getMonth() + 1)
								+ '-'
								+ (_date.getDate() < 10 ? '0'
										+ (_date.getDate()) : _date.getDate());
						value2 = dateString;
					}
				} else {
					if ($scope.paramValue != undefined) {
						value = $scope.paramValue;
					}
				}

				$http.get(endPointApps + 'notifications/list', {
					params : {
						'paramKey' : key,
						'paramValue' : value,
						'paramValue2' : value2,
						'secPage' : $scope.secPage
					}
				}).success(function(response) {

					$scope.datas = response.listResponse;
					$scope.message = response.message;
					$scope.status = response.status;
					$scope.error = response.error;
					$scope.pageNumber = response.pageNumber;
					$scope.pageSize = response.pageSize;
					$scope.totalPage = response.totalPage;
					$scope.last = response.last;

					if ($scope.datas != undefined) {
						if ($scope.datas.length > 0) {
							$scope.emptyData = false;
						} else {
							$scope.emptyData = true;
						}

						if ($scope.datas.length > 0) {
							$scope.hideData = true;
						} else {
							$scope.hideData = false;
						}
					}
				});
			};

			$scope.search = function() {
				$scope.loadData();
			}

			$scope.pagging = function(secPage) {
				$scope.secPage = secPage;
				$scope.loadData();
			}

			$scope.$watch("currentPage", function() {
				$scope.loadData($scope.currentPage);
			});
		});

APP.controller('NotifControllerUser', function($scope, $http, $rootScope,
		$location, $window) {
	$scope.keyIndex = 'id';
	$scope.linkAction = {};
	$scope.dataSize = '';

	$scope.loadData = function(page, qlue) {
		$scope.page = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}

		$http.get(endPointApps + 'notifications/list-user', {
			params : {}
		}).success(function(response) {

			$scope.datas = response.listResponse;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;

			if ($scope.datas != undefined) {
				$scope.dataSize = $scope.datas.length;
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
		});
	};

	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

