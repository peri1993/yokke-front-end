APP.controller('ListEmpAttendanceController', function($scope, $http, $rootScope, $location, $window) {
	$scope.keyIndex = 'id';
	$scope.linkAction = {
			username : "?username=",
			fullname : "&fullname="
	};
	$scope.dataSize = '';
	$scope.paramKey = '';
	$scope.paramValue = '';
	
	$scope.inputParam = function(key){
		$scope.paramKey = key;
	}
	
	$scope.onChangeValue = function(key, value){
		$scope.paramValue = value;			
	}
	
	$scope.find = function(){
		$scope.loadData();
	}

	$scope.loadData = function(page, qlue) {
		$scope.page = '';
		var key = '';
		var value = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		
		if($scope.paramKey != undefined){
			key = $scope.paramKey;
		}
		
		if($scope.paramValue != undefined){
			value = $scope.paramValue;
		}

		$http.get(endPointApps + 'list-attendance/list', {
			params : {
				'paramKey':key
				,'paramValue':value
			}
		}).success(function(response) {

			$scope.datas	= response.listResponse;
			$scope.message 	= response.message;
			$scope.status 	= response.status;
			$scope.error 	= response.error;

			if ($scope.datas != undefined) {
				$scope.dataSize = $scope.datas.length;
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
		});
	};
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('CreateScheduleController', function($scope, $http, $rootScope, $location, $window) {
	$scope.keyIndex = 'id';
	$scope.linkAction = {
			username : "?username=",
			fullname : "&fullname="
	};
	$scope.dataSize = '';
	$scope.getUsername = function() {
		return $("#username").val();
	}
	
	$scope.getFullname = function() {
		return $("#fullname").val();
	}
	$scope.listModel = [];
	
	$scope.refresh = function(){
		$scope.loadData($window.location.reload());
	}
	
	$scope.input = function() {
		var address = '';
		var empId	= '';
		var startDate = '';
		var endDate	  = '';
		var timeStart = $("#timeStart").val();
		var timeEnd	  = $("#timeEnd").val();
		
		if($scope.address != undefined){
			address = $scope.address;
		}
		if($scope.getUsername() != undefined){
			empId = $scope.getUsername();
		}
		if($scope.startDate != undefined){
			// startDate = $scope.startDate;
			var year = '';
			var date = '';
			var day = '';
			
			var _date = $scope.startDate;
			var year =  _date.getFullYear();
			var date = (_date.getMonth() + 1 < 10 ? '0'
					+ (_date.getMonth() + 1)
					: _date.getMonth() + 1);
			var day = (_date.getDate() < 10 ? '0'
					+ (_date.getDate()) : _date
					.getDate());
			
			/*
			 * dateString = _date.getFullYear() +''+ (_date.getMonth() + 1 < 10 ?
			 * '0' + (_date.getMonth() + 1) : _date.getMonth() + 1) ++
			 * (_date.getDate() < 10 ? '0' + (_date.getDate()) : _date
			 * .getDate());
			 */
			startDate = year+''+date+''+day;
		}
		if($scope.endDate != undefined){
			// endDate = $scope.endDate;
			var year = '';
			var date = '';
			var day = '';
			
			var _date = $scope.endDate;
			var year =  _date.getFullYear();
			var date = (_date.getMonth() + 1 < 10 ? '0'
					+ (_date.getMonth() + 1)
					: _date.getMonth() + 1);
			var day = (_date.getDate() < 10 ? '0'
					+ (_date.getDate()) : _date
					.getDate());
			
			/*
			 * var dateString = ''; var _date = $scope.endDate; dateString =
			 * _date.getFullYear() + (_date.getMonth() + 1 < 10 ? '0' +
			 * (_date.getMonth() + 1) : _date.getMonth() + 1) + (_date.getDate() <
			 * 10 ? '0' + (_date.getDate()) : _date .getDate());
			 */
			endDate = year+''+date+''+day;
		}
		
		$http.get(endPointApps + 'list-attendance/create', {
			params : {
				'address' 	: address,
				'empId'		: empId,
				'startDate'	: startDate,
				'endDate'	: endDate,
				'timeStart'	: timeStart,
				'timeEnd'	: timeEnd
			}
		}).success(function(response) {
			
			$scope.datas	= response;
			$scope.message 	= response.message;
			$scope.status 	= response.status;
			$scope.error 	= response.error;

			if ($scope.datas != undefined) {
				$scope.dataSize = $scope.datas.length;
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
			
			$('#listEmp').modal('toggle');
			$scope.loadData();
		});
		$scope.$watch("currentPage", function() {
			$scope.loadData($scope.currentPage);
		});
	}
	
	$scope.deleteData = function(model) {
		
		$scope.listModel.push(model);
		$http.get(endPointApps + 'list-attendance/delete', {
			params : {
				'listModel':JSON.stringify($scope.listModel)
			}
		}).success(function(response) {
			$scope.message 	= response.message;
			$scope.status 	= response.status;
			$scope.error 	= response.error;

			if ($scope.datas != undefined) {
				$scope.dataSize = $scope.datas.length;
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
			$('#listEmp').modal('toggle');
			$scope.loadData();
		});
		$scope.$watch("currentPage", function() {
			$scope.loadData($scope.currentPage);
		});
	}
	
	$scope.loadData = function(page, qlue) {
		$scope.page = '';
		var username = '';
		var fullname = '';

		if ($scope.getUsername != undefined) {
			username = $scope.getUsername();
		}
		
		if ($scope.getFullname != undefined) {
			fullname = $scope.getFullname();
		}
		
		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}

		$http.get(endPointApps + 'list-attendance/list-schedule', {
			params : {
				'username':username,
				'fullname':fullname
			}
		}).success(function(response) {

			$scope.datas	= response.listResponse;
			$scope.message 	= response.message;
			$scope.status 	= response.status;
			$scope.error 	= response.error;

			if ($scope.datas != undefined) {
				$scope.dataSize = $scope.datas.length;
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
		});
	};
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('HistoryAttendanceController', function($scope, $http, $rootScope, $location, $window) {
	$scope.keyIndex = 'id';
	$scope.linkAction = {
			filename : "?filename=",
			directory : "&directory=",
			status : "&status=",
			type : "&type="
	};
	$scope.dataSize = '';
	$scope.emptyData = false;
	$scope.fileIn = false;
	$scope.fileOut = false;
	
	$scope.onChangeValue = function(date){
		$scope.dateFilter = date;
	}
	
	$scope.getUsername = function() {
		return $("#username").val();
	}
	$scope.getFullname = function(){
		return $("#fullname").val();
	}
	
	$scope.preview = function(directory, status, filename) {

		$http.get(endPointApps + 'history-attendance/preview-file',
				{
					params : {
						'directory' : directory,
						'status' 	: status,
						'filename' 	: filename
					}
				}).success(function(response) {
			$scope.path = response.path;
			$scope.previewStatus = response.status;
			if ($scope.previewStatus == '200') {
				$window.open($scope.path, '_blank');
			}
		});
	};

	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
	
	$scope.loadData = function(page, qlue) {
		$scope.page = '';
		var username = '';
		var fullname = '';
		var linkPathIn = '#';
		var linkPathOut = '#';
				
		if ($scope.getUsername != undefined) {
			username = $scope.getUsername();
		}
		if ($scope.getFullname != undefined) {
			fullname = $scope.getFullname();
		}
		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}

		$http.get(endPointApps + 'history-attendance/list', {
			params : {
				'username': username
			}
		}).success(function(response) {
			$scope.datas	= response.listResponse;
			$scope.listFileUpload = $scope.datas.listFile;
			$scope.message 	= response.message;
			$scope.status 	= response.status;
			$scope.error 	= response.error;
			
			if ($scope.datas != undefined) {
				$scope.dataSize = $scope.datas.length;
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
		});
	};
	
	$scope.maps = function(latitude, longitude){
		 $window.open('https://google.com/maps/?q='+latitude+','+longitude);
	}
	
	$scope.search = function(){
		var dateString = '';
		var username = '';

		if ($scope.getUsername != undefined) {
			username = $scope.getUsername();
		}
		
		if ($scope.dateFilter != undefined) {
			_date = $scope.dateFilter;

			var _date = $scope.dateFilter;
			dateString = _date.getFullYear()
					+ ''
					+ (_date.getMonth() + 1 < 10 ? '0' + (_date.getMonth() + 1)
							: _date.getMonth() + 1)
					+ ''
					+ (_date.getDate() < 10 ? '0' + (_date.getDate()) : _date
							.getDate());
		
		}

		if (dateString.length == 8) {
			$http.get(endPointApps + 'history-attendance/list', {
				params : {
					'date' : dateString,
					'username': username
				}
			}).success(function(response) {
				$scope.datas	= response.listResponse;
				$scope.message 	= response.message;
				$scope.status 	= response.status;
				$scope.error 	= response.error;

				if ($scope.datas != undefined) {
					$scope.dataSize = $scope.datas.length;
					if ($scope.datas.length > 0) {
						$scope.emptyData = false;
					} else {
						$scope.emptyData = true;
					}

					if ($scope.datas.length > 0) {
						$scope.hideData = true;
					} else {
						$scope.hideData = false;
					}
				}
			});
		}else{
			$scope.datas = [];
			$scope.emptyData = true;
			$scope.hideData = false;
		}
	}
	
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('HistoryEmployeeAttendanceController', function($scope, $http, $rootScope, $location, $window) {
	$scope.keyIndex = 'id';
	$scope.dataSize = '';
	$scope.emptyData = false;
	$scope.flag = false;
	$scope.location = '';
	$scope.radius;
	$scope.linkAction = {
			filename : "?filename=",
			directory : "&directory=",
			status : "&status=",
			type : "&type="
	};
	$scope.fileIn = false;
	$scope.fileOut = false;
	$scope.dateFilter = null;
	
	$scope.onChangeValue = function(date){
		$scope.dateFilter = date;
	}
	
	$scope.getUsername = function() {
		return $("#username").val();
	}
	$scope.getFullname = function(){
		return $("#fullname").val();
	}
	
	$scope.loadData = function(page, qlue) {
		$scope.page = '';
		$scope.qlue = '';
		var linkPathIn = '#';
		var linkPathOut = '#';
		
		if ($scope.getUsername != undefined) {
			username = $scope.getUsername();
		}
		if ($scope.getFullname != undefined) {
			fullname = $scope.getFullname();
		}
		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}

		$http.get(endPointApps + 'history-employee-attendance/list', {
			params : {}
		}).success(function(response) {
			
			$scope.datas	= response.listResponse;
			$scope.listFileUpload = $scope.datas.listFile;
			$scope.message 	= response.message;
			$scope.status 	= response.status;
			$scope.error 	= response.error;

			if ($scope.datas != undefined) {
				$scope.dataSize = $scope.datas.length;
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
		});
	};
	
	$scope.search = function(){

		var dateString = '';
		
		if ($scope.dateFilter != undefined) {
			_date = $scope.dateFilter;

			var _date = $scope.dateFilter;
			dateString = _date.getFullYear()
					+ ''
					+ (_date.getMonth() + 1 < 10 ? '0' + (_date.getMonth() + 1)
							: _date.getMonth() + 1)
					+ ''
					+ (_date.getDate() < 10 ? '0' + (_date.getDate()) : _date
							.getDate());
		
		}

		if (dateString.length == 8) {
			$http.get(endPointApps + 'history-employee-attendance/list', {
				params : {
					'date' : dateString
				}
			}).success(function(response) {
				$scope.datas	= response.listResponse;
				$scope.message 	= response.message;
				$scope.status 	= response.status;
				$scope.error 	= response.error;

				if ($scope.datas != undefined) {
					$scope.dataSize = $scope.datas.length;
					if ($scope.datas.length > 0) {
						$scope.emptyData = false;
					} else {
						$scope.emptyData = true;
					}

					if ($scope.datas.length > 0) {
						$scope.hideData = true;
					} else {
						$scope.hideData = false;
					}
				}
			});
		}else{
			$scope.datas = [];
			$scope.emptyData = true;
			$scope.hideData = false;
		}
	}
	
	
	$scope.maps = function(latitude, longitude){
		 $window.open('https://google.com/maps/?q='+latitude+','+longitude);
	}
	
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('CreateAttendanceController', function($scope, $http, $rootScope, $location, $window) {
	$scope.keyIndex = 'id';
	$scope.dataSize = '';
	$scope.emptyData = false;
	$scope.contentQr;
	$scope.flag = false;
	$scope.hideData = false;
	$scope.location = '';
	$scope.hideCamera = false;
	$scope.flagSchedule = false;
	$scope.camera;
	$scope.latitude;
	$scope.longitude;
	$scope.lat;
	$scope.long;
	$scope.filetype = '';
	$scope.base64img = '';
	$scope.hideContent = false;
	$scope.loadingAnim = false;
	
	$scope.loadLocation = function() {
		var empArea = $scope.contentQr.slice(0,4);
		var empSubArea = $scope.contentQr.slice(5,9);
		var landscape = $scope.contentQr.slice(10,13);
		
		$http.get(endPointApps + 'qr-attendance/location', {
			params : {
				'empArea' 	: empArea,
				'empSubArea' : empSubArea,
				'landscape'	: landscape
			}
		}).success(function(response) {
			
			$scope.datasLocation = response.objectResponse.empSubAreaDescription;
			$scope.lat = response.objectResponse.latitudeCode;
			$scope.long = response.objectResponse.longitudeCode;
			$scope.location = $scope.datasLocation;
			$scope.countRadius();
			$scope.input();
			
		});
	}
	
	$scope.countRadius = function() 
    {
		
	      var R = 6371000; // meters
	      var dLat = toRad($scope.latitude-$scope.lat);
	      var dLon = toRad($scope.longitude-$scope.long);
	      var lat1 = toRad($scope.latitude);
	      var lat2 = toRad($scope.lat);

	      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	        Math.cos(lat1) * Math.cos(lat2)* Math.sin(dLon/2) * Math.sin(dLon/2) ; 
	      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	      $scope.radius = R * c;
	      console.log($scope.radius);
	      return $scope.radius;
	    }

	    // Converts numeric degrees to radians
	    function toRad(Value) 
	    {
	        return Value * Math.PI / 180;
	    }
	
	$scope.input = function() {
		$scope.loadingAnim = true;
		
		$('#listEmp').modal('toggle');
		var empArea = $scope.contentQr.slice(0,4);
		var empSubArea = $scope.contentQr.slice(5,9);
		var landscape = $scope.contentQr.slice(10,13);
		var latitudeCode = $scope.latitude;
		var longitudeCode = $scope.longitude;
		var lat2 = $scope.lat;
		var long2 = $scope.long;
		var empId = '';
		var endDate = $scope.contentQr.slice(37,45);
		var startDate = $scope.contentQr.slice(37,45);
		var barcodeTime = $scope.contentQr.slice(37,51);
		var landscape = '100';
		var radius = $scope.radius;
		var empSubAreaDesc = $scope.location;
		
		$http.post(endPointApps + 'qr-attendance/create-attendance', {
				//empArea 		: empArea,
				//empSubArea	 	: empSubArea,
				empSubAreaDesc	: empSubAreaDesc,
				//landscape 		: landscape,
				longitudeCode	: longitudeCode,
				latitudeCode	: latitudeCode,
				//startDate		: startDate,
				//endDate			: endDate,
				//barcodeTime		: barcodeTime,
				radius			: radius,
				content			: $scope.contentQr
		}).success(function(response) {
			$scope.datas	= response;
			$scope.message 	= response.message;
			$scope.status 	= response.status;
			$scope.error 	= response.error;
			
			if($scope.datas != null || $scope.datas != undefined){
				$scope.loadingAnim = false;
			}
			$scope.loadData();

		});
	}
	
	$scope.clickCamera = function(){
		 $scope.hideCamera = true;
		 $scope.QR();
	}
	
	$scope.QR = function() {
		 let scanner = new Instascan.Scanner({ video: document.getElementById('preview'), mirror: false });
	     scanner.addListener('scan', function (content) {
	    	 $scope.contentQr = content;
	    	 $scope.hideCamera = false;
	       scanner.stop();
	       $scope.loadLocation();
	     });
	     
	     if ($scope.hideCamera == true) {
	    	 Instascan.Camera.getCameras().then(function (cameras) {
		    	 if($scope.contentQr == null){
		    		if (cameras.length > 0) {
		    			var selectedCam = cameras[0];
		    		    $.each(cameras, (i, c) => {
		    		        if (c.name.indexOf('back') != -1) {
		    		            selectedCam = c;
		    		            return false;
		    		        }
		    		    });
		        		scanner.start(selectedCam);
		        	}else {
		        		console.error('No cameras found.');
		        	}
		    	 }
		     }).catch(function (e) {
		       console.error(e);
		     });
		}
	}
	
	$scope.refresh = function(){
		$scope.loadData($window.location.reload());
	}
	  
	$scope.flagScheduleCheck = function(page, qlue){
		
		var resultBarcode = "";
		var resultLocation = "";
		
		function getLocation() {
		  if (navigator.geolocation) {
		    navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
		  } else { 
		    alert("Geolocation is not supported by this browser.");
		    console.log("Geolocation is not supported by this browser.");
		  }
		}

		function geoSuccess(position) {
		  $scope.latitude = position.coords.latitude;
		  $scope.longitude = position.coords.longitude;
		  resultLocation =  $scope.latitude + "|" +$scope.longitude;
		  // alert(resultLocation);
		  console.log("This function get Location: "+resultLocation);
		}

		function geoError() {
		  alert("Geocoder failed to getting Location");
		}
		window.onload = function() {
	        getLocation();
	    }
		
		$http.get(endPointApps + 'qr-attendance/flag-schedule', {
			params : {
			}
		}).success(function(response) {
			$scope.status	= response.status;
			
			if ($scope.status == 200 || $scope.status == '200') {
				$scope.flagSchedule	= response.flagAttendance;
			}
			$scope.loadData();
		});
	}
	
	var formdata = new FormData();
	$scope.getTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formdata.append(key, value);
        });
    };
	
    $scope.files = [];
    $scope.getFileDetails = function (e) {
        $scope.$apply(function () {
            for (var i = 0; i < e.files.length; i++) {
                $scope.files.push(e.files[i])
            }
            $scope.upload();
        });
    };
    
    $scope.upload = function(){
    	var base64;
		 var filesSelected = document.getElementById("file").files;
		    if (filesSelected.length > 0) {
		    	$scope.hideContent = true;
		    	var fileToLoad = filesSelected[0];
		    	var fileReader = new FileReader();
		    	fileReader.readAsDataURL(fileToLoad);
		    	
		    	fileReader.onload = function(fileLoadedEvent) {
		    		
		    		var srcData = fileLoadedEvent.target.result; // <---
																				// data:
		    		
		    		var fileType = fileToLoad.type.split("/")[1];
		    		
		    		var newImage = document.createElement('img');
		    		newImage.style.width = "300px";
		    		newImage.src = srcData;
		    		base64 = newImage.src;
		    		$scope.base64img = base64.split(",")[1];
		    		//alert($scope.base64img);
						       
		    		if($scope.base64img != undefined){
		    			document.getElementById("imgTest").innerHTML = newImage.outerHTML;
		    			$scope.filetype = fileType;
		    		}else{
		    			$scope.hideContent = false; 
		    		}
		    	}
		    }else{
		    	$scope.hideContent = false; 
		    }
    }
    
    $scope.uploadImg = function(){
    	$scope.loadingAnim = true;
		$('#listEmp').modal('toggle');
    	var longitudeCode = $scope.longitude;
    	var latitudeCode = $scope.latitude;
    	var filetype = $scope.filetype;
    	
    	
    	if (filetype == null) {
			filetype = "png";
		}
    	
    	var body = {
    			pathImages : $scope.base64img
    	}
    	
    	var csrf = $scope._csrf.token;
    	var config = {
				headers : {
					'Accept' : 'application/json, */*',
					'x-csrf-token' : csrf
				}
			};
    	
    	$http.post(endPointApps + 'qr-attendance/create-attendance', {
    		pathImages : $scope.base64img,
    		longitudeCode : longitudeCode,
    		latitudeCode : latitudeCode,
    		filetype : filetype
			/*params : {
				'longitudeCode'	: longitudeCode,
				'latitudeCode'	: latitudeCode,
				'fileType'		: filetype,
			}, body*/
		}, config).success(function(response) {
			$scope.datas	= response;
			$scope.message 	= response.message;
			$scope.status 	= response.status;
			$scope.error 	= response.error;

			if($scope.datas != null || $scope.datas != undefined){
				$scope.loadingAnim = false;
			}
			$scope.loadData();

		});
    }
    
	$scope.loadData = function(page, qlue) {
		$scope.page = '';
		$scope.qlue = '';
		
		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		if ($scope.contentQr != undefined) {
			if ($scope.contentQr.length > 0) {
				$scope.flag = true;
			} else {
				$scope.flag = false;
			}
		} else {
			$scope.flag = false;
		}
		
		$http.get(endPointApps + 'qr-attendance/list', {
			params : {}
		}).success(function(response) {
			$scope.datasAbsence	= response;
			if($scope.datasAbsence != null || $scope.datasAbsence != undefined){
				$scope.hideData = true;
			}else{
				$scope.hideData = false;
			}
		});
	};
	$scope.$watch("currentPage", function() {
		$scope.flagScheduleCheck($scope.currentPage);
	});
});

// added by Deva for QR Code Scanner
var contentQr = '';

function showQRIntro() {
	return confirm("Bank Mega want use your camera to take a picture of a QR code .");
}
