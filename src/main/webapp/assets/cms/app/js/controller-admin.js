APP.controller('RoleManagementController',function($scope, $http, $rootScope, $location, $window){
	$scope.keyIndex = 'id';
	$scope.linkAction = {
			roleId : "?roleId=",
	};
	$scope.dataSize = '';
	$scope.listMenu = [];
	$scope.menu = {"name":null};
	$scope.emptyData = 'false';
	$scope.deleteFlag = 'N'; 
	$scope.hideData = false;
	
	$scope.loadData = function(page,qlue){
		$scope.page = '';
		
		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		
		$http.get(endPointApps + 'role-management/list-role', {
			params : {}
		}).success(function(response) {
			
			$scope.datas = response.listResponse;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			
			if($scope.datas.length > 0){
				$scope.hideData = true;
			}else {
				$scope.hideData = false;
			}
			if($scope.datas.length > 0){
				$scope.emptyData = 'false';
			}else{
				$scope.emptyData = 'true';
			}
			$scope.dataSize = $scope.datas.length;
		});
	};
	
	$scope.deleteRole = function(roleId){
		
		$http.get(endPointApps + 'role-management/delete-role', {
			params : {
				'roleId': roleId
			}
		}).success(function(response) {
			
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			if(($scope.status == '400' || $scope.status == 400) && $scope.error != undefined){
				$scope.deleteFlag = 'Y';
			}else{
				$scope.deleteFlag = 'N';
				$scope.loadData();				
			}
		});
	};
	
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('UserManagementController',function($scope, $http, $rootScope, $location, $window){
	$scope.keyIndex = 'id';
	$scope.linkAction = {
			userId : "?userId=",
	};
	$scope.dataSize = '';
	$scope.emptyData = 'false';
	$scope.deleteFlag = 'N';
	$scope.dateFlag = 'N';
	$scope.paramKey = '';
	$scope.paramValue = '';
	$scope.paramValue2 = '';
	$scope.hideData = false;
	$scope.reverse = false;
	$scope.column = '';
	
	$scope.inputParam = function(key){
		if('createdDate' == key || 'updatedDate' == key){
			$scope.dateFlag = 'Y';
		}else{
			$scope.dateFlag = 'N';
			$scope.onChangeValue();
		}
		
		$scope.paramKey = key;
	}
	
	$scope.onChangeValue = function(key, value){
		if('createdDate' == key || 'updatedDate'){
			$scope.paramValue = value;
		}else{
			$scope.paramValue = value;			
		}
	}
	
	$scope.onChangeValue2 = function(key, value){
		if('createdDate' == key || 'updatedDate'){
			$scope.paramValue2 = value;
		}else{
			$scope.paramValue2 = value;			
		}
	}
	
	$scope.search = function(key, value){
		$scope.loadData();
	}
	
	$scope.sortColumn = function(col){
		
		$scope.column = col;
		if($scope.reverse){
			$scope.reverse = false;
			$scope.reverseclass = 'arrow-up';
		}else{
			$scope.reverse = true;
			$scope.reverseclass = 'arrow-down';
		}
	};
	
	$scope.sortClass = function(col){
		
		if($scope.column == col ){
			if($scope.reverse){
				return 'arrow-down'; 
			}else{
			   	return 'arrow-up';
		}
		}else{
			return '';
		}
	};
	
	$scope.loadData = function(page,qlue){
		$scope.page = '';
		var key = '';
		var value = '';
		var value2 = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		if($scope.paramKey != undefined){
			key = $scope.paramKey;
		}
		if('createdDate' == key || 'updatedDate' == key){
			if($scope.paramValue != undefined && $scope.paramValue != ''){
				var dateString = '';
				var _date = $scope.paramValue;
				dateString = _date.getFullYear()
						+ '-'
						+ (_date.getMonth() + 1 < 10 ? '0' + (_date.getMonth() + 1)
								: _date.getMonth() + 1)
						+ '-'
						+ (_date.getDate() < 10 ? '0' + (_date.getDate()) : _date
								.getDate());
				value = dateString;
			}
			if($scope.paramValue2 != undefined && $scope.paramValue != ''){
				var dateString = '';
				var _date = $scope.paramValue2;
				dateString = _date.getFullYear()
						+ '-'
						+ (_date.getMonth() + 1 < 10 ? '0' + (_date.getMonth() + 1)
								: _date.getMonth() + 1)
						+ '-'
						+ (_date.getDate() < 10 ? '0' + (_date.getDate()) : _date
								.getDate());
				
				value2 = dateString;
			}
		}else {
			if($scope.paramValue != undefined){
				value = $scope.paramValue;
			}
		}
		
		if ($scope.secPage == undefined) {
			$scope.secPage = '0';
		}
		
		$http.get(endPointApps + 'user-management/list-user', {
			params : {
				'paramKey':key
				,'paramValue':value
				,'paramValue2':value2
				,'secPage' : $scope.secPage
			}
		}).success(function(response) {
			
			$scope.datas = response.listResponse;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			$scope.pageNumber = response.pageNumber;
			$scope.pageSize = response.pageSize;
			$scope.totalPage = response.totalPage;
			$scope.last = response.last;
			
			if($scope.datas != null){
				if($scope.datas.length > 0){
					$scope.hideData = true;
					$scope.emptyData = 'false';
				}else {
					$scope.hideData = false;
					$scope.emptyData = 'true';
				}
			}else{
				$scope.hideData = false;
				$scope.emptyData = 'true';
			}
			
			$scope.dataSize = $scope.datas.length;
		});
	};
	
	$scope.deleteUser = function(userId){
		
		$http.get(endPointApps + 'user-management/delete-user', {
			params : {
				'userId': userId
			}
		}).success(function(response) {
			
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			if(($scope.status == '400' || $scope.status == 400) && $scope.error != null){
				$scope.deleteFlag = 'Y';
			}else{
				$scope.loadData();
			}
		});
	};
	
	$scope.pagging = function(secPage) {
		$scope.secPage = secPage;
		$scope.loadData();
	}

	
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('AssignedTaskController',function($scope, $http, $rootScope, $location, $window){
	$scope.keyIndex = 'id';
	
	$scope.linkAction = {
			intancesId:'?instanceId='
	};
	$scope.dataSize = '';
	$scope.hideData = false;
	$scope.emptyData = false;
	$scope.dateFlag = 'N';
	$scope.paramKey = '';
	$scope.paramValue = '';
	$scope.paramValue2 = '';
	$scope.errorFlag = 'N';
	$scope.listModel = [];
	$scope.reverse = false;
	$scope.column = '';
	
	$scope.inputParam = function(key){
		$scope.paramKey = key;
	}
	
	$scope.onChangeValue = function(key, value){
		$scope.paramValue = value;			
	}
	
	$scope.find = function(){
		$scope.loadData();
	}
	
	$scope.submit = function(){
		
		var listModel = [];
		var username = '';
		
		if($scope.listModel != undefined){
			if($scope.listModel.length > 0){
				listModel = $scope.listModel;
			}
		}

		if($scope.detail != undefined){
			if($scope.detail.username != undefined && $scope.detail.username != ''){
				username = $scope.detail.username;
			}
		}
		
		$http.get(endPointApps + 'assigned-task/update-list', {
			params : {
				'listModel' : JSON.stringify(listModel)
				,'username': username
			}
		}).success(function(response) {
			
			$scope.status = response.status;
			$scope.message = response.message;
			if($scope.status == '200' || $scope.status == 200){
				$scope.errorFlag = 'N';
				$scope.hideError = false;
				$scope.listModel = [];
				$scope.loadData();
			}else{
				$scope.errorFlag = 'Y';
				$scope.hideError = true;
			}
			$scope.detail = {};
			$scope.username = '';
		});
		$scope.loadData($window.location.reload());
	}
	
	$scope.sortColumn = function(col){
		
		$scope.column = col;
		if($scope.reverse){
			$scope.reverse = false;
		}else{
			$scope.reverse = true;
		}
		
		$scope.loadData();
		
	};
	
	$scope.loadData = function(page,qlue){
		
		$scope.page = '';
		var paramKey = '';
		var paramValue = '';
		var orderBy = '';
		var column = ''
			
		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		if ($scope.paramKey != undefined) {
			paramKey = $scope.paramKey;
		}
		if ($scope.paramValue != undefined) {
			paramValue = $scope.paramValue;
		}
		if ($scope.secPage == undefined){
			$scope.secPage = '0';
		}
		/*if ($scope.reverse != undefined){
			orderBy = $scope.reverse;
		}
		if($scope.column != undefined){
			column = $scope.column;
		}*/
		$http.get(endPointApps + 'assigned-task/list-assign', {
			params : {
						'paramKey' 		: paramKey,
						'paramValue'	: paramValue,
						'orderBy'		: orderBy,
						'secPage'		: $scope.secPage
						,'column'		: column
			}
		}).success(function(response) {
			
			$scope.datas 	= response.listResponse;
			$scope.message 	= response.message;
			$scope.status 	= response.status;
			$scope.error 	= response.error;
			$scope.pageNumber = response.pageNumber;
			$scope.pageSize = response.pageSize;
			$scope.totalPage = response.totalPage;
			$scope.last 	= response.last;
			
			if($scope.status == 200 || $scope.status == '200'){
				$scope.hideError = false;
			}else{
				$scope.hideError = true;
			}
			
			if($scope.datas != null){
				if($scope.datas.length > 0){
					$scope.hideData = true;
				}else {
					$scope.hideData = false;
				}
			}else{
				$scope.hideData = false;
			}
			
		});
	};
	
	 $scope.checkAllList = function() {
	    	var toggle = $scope.isAllSelected;
	        angular.forEach($scope.datas, function(item){ 
	         item.selected = toggle;
	         if(item.selected == true){
	        	 $scope.listModel.push(item);
	         }else{
	        	 var index = $scope.listModel.indexOf(item);
	        	 $scope.listModel.splice(index, 1);
	         }
	       });
	    }
	    
	    $scope.checkList = function(item){
	    	$scope.isAllSelected = $scope.datas.every(function(){
	    		if(item.selected == true){
		        	$scope.listModel.push(item);
		        }else{
		        	var index = $scope.listModel.indexOf(item);
		        	$scope.listModel.splice(index, 1);
		        }
	    	})
	   }
	
	$scope.search = function(){
		var username = '';
		
		if($scope.username != undefined){
			username = $scope.username;
		}
		
		$http.get(endPointApps + 'assigned-task/search', {
			params : {
				'username' : username
			}
		}).success(function(response) {
			$scope.detail = response;
			if(response.status == 200 || response.status == '200' ){
				$scope.dataFlag = false;
			}else{
				$scope.dataFlag = true;
			}
		});
	}
	
	$scope.pagging = function(secPage) {
		$scope.secPage = secPage;
		$scope.loadData();
	}
	
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('DetailAssignerController',function($scope, $http, $rootScope, $location, $window){
	$scope.keyIndex = 'id';
	$scope.linkAction = {};
	$scope.dataSize = '';
	$scope.key = '';
	$scope.value = '';
	$scope.flag = false;
	
	$scope.instancesGetValue = function() {
		return $("#instanceId").val();
	}
	
	$scope.loadData = function(page,qlue){
		$scope.page = '';
		var instancesId = '';
		
		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		if($scope.instancesGetValue() != undefined){
			instancesId = $scope.instancesGetValue();
		}
		
		$http.get(endPointApps + 'assigned-task/detail', {
			params : {
				'instancesId' : instancesId
			}
		}).success(function(response) {
			$scope.datas = response.objectResponse;
		});
	};
	
	$scope.search = function(key){
		$scope.key = key;
		var username = '';
		
		if($scope.username != undefined){
			username = $scope.username;
		}
		
		$http.get(endPointApps + 'assigned-task/search', {
			params : {
				'username' : username
			}
		}).success(function(response) {
			$scope.detail = response;
		});
	}
	
	$scope.add = function(){
		if($scope.detail != undefined){
			if('ds1_id' == $scope.key){
				$scope.datas.approvalDs1 = $scope.detail.username;
				$scope.datas.nameApprovalDs1 = $scope.detail.fullname;
			}else{
				$scope.datas.approvalDs2 = $scope.detail.username;
				$scope.datas.nameApprovalDs2 = $scope.detail.fullname;
			}
			
			$scope.detail = {};
			$scope.username = '';
		}
	}
	
	$scope.reset = function(){
		$scope.flagError = false;
		$scope.flag = false;
		$scope.loadData();
	}
	
	$scope.update = function(){
		
		var ds1 = '';
		var ds2 = '';
		var instancesId = '';
		var assigner = '';
		
		if($scope.datas.approvalDs1 != undefined){
			ds1 = $scope.datas.approvalDs1;
		}
		
		if($scope.datas.approvalDs2 != undefined){
			ds2 = $scope.datas.approvalDs2;
		}
		
		if($scope.instancesGetValue() != undefined){
			instancesId = $scope.instancesGetValue();
		}
		
		if($scope.datas.assigner != undefined){
			assigner = $scope.datas.assigner;
		}
		
		$http.get(endPointApps + 'assigned-task/update', {
			params : {
				'ds1_id' : ds1
				,'ds2_id':ds2
				,'instancesId':instancesId
				,'assigner': assigner
			}
		}).success(function(response) {
			$scope.data = response;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			if($scope.status != '200' && $scope.status != 200){
				$scope.flag = false;
				$scope.flagError = true;
			}else{
				$scope.flag = true;
				$scope.flagError = false;
				$scope.datas.assigner= $scope.data.assigner;
			}
		});
	}
	
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});


APP.controller('BranchConfController',function($scope, $http, $rootScope, $location, $window){
	$scope.keyIndex = 'id';
	$scope.linkAction = {
	};
	$scope.dataSize = '';
	$scope.emptyData = 'false';
	$scope.deleteFlag = 'N';
	$scope.dateFlag = 'N';
	$scope.paramKey = '';
	$scope.paramValue = '';
	$scope.paramValue2 = '';
	$scope.hideData = false;
	$scope.reverse = false;
	$scope.column = '';
	$scope.flagEdit = false;
	$scope.enabledEdit =[];
	$scope.listModel = [];
	
	
	$scope.inputParam = function(key){
		$scope.paramKey = key;
	}
	
	$scope.onChangeValue = function(key, value){
		$scope.paramValue = value;	
	}
	
	$scope.search = function(key, value){
		$scope.loadData();
	}
	
	$scope.add = function(index){
		if($scope.enabledEdit[index] == undefined || $scope.enabledEdit[index] == false){
			$scope.enabledEdit[index] = true;
		}else {
			$scope.enabledEdit[index] = false;
		}
	}
	
	$scope.update = function(model, index){
		$scope.enabledEdit[index] = false;
		var listRes = [];
		$scope.listModel.push(model);
		
		$http.get(endPointApps + 'branch-conf/update', {
			params : {
				'listModel':JSON.stringify($scope.listModel)
			}
		}).success(function(response) {
			
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;

			$scope.listModel = [];
			$scope.loadDate();
		});
	}
	
	$scope.loadData = function(page,qlue){
		$scope.page = '';
		var key = '';
		var value = '';
		var value2 = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		if($scope.paramKey != undefined){
			key = $scope.paramKey;
		}
		
		if($scope.paramValue != undefined){
			value = $scope.paramValue;
		}
		if ($scope.secPage == undefined) {
			$scope.secPage = '0';
		}
		
		$http.get(endPointApps + 'branch-conf/list', {
			params : {
				'paramKey':key
				,'paramValue':value
				,'paramValue2':value2
				,'secPage' : $scope.secPage
			}
		}).success(function(response) {
			
			$scope.datas = response.listResponse;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			$scope.pageNumber = response.pageNumber;
			$scope.pageSize = response.pageSize;
			$scope.totalPage = response.totalPage;
			$scope.last = response.last;
			
			if($scope.datas != null){
				if($scope.datas.length > 0){
					$scope.hideData = true;
					$scope.emptyData = 'false';
				}else {
					$scope.hideData = false;
					$scope.emptyData = 'true';
				}
			}else{
				$scope.hideData = false;
				$scope.emptyData = 'true';
			}
			$scope.listModel = [];
		});
	};
	
	$scope.pagging = function(secPage) {
		$scope.secPage = secPage;
		$scope.loadData();
	}

	
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('AssignerHistoryController', function($scope, $http, $rootScope,
		$location, $window) {
	$scope.keyIndex = 'id';
	$scope.linkAction = {};
	$scope.dataSize = '';
	$scope.errorFlag = 'N';
	$scope.dateFlag = 'N';
	$scope.hideData = true;
	
	$scope.inputParam = function(key) {
		if ('createdDt' == key) {
			$scope.dateFlag = 'Y';
		} else {
			$scope.dateFlag = 'N';
			$scope.paramValue2 = '';
		}

		$scope.paramKey = key;
	}

	$scope.onChangeValue = function(key, value) {
		if ('createdDt' == key) {
			$scope.paramValue = value;
		} else {
			$scope.paramValue = value;
		}
	}

	$scope.onChangeValue2 = function(key, value) {
		if ('createdDt' == key) {
			$scope.paramValue2 = value;
		} else {
			$scope.paramValue2 = value;
		}
	}
	
	$scope.search = function() {
		$scope.loadData();
	}
	
	$scope.sortColumn = function(col){
		
		$scope.column = col;
		if($scope.reverse){
			$scope.reverse = false;
			$scope.reverseclass = 'arrow-up';
		}else{
			$scope.reverse = true;
			$scope.reverseclass = 'arrow-down';
		}
	};
	
	$scope.loadData = function(page, qlue) {
		var key 	= '';
		var value 	= '';
		var value2	= '';
		$scope.page = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		if ($scope.secPage == undefined){
			$scope.secPage = '0';
		}
		if ($scope.paramKey != undefined) {
			key = $scope.paramKey;
		}
		if ('createdDt' == key) {
			if ($scope.paramValue != undefined
					&& $scope.paramValue != '') {
				var dateString = '';
				var _date = $scope.paramValue;
				dateString = _date.getFullYear()
						+ '-'
						+ (_date.getMonth() + 1 < 10 ? '0'
								+ (_date.getMonth() + 1) : _date
								.getMonth() + 1)
						+ '-'
						+ (_date.getDate() < 10 ? '0'
								+ (_date.getDate()) : _date.getDate());
				value = dateString;
			}
			if ($scope.paramValue2 != undefined
					&& $scope.paramValue != '') {
				var dateString = '';
				var _date = $scope.paramValue2;
				dateString = _date.getFullYear()
						+ '-'
						+ (_date.getMonth() + 1 < 10 ? '0'
								+ (_date.getMonth() + 1) : _date
								.getMonth() + 1)
						+ '-'
						+ (_date.getDate() < 10 ? '0'
								+ (_date.getDate()) : _date.getDate());
				value2 = dateString;
			}
		} else {
			if ($scope.paramValue != undefined) {
				value = $scope.paramValue;
			}
		}
		
		$http.get(endPointApps + 'history-assigner/list', {
			params : {
				'paramKey'		: key,
				'paramValue'	: value,
				'paramValue2'	: value2,
				'secPage'		: $scope.secPage,
			}
		}).success(function(response) {
			
			$scope.datas = response.listResponse;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			$scope.pageNumber = response.pageNumber;
			$scope.pageSize = response.pageSize;
			$scope.totalPage = response.totalPage;
			$scope.last	= response.last;
			$scope.totalRows = response.totalRows;

			if ($scope.datas != undefined) {
				$scope.dataSize = $scope.datas.length;
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
		});
	};
	
	$scope.pagging = function(secPage) {
		$scope.secPage = secPage;
		$scope.loadData();
	}

	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('AssignerAllHistoryController', function($scope, $http, $rootScope,
		$location, $window) {
	$scope.keyIndex = 'id';
	$scope.linkAction = {};
	$scope.dataSize = '';
	$scope.paramKey = '';
	$scope.paramValue = '';
	$scope.paramValue2= '';
	$scope.errorFlag = 'N';
	$scope.dateFlag = 'N';
	$scope.hideData = true;
	
	$scope.inputParam = function(key) {
		if ('createdDt' == key) {
			$scope.dateFlag = 'Y';
		} else {
			$scope.dateFlag = 'N';
			$scope.paramValue2 = '';
		}

		$scope.paramKey = key;
	}

	$scope.onChangeValue = function(key, value) {
		if ('createdDt' == key) {
			$scope.paramValue = value;
		} else {
			$scope.paramValue = value;
		}
	}

	$scope.onChangeValue2 = function(key, value) {
		if ('createdDt' == key) {
			$scope.paramValue2 = value;
		} else {
			$scope.paramValue2 = value;
		}
	}
	
	$scope.search = function() {
		$scope.loadData();
	}
	
	$scope.sortColumn = function(col){
		
		$scope.column = col;
		if($scope.reverse){
			$scope.reverse = false;
			$scope.reverseclass = 'arrow-up';
		}else{
			$scope.reverse = true;
			$scope.reverseclass = 'arrow-down';
		}
	};
	
	$scope.loadData = function(page, qlue) {
		var key 	= '';
		var value	= '';
		var value2	= '';
		$scope.page = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		if ($scope.secPage == undefined) {
			$scope.secPage = '0';
		}
		if ($scope.paramKey != undefined) {
			key = $scope.paramKey;
		}
		if ('createdDt' == key) {
			if ($scope.paramValue != undefined
					&& $scope.paramValue != '') {
				var dateString = '';
				var _date = $scope.paramValue;
				dateString = _date.getFullYear()
						+ '-'
						+ (_date.getMonth() + 1 < 10 ? '0'
								+ (_date.getMonth() + 1) : _date
								.getMonth() + 1)
						+ '-'
						+ (_date.getDate() < 10 ? '0'
								+ (_date.getDate()) : _date.getDate());
				value = dateString;
			}
			if ($scope.paramValue2 != undefined
					&& $scope.paramValue != '') {
				var dateString = '';
				var _date = $scope.paramValue2;
				dateString = _date.getFullYear()
						+ '-'
						+ (_date.getMonth() + 1 < 10 ? '0'
								+ (_date.getMonth() + 1) : _date
								.getMonth() + 1)
						+ '-'
						+ (_date.getDate() < 10 ? '0'
								+ (_date.getDate()) : _date.getDate());
				value2 = dateString;
			}
		} else {
			if ($scope.paramValue != undefined) {
				value = $scope.paramValue;
			}
		}

		$http.get(endPointApps + 'history-all/list', {
			params : {
				'paramKey'		: key,
				'paramValue'	: value,
				'paramValue2'	: value2,
				'secPage' 		: $scope.secPage
			}
		}).success(function(response) {
			$scope.datas = response.listResponse;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			$scope.pageNumber = response.pageNumber;
			$scope.pageSize = response.pageSize;
			$scope.totalPage = response.totalPage;
			$scope.last	= response.last;
			$scope.totalRows = response.totalRows;

			if ($scope.datas != undefined) {
				if ($scope.datas.length > 0) {
					$scope.emptyData = false;
				} else {
					$scope.emptyData = true;
				}

				if ($scope.datas.length > 0) {
					$scope.hideData = true;
				} else {
					$scope.hideData = false;
				}
			}
		});
	};
	
	$scope.pagging = function(secPage) {
		$scope.secPage = secPage;
		$scope.loadData();
	}

	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});
