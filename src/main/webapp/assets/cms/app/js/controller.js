/*APP.directive("datepicker", [
		'$http',
		function($http) {
			
			return {
				restrict : "A",
				require : "ngModel",
				link : function(scope, elem, attrs, ngModelCtrl) {
					var updateModel = function(dateText) {
						scope.$apply(function() {
							ngModelCtrl.$setViewValue(dateText);
						});
					};
					$http({
						method : 'GET',
						url : endPointApps + 'leave/getHolidayCurrentYear'
					}).then(function(response) {
						scope.holiday = response.data;
						elem.datepicker(options);
					}, function(response) {
						alert("Error: No data returned");
					});
					var options = {
						format : "dd/mm/yyyy",
						todayHighlight : true,
						daysOfWeekDisabled : [ 0, 6 ],
						startDate : new Date(),
						autoclose : true,
						beforeShowDay : function(date) {
							var disabledDates = [];
							var test = '';
							disabledDates = scope.holiday;
							var allDates = date.getDate() + '/'
									+ (date.getMonth() + 1) + '/'
									+ date.getFullYear();

							if (disabledDates.indexOf(allDates) != -1) {
								return false;
							} else {
								return true;
							}
						},
						onSelect : function(dateText) {
							updateModel(dateText);
						}
					};
				}
			}
		} ]);
*/
APP.controller('SidebarController', function($scope, $http, $rootScope,
		$location, $window) {
			$rootScope.pendingNotificationCount = 0;
			if (localStorage.getItem('pendingNotificationCount')) {
				$rootScope.pendingNotificationCount = localStorage.getItem('pendingNotificationCount');
	}
});

APP.controller('LeaveController', function($scope, $http, $rootScope, $location, $window, $filter) {
	$scope.keyIndex = 'id';
	$scope.linkAction = {};
	$scope.employee_id = null;
	$scope.messageblockleave = false;
	$scope.loadingAnim = false;
	$scope.nextQuota = {
		"leaveQuota" : '',
		"remainQuota" : '',
		"quotaTaken" : '',
		"totalRemainingQuota" : ''
	}
	$scope.currentQuota = {
		'leaveQuota' : '',
		'remainingQuota' : '',
		'currentQuotaTaken' : '',
		'currentTotalRemainingToken' : '',
	};
	$scope.createFlag = false;
	$scope.errorFlag = false;
	$scope.carryOverFlag = false;
	$scope.hideData	= false;
	$scope.currentQuotaDisplay = false;
	
	$scope.invalid_token = function(){
		
		$http.get(endPointApps + 'leave/invalid-token',{
			params : {}
		}).success(function(response){
			
		})
	}
	

	$scope.showCurrentQuotaLeave = function(absence) {
		
		if (absence != undefined) {
			$scope.loadDataRemainingQuota(absence);
		}
	}
	
	$scope.blockLeaveQuota = function(page, qlue){
		
		$scope.page = '';
		
		if(page != undefined){
			$scope.currentPage = page;
		}
		if(qlue != undefined){
			$scope.qlue = qlue;
		}
		$http.get(endPointApps + 'leave/checkBlockLeave',{
			params : {}
		}).success(function(response){
			$scope.datasBlockLeave = response;
			$scope.status = response.status;
			$scope.error = response.error;
			
			if ($scope.error == 'invalid_token') {
				$scope.invalid_token();
			}
			
			if($scope.datasBlockLeave == true){
				$scope.datasBlockLeave = 'Iya';
			}else{
				$scope.datasBlockLeave = 'Tidak';
			}
		})
	}
	
	$scope.loadLastRemainingQuota = function(page, qlue) {
		$scope.page = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		$http.get(endPointApps + 'leave/findLastRemaining', {
			params : {}
		}).success(function(response) {
			$scope.datasLastRemaining = response.remaining;
			$scope.datasLastRemainingPending = response.remainingPending;
			$scope.totalRequest = response.totalRequest;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			
			if ($scope.error == 'invalid_token') {
				$scope.invalid_token();
			}
			
			if ($scope.datasLastRemaining != undefined) {
				if($scope.datasLastRemaining.length > 0){
					$scope.hideData = true;
				}else{
					$scope.hideData = false;
				}
			} else {
				$scope.hideData = false;
			}
			
		});
	}
	
	$scope.loadLastRemainingCarryQuota = function(page, qlue) {
		
		$scope.page = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		$http.get(endPointApps + 'leave/findLastRemainingCarryOver', {
			params : {}
		}).success(function(response) {
			$scope.datasLastRemainCarry = response.remaining;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			
			if ($scope.error == 'invalid_token') {
				$scope.invalid_token();
			}
			
			if ($scope.datasLastRemainCarry != undefined) {
				if($scope.datasLastRemainCarry.length > 0){
					$scope.hideDataC = true;
				}else{
					$scope.hideDataC = false;
				}
			} else {
				$scope.hideDataC = false;
			}
		});
	}
	
	$scope.loadDataRemainingQuota = function(absence) {
		$scope.remaining = '0';
		$scope.carryOverFlag = false;
		$scope.currentQuotaDisplay = false;
		$http.get(endPointApps + 'leave/findRemainingQuota',
						{
							params : {}
						}).success(
						function(response) {
							
							$scope.listRemaining = response.listResponse;
							
							if($scope.listRemaining != undefined && $scope.listRemaining != null){
								if($scope.listRemaining.length > 0){
									$scope.remaining = $scope.listRemaining[0].remainingQuota;
								}
							}
							if(absence.id.absencetype == '1000'){
								$scope.currentQuota.leaveQuota = '0';
								if($scope.listRemaining.length > 1){
									if($scope.listRemaining[1].remainingQuota >= 1){
										$scope.carryOverFlag = true;
										$scope.remaining = $scope.listRemaining[0].remainingQuota;
										$scope.nextQuota.remainingQuota = $scope.listRemaining[1].remainingQuota;
									}
								}
							}else{
								$scope.currentQuota.leaveQuota = absence.leaveQuota;
							}
							
							if($scope.currentQuota.leaveQuota != '0' || $scope.currentQuota.leaveQuota != 0 ){
								$scope.hideData = false;
							}else{
								$scope.hideData = true;
							}
							
							$scope.currentQuota.remainingQuota = $scope.remaining;
							$scope.quotaTakenFunc($scope.startdate,$scope.enddate);	
							if($scope.currentQuota.leaveQuota == '0' || $scope.currentQuota.leaveQuota == 0){
								$scope.currentQuotaDisplay = true;
							}else{
								$scope.currentQuotaDisplay = false;
							}
						});
	};
	
	$scope.quotaTakenFunc = function(startdate, enddate) {
		var value_start_date = '';
		var value_end_date = '';
		if (startdate != undefined && enddate != undefined) {
			var dateStringStartDate = '';
			var _date_start_date = startdate;
			dateStringStartDate = (_date_start_date.getDate() < 10 ? '0' + (_date_start_date.getDate()) : _date_start_date
					.getDate())
			+ '/'
			+ (_date_start_date.getMonth() + 1 < 10 ? '0' + (_date_start_date.getMonth() + 1)
					: _date_start_date.getMonth() + 1)
			+ '/'
			+  _date_start_date.getFullYear();
			value_start_date = dateStringStartDate;
			
			var dateStringEndDate = '';
			var _date_end_date = enddate;
			dateStringEndDate = 
			(_date_end_date.getDate() < 10 ? '0' + (_date_end_date.getDate()) : _date_end_date
					.getDate())
			+ '/'
			+ (_date_end_date.getMonth() + 1 < 10 ? '0' + (_date_end_date.getMonth() + 1)
					: _date_end_date.getMonth() + 1)
			+ '/'
			+ _date_end_date.getFullYear();
			value_end_date = dateStringEndDate;
			
			$http.get(endPointApps + 'leave/takenQuota',
					{
						params : {
							'startDate' : value_start_date,
							'endDate'	: value_end_date
						}
				}).success(
						function(response) {
								
								$scope.quotaTaken = response;
								if ($scope.currentQuota.leaveQuota != ''
										&& $scope.currentQuota.remainingQuota != '') {
									if ($scope.quotaTaken <= $scope.currentQuota.leaveQuota) {
										$scope.currentQuota.currentQuotaTaken = $scope.quotaTaken;
										$scope.currentQuota.currentTotalRemainingToken = $scope.currentQuota.remainingQuota;
									} else {
										$scope.currentQuota.currentQuotaTaken = $scope.quotaTaken
												- parseInt($scope.currentQuota.leaveQuota);
										$scope.currentQuota.currentTotalRemainingToken = 
											parseInt($scope.currentQuota.remainingQuota)
												- ($scope.quotaTaken - parseInt($scope.currentQuota.leaveQuota));

									}
								}
								if($scope.currentQuota.leaveQuota != '' && $scope.nextQuota.remainingQuota != '' && $scope.carryOverFlag){
									if ($scope.quotaTaken <= $scope.currentQuota.leaveQuota) {
										$scope.nextQuota.currentQuotaTaken = 0;
										$scope.nextQuota.currentTotalRemainingToken = $scope.nextQuota.remainingQuota;
									} else {
										$scope.nextQuota.currentQuotaTaken = $scope.quotaTaken
												- parseInt($scope.currentQuota.leaveQuota);
										var totalCarryOver = parseInt($scope.nextQuota.remainingQuota) - $scope.quotaTaken;
										if(totalCarryOver < 0){
											$scope.nextQuota.currentTotalRemainingToken = 0;
											var carryOver = $scope.quotaTaken - parseInt($scope.nextQuota.remainingQuota);
											$scope.currentQuota.currentQuotaTaken = carryOver;
											$scope.currentQuota.currentTotalRemainingToken = parseInt($scope.currentQuota.remainingQuota)- carryOver;
										}else{
											$scope.nextQuota.currentTotalRemainingToken = totalCarryOver;
											$scope.currentQuota.currentQuotaTaken = 0;
											$scope.currentQuota.currentTotalRemainingToken = $scope.currentQuota.remainingQuota;
										}
									}
								}
							},
							function error(response) {
								$scope.postResultMessage = "Error with status: "
										+ response.statusText;
							});
		}
	}

	$scope.loadDataApproval = function(page, qlue) {
		
		$scope.messageblockleave = false;
		var totalQuotaAll = null;
		$scope.page = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}

		$http.get(endPointApps + 'leave/findApproval', {
			params : {}
		}).success(function(response) {
			$scope.datas = response;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			
			if ($scope.error == 'invalid_token') {
				$scope.invalid_token();
			}
			
			$scope.loadDataAbsenceType();
			$scope.loadLastRemainingQuota();
			$scope.loadLastRemainingCarryQuota();
			$scope.blockLeaveQuota();
		});
	};

	$scope.loadDataAbsenceType = function(page, qlue) {
		$scope.page = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		$http.get(endPointApps + 'leave/findAbsenceType', {
			params : {}
		}).success(function(response) {
			$scope.datasAbsence = response;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			
			if ($scope.error == 'invalid_token') {
				$scope.invalid_token();
			}
		});
	};

	$scope.validate = function() {
		if ($scope.subject == null
				|| $scope.subject == undefined) {
			$scope.validateMandatory = true;
			$scope.messageValidate = 'Please fill Subject';
			return false;
		}
		if ($scope.description == null
				|| $scope.description == undefined) {
			$scope.validateMandatory = true;
			$scope.messageValidate = 'Please fill Description';
			return false;
		}
		if ($scope.absence == null
				|| $scope.absence == undefined) {
			$scope.validateMandatory = true;
			$scope.messageValidate = 'Please fill absences Type';
			return false;
		}
		if ($scope.startdate == null
				|| $scope.startdate == undefined) {
			$scope.validateMandatory = true;
			$scope.messageValidate = 'Please fill Start Date';
			return false;
		}
		if ($scope.enddate == null
				|| $scope.enddate == undefined) {
			$scope.validateMandatory = true;
			$scope.messageValidate = 'Please fill End Date';
			return false;
		}
		if ($scope.reason == null || $scope.reason == undefined) {
			$scope.validateMandatory = true;
			$scope.messageValidate = 'Please fill Reason';
			return false;
		}
		if($scope.startdate > $scope.enddate){
			$scope.validateMandatory = true;
			$scope.messageValidate = 'Silahkan cek kembali tanggal cuti anda';
			return false;
		}
		return true;
	}

	$scope.submit = function() {
		
		$scope.status = null;
		$scope.message = null;
		$scope.errorFlag = false;
		
		var cek = $scope.validate();
		if (cek == 'true' || cek == true) {
			$scope.validateMandatory = false;
			$scope.input();
		} else {
			return;
		}
	}
	

	$scope.reset = function() {
		$scope.messageblockleave = false;
		$scope.subject = '';
		$scope.description = '';
		$scope.absence = '';
		$scope.startdate = '';
		$scope.enddate = '';
		$scope.reason = '';
		$scope.comments = '';
		$scope.quotaTaken = '';
		$scope.currentQuota = {
				'leaveQuota' : '',
				'remainingQuota' : '',
				'currentQuotaTaken' : '',
				'currentTotalRemainingToken' : '',
			};
		
		$scope.nextQuota = {
				'leaveQuota' : '',
				'remainingQuota' : '',
				'currentQuotaTaken' : '',
				'currentTotalRemainingToken' : '',
			};
	}
	
	$scope.input = function() {
		$scope.loadingAnim = true;
		var employeeId = '';
		var employeeName = '';
		var approvalName1 = '';
		var approvalId1 = '';
		var approvalName2 = '';
		var approvalId2 = '';
		var leaveReason = '';
		var subject = '';
		var startdate = '';
		var enddate = '';
		var comments = '';
		var absenceType = '';
		var description = '';
		var quotaTaken = '';
		var currentQuota = '';
		var remainingQuota = '';
		var carryOverFlag = 'N';
		var lastRemaining = '';
		
		if ($scope.datas.username != undefined) {
			employeeId = $scope.datas.username;
		}
		if ($scope.datas.fullname != undefined) {
			employeeName = $scope.datas.fullname;
		}
		if ($scope.datas.nameApproval1 != undefined) {
			approvalName1 = $scope.datas.nameApproval1;
		}
		if ($scope.datas.approval1 != undefined) {
			approvalId1 = $scope.datas.approval1;
		}
		if ($scope.datas.nameApproval2 != undefined) {
			approvalName2 = $scope.datas.nameApproval2;
		}
		if ($scope.datas.approval2 != undefined) {
			approvalId2 = $scope.datas.approval2;
		}
		if ($scope.reason != undefined) {
			leaveReason = $scope.reason;
		}
		if ($scope.subject != undefined) {
			subject = $scope.subject;
		}
		if ($scope.startdate != undefined) {
			var dateStringStartDate = '';
			var _date_start_date = $scope.startdate;
			dateStringStartDate = (_date_start_date.getDate() < 10 ? '0' + (_date_start_date.getDate()) : _date_start_date
					.getDate())
			+ '/'
			+ (_date_start_date.getMonth() + 1 < 10 ? '0' + (_date_start_date.getMonth() + 1)
					: _date_start_date.getMonth() + 1)
			+ '/'
			+  _date_start_date.getFullYear();
			startdate = dateStringStartDate;
		}
		if ($scope.enddate != undefined) {
			var dateStringEndDate = '';
			var _date_end_date = $scope.enddate;
			dateStringEndDate = 
			(_date_end_date.getDate() < 10 ? '0' + (_date_end_date.getDate()) : _date_end_date
					.getDate())
			+ '/'
			+ (_date_end_date.getMonth() + 1 < 10 ? '0' + (_date_end_date.getMonth() + 1)
					: _date_end_date.getMonth() + 1)
			+ '/'
			+ _date_end_date.getFullYear();
			enddate = dateStringEndDate;
		}
		if ($scope.comments != undefined) {
			comments = $scope.comments;
		}
		if ($scope.absence != undefined) {
			absenceType = $scope.absence;
		}
		if ($scope.description != undefined) {
			description = $scope.description;
		}
		if('1000' == absenceType.id.absencetype){
			if($scope.nextQuota.remainingQuota != undefined && $scope.nextQuota.remainingQuota != null){
				currentQuota = $scope.nextQuota.currentQuotaTaken;
				remainingQuota = $scope.nextQuota.remainingQuota;
				lastRemaining = $scope.currentQuota.remainingQuota;
				carryOverFlag = 'Y';
			}else{
				currentQuota = $scope.currentQuota.currentQuotaTaken;
				remainingQuota = $scope.currentQuota.remainingQuota;
			}
		}else{
			currentQuota = $scope.currentQuota.currentQuotaTaken;
			remainingQuota = $scope.currentQuota.remainingQuota;
		}

		$http.get(endPointApps + 'leave/createLeave',
						{
							params : {
								'absenceType' : absenceType.id.absencetype,
								'absenceDescription' : absenceType.description,
								'employeeId' : employeeId,
								'employeeName' : employeeName,
								'approvalName1' : approvalName1,
								'approvalId1' : approvalId1,
								'approvalName2' : approvalName2,
								'approvalId2' : approvalId2,
								'leaveReason' : leaveReason,
								'subject' : subject,
								'startdate' : startdate,
								'enddate' : enddate,
								'comments' : comments,
								'description' : description,
								'quotaTaken' : currentQuota,
								'remainingQuota' : remainingQuota,
							    'totalTakenQuota' : $scope.quotaTaken 
							    ,'carryOverFlag':carryOverFlag
							    ,'lastRemaining' : lastRemaining
							}
						})
				.success(
						function(response) {
							$scope.response = response;
							$scope.message = response.message;
							$scope.status = response.status;
							$scope.taskId = response.taskId;
							$scope.instanceId = response.processId;
							$scope.error = response.error;
							
							if($scope.response != null || $scope.response != undefined){
								$scope.loadingAnim = false;
							}
							
							if (($scope.status == 200 || $scope.status == '200')) {
								$scope.createFlag = true;
								$scope.errorFlag = false;
								$scope.reset();
							}else{
								$scope.errorFlag = true;
								$scope.createFlag = false;
							}
							
							
						});
	};

	$scope.$watch("currentPage", function() {
		$scope.loadDataApproval($scope.currentPage);
	});

});

APP.controller('LeaveHistoryController', function($scope, $http, $rootScope, $location, $window){
	
	$scope.keyIndex = 'id';
	$scope.linkAction = {};
	$scope.hideData = false;
	$scope.emptyData = false;
	$scope.dateFlag = 'N';
	$scope.paramKey = '';
	$scope.paramValue = '';
	$scope.paramValue2 = '';
	$scope.errorFlag = 'N';
	$scope.column = '';
	$scope.reverse = false;
	
	$scope.inputParam = function(key){
		if('startDt' == key || 'endDt' == key){
			$scope.dateFlag = 'Y';
		}else{
			$scope.dateFlag = 'N';
			$scope.paramValue2 = '';
		}
		
		$scope.paramKey = key;
	}
	
	$scope.onChangeValue = function(key, value){
		if('startDt' == key || 'endDt' == key){
			$scope.paramValue = value;
		}else{
			$scope.paramValue = value;			
		}
	}
	
	$scope.onChangeValue2 = function(key, value){
		if('startDt' == key || 'endDt' == key){
			$scope.paramValue2 = value;
		}else{
			$scope.paramValue2 = value;			
		}
	}
	
	$scope.search = function(){
		$scope.loadData();
	}
	
	$scope.sortColumn = function(key){
		$scope.column = key;
		if($scope.reverse){
			$scope.reverse = false;
		}else{
			$scope.reverse = true;
		}
		
		$scope.loadData();
	};
	
	$scope.sortClass = function(col){
		if($scope.column == col ){
			if($scope.reverse){
				return 'arrow-down'; 
			}else{
			   	return 'arrow-up';
		}
		}else{
			return '';
		}
	}; 
	
	$scope.loadData = function(page, qlue) {
		
		$scope.page = '';
		var key = '';
		var value = '';
		var value2 = '';
		var column = '';
		var orderBy = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		
		if ($scope.paramKey != undefined) {
			paramKey = $scope.paramKey;
		}
		if ($scope.paramValue != undefined) {
			paramValue = $scope.paramValue;
		}
		if ($scope.secPage == undefined){
			$scope.secPage = '0';
		}
		
		if($scope.paramKey != undefined){
			key = $scope.paramKey;
		}
		
		/*if($scope.reverse != undefined){
			orderBy = $scope.reverse;
		}
		
		if($scope.column != undefined){
			column = $scope.column;
		}*/
		
		if('startDt' == key || 'endDt' == key){
			if($scope.paramValue != undefined && $scope.paramValue != ''){
				var dateString = '';
				var _date = $scope.paramValue;
				dateString = _date.getFullYear()
						+ (_date.getMonth() + 1 < 10 ? '0' + (_date.getMonth() + 1)
								: _date.getMonth() + 1)
						+ (_date.getDate() < 10 ? '0' + (_date.getDate()) : _date
								.getDate());
				value = dateString;
			}
			if($scope.paramValue2 != undefined && $scope.paramValue != ''){
				var dateString = '';
				var _date = $scope.paramValue2;
				dateString = _date.getFullYear()
						+ (_date.getMonth() + 1 < 10 ? '0' + (_date.getMonth() + 1)
								: _date.getMonth() + 1)
						+ (_date.getDate() < 10 ? '0' + (_date.getDate()) : _date
								.getDate());
				
				value2 = dateString;
			}
			
			if(value > value2){
				$scope.errorFlag = 'Y';
				$scope.hideError = true;
				$scope.status = 400;
				$scope.message = 'Start Date harus lebih kecil dari end Date !!!';
				return;
			}
		}else {
			if($scope.paramValue != undefined){
				value = $scope.paramValue;
			}
		}
		
		$http.get(endPointApps + 'history/list', {
			params : {
				'paramKey'		: key
				,'paramValue'	: value
				,'paramValue2'	: value2
				,'column'		: column
				,'orderBy'		: orderBy
				,'secPage'		: $scope.secPage
			}
		}).success(function(response) {
			
			$scope.datas = response.listResponse;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			$scope.pageNumber = response.pageNumber;
			$scope.pageSize = response.pageSize;
			$scope.totalPage = response.totalPage;
			$scope.totalRows = response.totalRows;
			$scope.last = response.last;

			if($scope.status == 200 || $scope.status == '200'){
				$scope.errorFlag = 'N';
				$scope.hideError = false;
			}else{
				$scope.errorFlag = 'Y';
				$scope.hideError = true;
			}
			
			if($scope.datas != null){
				if($scope.datas.length > 0){
					$scope.hideData = true;
					$scope.emptyData = 'false';
					$scope.countDatas = $scope.datas.length;
				}else {
					$scope.hideData = false;
					$scope.emptyData = 'true';
					$scope.countDatas = '0';
				}
			}else{
				$scope.hideData = false;
				$scope.emptyData = 'true';
			}
			
		});
	};
	
	$scope.pagging = function(secPage) {
		$scope.secPage = secPage;
		$scope.loadData();
	}
    
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('DetailHistoryTaskController', function($scope, $http, $rootScope, $location, $window) {
	
	$scope.keyIndex = 'id';
	$scope.linkAction = {
			downloadFile : "?filename=",
			downloadType : "&type=",
			downloadCreatedBy : "&createdBy=",
			downloadDirectory : "&directory="
	};
	
	$scope.buttonDownload = false;
	$scope.uploadFile = false;
	$scope.module = '';

	$scope.instancesGetValue = function() {
		return $("#instanceId").val();
	}
	
	$scope.checkFileUploadFlag = function() {
		return $("#fileUploadFlag").val();
	}

	$scope.checkModule = function() {
		return $("#module").val();
	}
	
	$scope.nextQuota = {
		"leaveQuota" : '',
		"remainQuota" : '',
		"quotaTaken" : '',
		"totalRemainingQuota" : ''
	}
	
	$scope.datasRemaining = {
			
	}

	$scope.approveFlag = false;
	$scope.validateSubmit = false;

	
	$scope.loadDataAbsenceTypeById = function(id) {
		
		$scope.page = '';

		$http.get(endPointApps + 'leave/findAbsenceType-all/', {
			params : {}
		}).success(function(response) {
			
			$scope.datasAbsence = response;
			angular.forEach(response, function(value,key){
				if(value.id.absencetype == id){
					if('' == value.leaveQuota){
						$scope.datasRemaining.originalQuota = '0';
					}else{
						$scope.datasRemaining.originalQuota = value.leaveQuota;
					}
				return ;
				}
			});
		});
	};
	
	$scope.loadDataRemainingQuota = function(startdate, enddate) {
		$http.get(endPointApps + 'leave/findRemainingQuotaDtl/' + $scope.datas.employee_id, {
							params : {}
				}).success(
						function(response) {

							$scope.currentQuotaTaken = parseInt(response.originalQuota)
									- parseInt(response.remainingQuota);
							if (isNaN($scope.currentQuotaTaken))
								$scope.currentQuotaTaken = '';
							$scope.currentTotalRemainingToken = response.originalQuota;
							$scope.datasRemaining = response;
							$scope.message = response.message;
							$scope.status = response.status;
							$scope.error = response.error;
							$scope.quotaTakenFunc(startdate,
									enddate);
						});
	};

	$scope.quotaTakenFunc = function(startdate, enddate) {
		var value_start_date = '';
		var value_end_date = '';
		
		if (startdate != undefined && enddate != undefined) {
			var dateStringStartDate = '';
			var _date_start_date = startdate;
			dateStringStartDate = _date_start_date.getFullYear()
			+ '/'
			+ (_date_start_date.getMonth() + 1 < 10 ? '0' + (_date_start_date.getMonth() + 1)
					: _date_start_date.getMonth() + 1)
			+ '/'
			+ (_date_start_date.getDate() < 10 ? '0' + (_date_start_date.getDate()) : _date_start_date
					.getDate());
			value_start_date = dateStringStartDate;
			
			var dateStringEndDate = '';
			var _date_end_date = enddate;
			dateStringEndDate = _date_end_date.getFullYear()
			+ '/'
			+ (_date_end_date.getMonth() + 1 < 10 ? '0' + (_date_end_date.getMonth() + 1)
					: _date_end_date.getMonth() + 1)
			+ '/'
			+ (_date_end_date.getDate() < 10 ? '0' + (_date_end_date.getDate()) : _date_end_date
					.getDate());
			value_end_date = dateStringEndDate;
			
			var csrf = $scope._csrf.token;
			var body = {
				startDate : value_start_date,
				endDate : value_end_date
			}
			var config = {
				headers : {
					'Accept' : 'application/json, */*',
					'x-csrf-token' : csrf
				}
			};
			$http.get(endPointApps + 'leave/takenQuota',
					{
						params : {
							'startDate' : value_start_date,
							'endDate'	: value_end_date
						}
				}).success(
						function(response) {
								$scope.quotaTaken = response.data;
								$scope.nextQuota.leaveQuota = $scope.datasRemaining.originalQuota;
								$scope.nextQuota.quotaTaken = response.data;
								$scope.nextQuota.remainQuota = parseInt($scope.nextQuota.leaveQuota)
										- response.data;
								$scope.nextQuota.totalRemainingQuota = $scope.nextQuota.remainQuota;

							},
							function error(response) {
								$scope.postResultMessage = "Error with status: "
										+ response.statusText;
							});
		}
	}

	$scope.loadData = function(page, qlue) {
		
		$scope.instancesId = '';
		$scope.page = '';
		
		if ($scope.instancesGetValue() != undefined) {
			var instancesId = $scope.instancesGetValue();
		}

		$http.get(endPointApps + 'inbox/task-detail', {
			params : {
				'instanceId' : instancesId
			}
		}).success(
				function(response) {
					 
					$scope.datas = response;
					$scope.listFileUpload = response.listFileUpload;
					$scope.message = response.message;
					$scope.status = response.status;
					$scope.error = response.error;
					$scope.loadDataAbsenceTypeById(response.absences_type);
					
					if(response.carryOverFlag != undefined && response.carryOverFlag != 'N'){
						$scope.nextremainingQuota = response.remaining_quota;
						$scope.currentQuotaTaken = response.quota_taken;
						$scope.nextQuotaTaken = response.quota_taken;
						if(parseInt(response.quota_taken) > parseInt(response.remaining_quota)){
							$scope.nextTotalRemainingToken = '0';
							$scope.quotaTaken = response.total_taken_quota;
							var carryOver = parseInt(response.quota_taken)-parseInt(response.remaining_quota);
							$scope.currentQuotaTaken = carryOver;
							$scope.datasRemaining.remainingQuota = $scope.datas.lastRemaining;
							$scope.currentTotalRemainingToken = $scope.datasRemaining.remainingQuota - carryOver;
							//$scope.loadDataRemainingQuotaUser($scope.datas.employee_id, carryOver);
						}else{
							$scope.nextTotalRemainingToken = parseInt(response.remaining_quota)
                            -
                            parseInt(response.quota_taken);
							$scope.quotaTaken = response.total_taken_quota;
							var carryOver = '0';
							$scope.currentQuotaTaken = carryOver;
							//$scope.loadDataRemainingQuotaUser($scope.datas.employee_id, carryOver);
							$scope.datasRemaining.remainingQuota = $scope.datas.lastRemaining;
							$scope.currentTotalRemainingToken = $scope.datasRemaining.remainingQuota - carryOver;
						}
						
					}else{
						$scope.datasRemaining.remainingQuota = response.remaining_quota;
						$scope.currentQuotaTaken = response.quota_taken;
						
						if ('1000' == response.absences_type || '3100' == response.absences_type) {
							$scope.currentTotalRemainingToken = parseInt(response.remaining_quota)
							- parseInt(response.quota_taken);
						} else {
							$scope.currentTotalRemainingToken = response.remaining_quota;
						}
						
						$scope.quotaTaken = response.total_taken_quota;
					}
					
					$scope.loadLogs();
					var isFile = $scope.checkFileUploadFlag();
					if (isFile == 'true') {
						$scope.uploadFile = true;
					}else{
						$scope.uploadFile = false;
					}
					
					$scope.module = $scope.checkModule();
					
					if($scope.listFileUpload != undefined){
						if($scope.listFileUpload.length > 0){
							$scope.buttonDownload = true;
						}
					}
					
					if(response.carryOverFlag != undefined && response.carryOverFlag != 'N'){
						$scope.carryFlag = true;
					}else{
						$scope.carryFlag = false;
					}
					
				});
	};

	$scope.loadDataRemainingQuotaUser = function(requester, carryOver) {
		$scope.remaining = '0';
		$http.get(endPointApps + 'leave/findRemainingQuotaByRequester',
						{
							params : {
								'requester':requester
							}
						}).success(
						function(response) {
							
							$scope.listRemaining = response.listResponse;
							if($scope.listRemaining != undefined){
								if($scope.listRemaining.length > 1){
									$scope.datasRemaining.remainingQuota = $scope.listRemaining[0].remainingQuota;
									$scope.currentTotalRemainingToken = parseInt($scope.datasRemaining.remainingQuota)-carryOver;
								}
							}
						});
	};
	
	$scope.cekActiveTask = function(){
		
		var instancesId = '';

		if ($scope.instancesGetValue() != undefined) {
			instancesId = $scope.instancesGetValue();
		}

		$http.get(endPointApps + 'inbox/cek-active-data', {
			params : {
				'instanceId' : instancesId
			}
		}).success(function(response) {
			$scope.ActiveMessage = response.message;
			$scope.ActiveStatus = response.status;
			$scope.ActiveError = response.error;
			
			if(($scope.ActiveStatus == '200' || $scope.ActiveStatus == 200) 
					&& $scope.ActiveMessage == 'completed'){
				$scope.updateData();
			}
			else{	
				$window.location.href = endPointApps + "/inbox";
			}
		});
	}
	
	$scope.loadLogs = function(){
		$scope.instancesId = '';
		$scope.page = '';
		
		if ($scope.instancesGetValue() != undefined) {
			var instancesId = $scope.instancesGetValue();
		}
		$http.get(endPointApps + 'inbox/history-task-logs', {
			params : {
				'instancesId' : instancesId
			}
		}).success(function(response) {
               $scope.dataLogs = response.listResponse;  
			});
	};
	
	$scope.preview = function(directory, createdBy, filename){
		
		$http.get(endPointApps + 'permit-request/preview-file', {
			params : {
				'directory' : directory
				,'createdBy': createdBy
				,'filename' : filename
			}
		}).success(function(response) {
			$scope.path = response.path;
			$scope.previewStatus = response.status;
			if($scope.previewStatus == '200'){
				$window.open($scope.path, '_blank');
			}
		});
	};
	
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('PermitHistoryController', function($scope, $http, $rootScope, $location, $window, $filter) {
	
	$scope.PAGE_SIZE = PAGE_SIZE;
	
	$scope.keyIndex = 'leaveStartDate';
	$scope.linkAction = {};
	$scope.hideData = false;
	$scope.emptyData = false;
	$scope.dateFlag = 'N';
	$scope.paramKey = '';
	$scope.paramValue = '';
	$scope.paramValue2 = '';
	$scope.errorFlag = 'N';
	$scope.reverse = false;
	$scope.column = '';

	$scope.inputParam = function(key){
		if('startDt' == key || 'endDt' == key){
			$scope.dateFlag = 'Y';
		}else{
			$scope.dateFlag = 'N';
			$scope.paramValue2 = '';
		}
		
		$scope.paramKey = key;
	}
	
	$scope.onChangeValue = function(key, value){
		if('startDt' == key || 'endDt' == key){
			$scope.paramValue = value;
		}else{
			$scope.paramValue = value;			
		}
	}
	
	$scope.onChangeValue2 = function(key, value){
		if('startDt' == key || 'endDt' == key){
			$scope.paramValue2 = value;
		}else{
			$scope.paramValue2 = value;			
		}
	}
	
	$scope.search = function(){
		$scope.loadData();
	}
	
	$scope.sortColumn = function(col){
		$scope.column = col;
		if($scope.reverse){
			$scope.reverse = false;
		}else{
			$scope.reverse = true;
		}
		
		$scope.loadData();
	};
	
	$scope.sortClass = function(col){
		
		if($scope.column == col ){
			if($scope.reverse){
				return 'arrow-down'; 
			}else{
			   	return 'arrow-up';
		}
		}else{
			return '';
		}
	};
	
	$scope.loadData = function(page, qlue) {
		
		$scope.page = '';
		var key = '';
		var value = '';
		var value2 = '';
		var column = '';
		var orderBy = '';

		if (page != undefined) {
			$scope.currentPage = page;
		}
		if (qlue != undefined) {
			$scope.qlue = qlue;
		}
		
		if ($scope.paramKey != undefined) {
			paramKey = $scope.paramKey;
		}
		if ($scope.paramValue != undefined) {
			paramValue = $scope.paramValue;
		}
		if ($scope.secPage == undefined){
			$scope.secPage = '0';
		}
		
		if($scope.paramKey != undefined){
			key = $scope.paramKey;
		}
		if('startDt' == key || 'endDt' == key){
			if($scope.paramValue != undefined && $scope.paramValue != ''){
				var dateString = '';
				var _date = $scope.paramValue;
				dateString = _date.getFullYear()
						+ (_date.getMonth() + 1 < 10 ? '0' + (_date.getMonth() + 1)
								: _date.getMonth() + 1)
						+ (_date.getDate() < 10 ? '0' + (_date.getDate()) : _date
								.getDate());
				value = dateString;
			}
			if($scope.paramValue2 != undefined && $scope.paramValue != ''){
				var dateString = '';
				var _date = $scope.paramValue2;
				dateString = _date.getFullYear()
						+ (_date.getMonth() + 1 < 10 ? '0' + (_date.getMonth() + 1)
								: _date.getMonth() + 1)
						+ (_date.getDate() < 10 ? '0' + (_date.getDate()) : _date
								.getDate());
				
				value2 = dateString;
			}
			if(value > value2){
				$scope.errorFlag = 'Y';
				$scope.hideError = true;
				$scope.status = 400;
				$scope.message = 'Start Date harus lebih kecil dari end Date !!!';
				return;
			}
		}else {
			if($scope.paramValue != undefined){
				value = $scope.paramValue;
			}
		}
		
		/*if($scope.reverse != undefined){
			orderBy = $scope.reverse;
		}
		
		if($scope.column != undefined){
			column = $scope.column;
		}*/
		
		$http.get(endPointApps + 'permit-history/list', {
			params : {
				'paramKey':key
				,'paramValue':value
				,'paramValue2':value2
				,'secPage'		: $scope.secPage
				,'orderBy'		: orderBy
				,'column'		: column
			}
		}).success(function(response) {
			
			$scope.datas = response.listResponse;
			$scope.message = response.message;
			$scope.status = response.status;
			$scope.error = response.error;
			$scope.pageNumber = response.pageNumber;
			$scope.pageSize = response.pageSize;
			$scope.totalPage = response.totalPage;
			$scope.totalRows = response.totalRows;
			$scope.last = response.last;
			
			if($scope.status == 200 || $scope.status == '200'){
				$scope.errorFlag = 'N';
				$scope.hideError = false;
			}else{
				$scope.errorFlag = 'Y';
				$scope.hideError = true;
			}
			
			if($scope.datas != null){
				if($scope.datas.length > 0){
					$scope.hideData = true;
					$scope.emptyData = 'false';
				}else {
					$scope.hideData = false;
					$scope.emptyData = 'true';
				}
			}else{
				$scope.hideData = false;
				$scope.emptyData = 'true';
			}
			
			if ($scope.datas != undefined && $scope.datas != null) {
				$scope.countDatas = $scope.datas.length;
			} else {
				$scope.countDatas = '0';
			}
		});
	};
	
	$scope.pagging = function(secPage) {
		$scope.secPage = secPage;
		$scope.loadData();
	}
    
	$scope.$watch("currentPage", function() {
		$scope.loadData($scope.currentPage);
	});
});

APP.controller('PermitRequestController', function($scope, $http, $rootScope, $location, $window, $filter) {
	$scope.keyIndex = 'id';
	$scope.linkAction = {};
	$scope.messageValidate = '';
	$scope.loadingAnim = false;
	$scope.hideData = false;
	$scope.currentQuotaDisplay = false;
	$scope.status = undefined;
	$scope.message = undefined;
	$scope.errorFlag = false;
	
	$scope.nextQuota = {
			"leaveQuota" : '',
			"remainQuota" : '',
			"quotaTaken" : '',
			"totalRemainingQuota" : ''
		}
		$scope.currentQuota = {
			'leaveQuota' : '',
			'remainingQuota' : '',
			'currentQuotaTaken' : '',
			'currentTotalRemainingToken' : '',
		};
		$scope.createFlag = false;
		$scope.errorFlag = false;
		$scope.quotadeductionFlag = false;
		$scope.filename = '';
		$scope.filesize = '';
		$scope.filetype = '';
		$scope.uploadRequestList = [];
		$scope.uploadRequest = {"fileName":"", "fileSize":"", "fileType":"", "fileNo":"", "fileDescription":""};
		
		$scope.showCurrentQuotaLeave = function(absence) {
				if (absence != undefined) {
					$scope.loadDataRemainingQuota(absence);
				}
				if(absence.quotadeduction == 'Y'){
					$scope.quotadeductionFlag = true;
				}else{
					$scope.quotadeductionFlag = false;
				}
				
		}

		$scope.loadDataRemainingQuota = function(absence) {
			$scope.remaining = '0';
			$http.get(endPointApps + 'leave/findRemainingQuota',
				{
					params : {}
				}).success(function(response) {
					$scope.listRemaining = response.listResponse;
					if($scope.listRemaining != undefined && $scope.listRemaining != null){
						if($scope.listRemaining.length > 0){
							$scope.remaining = $scope.listRemaining[0].remainingQuota;
						}
					}
					if(absence.id.absencetype == '1100' ||absence.id.absencetype == '1200' ||absence.id.absencetype == '3100' ||absence.id.absencetype == '' || absence.id.absencetype == null || absence.id.absencetype == undefined){
						$scope.currentQuota.leaveQuota = '0';
					}else{
						$scope.currentQuota.leaveQuota = absence.leaveQuota;
					}
					
					$scope.currentQuota.remainingQuota = $scope.remaining;
					$scope.quotaTakenFunc($scope.startdate,$scope.enddate);	
					
					if($scope.currentQuota.leaveQuota == '0' || $scope.currentQuota.leaveQuota == 0){
						$scope.currentQuotaDisplay = true;
					}else{
						$scope.currentQuotaDisplay = false;
					}
				});
			};
		
		$scope.quotaTakenFunc = function(startdate, enddate) {
			var value_start_date = '';
			var value_end_date = '';

			if (startdate != undefined && enddate != undefined) {
				var dateStringStartDate = '';
				var _date_start_date = startdate;
				dateStringStartDate = (_date_start_date.getDate() < 10 ? '0' + (_date_start_date.getDate()) : _date_start_date
						.getDate())
				+ '/'
				+ (_date_start_date.getMonth() + 1 < 10 ? '0' + (_date_start_date.getMonth() + 1)
						: _date_start_date.getMonth() + 1)
				+ '/'
				+  _date_start_date.getFullYear();
				value_start_date = dateStringStartDate;
				
				var dateStringEndDate = '';
				var _date_end_date = enddate;
				dateStringEndDate = 
				(_date_end_date.getDate() < 10 ? '0' + (_date_end_date.getDate()) : _date_end_date
						.getDate())
				+ '/'
				+ (_date_end_date.getMonth() + 1 < 10 ? '0' + (_date_end_date.getMonth() + 1)
						: _date_end_date.getMonth() + 1)
				+ '/'
				+ _date_end_date.getFullYear();
				value_end_date = dateStringEndDate;
				
				$http.get(endPointApps + 'permit-request/takenQuota',
						{
							params : {
								'startDate' : value_start_date,
								'endDate'	: value_end_date
							}
					}).success(
							function(response) {
									$scope.quotaTaken = response;
									$scope.currentQuota.currentQuotaTaken = $scope.quotaTaken;
									if($scope.quotadeductionFlag){
										$scope.currentQuota.currentTotalRemainingToken = parseInt($scope.currentQuota.remainingQuota)
										- $scope.quotaTaken;
										
									}else{
										$scope.currentQuota.currentTotalRemainingToken = parseInt($scope.currentQuota.remainingQuota);
									}
								},
								function error(response) {
									$scope.postResultMessage = "Error with status: "+ response.statusText;
								});
			}
		}
		
		$scope.loadDataApproval = function(page, qlue) {
			$scope.page = '';

			if (page != undefined) {
				$scope.currentPage = page;
			}
			if (qlue != undefined) {
				$scope.qlue = qlue;
			}

			$http.get(endPointApps + 'permit-request/findApproval', {
				params : {}
			}).success(function(response) {
				$scope.datas = response;
				$scope.loadDataAbsenceType();
			});
		};

		$scope.loadDataAbsenceType = function(page, qlue) {
			$scope.page = '';

			if (page != undefined) {
				$scope.currentPage = page;
			}
			if (qlue != undefined) {
				$scope.qlue = qlue;
			}
			$http.get(endPointApps + 'permit-request/findAbsenceType', {
				params : {}
			}).success(function(response) {
				$scope.datasAbsence = response;
			});
		};

		$scope.validate = function() {
			
			$scope.upload();
			if ($scope.subject == null
					|| $scope.subject == undefined) {
				$scope.validateMandatory = true;
				$scope.messageValidate = 'Please fill Subject';
				return false;
			}
			if ($scope.description == null
					|| $scope.description == undefined) {
				$scope.validateMandatory = true;
				$scope.messageValidate = 'Please fill Description';
				return false;
			}
			if ($scope.absence == null
					|| $scope.absence == undefined) {
				$scope.validateMandatory = true;
				$scope.messageValidate = 'Please fill absences Type';
				return false;
			}
			if ($scope.startdate == null
					|| $scope.startdate == undefined) {
				$scope.validateMandatory = true;
				$scope.messageValidate = 'Please fill Start Date';
				return false;
			}
			if ($scope.enddate == null
					|| $scope.enddate == undefined) {
				$scope.validateMandatory = true;
				$scope.messageValidate = 'Please fill End Date';
				return false;
			}
			if ($scope.reason == null || $scope.reason == undefined) {
				$scope.validateMandatory = true;
				$scope.messageValidate = 'Please fill Reason';
				return false;
			}
			/*if($scope.currentQuota.currentTotalRemainingToken < 0){
				$scope.validateMandatory = true;
				$scope.messageValidate = 'Quota cuti tidak mencukupi';
				return false;
			}*/
			if($scope.startdate > $scope.enddate){
				$scope.validateMandatory = true;
				$scope.messageValidate = 'Silahkan cek kembali tanggal cuti anda';
				return false;
			}
			if($scope.absence.id.absencetype == '3120'){
				if($scope.uploadRequestList == undefined || $scope.uploadRequestList == null || $scope.uploadRequestList.length == 0){
					$scope.validateMandatory = true;
					$scope.messageValidate = 'Silahkan upload file keterangan anda';
					return false;
				}
			}

			return true;
		}

		$scope.submit = function() {
			
			$scope.status = null;
			$scope.message = null;
			$scope.errorFlag = false;
			
			var cek = $scope.validate();
			if (cek == 'true' || cek == true) {
				$scope.validateMandatory = false;
				$scope.input();
			} else {
				return;
			}
		}

		$scope.reset = function() {
			$scope.messageblockleave = false;
			$scope.subject = '';
			$scope.description = '';
			$scope.absence = '';
			$scope.startdate = '';
			$scope.enddate = '';
			$scope.reason = '';
			$scope.comments = '';
			$scope.quotaTaken = '';
			$scope.file = '';
			$scope.filename = '';
			$scope.currentQuota = {
					'leaveQuota' : '',
					'remainingQuota' : '',
					'currentQuotaTaken' : '',
					'currentTotalRemainingToken' : '',
				};
			
			$scope.number = 0;
			$scope.getNumber($scope.number);
			$scope.a = Array($scope.number);
		}
		
		$scope.input = function() {
			$scope.loadingAnim = true;
			var employeeId = '';
			var employeeName = '';
			var approvalName1 = '';
			var approvalId1 = '';
			var approvalName2 = '';
			var approvalId2 = '';
			var leaveReason = '';
			var subject = '';
			var startdate = '';
			var enddate = '';
			var comments = '';
			var absenceType = '';
			var description = '';
			var quotaTaken = '';
			var listFile = [];
			
			if ($scope.datas.username != undefined) {
				employeeId = $scope.datas.username;
			}
			if ($scope.datas.fullname != undefined) {
				employeeName = $scope.datas.fullname;
			}
			if ($scope.datas.nameApproval1 != undefined) {
				approvalName1 = $scope.datas.nameApproval1;
			}
			if ($scope.datas.approval1 != undefined) {
				approvalId1 = $scope.datas.approval1;
			}
			if ($scope.datas.nameApproval2 != undefined) {
				approvalName2 = $scope.datas.nameApproval2;
			}
			if ($scope.datas.approval2 != undefined) {
				approvalId2 = $scope.datas.approval2;
			}
			if ($scope.reason != undefined) {
				leaveReason = $scope.reason;
			}
			if ($scope.subject != undefined) {
				subject = $scope.subject;
			}
			if ($scope.startdate != undefined) {
				var dateStringStartDate = '';
				var _date_start_date = $scope.startdate;
				dateStringStartDate = (_date_start_date.getDate() < 10 ? '0' + (_date_start_date.getDate()) : _date_start_date
						.getDate())
				+ '/'
				+ (_date_start_date.getMonth() + 1 < 10 ? '0' + (_date_start_date.getMonth() + 1)
						: _date_start_date.getMonth() + 1)
				+ '/'
				+  _date_start_date.getFullYear();
				startdate = dateStringStartDate;
			}
			if ($scope.enddate != undefined) {
				var dateStringEndDate = '';
				var _date_end_date = $scope.enddate;
				dateStringEndDate = 
				(_date_end_date.getDate() < 10 ? '0' + (_date_end_date.getDate()) : _date_end_date
						.getDate())
				+ '/'
				+ (_date_end_date.getMonth() + 1 < 10 ? '0' + (_date_end_date.getMonth() + 1)
						: _date_end_date.getMonth() + 1)
				+ '/'
				+ _date_end_date.getFullYear();
				enddate = dateStringEndDate;
			}
			if ($scope.comments != undefined) {
				comments = $scope.comments;
			}
			if ($scope.absence != undefined) {
				absenceType = $scope.absence;
			}
			if ($scope.description != undefined) {
				description = $scope.description;
			}
			if($scope.uploadRequestList != undefined && $scope.uploadRequestList.length > 0){
				listFile = $scope.uploadRequestList;
			}
			$http.get(endPointApps + 'permit-request/createPermit',
							{
								params : {
									'absenceType' : absenceType.id.absencetype,
									'absenceDescription' : absenceType.description,
									'employeeId' : employeeId,
									'employeeName' : employeeName,
									'approvalName1' : approvalName1,
									'approvalId1' : approvalId1,
									'approvalName2' : approvalName2,
									'approvalId2' : approvalId2,
									'leaveReason' : leaveReason,
									'subject' : subject,
									'startdate' : startdate,
									'enddate' : enddate,
									'comments' : comments,
									'description' : description,
									'quotaTaken' : $scope.currentQuota.currentQuotaTaken,
									'remainingQuota' : $scope.currentQuota.remainingQuota,
								    'totalTakenQuota' : $scope.quotaTaken
								    ,'listUpload' :JSON.stringify(listFile)
								}
							})
					.success(
							function(response) {
								$scope.response = response;
								$scope.message = response.message;
								$scope.status = response.status;
								$scope.taskId = response.taskId;
								$scope.instanceId = response.processId;
								$scope.error = response.error;
								
								if($scope.response != undefined || $scope.response != null){
									$scope.loadingAnim = false;
								}
								
								if (($scope.status == 200 || $scope.status == '200')) {
									$scope.createFlag = true;
									$scope.errorFlag = false;
									$scope.reset();
									$scope.loadDataApproval();
									$scope.number = 1;
									$scope.a = 1;
									$scope.getNumber(1);
									$scope.getFileDetailsRemove();
								}else{
									$scope.createFlag = false;
									$scope.errorFlag = true;
								}
							});
		};
		
		var formdata = new FormData();
		$scope.getTheFiles = function ($files) {
            angular.forEach($files, function (value, key) {
                formdata.append(key, value);
            });
        };
        
        $scope.files = [];
        $scope.getFileDetails = function (e) {
            $scope.$apply(function () {
                for (var i = 0; i < e.files.length; i++) {
                    $scope.files.push(e.files[i])
                }

            });
        };
        
        $scope.getFileDetailsRemove = function () {
        	var length = $scope.files.length;
            for (var i = 0; i < length; i++) {
                var index = $scope.files.indexOf(i);
                $scope.files.splice(index, 1)
            }
            document.getElementById('file0').value = '';
            document.getElementById('ket0').value = '';
        };
		
		$scope.upload = function(){
			
			$scope.uploadRequestList = [];
			$scope.uploadRequest = {"fileName":"", "fileSize":"", "fileType":"", "fileNo":"", "fileDescription":""};
			
			var csrf = $scope._csrf.token;
			var uploadList = [];
			var fileNo = 0;
			for (var i in $scope.files) {
				fileNo = i + 1;
				var file = 'file'+i;
				var ket = 'ket'+i;
					var fileupload = document.getElementById(file).files[0], 
					r = new FileReader();
					var ket = document.getElementById(ket).value;
					uploadList.push(fileupload);
					filename = fileupload.name;
					$scope.filetype = fileupload.type;
					$scope.filesize = fileupload.size;
					
					var fileName = fileupload.name;
					fileName = 'file'+fileNo+'-'+ket+'-'+fileName;
					filename = fileName;
					$scope.uploadRequest = {"fileName":fileName, "fileSize":fileupload.size, "fileType":fileupload.type, "fileNo":"", "fileDescription":ket};
					$scope.uploadRequestList.push($scope.uploadRequest);
			}
			if(fileupload == undefined){
				$scope.messageValidate = 'Silahkan pilih file untuk di Upload';
			}else{
				$scope.messageValidate = 'Data berhasil di upload';
			}
			for(var i=0; i < uploadList.length; i++){
				var file = uploadList[i],
				r = new FileReader();
				var fileName = $scope.uploadRequestList[i].fileName;
				var fileSize = $scope.uploadRequestList[i].fileSize;
				var fileType = $scope.uploadRequestList[i].fileType;
				var fileDesc = $scope.uploadRequestList[i].fileDescription;
				
				$http({
		            method: 'POST',
		            url: endPointApps + '/permit-request/upload-file',
		            headers: {
		            	'Accept' : 'application/json, */*',
						'x-csrf-token' : csrf,
		                'Content-Type': 'multipart/form-data'
		            },
		            params :{
		            	filename:fileName,
		            	fileSize:fileSize,
		            	fileType:fileType,
		            	fileDesc:fileDesc
		            },
		            data: {
		            	upload: file
		            },
		            transformRequest: function (data, headersGetter) {
		                var formData = new FormData();
		                angular.forEach(data, function (value, key) {
		                    formData.append(key, value);
		                });

		                var headers = headersGetter();
		                delete headers['Content-Type'];

		                return formData;
		            }
		        })
		        .success(function (response) {
		        	$scope.filename = response.filename;
		        	$scope.messageupload = response.messageupload;
		        })
		        .error(function (data, status) {
		        });
			}
		}
		$scope.validate
		
		$scope.getBase64 = function(){
			/*var csrf = $scope._csrf.token;
			var fileupload = document.getElementById('files').files[0],
	        r = new FileReader();
			var filename = '';
			if(fileupload != undefined){
				filename = fileupload.name;
				$scope.filetype = fileupload.type;
				$scope.filesize = fileupload.size;
			}*/
			$scope.uploadRequestList = [];
			$scope.uploadRequest = {"fileName":"", "fileSize":"", "fileType":"", "fileNo":"", "fileDescription":""};
			
			var csrf = $scope._csrf.token;
			var uploadList = [];
			var fileNo = 0;
			for (var i in $scope.files) {
				fileNo = i + 1;
				var file = 'file0'+i;
				var ket = 'ket0'+i;
					var fileupload = document.getElementById(file).files[0], 
					r = new FileReader();
					var ket = document.getElementById(ket).value;
					uploadList.push(fileupload);
					filename = fileupload.name;
					$scope.filetype = fileupload.type;
					$scope.filesize = fileupload.size;
					
					var fileName = fileupload.name;
					fileName = 'file'+fileNo+'-'+ket+'-'+fileName;
					filename = fileName;
					$scope.uploadRequest = {"fileName":fileName, "fileSize":fileupload.size, "fileType":fileupload.type, "fileNo":"", "fileDescription":ket};
					$scope.uploadRequestList.push($scope.uploadRequest);
			}
			
			r.readAsDataURL(fileupload);
			   r.onload = function () {
				   
				   var csrf = $scope._csrf.token;
					var body = {
						fileByte : r.result
					}
					var config = {
						headers : {
							'Accept' : 'application/json, */*',
							'x-csrf-token' : csrf
						}
					};
					$http.post(endPointApps + 'permit-request/upload-file2',body, config)
							.then(function(response) {
										
							},function error(response) {
								$scope.postResultMessage = "Error with status: "+ response.statusText;
							});
			   };
			   r.onerror = function (error) {
			     console.log('Error: ', error);
			   };
			   
		}
		
		$scope.number = 1;
		$scope.getNumber = function(num){
			return new Array(num);
		};
		
		$scope.add = function(){
			if($scope.number != 5){
				$scope.number = $scope.number +1;
				$scope.getNumber($scope.number);
				$scope.a = Array($scope.number);
			}
			
		}
		$scope.minus = function(){
			if($scope.number > 1){
				$scope.number = $scope.number -1;
				$scope.getNumber($scope.number);
				$scope.a = Array($scope.number);
			}else{
				$scope.number = $scope.number;
				$scope.getNumber($scope.number);
				$scope.a = Array($scope.number);
			}
		}		
		
	$scope.$watch("currentPage", function() {
		$scope.loadDataApproval($scope.currentPage);
	});
});

/**
 * Helper
 */

function getTo(pageSize, currentPage, total) {
	var to = (pageSize * currentPage);
	to = (to > total) ? total : to;
	return to;
}

function stringToDate(_date, _format, _delimiter) {
	if (_format == undefined) {
		_format = "dd-mm-yyyy";
	}
	if (_delimiter == undefined) {
		_delimiter = "-";
	}

	var formatLowerCase = _format.toLowerCase();
	var formatItems = formatLowerCase.split(_delimiter);
	var dateItems = _date.split(_delimiter);
	var monthIndex = formatItems.indexOf("mm");
	var dayIndex = formatItems.indexOf("dd");
	var yearIndex = formatItems.indexOf("yyyy");
	var month = parseInt(dateItems[monthIndex]);
	month -= 1;

	var formatedDate = new Date(dateItems[yearIndex], month,
			dateItems[dayIndex]);
	return formatedDate;
}

function deleteConfirm(callback) {
	var isConfirmed = confirm("Are you sure to delete this record ?");
	if (isConfirmed) {
		if (typeof callback == 'function') {
			callback();
		}
	} else {
		return false;
	}
}
function invalidConfirm(callback) {
	var isConfirmed = confirm("Invalid Token, please re-login");
	// if (isConfirmed) {
	// if (typeof callback == 'function') {
	callback();
	// }
	// } else {
	// return false;
	// }
}
function getParameterByName(name, url) {
	if (!url)
		url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex
			.exec(url);
	if (!results)
		return null;
	if (!results[2])
		return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function goBack() {
	return window.history.back();
}

function setStatusDesc(approvalStatus) {
	switch (approvalStatus) {
	case 1:
		return 'Approved';
	case 2:
		return 'Rejected';
	default:
		return 'Pending Approval';
	}
}
function getFilename(inputFile) {
	if (inputFile != undefined && inputFile != '') {
		var myArr = inputFile.split("\\");
		return myArr[myArr.length - 1];
	}
	return null;
}

function alpha(e) {
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
}